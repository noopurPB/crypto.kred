//
//  SignupResponse.swift
//  Grab
//
//  Created by Noopur Virmani on 27/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class SignupResponse: Mappable{
    
    var status: String?
   
    var token: String?
    var error: String?
    var message: String?
    
    var user: GrabUser?
    
    var id: String?
    var address: String?
    var password: String?
    
    //var my_grabs = [Grab]()
  //  var linked: SocialUserAccount?
   
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        
        self.status <- map["status"]
        
        self.token <- map["token"]
        
        self.error <- map["error"]
        
        self.user <- map["login"]
        
        if(map["user"].isKeyPresent == true){
            self.user <- map["user"]
        }
        
        self.message <- map["message"]
        
        self.id <- map["id"]
        
        self.password <- map["password"]
        
        self.address <- map["address"]
        
        //self.my_grabs <- map["my_grabs"]
        
        //self.linked <- map["linked"]
        
        

    }
    
}
