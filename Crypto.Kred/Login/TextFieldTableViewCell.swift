//
//  TextFieldTableViewCell.swift
//  Grab
//
//  Created by Noopur Virmani on 22/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol TextFieldTableViewCellDelegate{
    @objc optional func textFieldValueChanged(_ sender: AnyObject)
}
class TextFieldTableViewCell: BaseTableViewCell{

    var delegate: TextFieldTableViewCellDelegate?
    
    @IBOutlet var smallIconBtn: UIButton!
    @IBOutlet var textField: UITextField!
    
    @IBOutlet var secTextField: UITextField!
    @IBOutlet var thirdTextField: UITextField!
    
    @IBOutlet var topDividerView: UIView!
    
    @IBOutlet var centreView: [UIView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.preservesSuperviewLayoutMargins = false
        self.contentView.preservesSuperviewLayoutMargins = false
        if(textField != nil){
            //textField.tintColor = UIColor.blackColor()
            textField.font = Theme.regularFont(16.0)
            textField.textColor = CKColor.textFieldColor
            textField.tintColor = Color.darkGreyColor

        }
        if(secTextField != nil){
            //secTextField.tintColor = UIColor.blackColor()
            secTextField.font = Theme.regularFont(16.0)
            secTextField.textColor = Color.darkGreyColor
            secTextField.tintColor = Color.darkGreyColor
        }
        if(centreView != nil){
            for view in centreView{
                view.backgroundColor = CKColor.dividerColor
            }
        }
        if(topDividerView != nil){
            topDividerView.backgroundColor = CKColor.dividerColor
        }
    }

    @IBAction func textFieldValueChanged(_ sender: AnyObject) {
        delegate?.textFieldValueChanged!(sender)
    }
    
}
