//
//  WebViewController.swift
//  Grab
//
//  Created by Noopur Virmani on 09/05/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation

class WebViewController: CKBaseVC {
    
    @IBOutlet var webview: UIWebView!
    var url : String?
    var navTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView(url ?? "")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //NavigationBarStyle.showNavigationBackBarItem(self, navTitle: navTitle ?? "")
        navBarStyle?.showNavigationBackBarItem(self, navTitle: navTitle ?? "")
        navBarStyle?.delegate = self
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.navigationController?.navigationBar.alpha = 1.0;
        
    }
    

    
    func loadWebView(_ urlString: String?){
        
         let escapedAddress = urlString!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url: URL = URL(string: escapedAddress!)!
        //clearCookies(forURL: url)
        let mutableUrlRequest  = NSMutableURLRequest(url:  url, cachePolicy: NSURLRequest.CachePolicy.returnCacheDataElseLoad,  timeoutInterval: 10.0)
        //mutableUrlRequest.HTTPShouldHandleCookies = false
        webview.loadRequest(mutableUrlRequest as URLRequest)
    }
    
    
    func clearCookies(forURL URL: Foundation.URL) -> Void {
        let cookieStorage = HTTPCookieStorage.shared
        let cookies = cookieStorage.cookies(for: URL) ?? []
        for cookie in cookies {
           // print("Deleting cookie for domain: \(cookie.domain)")
            cookieStorage.deleteCookie(cookie)
        }
    }
    
    
    //MARK: WebView Delegate Methods
    
    func webView(_ webView: UIWebView!, didFailLoadWithError error: NSError!) {
        //print("Webview fail with error \(error)");
    }
    
    func webView(_ webView: UIWebView!, shouldStartLoadWithRequest request: URLRequest!, navigationType: UIWebViewNavigationType) -> Bool {
        
        return true;
    }
    
    func webViewDidStartLoad(_ webView: UIWebView!) {
        showSearchView(self.view, searchText: "")
       // print("Webview started Loading")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView!) {
        hideSearchView()
       // print("Webview did finish load")
}

}

extension WebViewController: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
}



