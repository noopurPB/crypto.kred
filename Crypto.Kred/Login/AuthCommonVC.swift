//
//  AuthCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class AuthCommonVC: CKBaseVC, UIViewControllerTransitioningDelegate {

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
    
    
    func getWalletAPI(responseHandler:@escaping (_ result: Bool) -> Void){
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        let cks = CKGetService();
        cks.getWallet(params: params, responseHandler: { response in
            if(response.wallets.count > 0){
                let walletID = response.wallets[0].wallet ?? 0
                CKGlobalMethods.saveStringValueInPrefrences(String(format: "%d", walletID), key: CKConstants.PREF_WALLET_KEY);
                CKGlobalVariables.sharedManager.walletId = walletID;
                responseHandler(true)
            }
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    
    
    
    func saveLoginCredentialToPrefrences(_ token: String?, email: String?, password: String?, username: String?, avatar: String?, userId: String?){
        CKGlobalMethods.saveStringValueInPrefrences(token ?? "", key: CKConstants.PREF_TOKEN_KEY)
        CKGlobalMethods.saveStringValueInPrefrences(email ?? "", key: CKConstants.PREF_EMAIL_KEY)
        CKGlobalMethods.saveStringValueInPrefrences(email ?? "", key: CKConstants.PREF_RESET_EMAIL_KEY)
        CKGlobalMethods.saveStringValueInPrefrences(userId ?? "", key: CKConstants.PREF_USER_ID)
        if(password != nil){
            CKGlobalMethods.saveStringValueInPrefrences(password ?? "", key: CKConstants.PREF_PASSWORD_KEY)
        }
        CKGlobalMethods.saveStringValueInPrefrences(username ?? "", key: CKConstants.PREF_USERNAME)
        CKGlobalMethods.saveStringValueInPrefrences(avatar ?? "", key: CKConstants.PREF_AVATAR)
        if(avatar != nil){
            CKGlobalVariables.sharedManager.loggedInAvatar = avatar!
        }
    }
    
    
    func viewAfterAuthCalled(fromLogin: Bool){
        let storyboard = UIStoryboard(name: "CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKMarketPlaceVC") as?
        CKMarketPlaceVC
        if(fromLogin == true){
            controllerObejct?.transitioningDelegate = self
        }
        self.navigationController?.pushViewController(controllerObejct!, animated: false)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    

    
    
}
