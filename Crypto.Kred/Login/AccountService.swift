//
//  AccountService.swift
//  Grab
//
//  Created by Noopur Virmani on 27/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class AccountService: CKBaseService {
    
    //MARK: Signup Service
    
     func signupServiceMethod(params: [String: Any] , responseHandler:@escaping (_ result: SignupResponse) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.SIGNUP_URL
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
       // postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
        
        getObjectForUrl(path, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
        
    }
    
    //MARK: Login Service
    
     func loginServiceMethod(_ email: String?, password: String?, responseHandler:  @escaping ( _ result: SignupResponse) -> Void, errorHandler:@escaping ( _ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        
        params["email"] = email
        params["password"] = password
        
        let path = CKServiceConstants.LOGIN_URL
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
        
    }
    
    //MARK: Forgot  password Service
    
    func forgotPasswordServiceMethod( _ email: String?, responseHandler:  @escaping ( _ result: SignupResponse) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        
        params["email"] = email
               
        let path = CKServiceConstants.FORGOT_PASSWORD
        
        func objectResponseHandler( _ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
        
    }


    //MARK: Logout Service
    
     func logoutServiceMethod( _ token: String?, responseHandler:  @escaping ( _ result: SignupResponse) -> Void, errorHandler:@escaping ( _ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        //params = CKServiceConstants.initializeParams()
        
        params["token"] = token as AnyObject?
       
        let path = CKServiceConstants.LOGOUT_URL
        
        func objectResponseHandler( _ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
        
    }
    
    
    //MARK: Get User Service
    /*
    func getUserBioServiceMethod(_ userId: String?, responseHandler:@escaping (_ result: Member) -> Void, cacheHandler:  @escaping (_ result: Member) -> Void, errorHandler:  @escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        if(userId != nil && userId != ""){
            params["user"] = userId as AnyObject
        }
        
        
        let path = CKServiceConstants.USER_PROFILE
        
        func objectResponseHandler( _ response: AnyObject){
            let response = Mapper<Member> ().map(JSONObject:response)
            if(response != nil){
                responseHandler(response!)
            }else{
                errorHandler(CKConstants.AppErrorType.UNEXPECTED_ERROR)
            }
        }
        
        func objectCacheHandler( _ response: AnyObject){
            let response = Mapper<Member> ().map(JSONObject:response)
            if(response != nil){
                cacheHandler(response!)
            }else{
                errorHandler(CKConstants.AppErrorType.UNEXPECTED_ERROR)
            }
        }
        
        getObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,cacheHandler:objectCacheHandler,errorHandler: errorHandler)
    }*/

    
    //MARK: Update User Bio
    /*
     func updateUserBioServiceMethod( _ member: Member? ,responseHandler:  @escaping ( _ result: SignupResponse) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
       
        
       /* var styleParams: [String: AnyObject] = ["avatar": avatar as AnyObject? ?? "" as AnyObject]
        params["style"] = styleParams as AnyObject?*/
        
        var bioParams: [String: Any] = ["name": member?.user?.bio?.name ?? ""]
        bioParams["avatar"] = member?.user?.bio?.avatar ?? ""
        bioParams["description"] = member?.user?.bio?.descriptionL ?? ""
        
        bioParams["url"] = member?.user?.bio?.url ?? ""
        
        bioParams["organisation"] = member?.user?.bio?.organisation ?? ""
        bioParams["position"] = member?.user?.bio?.position ?? "" 
        
            
        var geoParams: [String: Any] = ["country": member?.user?.geo?.country ?? "" ]
        geoParams["state"] = member?.user?.bio?.organisation ?? ""
        geoParams["city"] = member?.user?.bio?.organisation  ?? ""
        
        //We don't store phone numbers on user records.
        
        params["tags"] = member?.user?.tags
        params["bio"] = bioParams
        params["geo"] = geoParams
        
        let path = CKServiceConstants.USER_PROFILE
        
        func objectResponseHandler( _ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }*/
    

    //MARK Edit user tags
   /* class func editUserTagsServiceMethod( _ member: Member? ,responseHandler:  @escaping ( _ result: SignupResponse) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        
        /* var styleParams: [String: AnyObject] = ["avatar": avatar as AnyObject? ?? "" as AnyObject]
         params["style"] = styleParams as AnyObject?*/
        
       /* var bioParams: [String: AnyObject] = ["name": member?.user?.bio?.name as AnyObject? ?? "" as AnyObject]
        bioParams["avatar"] = member?.user?.bio?.avatar ?? ""
        bioParams["description"] = member?.user?.bio?.descriptionL ?? ""
        
        bioParams["url"] = member?.user?.bio?.url ?? ""
        
        bioParams["organisation"] = member?.user?.bio?.organisation ?? ""
        bioParams["position"] = member?.user?.bio?.position ?? ""
        
        
        var geoParams: [String: AnyObject] = ["country": member?.user?.geo?.country as AnyObject? ?? "" as AnyObject]
        geoParams["state"] = member?.user?.bio?.organisation ?? ""
        geoParams["city"] = member?.user?.bio?.organisation ?? ""
        */
        //We don't store phone numbers on user records.
        params["contacts"] =  member?.id != nil ? member?.id : ""
        params["tags"] =   member?.tags //member?.user?.tags ??
        
        /*if(member?.user != nil){
            var tagsParams: [String: AnyObject] =
            ["tags":  member?.user?.tags ?? (member?.tags)!]
            
            params["user"] =  tagsParams
            
        }else{
           
            params["tags"] =  member?.user?.tags ?? member?.tags
        }*/
       // params["bio"] = bioParams as AnyObject?
       // params["geo"] = geoParams as AnyObject?
        
        
        let path = CKServiceConstants.CONTACT_EDIT
       
        func objectResponseHandler( _ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
   */
    
    //MARK: SYNC Phonebook contact
    
    /*class func phoneContactSyncServiceMethod(_ emailIds: [String] ,responseHandler:  @escaping ( _ result: PhonebookResponseObject) -> Void, cacheHandler:  @escaping (_ result: PhonebookResponseObject) -> Void, errorHandler:  @escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        if(emailIds.count > 0){
            params["email"] =  emailIds.joined(separator: ",")
        }
        
        let path = CKServiceConstants.USER_CHECK
        
        func objectResponseHandler( _ response: AnyObject){
            let response = Mapper<PhonebookResponseObject> ().map(JSONObject:response)
            if(response != nil){
                responseHandler(response!)
            }else{
                errorHandler(CKConstants.AppErrorType.UNEXPECTED_ERROR)
            }
        }
        
        func objectCacheHandler( _ response: AnyObject){
            let response = Mapper<PhonebookResponseObject> ().map(JSONObject:response)
            if(response != nil){
                cacheHandler(response!)
            }else{
                errorHandler(CKConstants.AppErrorType.UNEXPECTED_ERROR)
            }
        }
        
        //postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
        
        getObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,cacheHandler:objectCacheHandler,errorHandler: errorHandler)
    }*/
    
       
    //MARK: Account Update
    /*
    func updateAccountServiceMethod(_ email: String?, password: String? ,responseHandler:  @escaping ( _ result: SignupResponse) -> Void, errorHandler:@escaping ( _ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        
        if(email != CKGlobalVariables.sharedManager.loggedInEmail){
            params["email"] = email
        }
        
        if(password != nil && password != ""){
            params["password"] = password
        }
        
        let path = CKServiceConstants.ACCOUNT_UPDATE
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<SignupResponse> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
        
    }*/
    
    
    
   

    
}
