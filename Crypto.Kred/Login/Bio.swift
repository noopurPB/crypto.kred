//
//  Bio.swift
//  Grab
//
//  Created by Noopur Virmani on 21/03/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class Bio: NSObject, Mappable{
    
    var cls: String?
    var age: String?
    var avatar: String?
    var descriptionL: String?
    var gender: String?
    var language: String?
    var name: String?
    var photo: String?
    var real_name: String?
    var screen_name: String?
    var timezone: String?
    var tz: String?
    var url: String?
    
    
    var organisation: String?
    var position: String?
    var country: String?
    var state: String?
    var city: String?
    var phone: String?

    override init(){
        
    }

    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
         self.cls <- map["cls"]
         self.age <- map["age"]
         self.avatar <- map["avatar"]
         self.descriptionL <- map["description"]
         self.gender <- map["gender"]
         self.language <- map["language"]
         self.name <- map["name"]
        
         //let string = name as! String
         //print(string)
         self.real_name <- map["real_name"]
         self.screen_name <- map["screen_name"]
         self.photo <- map["photo"]
         self.timezone <- map["timezone"]
         self.tz <- map["tz"]
         self.url <- map["url"]
        
         self.organisation <- map["organisation"]
         self.position <- map["position"]
         self.country <- map["country"]
         self.state <- map["state"]
         self.city <- map["city"]
         self.phone <- map["phone"]

    }
    
}
