//
//  LoginTableViewCell.swift
//  Grab
//
//  Created by Noopur Virmani on 21/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


@objc protocol LoginTableViewCellDelegate {
    @objc optional func loginSignUpBtnClickedDelegateMethod(_ loginPressed: Bool)
    @objc optional func nextBtnClickedDelegateMethod()
    @objc optional func backBtnClickedDelegateMethod()
    @objc optional func forgotPasswordClickedDelegateMethod()
}


class  LoginTableViewCell: BaseTableViewCell{
    
    var delegate: LoginTableViewCellDelegate?
    
    @IBOutlet var logoBtn: UIButton!
    @IBOutlet var iconBtn: UIButton!
    @IBOutlet var signupArrow: UIImageView!
    @IBOutlet var loginArrow: UIImageView!
    @IBOutlet var loginTextField: UITextField!
    
    @IBOutlet weak var backButton: UIButton!
    //@IBOutlet var nextButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    
    @IBOutlet var forgotButton: UIButton!
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var signupButton: UIButton!
   
    @IBAction func loginBtnClicked(_ sender: AnyObject) {
        delegate?.loginSignUpBtnClickedDelegateMethod!(true)
    }
    
    @IBAction func signupBtnClicked(_ sender: AnyObject) {
        delegate?.loginSignUpBtnClickedDelegateMethod!(false)
    }
    
  
    @IBAction func nextBtnClicked(_ button: UIButton) {
        delegate?.nextBtnClickedDelegateMethod!()
    }
    
    @IBAction func forgotBtnClicked(_ sender: AnyObject) {
        delegate?.forgotPasswordClickedDelegateMethod!()
       
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(nextButton != nil){
            nextButton.setTitleColor(UIColor.white, for: UIControlState())
            nextButton.titleLabel?.font = Theme.regularFont(18.0)
        }
        if(forgotButton != nil){
            forgotButton.setTitleColor(Color.loginTextColor, for: UIControlState())
            forgotButton.titleLabel?.font = Theme.regularFont(17.0)
        }
        
        if(loginButton != nil){
            loginButton.setTitleColor(UIColor.white, for: UIControlState())
            loginButton.titleLabel?.font = Theme.mediumFont(19.0)
        }
        if(signupButton != nil){
            signupButton.setTitleColor(UIColor.white, for: UIControlState())
            signupButton.titleLabel?.font = Theme.mediumFont(19.0)
        }

        if(nextButton != nil){
           // nextButton.backgroundColor = Color.themeAnotherColor
        }
        
        if(backButton != nil){
            self.backButton.setFAIcon(icon: .FATimes, iconSize: 25.0, forState: UIControlState.normal)
        }
    }
    
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        delegate?.backBtnClickedDelegateMethod!()
    }
    
    func buttonStyle(_ button: UIButton){
        button.layer.shadowOpacity = 1.0
        button.layer.shadowOffset = CGSize(width: 5, height: 5)
        button.layer.shadowColor = UIColor.red.cgColor
        button.layer.masksToBounds = false
        button.layer.shadowRadius = 4
        button.clipsToBounds = true
        button.layer.cornerRadius = 5.0
        button.layer.borderColor = UIColor.darkGray.cgColor
        button.layer.borderWidth = 1.0
    }
    
    
    
    func slideInFromLeft(_ duration: TimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let _: AnyObject = completionDelegate {
           // slideInFromLeftTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        loginArrow.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
   }
