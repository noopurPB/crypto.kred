//
//  TermsConditionViewController.swift
//  Grab
//
//  Created by Noopur Virmani on 19/02/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import MessageUI
import WebKit


protocol TermsConditionViewControllerDelegate{
    func agreedTermsandConditionDelegateMethod()
}


class TermsConditionViewController: AuthCommonVC ,MFMailComposeViewControllerDelegate , UITableViewDataSource , UITableViewDelegate, WKNavigationDelegate, WKUIDelegate{
    
    var delegate:  TermsConditionViewControllerDelegate?
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet var tableView: UITableView!
    
    var array = [NSLocalizedString("pay_overview", comment: ""),
                 NSLocalizedString("pay_name_policy", comment: ""),
                 NSLocalizedString("pay_accept_policy", comment: ""),
                 NSLocalizedString("pay_privacy_policy", comment: ""),
                 NSLocalizedString("pay_complaint_resolution", comment: ""),
                 NSLocalizedString("pay_registrar", comment: "")]
    
    var fileArray = [ "Overview","NamingPolicy","AcceptableUsePolicy",
                      "PrivacyWhoisPolicy",
                      "ComplaintResolutionService" ,"Registry-RegistrarAgreement"]

    @IBOutlet weak var navTitle: UILabel!
    var agreeBarButton: UIBarButtonItem?
    
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var disagreeBtn: UIButton!
    // var email: String?
   // var password: String?
   // var name: String?
   // var emailUpdates: Bool?
    
    var signupObjects: SignupObjects?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self;
        webView.uiDelegate = self
        
       // if let pdf = Bundle.main.path(forResource: "Overview", ofType: "pdf", inDirectory: nil, forLocalization: nil)  {
            let url = URL(string: "https://drive.google.com/file/d/1D_FntIdh6-V5XHE1nkYmbEMmiaoJe4LT/view")!
            let request = URLRequest(url: url)
            webView.load(request)
            // webView.allowsBackForwardNavigationGestures = true
      //  }
        
       // let url = URL(string: "https://app.crypto.kred/coin/thekramer1/1")!
        
       // webView.allowsBackForwardNavigationGestures = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initUI()
    }
    
    
    func initUI(){
        navBarStyle?.showNavigationLeftTitleItem(self, navTitle : NSLocalizedString("terms_condition", comment: ""), barItemTitle: NSLocalizedString("disagree", comment: ""))
        navBarStyle?.delegate = self
        agreeBarButton = UIBarButtonItem(title: NSLocalizedString("agree", comment: "") , style: UIBarButtonItemStyle.plain, target: self, action:#selector(TermsConditionViewController.agreeBarButtonClicked) )
        self.navigationItem.rightBarButtonItem = agreeBarButton
        
        /*
        self.navTitle.text = NSLocalizedString("terms_condition", comment: "")
        agreeBtn.setTitle(NSLocalizedString("agree", comment: ""), for: UIControlState.normal)
        disagreeBtn.setTitle(NSLocalizedString("disagree", comment: ""), for: UIControlState.normal)
        self.navTitle.textColor = CKColor.barTintColor;
        self.navTitle.font = CKTheme.regularFont(15.0);*/
    }
    
    
    
    func webView(_: WKWebView, didStartProvisionalNavigation: WKNavigation!){
        self.showSearchView(self.webView, searchText: "")
    }
    
    func webView(_: WKWebView, didFailProvisionalNavigation: WKNavigation!, withError: Error){
        self.hideSearchView()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         self.hideSearchView()
    }
    
  
    
  
    @IBAction func agreeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func disagreeClicked(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @objc func agreeBarButtonClicked(){
      // self.signupServiceCalled()
        self.dismiss(animated: true, completion: {
            self.delegate?.agreedTermsandConditionDelegateMethod()
        })
    }
    

    @IBAction func emailSendBtnClicked(_ sender: AnyObject) {
        sendEmailButtonTapped()
    }
    
    //MARK: Services
    
    func signupServiceCalled(){
        self.showLoadingDialog(NSLocalizedString("let_start", comment: ""))
        let cks = AccountService()
        
        
        var params = [String: Any]()
        
        params["email"] = signupObjects?.email ?? ""
        params["username"] = signupObjects?.username ?? ""
        params["phone"]  = signupObjects?.phoneNo ?? ""
        //params["redirect_uri"] = ""
        
        cks.signupServiceMethod(params: params, responseHandler:
            signupCompletionHandler, errorHandler: {
            error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    //MARK: Service handler
    
    func signupCompletionHandler(_ responseData: SignupResponse){
        self.hideLoadingDialog()
        if(responseData.token != nil){
            if(responseData.user != nil && responseData.user?.bio != nil){
                saveLoginCredentialToPrefrences(responseData.token ?? "" , email: signupObjects?.email, password: "" , username: responseData.user?.bio?.name ?? "", avatar: responseData.user?.bio?.avatar ?? "", userId: CKGlobalVariables.sharedManager.loggedInUserId)
            }else{
                saveLoginCredentialToPrefrences(responseData.token ?? "" , email: signupObjects?.email, password: responseData.password , username: signupObjects?.username, avatar:"", userId: responseData.id)
            }
            
            self.getWalletAPI(responseHandler: { responseData in
                //self.viewAfterAuthCalled(fromLogin: false)
                self.dismiss(animated: true, completion: nil)
            })
            
          
        }else{
            hideLoadingDialog()
            self.navigationController?.popViewController(animated: true)
            showErrorMessage(responseData.message ?? "")
        }
    }
    
  
    
    /************* Mail **************/
    func sendEmailButtonTapped() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([signupObjects?.email ?? ""])
        mailComposerVC.setSubject(NSLocalizedString("mail_terms_condition_subject", comment: ""))
        mailComposerVC.setMessageBody(createHTMLBodyForEmail(), isHTML: true)
        
       /* if(mimeTypefrom() != nil){
            mailComposerVC.addAttachmentData(mimeTypefrom()!, mimeType: documentType, fileName: documentName)
        }*/
        return mailComposerVC
    }
    
    /* func mimeTypefrom() -> NSData? {
       if let filePath = NSBundle.mainBundle().pathForResource(documentName, ofType: documentType) {
            if let fileData = NSData(contentsOfFile: filePath) {
                print("File data loaded.")
                return fileData
            }
        }
        return nil
    }*/
    
    
    func createHTMLBodyForEmail() -> String{
    
       /*let emailBody = String(format: "<p><span style='font-family: helvetica; font-size: small;'><strong>Hi there, </strong></span></p>
        <p><span style='font-family: helvetica; font-size: small;"><strong>Noopur has sent you the attached Terms and Conditions from the Grab iOS App.</strong></span></p>
        <p><span style='font-family: helvetica; font-size: small;'><strong> Visit <a title='Grab.Live' href='https://www.google.co.in/'>Grab.Live</a> to learn more about Grab</strong></span></p>")*/
        
        let emailBody = String(format: "<html><body><p><span style='font-family: helvetica; font-size: small;'><strong>Hi there, </strong></span></p><p><span style='font-family: helvetica; font-size: small;'><strong>%@ has sent you the attached Terms and Conditions from the Grab iOS App.</strong></span></p><p><span style='font-family: helvetica; font-size: small;'><strong>Visit <a href='%@'>%@</a> to learn more about Grab</strong></span></p></body></html>", signupObjects?.username ?? "", CKServiceConstants.COMPANY_URL , CKServiceConstants.COMPANY_URL_NAME)
        
        /*let emailBody = String(format: "<html><body><div style='font-family:helvetica;font-weight: bold;color:black;font-size:14px;'>Hi there,\n\n %@ has sent you the attached Terms and Conditions from the Grab iOS App.\n\n Visit<a href='http://www.grab.live/'>Grab.Live</a> to learn more about Grab</div></body></html>", name!)*/
       return emailBody
    }
    
    func showSendMailErrorAlert() {
        
        
        let alert = UIAlertController(title:"",
                                      message: NSLocalizedString("email_not_send", comment: ""),
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = Color.themeDarkColor
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ alertView in
            
        })) //Destructive
        self.present(alert, animated: true, completion: nil);
        
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // MFMailComposeResultSaved
        
        controller.dismiss(animated: true, completion: {
            //print(result)
            /*switch (result) {
            case MFMailComposeResultCancelled:
                print("Message was cancelled")
                self.dismissViewControllerAnimated(true, completion: nil)
            case MFMailComposeResultFailed:
                print("Message failed")
                self.dismissViewControllerAnimated(true, completion: nil)
            case MFMailComposeResultSent:
                print("Message was sent")
                self.dismissViewControllerAnimated(true, completion: nil)
            default:
                break;
            }*/
        })
        
    }
    
    
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BaseTableViewCell", for: indexPath) as! BaseTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.titleLabel.font = Theme.regularFont(15.0)
        cell.titleLabel.textColor = Color.themeDarkColor
        
        cell.titleLabel.text = array[indexPath.row]
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if let pdf = Bundle.main.path(forResource: fileArray[indexPath.row], ofType: "pdf", inDirectory: nil, forLocalization: nil)  {
            webViewControllerCalled(array[indexPath.row], url: pdf)
        }
    }
}

extension TermsConditionViewController: NavigationBarStyleDelegate{
    
    func titleBarClickedDelegateMethod() {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
}


class SignupObjects: NSObject{
    var email: String?
    var username: String?
    var phoneNo: String?
    var keepUpdates: Bool?
    
    override init() {
        super.init()
        
    }
}


