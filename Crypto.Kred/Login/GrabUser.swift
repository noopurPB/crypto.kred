//
//  GrabUser.swift
//  Grab
//
//  Created by Noopur Virmani on 21/03/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class GrabUser: NSObject, Mappable{
     
    var bio: Bio?
    var address: String?
    var username: String?
    var source: String?
  //  var kred: Kred?
    var id: String?
    var token: String?
   // var me: Membership?
    var memberships: Int?
    var screen_name_lower: String?
  //  var analytics: Analytics?
    var type: String?
    var user: GrabUser?
   // var geo: Geo?
    
    var tags = [String]()
    
    override init(){
        
    }
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self.id <- map["id"]
        
     //   self.kred <- map["kred"]
        
        self.address <- map["address"]
        
        self.bio <- map["bio"]
        
        self.username <- map["name"]
        
        if(bio?.name == nil || bio?.name == ""){
            self.bio?.name = self.username ?? ""
        }
        
        self.token <- map["token"]
        
        if(map["me"].isKeyPresent == true){
         //   self.me <- map["me"]
            //self.membership <- map["me"]
        }
        
        /*if(map["membership"].isKeyPresent == true && self.me == nil){
           // self.me <- map["membership"]
           // self.membership <- map["membership"]
        }*/
        
        self.memberships <- map["memberships"]
        
        self.screen_name_lower <- map["screen_name_lower"]
        
       // self.analytics <- map["analytics"]
        
        self.type <- map["type"]
        
        self.tags <- map["tags"]
        
      //  self.geo <- map["geo"]
        
        self.source <- map["source"]
        
        self.user <- map["user"]
        
        /*if(self.membership == nil && self.me != nil){
            self.membership = self.me
        }*/
              
    }
    
}
