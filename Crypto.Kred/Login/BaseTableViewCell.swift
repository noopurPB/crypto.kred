//
//  BaseTableViewCell.swift
//  Grab
//
//  Created by Noopur Virmani on 21/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol  BaseTableViewCellDelegate{
    @objc optional func thumbnailBtnClicked(_ sender: AnyObject)
    @objc optional func addBtnClicked(_ sender: AnyObject)
}


class BaseTableViewCell: UITableViewCell{
    
    var baseDelegate: BaseTableViewCellDelegate?
    @IBOutlet var dividerView: UIView!
   
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subTitleLabel: UILabel!
    @IBOutlet var triTitleLAbel: UILabel!
    @IBOutlet var thumbnailBtn: UIButton!
    
    @IBOutlet var addBtn: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var smallIconImageView: UIImageView!
    @IBOutlet var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.preservesSuperviewLayoutMargins = false
        self.contentView.preservesSuperviewLayoutMargins = false
        if(dividerView != nil){
            dividerView.backgroundColor = CKColor.dividerColor
        }
        
        if(activityIndicator != nil){
            activityIndicator.color = CKColor.themeAnotherColor
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
       /* for subview: UIView in self.contentView.superview!.subviews {
            if NSStringFromClass().hasSuffix("SeparatorView") {
                subview.hidden = false
            }
        }*/
    }
    

    @IBAction func thumbnailBtnClicked(_ sender: AnyObject) {
        baseDelegate?.thumbnailBtnClicked!(sender)
    }

    @IBAction func addBtnClicked(_ sender: AnyObject) {
         baseDelegate?.addBtnClicked!(sender)
    }
    
    @IBAction func inviteBtnClicked(_ sender: AnyObject) {
        baseDelegate?.addBtnClicked!(sender)
    }
}
