//
//  ForgetPasswordVC.swift
//  Grab
//
//  Created by Noopur Virmani on 27/08/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation

class ForgetPasswordVC: CKBaseVC {

    @IBOutlet var fPTextField: UITextField!
    @IBOutlet var fPRemember: UIButton!
    
    @IBOutlet var fPLoginBtn: UIButton!
    @IBOutlet var fPLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGestureToView()
        initUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.alpha = 0.0;
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if (self.navigationController?.topViewController == self) {
            self.navigationController?.navigationBar.alpha = 0.0;
        }
    }
    
    
    func initUI(){
        fPTextField.becomeFirstResponder()
        fPLabel.textColor = UIColor.white
        fPLabel.font = Theme.boldFont(19.0)
        fPTextField.placeholder = NSLocalizedString("email", comment: "");
        //fPTextField.textColor = UIColor.whiteColor()
        CKGlobalMethods.loginTextFieldStyle(fPTextField, placeholder: fPTextField.placeholder)
        fPRemember.titleLabel?.font = Theme.regularFont(16.0)
        fPRemember.setTitleColor(Color.loginTextColor, for: UIControlState())
    }
    
    
    @IBAction func sendBtnClicked(_ sender: AnyObject) {
        if(CKGlobalMethods.isValidEmail(CKGlobalMethods.trimString(fPTextField!.text)) == false){
            showErrorMessage(NSLocalizedString("login_valid_email", comment: ""))
        }else{
            forgotServiceCalled()
        }
    }
    

    @IBAction func rememberBtnClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    
    //MARK:  Forgot Password Service Method
    
    func forgotServiceCalled(){
        func responseHandler(_ response: SignupResponse){
            hideLoadingDialog()
            if(response.error == nil){
                CKGlobalMethods.showAlertWithTitleandMessage("", message: NSLocalizedString("forget_password_success", comment: ""))
            }else{
                showErrorMessage(response.error ?? "")
            }
        }
        showLoadingDialog("")
        
        let cks = AccountService()
        cks.forgotPasswordServiceMethod(fPTextField.text, responseHandler: responseHandler , errorHandler: errorHandler)
    }
    
    
   
}
