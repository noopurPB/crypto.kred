//
//  LoginViewController.swift
//  Grab
//
//  Created by Noopur Virmani on 21/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewControllerDelegate{
    func loginSignupSuccessDelegateMethod()
}

class LoginViewController: AuthCommonVC, LoginTableViewCellDelegate, UITextFieldDelegate, TextFieldTableViewCellDelegate, BaseTableViewCellDelegate , UITableViewDelegate, UITableViewDataSource {
    
    var delegate: LoginViewControllerDelegate?
    
    @IBOutlet var loginTableView: UITableView!
    var loginPressed: Bool?
    
    var emailTextField: UITextField?
    var passwordTextField: UITextField?
    var usernameTextField: UITextField?
    var phoneTextField: UITextField?
    
    var keepMeUpdate: Bool = true
    
    var itemIndex: Int = 0
    
    var loginBtn: TKTransitionSubmitButton?
    var loginNextBtn: UIButton?
    
    var signupRows: Int = 6;
    var loginRows: Int = 5;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGestureToView()
      
        addRightSwipeGesture()
        addLeftSwipeGesture()
        loginBtnAnimationFunction()
        self.loginTableView.isHidden = false
        self.loginTableView.alpha = 1.0
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        showLoginKeyboardNotification()
        self.loginTableView.isScrollEnabled = false
    }
    
    
    @IBAction func loginBackBtnClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Login Btn Animations
    
    func loginBtnAnimationFunction(){
        let XAxis:CGFloat =  52.0/2.0
        loginBtn = TKTransitionSubmitButton(frame: CGRect(x: XAxis, y: 0, width: self.view.frame.size.width - 52, height: 44))
       // loginBtn?.center = self.view.center
        loginBtn?.frame.bottom = self.view.frame.height - 60
        
        //loginBtn?.setTitle("Sign in", for: UIControlState())
        loginBtn?.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 14)
        loginBtn?.addTarget(self, action: #selector(LoginViewController.test(_:)), for: UIControlEvents.touchUpInside)
        loginBtn?.backgroundColor = UIColor.clear
        self.view.addSubview(loginBtn!)
        self.loginBtn?.isHidden = true
        self.view.bringSubview(toFront: self.loginTableView)
    }
   
    
    func setLoginAnimationBtnFrame(indexPath: IndexPath, cell: LoginTableViewCell){
        let rect = self.loginTableView.rectForRow(at: indexPath)
        self.loginNextBtn = cell.nextButton
        self.loginBtn?.isHidden = false
        
        var newFrame = rect
        newFrame.origin.x = (self.loginNextBtn?.frame.origin.x)! + 5
        newFrame.origin.y =  rect.origin.y + 10
        newFrame.size.width = (self.loginNextBtn?.frame.size.width)! - 20
        newFrame.size.height = 45;
        
        self.loginBtn?.frame = newFrame
        
    }
    
    //MARK: Tableview Datasource and Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 310
        }else{
            return 50
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(loginPressed == true){
            return loginRows
        }else{
            return signupRows
        }
    }
    
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoginTopCell") as! LoginTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.delegate = self
            if(self.loginPressed == true){
                cell.loginArrow.isHidden = false
                cell.signupArrow.isHidden = true
            }else{
                cell.loginArrow.isHidden = true
                cell.signupArrow.isHidden = false
            }
            cell.logoBtn.alpha = 1.0
            cell.loginArrow.alpha = 1.0
            cell.signupArrow.alpha = 1.0
            return cell
            
         }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTextFieldViewCell", for: indexPath) as! TextFieldTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textField.delegate = self
            cell.delegate = self
            cell.textField.placeholder = NSLocalizedString("email", comment: "");
            cell.textField.returnKeyType = UIReturnKeyType.default
            cell.textField.keyboardType = UIKeyboardType.emailAddress
            if(self.loginPressed == true){
                cell.textField.text = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_EMAIL_KEY)
            }else{
                cell.textField.text = ""
            }
            //cell.textField.text = "cameronbale@peoplebrowsr.com"
            CKGlobalMethods.loginTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
            cell.textField.tag = indexPath.row
            emailTextField = cell.textField
            cell.textField.alpha = 1.0
            return cell;
            
         }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTextFieldViewCell", for: indexPath) as! TextFieldTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textField.delegate = self
            cell.delegate = self
            cell.textField.returnKeyType = UIReturnKeyType.default
            //cell.textField.keyboardType = UIKeyboardType.Default
            cell.textField.tag = indexPath.row
            if(self.loginPressed == true){
                cell.textField.placeholder = NSLocalizedString("password", comment: "");
                cell.textField.isSecureTextEntry = true
                cell.textField.text = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_PASSWORD_KEY)
                passwordTextField = cell.textField
                //cell.textField.text = "alpha99"
            }else{
                cell.textField.isSecureTextEntry = false
                cell.textField.placeholder = NSLocalizedString("username", comment: "");
                cell.textField.text = ""
                self.usernameTextField = cell.textField
            }
             CKGlobalMethods.loginTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
             cell.textField.alpha = 1.0
            return cell
            
            
            //
         }else if(indexPath.row == 3){
            
            if(loginPressed == true){
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoginNextCell") as! LoginTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.nextButton.setTitle(NSLocalizedString("log_in", comment: ""), for: UIControlState())
                cell.nextButton.tag = indexPath.row
                cell.delegate = self
                setLoginAnimationBtnFrame(indexPath: indexPath, cell: cell)
                return cell
            
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTextFieldViewCell", for: indexPath) as! TextFieldTableViewCell
                cell.textField.delegate = self
                cell.delegate = self
                cell.textField.returnKeyType = UIReturnKeyType.default
                cell.textField.tag = indexPath.row
                 cell.textField.placeholder = NSLocalizedString("phone_placeholder_text", comment: "");
                cell.textField.isSecureTextEntry = false
                cell.textField.text = ""
                self.phoneTextField = cell.textField
                CKGlobalMethods.loginTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
                return cell
                
            }
            
        }else if(indexPath.row == 4){
            
            if(loginPressed == true){
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoginForgotCell") as! LoginTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.forgotButton.setTitle(NSLocalizedString("forgot_password", comment: ""), for: UIControlState())
                cell.delegate = self
                return cell
            }
             else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoginNextCell") as! LoginTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.nextButton.setTitle(NSLocalizedString("sign_up", comment: ""), for: UIControlState())
                //self.loginBtn?.isHidden = true
                cell.nextButton.tag = indexPath.row
                cell.delegate = self
                setLoginAnimationBtnFrame(indexPath: indexPath, cell: cell)
                return cell
                
                
                
            }
            
            
        }else {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "BaseCheckTableViewCell") as! BaseTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.baseDelegate = self
            cell.thumbnailBtn.titleLabel!.font = Theme.regularFont(16.0)
            cell.thumbnailBtn.setTitleColor(UIColor.white, for: UIControlState())
            cell.thumbnailBtn.isSelected = keepMeUpdate
            cell.thumbnailBtn.setTitle(NSLocalizedString("keep_me_update", comment: ""), for: UIControlState())
            return cell

        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: LoginTableViewCellDelegate
    
    func backBtnClickedDelegateMethod() {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    func loginSignUpBtnClickedDelegateMethod(_ loginPressed: Bool){
        self.loginPressed = loginPressed
        reloadTableViewWithAnimations()
    }
    
    
    func nextBtnClickedDelegateMethod(){
        if(loginPressed == true){
            loginValidations()
        }else{
            completeSignupValidation()
        }
    }
    
    
    func forgotPasswordClickedDelegateMethod(){
        forgetPasswordControllerCalled()
    }
    
    func forgetPasswordControllerCalled(){
        let controllerObejct = self.storyboard!.instantiateViewController(withIdentifier: "ForgetPasswordVC") as? ForgetPasswordVC
        
        DispatchQueue.main.async(execute: {
            let navigationC = UINavigationController(rootViewController: controllerObejct!)
            self.present(navigationC, animated: true, completion: nil)
        })
    }
    

  
    
    //MARK: Gesture
    func addRightSwipeGesture(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(LoginViewController.rightSwipedResponder))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    
    func addLeftSwipeGesture(){
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(LoginViewController.leftSwipedResponder))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    
    //MARK: Gesture
    @objc func leftSwipedResponder(){
        if(loginPressed == false){
            self.loginPressed = true
            reloadTableViewWithAnimations()
        }
    }
    
    
    @objc func rightSwipedResponder(){
        if(loginPressed == true){
            self.loginPressed = false
            self.loginTableView.reloadData()
            reloadTableViewWithAnimations()
        }
    }
    
    
    func reloadTableViewWithAnimations(){
        self.loginTableView.reloadData()
        let indexPath1 = IndexPath(row: 1, section: 0)
        let indexPath2 = IndexPath(row: 2, section: 0)
        let indexPath3 = IndexPath(row: 3, section: 0)
        let indexPath4 = IndexPath(row: 4, section: 0)
        if(loginPressed == true){
            self.loginTableView.reloadRows(at: [indexPath1,indexPath2, indexPath3, indexPath4], with: UITableViewRowAnimation.left)
        }else{
            let indexPath5 = IndexPath(row: 5, section: 0)
            self.loginTableView.reloadRows(at: [indexPath1,indexPath2, indexPath3, indexPath4, indexPath5], with: UITableViewRowAnimation.right)
        }
    }
    
    
    //MARK:  Validations
    func loginValidations(){
        var message = ""
        if(CKGlobalMethods.trimString(emailTextField?.text) == "" || CKGlobalMethods.trimString(passwordTextField?.text) == ""){
            message = NSLocalizedString("login_valid_value", comment: "")
        }else if(CKGlobalMethods.isValidEmail(CKGlobalMethods.trimString(emailTextField!.text)) == false){
            message = NSLocalizedString("login_valid_email", comment: "")
        }else {
            message = ""
        }
        if(message == ""){
            loginServiceCalled()
        }else{
            showErrorMessage(message)
        }
    }
    
    
    func completeSignupValidation(){
        var message = ""
        if(CKGlobalMethods.isValidEmail(CKGlobalMethods.trimString(emailTextField!.text)) == false){
            message = NSLocalizedString("login_valid_email", comment: "")
        }else if(CKGlobalMethods.trimString(usernameTextField?.text) == ""){
            message = NSLocalizedString("login_valid_username", comment: "")
        }
        else if(CKGlobalMethods.trimString(phoneTextField?.text) == ""){
            message = NSLocalizedString("login_valid_password", comment: "")
        }
        else{
            message = ""
        }
        if(message == ""){
            DismissKeyboard()
            termsConditionViewControllerCalled(CKGlobalMethods.trimString(usernameTextField?.text),
                                               email: CKGlobalMethods.trimString(emailTextField?.text),
                                               password: CKGlobalMethods.trimString(passwordTextField?.text), emailUpdates: keepMeUpdate)
            
            
        }else{
            showErrorMessage(message)
        }
    }


    
    func termsConditionViewControllerCalled(_ name: String?, email: String?, password: String, emailUpdates: Bool?){
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionViewController") as?
        TermsConditionViewController
        
        let signupObj = SignupObjects()
        signupObj.email = email;
        signupObj.keepUpdates = emailUpdates;
        signupObj.phoneNo = self.phoneTextField?.text;
        signupObj.username = self.usernameTextField?.text;
        controllerObejct?.signupObjects = signupObj;
        controllerObejct?.delegate = self;
        let navC = UINavigationController(rootViewController: controllerObejct!)
        self.present(navC, animated: true, completion: nil)
        //self.view.addSubview((controllerObejct?.view)!)
       // navC.pushViewController(controllerObejct!, animated: true)
    }

    
    
    
    
    //MARK: BaseTableViewCellDelegate
    
    func thumbnailBtnClicked(_ sender: AnyObject){
        let button : UIButton = sender as! UIButton
        if(button.isSelected == true){
            button.isSelected = false
        }else{
            button.isSelected = true
        }
        keepMeUpdate = button.isSelected
    }
    
    //MARK: TextFieldTableViewCellDelegate
    
    func textFieldValueChanged(_ sender: AnyObject){
        let textField: UITextField = sender as! UITextField
        if(self.loginPressed == true){
            if(textField.tag == 1){
                if(textField.text == ""){
                    CKGlobalMethods.saveStringValueInPrefrences(textField.text, key: CKConstants.PREF_EMAIL_KEY)
                }else if (textField.text?.count == 1){
                    setResetEmail(textField)
                }
            }else{
                if(textField.text == ""){
                    CKGlobalMethods.saveStringValueInPrefrences(textField.text, key: CKConstants.PREF_PASSWORD_KEY)
                }
            }
        }
    }
    
    func setResetEmail(_ textField: UITextField?){
        let resetEmail = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_RESET_EMAIL_KEY)
        if(resetEmail != ""){
            let index = resetEmail!.startIndex
            let textFieldIndex = textField?.text?.startIndex
            if((textField?.text![textFieldIndex!])! == resetEmail![index]){
                emailTextField?.text = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_RESET_EMAIL_KEY)
            }
        }
    }

    
    // first letter uppercase
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /*if(textField == self.usernameTextField && loginPressed == false){
            if(range.location == 0 && range.length == 0){
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
                
                textField.text = newString
                return false
            }else{
                return true
            }
        }else{
            return true
        }*/
        return true
    }
    

    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
        
    }
    
    //MARK: Keyboard show notification
    func showLoginKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: Keyboard show hide method
    @objc func keyboardWillShow(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                self.loginTableView.contentInset = contentInsets
                self.loginTableView.scrollIndicatorInsets = contentInsets
                self.loginTableView.isScrollEnabled = true
                
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.loginTableView.contentInset = UIEdgeInsets.zero;
        self.loginTableView.scrollIndicatorInsets = UIEdgeInsets.zero;
        self.loginTableView.isScrollEnabled = false
    }

    
    
    
    //MARK: Services
    
    func signupServiceCalled(){
        self.showLoadingDialog(NSLocalizedString("let_start", comment: ""))
        let cks = AccountService()
        
        var params = [String: Any]()
        
        params["email"] = CKGlobalMethods.trimString(emailTextField?.text)
        params["username"] = self.usernameTextField?.text ?? ""
        params["phone"]  = self.phoneTextField?.text ?? ""
        //params["redirect_uri"] = ""
        
        cks.signupServiceMethod(params: params, responseHandler:
            loginCompletionHandler, errorHandler: {
                error in
                self.hideLoadingDialog()
                self.errorHandler(error)
        })
    }
    
    //MARK: Service handler
    /*
    func signupCompletionHandler(_ responseData: SignupResponse){
        self.hideLoadingDialog()
        if(responseData.token != nil){
            if(responseData.user != nil && responseData.user?.bio != nil){
                saveLoginCredentialToPrefrences(responseData.token ?? "" , email: signupObjects?.email, password: "" , username: responseData.user?.bio?.name ?? "", avatar: responseData.user?.bio?.avatar ?? "", userId: CKGlobalVariables.sharedManager.loggedInUserId)
            }else{
                saveLoginCredentialToPrefrences(responseData.token ?? "" , email: signupObjects?.email, password: responseData.password , username: signupObjects?.username, avatar:"", userId: responseData.id)
            }
            
            self.getWalletAPI(responseHandler: { responseData in
                //self.viewAfterAuthCalled(fromLogin: false)
                self.dismiss(animated: true, completion: nil)
            })
            
            
        }else{
            hideLoadingDialog()
            self.navigationController?.popViewController(animated: true)
            showErrorMessage(responseData.message ?? "")
        }
    }*/
    
    

    //MARK: Login Service
    
    func loginServiceCalled(){
        self.DismissKeyboard()
        /*
        self.loginNextBtn?.isHidden = true
        self.loginBtn?.isHidden = false
        self.loginBtn?.backgroundColor = Color.themeAnotherColor
        self.loginBtn?.startLoadingAnimation()*/
        
        showLoadingDialog(NSLocalizedString("get_ready_grab", comment: ""))
        
        let cks = AccountService()
        cks.loginServiceMethod(CKGlobalMethods.trimString(emailTextField?.text), password: CKGlobalMethods.trimString(passwordTextField?.text),responseHandler: loginCompletionHandler, errorHandler:
            {  error in
                self.loginNotSucceded()
                self.errorHandler(error)
        })
       // InboxStreamService.getStreamAppSession({ response in }, errorHandler: { error in })
    }
    
    
    //MARK: Service handler
    
    func loginCompletionHandler(_ responseData: SignupResponse){
        self.hideLoadingDialog()
        if(responseData.error == nil && responseData.token != nil){
            CKGlobalVariables.sharedManager.loggedInUserId = responseData.user?.id ?? responseData.id ?? ""
            if(responseData.user != nil && responseData.user?.bio != nil){
                saveLoginCredentialToPrefrences(responseData.token ?? "" , email: emailTextField?.text ?? "", password: passwordTextField?.text ?? "" , username: responseData.user?.bio?.name ?? "", avatar: responseData.user?.bio?.avatar ?? "", userId: CKGlobalVariables.sharedManager.loggedInUserId)
            }else{
                saveLoginCredentialToPrefrences(responseData.token ?? "" , email: emailTextField?.text ?? "", password: passwordTextField?.text ?? "" , username: "", avatar:"", userId: CKGlobalVariables.sharedManager.loggedInUserId)
            }
            self.getWalletAPI(responseHandler: { responseData in
                self.finishLoading()
              //  self.dismiss(animated: true, completion: nil)
            })
            
        }else{
            hideLoadingDialog()
            self.loginNotSucceded()
            if(responseData.error == nil){
                showErrorMessage(NSLocalizedString("login_valid_value", comment: ""))
            }else{
                showErrorMessage(responseData.error)
            }
        }
    }
    
  
   
    
    func loginNotSucceded(){
        self.loginNextBtn?.isHidden = false
        self.loginBtn?.isHidden = true
        self.loginBtn?.backgroundColor = Color.themeAnotherColor
        self.loginBtn?.startFinishAnimation(1, completion: { () -> () in })
    }
    
  
    
    func finishLoading(){
       // self.loginBtn?.startFinishAnimation(1, completion: { () -> () in
            //self.viewAfterAuthCalled(fromLogin: true)
            self.dismiss(animated: true, completion: { () -> () in
                self.delegate?.loginSignupSuccessDelegateMethod()
            })
        //})
    }
    
    
    @IBAction func test(_ sender: TKTransitionSubmitButton) {
        nextBtnClickedDelegateMethod()
    }
    
}

extension LoginViewController: TermsConditionViewControllerDelegate{
    
    func agreedTermsandConditionDelegateMethod() {
        self.signupServiceCalled()
    }
}


/*
extension UIColor {
    var gl:CAGradientLayer!
    
    func setGradientColor(){
        let colorTop =  UIColor.black.cgColor //UIColor(red: 192.0 / 255.0, green: 38.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor.white.cgColor //UIColor(red: 35.0 / 255.0, green: 2.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
    }
}*/

