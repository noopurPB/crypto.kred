//
//  AppDelegate.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import UIKit
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainStoryboard: UIStoryboard?
    var navigationC: UINavigationController?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        createMenuView()
        STPPaymentConfiguration.shared().publishableKey = CKConstants.publishableKey;
        
        return true
    }
    
    
    //MARK: Side Menu
    func createMenuView() {
        window = UIWindow(frame: UIScreen.main.bounds)
        mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        navigationC = mainStoryboard?.instantiateViewController(withIdentifier: "navigationController") as? UINavigationController
        
        let rightViewController = mainStoryboard?.instantiateViewController(withIdentifier: "SideMenuController") as? SideMenuController
        let containerViewController = SWRevealViewController(rearViewController: nil, frontViewController: navigationC)
        containerViewController?.rightViewController = rightViewController;
        
        containerViewController?.rearViewRevealWidth = UIScreen.main.bounds.width - 10
        containerViewController?.rightViewRevealWidth = UIScreen.main.bounds.width - 50
        
        window!.rootViewController = containerViewController
        window!.makeKeyAndVisible()
    }
    
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
   

}

