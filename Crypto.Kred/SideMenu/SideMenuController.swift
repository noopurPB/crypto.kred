//
//  SideMenuController.swift
//  Grab
//
//  Created by Noopur Virmani on 28/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//


import Foundation
import UIKit

protocol SideMenuControllerDelegate{
    func tapOnRowAtIndexpath(_ indexpath: NSInteger, searchText: String?)
}

class SideMenuController: CKBaseVC, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var delegate: SideMenuControllerDelegate?
    var menuList: [String] = [
                               NSLocalizedString("login", comment: ""),
                               NSLocalizedString("signup", comment: ""),
                               NSLocalizedString("buy_more", comment: ""),
                               "",
                               NSLocalizedString("create_a_coin", comment: ""),
                               NSLocalizedString("my_designs", comment: ""),
                               NSLocalizedString("claim_a_coin", comment: ""),
                               "",
                              // NSLocalizedString("give_a_coin", comment: ""),
                               NSLocalizedString("ck_explore", comment: ""),
                               NSLocalizedString("my_collection", comment: ""),
                               NSLocalizedString("marketplace", comment: ""),
                               NSLocalizedString("ck_newsfeeds", comment: ""),
                               "",
                               NSLocalizedString("ck_contacts", comment: ""),
                               NSLocalizedString("ck_transaction_history", comment: ""),
                               "",
                               NSLocalizedString("connect_my_eth_wallet", comment: ""),
                               NSLocalizedString("ck_outreach", comment: ""),
                               NSLocalizedString("export_ckr", comment: "")]
                              // NSLocalizedString("logout", comment: "")]
   
    @IBOutlet var menuTableView: UITableView!
    var unmintedBalance: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.sideMenuDarkColor
        if #available(iOS 11.0, *) {
            self.menuTableView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.getBalanceService();
        }
        
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    /* func methodOfReceivedNotification(notification: NSNotification){
     //Take Action on Notification
     // self.menuTableView .reloadData()
     }*/
    
    // #pragma mark - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(userIsLoggedIn() && (indexPath.row == 0 ||  indexPath.row == 1)){
            return 0;
        }else{
            return 30;
        }
     }
    
    /* func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 80.0
     }*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(userIsLoggedIn()){
            return menuList.count;
        }else{
            return 2;
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if(indexPath.row == 3 || indexPath.row == 7 || indexPath.row == 12 || indexPath.row == 15){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKLineTVCell") as! CKBaseTVCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.contentView.backgroundColor = Color.sideMenuDarkColor
            return cell
        }
        
        
        
        var identifier = "";
        
        if(indexPath.row == 2){
            identifier = "SideTableCountViewCell";
        }else{
            identifier = "SideTableViewCell";
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SideTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.contentView.backgroundColor = Color.sideMenuDarkColor
        cell.titleLabel.font = CKTheme.regularFont(14.0)
        cell.titleLabel.text = menuList[indexPath.row]
        cell.titleLabel.sizeToFit()
        if(indexPath.row == 2){
            let countTxt  = String(format: "%d %@", unmintedBalance ?? 0  ,  NSLocalizedString("kred_name", comment: ""), "")
            cell.countLabel.text = countTxt;
            cell.countLabel.sizeToFit();
            
            let frame: CGRect = cell.countLabel.frame;
            cell.widthConstraint.constant = frame.size.width+20
            
        }
            return cell
            
      //  }
        
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //dismissViewControllerAnimated(true, completion: nil)
        delegate?.tapOnRowAtIndexpath(indexPath.row, searchText: nil)
    }
    
    @IBAction func logoutBtnClicked(_ sender: UIButton) {
        delegate?.tapOnRowAtIndexpath(100, searchText: nil)
    }
    
    //MARK: Get Balance Service
    
    func getBalanceService(){
        
        func commonHandler(response: CKCoinsResponseObject){
            self.unmintedBalance = response.balance?.unminted_amount;
            CKGlobalVariables.sharedManager.walletBalance = self.unmintedBalance;
            self.menuTableView.reloadData()
        }
        
        
        let cks = CKGetService();
        
        var params = [String: Any]()
        params["wallet"] = CKGlobalVariables.sharedManager.walletId;
        params["currency"] = CKServiceConstants.COIN_CURRENCY;
        
        
        cks.getCoinBalance(params: params , responseHandler: {
            response in
            commonHandler(response: response);
            
        }, cacheHandler: { response in
            commonHandler(response: response);
            
        }, errorHandler: { error in })
        
    }
    
}

