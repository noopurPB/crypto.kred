//
//  SideTableViewCell.swift
//  Grab
//
//  Created by Noopur Virmani on 29/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

protocol SideTableViewCellDelegate{
    func crossBtnClickedDelegateMethod()
    func sideLogoutBtnClickedDelegateMethod()
    func topHeaderBtnClickedDelegateMethod(_ tag: Int)
}


class SideTableViewCell: CKBaseTVCell  {
    
    @IBOutlet weak var countLabel: UILabel!
    var delegate: SideTableViewCellDelegate?
   // @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        if(titleLabel != nil){
            titleLabel.textColor = UIColor.white
            titleLabel.font = Theme.regularFont(16.0)
        }
        
        if(countLabel != nil){
            countLabel.textColor = UIColor.white
            countLabel.font = Theme.regularFont(15.0)
            countLabel.clipsToBounds = true;
            countLabel.layer.cornerRadius = 12.5;
        }
    }
}
