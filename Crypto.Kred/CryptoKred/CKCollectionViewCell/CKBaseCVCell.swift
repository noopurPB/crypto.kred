//
//  CKBaseCVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CKBaseCVCellDelegate{
    @objc optional func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton)
}


class CKBaseCVCell: UICollectionViewCell{
    
    @IBOutlet weak var titleLabel: UILabel!
    var baseDelegate: CKBaseCVCellDelegate?
  
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var coinView: CoinView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func rightBtnClicked(_ sender: UIButton) {
        baseDelegate?.rightBtnClickedTVCellDelegateMethod!(sender)
    }
}
