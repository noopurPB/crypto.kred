//
//  CKHeaderCVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CKHeaderCVCell: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var thirdTitleLabel: UILabel!
    
    @IBOutlet weak var headerBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = CKColor.tableviewCellColor2
        if(titleLabel != nil){
            self.titleLabel.font =  CKTheme.mediumFont(15.0)
            self.titleLabel.textColor = CKColor.titleGreyColor
        }
    }
    
    
    @IBAction func headerClicked(_ sender: UIButton) {
        
    }
}
