//
//  CKCoinCVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import Font_Awesome_Swift

class CKCoinCVCell: CKBaseCVCell{
    
    @IBOutlet weak var leftArrowBtn: UIButton!
    @IBOutlet weak var rightArrowBtn: UIButton!
    @IBOutlet weak var coinNameLbl: UILabel!
    var coin: Coin?
    //@IBOutlet weak var coinValueBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    var isFront: Bool = true;
    
    @IBOutlet weak var timerLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var backCoinView: UIView!
    var coinViewSingle: CoinViewSingle?
    
    @IBOutlet weak var actionBtn: UIButton!
    
    @IBOutlet weak var actionBtnWidthConstraint: NSLayoutConstraint!
    var actionBtnFontSize: CGFloat = 15.0;
   
    override func awakeFromNib() {
        super.awakeFromNib()
        //addRightSwipeGestureToView()
        //addLeftSwipeGestureToView()
        if(coinNameLbl != nil){
            self.coinNameLbl.font = Theme.mediumFont(17.0);
            self.coinNameLbl.textColor = Color.domainRegGreyColor;
        }
       
        
        //let image = UIImage(named: "coin_like")?.withRenderingMode(.alwaysTemplate)
        //self.likeBtn.setImage(image, for: UIControlState.normal)
        //self.likeBtn.setImage(image, for: UIControlState.selected)
       // loadCoin(webView: coinWebView)
        
        if(timerLabel != nil){
            timerLabel.textColor = CKColor.titleGreyColor;
            timerLabel.font = CKTheme.mediumFont(12.0)
            timerLabel.clipsToBounds = true;
            timerLabel.layer.cornerRadius = 10.0;
            timerLabel.layer.borderColor = CKColor.dividerColor.cgColor;
            timerLabel.layer.borderWidth = 1.0;
        }
        
        if(actionBtn != nil){
            CKTheme.getCKThemeBorderButtonStyle(self.actionBtn, fontSizeL: actionBtnFontSize)  //getCKButtonStyle(self.actionBtn, fontSizeL: 15.0)
            
        }
        coinViewSingle = CoinViewSingle(frame: CGRect(x: 0, y: 0, width: backCoinView.frame.width-10, height: backCoinView.frame.height-10))
        self.backCoinView.addSubview(coinViewSingle!)
        
        if(rightArrowBtn != nil){
            self.rightArrowBtn.setFAIcon(icon: .FAAngleRight, iconSize: 20.0, forState: UIControlState.normal)
            self.rightArrowBtn.setFATitleColor(color: CKColor.titleGreyColor)
        }
        if(leftArrowBtn != nil){
            self.leftArrowBtn.setFAIcon(icon: .FAAngleLeft, iconSize: 20.0, forState: UIControlState.normal)
            self.leftArrowBtn.setFATitleColor(color: CKColor.titleGreyColor)
        }
        
        
    }
    
    @IBAction func actionBtnClicked(_ sender: Any) {
        print("action_clicked")
    }
    
    @IBAction func likeBtnClicked(_ sender: Any) {
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(actionBtn != nil){
            //CKTheme.getCKButtonStyle(self.actionBtn, fontSizeL: actionBtnFontSize)  //getCKButtonStyle(self.actionBtn, fontSizeL: 15.0)
        }
        rotateCoin(direction: "fromLeft")
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(actionBtn != nil){
           // CKTheme.getCKThemeBorderButtonStyle(self.actionBtn, fontSizeL: actionBtnFontSize)  //getCKButtonStyle(self.actionBtn, fontSizeL: 15.0)
        }
        rotateCoin(direction: "fromLeft")
        super.touchesEnded(touches, with: event)
    }
    
    
    func updateCoinView(coinL: Coin){
        if(coinL.type != nil && coinL.type == "auction"){
             self.timerLabel.isHidden = false;
             let date = CKGlobalMethods.convertDateFromString(dateL: coinL.auctionCreated ?? "")
            let timeleft = CKGlobalMethods.setTimeLeft(date: date);
            let timeFormatLeft = " " + timeleft + "";
            //self.timerLabel.text = timeleft;
            self.timerLabel.setFAText(prefixText: " ", icon: .FAClockO, postfixText: timeFormatLeft, size: 12)
            self.timerLabelWidthConstraint.constant = 100.0
            
        }else if(coinL.coins != nil && coinL.coins! > 1){
            self.timerLabel.isHidden = false;
            self.timerLabel.text = String(format: "%d COINS", coinL.coins ?? 0)
            self.timerLabelWidthConstraint.constant = 90.0
        }else{
            self.timerLabel.isHidden = true;
        }
        
        self.coin = coinL;
        self.coinNameLbl.text = self.coin?.coinName ?? ""
        
        
        let price = self.coin?.sale_price ?? self.coin?.auction_price;
        
        if(price != nil){
         //   self.coinValueBtn.setTitle(String(format: "%@ %@", price ?? "0", NSLocalizedString("kred_name", comment: "")), for: UIControlState.normal)
        }else{
          //  self.coinValueBtn.setTitle(String(format: "%.2f %@", self.coin?.value ?? 0, NSLocalizedString("kred_name", comment: "")), for: UIControlState.normal)
        }
        
        
        if(self.coin?.likes != nil && self.coin?.likes != 0){
            //self.likeBtn.setTitle(String(format: "%d", self.coin?.likes ?? 0), for: UIControlState.normal)
            self.likeBtn.setFAText(prefixText: "", icon: .FAHeart, postfixText: String(format: " %d", self.coin?.likes ?? 0), size: 15.0, forState: UIControlState.normal)
        }else{
            
            self.likeBtn.setFAText(prefixText: "", icon: .FAHeart, postfixText: "", size: 15.0, forState: UIControlState.normal)
            //self.likeBtn.setTitle("", for: UIControlState.normal)
        }
        
        
        if(self.coin?.liked != nil && self.coin?.liked == true){
            //self.likeBtn.tintColor = CKColor.themePinkColor
            self.likeBtn.setFATitleColor(color: CKColor.themePinkColor)
        }else{
            //self.likeBtn.tintColor = CKColor.titleGreyColor
             self.likeBtn.setFATitleColor(color: CKColor.titleGreyColor)
        }
        self.loadCoin(coin: coinL)
        //self.loadCoin(webView: webView, coinL: coinL)
    }
    
   
    
    
    
    func loadCoin(coin: Coin){
        coinViewSingle?.coin = coin;
        coinViewSingle?.showFrontCoin = isFront;
        coinViewSingle?.loadCoin(webView: coinViewSingle!.webView!)
    }
   
    
    //MARK: Tap gesture
    func addRightSwipeGestureToView(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(CKCoinCVCell.respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.addGestureRecognizer(swipeRight)
    }
    
    func addLeftSwipeGestureToView(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(CKCoinCVCell.respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.addGestureRecognizer(swipeRight)
    }
    
  
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                rotateCoin(direction: "fromLeft")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                rotateCoin(direction: "fromRight")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    func rotateCoin(direction: String){
        self.isFront = !self.isFront;
        self.loadCoin(coin: self.coin!)
        self.backCoinView.rotate360Degrees(direction: direction)
        
    }
    
    
    @IBAction func rightArrowBtnClicked(_ sender: UIButton) {
    }
    @IBAction func leftArrowBtnClicked(_ sender: UIButton) {
    }
    
}


extension UIView {
    func rotate360Degrees(direction: String) {
        let transition: CATransition = CATransition()
        transition.startProgress = 0;
        transition.endProgress = 1.0;
        transition.type = "flip"
        transition.subtype = direction
        transition.duration = 0.5
        transition.repeatCount = 1;
        self.layer.add(transition, forKey: "transition")
    }
    
}





