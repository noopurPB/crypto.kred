//
//  CKLoadMoreCVCell.swift.swift
//  Grab
//
//  Created by Noopur Virmani on 11/05/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CKLoadMoreCVCell: CKBaseCVCell {
    
    @IBOutlet var loadmoreLabel: UILabel!
    @IBOutlet var loadMoreIndicator: UIActivityIndicatorView!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if(loadMoreIndicator != nil){
            loadMoreIndicator.color = Color.themeDarkColor
            //loadMoreIndicator.hidden = true
        }
        
        if(loadmoreLabel != nil){
            loadmoreLabel.isHidden = true
        }
        
    }
    
    
}
