//
//  CKBalance.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class CKBalance: NSObject ,Mappable {
   /*
    amount = 100;
    coins = 100;
    created = "2018-02-14T04:01:17.000000";
    currency = 1;
    minted = 15;
    "minted_amount" = 15;
    "minted_coins" = 15;
    unminted = 85;
    "unminted_amount" = 85;
    "unminted_balances" = 89;
    updated = "2018-02-14T04:05:23.000000";
    wallet = 90;*/
    
    var unminted_balances: Int?
    var unminted_amount: Int?
   
    override init() {
        super.init()
        
    }
    
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        self.unminted_balances <- map["unminted_balances"]
        self.unminted_amount <- map["unminted_amount"]
    }
    
}


class CKInvoice: Mappable{
   
    var invoice: Int?
    var currency: Int?
    var uuid: String?
    var payment_method: String?
    var payment_amount: Float?
    var amount: Float?
    var payment_currency: String?
  
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        self.invoice <- map["invoice"]
        self.uuid <- map["uuid"]
        self.payment_method <- map["payment_method"]
        self.payment_amount <- map["payment_amount"]
        self.amount <- map["amount"]
        self.currency <- map["currency"]
        self.payment_currency <- map["payment_currency"]
        
    }
    
}

