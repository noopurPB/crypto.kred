//
//  CKLargeSize.swift
// 
//
//  Created by Noopur Virmani on 26/05/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKLargeSize: Mappable{
    
    var h:  CGFloat?
    var w:  CGFloat?
    var resize: String?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.h <- map["h"]
        self.w <- map["w"]
        self.resize <- map["resize"]
    }
}
