//
//  Wallet.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class Wallet: NSObject ,Mappable {
    
    var wallet: Int?
    
    override init() {
        super.init()
        
    }
    
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        self.wallet <- map["wallet"]
      
        
    }
    
}
