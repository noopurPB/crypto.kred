//
//  CKMessage.swift
// 
//
//  Created by Noopur Virmani on 21/03/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation

import ObjectMapper

class CKMessage: NSObject , Mappable{
    
    var status: String?
    var text: String?
    var ago: String?
    var user: GrabUser?
    var id: String?
    var time: Double?
    var tags = [String]()
    var media = [CKMedia]()
    var mediaStr: String?
    var type: String?
    var data: CKData?
    var pinned: Bool?
    var screen_name: String?
    var liked: Bool?
    var likes: Int?
    
    var source: String?
    var face: GrabUser?
    var parent: CKMessage?
  //var analytics: Analytics?
    var _cls : String?
    var comments = [CKMessage]()
  //var grabs = [Grab]()
  // var recent = [Recent]()
    var created: Double?
    var commentsCount: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self._cls <- map["_cls"]
        
        self.comments <- map["comments"]
        
        self.commentsCount <- map["comments"]
    
        
        
        if(map["ftext"].isKeyPresent){
            self.text <- map["ftext"]
        }else{
            self.text <- map["text"]
        }
        
        self.ago <- map["ago"]
        
        self.user <- map["user"]
        
        self.face <- map["face"]
        
        /*if(self.user == nil && self.face != nil){
            self.user = self.face
        }*/
        
        
        self.id <- map["id"]
        
        if(self.id?.contains("$oid: ") == true){
            let replaced = id?.replacingOccurrences(of: "\n", with: "")
            self.id = replaced!.replacingOccurrences(of: "$oid: ", with: "")
        }
        
        self.time <- map["time"]
        
        self.tags <- map["tags"]
        
        self.media <- map["media"]
        
        self.mediaStr <- map["media"]
        
        self.type <- map["type"]
          
        self.data <- map["data"]
        
        self.pinned <- map["pinned"]
        
        self.screen_name <- map["screen_name"]
        
        self.liked <- map["liked"]
        
        self.likes <- map["likes"]
        
        self.source <- map["source"]
        
        self.parent <- map["parent"]
        
        //self.analytics <- map["analytics"]
        
        //self.grabs <- map["grabs"]
        
        //self.recent <- map["recent"]
        
        self.created <- map["created"]
        
    }
    
    
    init(cls: String?){
        self._cls = cls
    }
    
    
    init(message: CKMessage,  liked: Bool){
        
        self._cls = message._cls
        
        self.comments = message.comments
        
        self.text = message.text
        
        self.ago = message.ago
        
       // self.user = message.user
        
       // self.face = message.face
        
        self.id = message.id
        
        self.time = message.time
        
        self.tags = message.tags
        
        self.media = message.media
        
        self.type = message.type
        
        self.data = message.data
        
        self.pinned = message.pinned
        
        self.screen_name = message.screen_name
        
        self.liked = message.liked
        
        self.likes = message.likes;
        
        self.source = message.source
        
        self.parent = message.parent
        
       // self.analytics = message.analytics
        
        self.created = message.created
    }

    
}


