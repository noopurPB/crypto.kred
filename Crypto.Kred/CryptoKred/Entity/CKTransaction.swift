//
//  CKTransaction.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 03/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKTransaction: Mappable{
    
    var after: CKAfter?
    var descriptionL: String?
    var balance_adj: Int?
    var collection_adj: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.after <- map["after"]
        self.descriptionL <- map["description"]
        self.balance_adj <- map["balance_adj"]
        self.collection_adj <- map["collection_adj"]
    }
    
}

class CKAfter: Mappable{
    
    var balance: CGFloat?
    var collection: CGFloat?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.collection <- map["collection"]
        self.balance <- map["balance"]
    }
    
}
