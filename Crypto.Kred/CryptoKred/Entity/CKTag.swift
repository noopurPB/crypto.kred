//
//  CKTag.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 03/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKTag: Mappable{
    
    var count: Int?
    var tag: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.count <- map["count"]
        self.tag <- map["tag"]
    }
    
}
