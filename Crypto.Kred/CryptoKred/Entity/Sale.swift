//
//  Sale.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 23/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class Sale: NSObject ,Mappable {
    
    var buyer: Int?
    var coin: Coin?
    var price: CGFloat?
    var user: String?
    var type: String?
    
    var max_price: CGFloat?
    var min_price: CGFloat?
    
    var maximum: Int?  //maximum price
    var minimum: Int? //minimum price
    var start: String? //The start time for the auction.
    var end: String? //The end time for the auction.
    
    override init() {
        super.init()
        
    }
    
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        self.buyer <- map["buyer"]
        self.coin  <- map["coin"]
        self.price <- map["price"]
       // let priceL = map["price"]
        self.type <- map["type"]
        self.user <- map["user"]
        
        self.start <- map["start"];
        self.end <- map["end"];
        
        self.maximum <- map["maximum"];
        self.minimum <- map["minimum"];
        
        self.max_price <- map["max_price"]
        self.min_price <- map["min_price"]
    }
    
}
