//
//  CKLimits.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 16/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKLimits: Mappable{
    var allowed : CKAllowed?
    var discount: CKDiscount?
    var kyc: CKKyc?
    var owned : [String: Any]?
    var purchased: [String: Any]?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        self.allowed <- map["allowed"]
        self.discount <- map["discount"]
        self.kyc <- map["kyc"]
        self.owned <- map["owned"]
        self.purchased <- map["purchased"]
    }
    
}

class CKDiscount: Mappable{
    var code: String?
    var expires: Double?
    var percent: Int?
    var reason: String?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        self.code <- map["code"]
        self.expires <- map["expires"]
        self.percent <- map["percent"]
        self.reason <- map["reason"]
    }
    
}

class CKKyc: Mappable{
    var accept_crypto: Bool?
    var purchase_limit: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.accept_crypto <- map["accept_crypto"]
        self.purchase_limit <- map["purchase_limit"]
        
        
    }
    
}


class CKAllowed: Mappable{
    var daily: [String: Any]?
    var maximum: [String: Any]?
    var today: [String: Any]?
    var total: [String: Any]?
   
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.daily <- map["daily"]
        self.maximum <- map["maximum"]
        self.today <- map["today"]
        self.total <- map["total"]
    }
    
}

