//
//  Coin.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 25/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class Coin: NSObject ,Mappable {
    
    var coin: Int?
   
    var coinName: String?
    var issuerName: String?
    
    var front: String?
    
    var backImage: UIImage?
    var back: String?
    
    var value: CGFloat?
    var valueStr: Double?
    var count: Int?
    
    var coins: Int?
    var batch: Int?
    
    var coinColor: String?
    
    var circulation: Int?
    
    var likes: Int?
    
    var liked: Bool?
    
    var created: String?
    var auctionCreated: String?
    
    var grab: String?
    var user: String?
    var creator: String?
    
    var sale_price: String?
    var auction_price: String?
    
    var history: CKHistory?
    
    var pattern: String?
    
    var pattern_color: String?
    var animation: String?
    
    var privateCoin: Bool?
    var nsfw: Bool?
    var mesh: Bool?
    
    var code: String?
    var sequence: Int?
    var symbol: String?
    
    var text_color: String?
    var type: String?
    
    var max_price: CGFloat?
    var min_price: CGFloat?
    var coinRequested: Bool?
    
    override init() {
        super.init()
    }

    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        self.coin <- map["coin"]
        self.back <- map["back"]
        self.front <- map["face"]
        self.coinColor <- map["color"]
        if(self.coinColor != nil && !(self.coinColor?.contains("#"))!){
            self.coinColor = "#" + self.coinColor!;
        }
        self.coinName <- map["name"]
        
        self.value <- map["value"]
        self.valueStr <- map["value"]
        
        /*if(self.value == nil && self.valueStr != nil){
            
            let x = Double(self.valueStr!).rounded(toPlaces: 2)
            self.value = Float(x);
            
            //self.value = (self.valueStr! as NSString).floatValue
        }*/
        
        self.count <- map["count"]
        self.coins <- map["coins"]
        self.batch <- map["batch"]
        
        self.circulation <- map["circulation"]
        self.likes <- map["likes"]
        self.liked <- map["liked"]
        
        self.issuerName <- map["domain"]
        self.grab <- map["grab"]
        self.user <- map["user"]
        self.creator <- map["creator"]
        
        self.sale_price <- map["sale_price"]
        self.history <- map["history"]
        
        self.created <- map["created"]
        self.pattern <- map["pattern"]
        self.pattern_color <- map["pattern_color"]
        self.nsfw <- map["nsfw"]
        self.privateCoin <- map["private"]
        self.mesh <- map["mesh"]
        self.animation <- map["animation"]
        
        self.code <- map["code"]
        self.sequence <- map["sequence"]
        self.symbol  <- map["symbol"]
        
        self.text_color <- map["text_color"]
        self.type <- map["type"]
        
        self.sequence <- map["sequence"]
        
        self.max_price <- map["max_price"]
        self.min_price <- map["min_price"]
        
    }
    
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

