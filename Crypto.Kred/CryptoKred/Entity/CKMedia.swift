//
//  CKMedia.swift
//  
//
//  Created by Noopur Virmani on 04/05/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKMedia: Mappable{
    
    var type:  String?
    
    var url: String?
    
    var sizes: CKSizes?
    
    var expanded_url: String?
    
    var media_url: String?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self.type <- map["type"]
        
        self.url <- map["url"]
        
        self.expanded_url <- map["expanded_url"]
        
        self.sizes <- map["sizes"]
        
        self.media_url <- map["media_url"]
        
        
    }
    
}
