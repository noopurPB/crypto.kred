//
//  CKAuction.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class CKAuction: NSObject ,Mappable {
    
    var maximum: Int?  //maximum price
    var minimum: Int? //minimum price
    var start: String? //The start time for the auction.
    var end: String? //The end time for the auction.
    var auctionDay: String?
    
    var coin: Coin?
    var auction: Int?
    var price: Float?
    var user: String?
    var created: String?
   
    
    override init() {
        super.init()
        
    }
    
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        self.auction <- map["auction"];
        self.coin <- map["coin"];
        self.price <- map["price"];
        self.user <- map["user"];
        self.created <- map["created"];
        self.start <- map["start"];
        self.end <- map["end"];
        
    }
    
}
