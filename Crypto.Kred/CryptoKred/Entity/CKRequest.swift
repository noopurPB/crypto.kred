//
//  CKRequest.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 30/06/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKRequest: Mappable{
    
    var coin:  Coin?
    var status: String?
    var batch: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.status <- map["status"]
        self.coin <- map["coin"]
        self.batch <- map["batch"]
    }
    
}
