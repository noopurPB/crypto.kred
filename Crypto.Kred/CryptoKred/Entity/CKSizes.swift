//
//  CKSizes.swift
// 
//
//  Created by Noopur Virmani on 26/05/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKSizes: Mappable{
    
    var large:  CKLargeSize?
    var medium: CKLargeSize?
    var small:  CKLargeSize?
    var thumb:  CKLargeSize?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self.large <- map["large"]
        self.medium <- map["medium"]
        self.small <- map["small"]
        self.thumb <- map["thumb"]
    
    }
}
