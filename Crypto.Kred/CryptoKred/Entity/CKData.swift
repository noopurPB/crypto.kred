//
//  CKData.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 10/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKData: Mappable{
    
    var type:  String?
    
    var url: String?
    
    var sizes: CKSizes?
    
    var expanded_url: String?
    
    var media_url: String?
    
    var coin: Coin?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self.type <- map["type"]
        
        self.url <- map["url"]
        
        self.expanded_url <- map["expanded_url"]
        
        self.sizes <- map["sizes"]
        
        self.media_url <- map["media_url"]
        
        self.coin <- map["coin"]
        
    }
    
}
