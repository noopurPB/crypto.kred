//
//  CKHistory.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 14/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKHistory: NSObject ,Mappable {
    
    var avatar: String?
    var name: String?
    var price: Int?
    var user: String?
    var action: String?
    
    override init() {
        super.init()
        
    }
    
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        self.avatar <- map["avatar"];
        self.name  <- map["name"];
        self.price <- map["price"]
        self.user <- map["user"]
        self.action <- map["action"]  //hold - pending
        //give - request , null
    }
    
}
