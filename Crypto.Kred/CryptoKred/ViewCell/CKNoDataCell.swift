//
//  CKNoDataCell.swift
//  Grab
//
//  Created by Noopur Virmani on 05/02/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit;


@objc protocol CKNoDataCellDelegate{
    @objc optional func noCoinBtnClickedDelegateMethod(_ sender: UIButton)
}


class CKNoDataCell: UIView{

    @IBOutlet weak var noCoinImageBtn: UIButton!
    @IBOutlet weak var noCoinTitle1: UILabel!
    @IBOutlet weak var noCoinTitle2: UILabel!
    @IBOutlet weak var noCoinBtn: UIButton!
    @IBOutlet weak var btnWidthConstraint: NSLayoutConstraint!
    
    var delegate: CKNoDataCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.backgroundColor = UIColor.gray;
        
        if(noCoinTitle1 != nil){
            noCoinTitle1.textColor = CKColor.titleGreyColor
            noCoinTitle1.font = CKTheme.mediumFont(20.0)
        }
        
        if(noCoinTitle2 != nil){
            noCoinTitle2.textColor = CKColor.titleGreyColor
            noCoinTitle2.font = CKTheme.mediumFont(14.0)
        }
        
        if(noCoinBtn != nil){
            CKTheme.getCKButtonStyle(self.noCoinBtn, fontSizeL: 15.0)
        }
        
        
    }
    @IBAction func noCoinBtnClicked(_ sender: UIButton) {
        delegate?.noCoinBtnClickedDelegateMethod!(sender)
    }
}
