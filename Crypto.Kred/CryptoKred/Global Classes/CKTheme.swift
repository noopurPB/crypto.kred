//
//  CKTheme.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 09/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKTheme {
    
    static let ALERT_VIEW_FILTER_RADIUS: CGFloat = 6.0;// seconds
    
    static let BUTTON_CORNER_RADIUS: CGFloat = 20.0;// seconds
    
    
    static let CELL_TITLE_LABEL_FONT: CGFloat = 17.0;// seconds
    
    
    class func mediumFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Medium", size: size)!
    }
    
    class func boldFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Bold", size: size)!
    }
    
    class func lightFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Light", size: size)!
    }
    
    class func thinFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Thin", size: size)!
    }
    
    class func regularFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue", size: size)!
    }
    
    class func awesomeFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "FontAwesome", size: size)!
    }
    
    class func getCKLightButtonStyle(_ button: UIButton, fontSizeL: CGFloat?){
        button.clipsToBounds = true
        button.layoutIfNeeded()
        button.layer.borderWidth = 1.0
        button.layer.borderColor = CKColor.darkGrayColor.cgColor;
        button.layer.cornerRadius =  button.frame.height/2 //Theme.BUTTON_CORNER_RADIUS
        button.backgroundColor = UIColor.white
        button.setTitleColor(CKColor.darkGrayColor, for: UIControlState())
        let fontSize = button.frame.height/2
        button.titleLabel?.font = Theme.mediumFont(fontSizeL ?? fontSize)
    }
    
    class func getCKThemeBorderButtonStyle(_ button: UIButton, fontSizeL: CGFloat?){
        button.clipsToBounds = true
        button.layoutIfNeeded()
        button.layer.borderWidth = 1.0
        button.layer.borderColor = CKColor.themeAnotherColor.cgColor;
        button.layer.cornerRadius =  button.frame.height/2 //Theme.BUTTON_CORNER_RADIUS
        button.backgroundColor = UIColor.white
        button.setTitleColor(CKColor.themeAnotherColor, for: UIControlState())
        let fontSize = button.frame.height/2
        button.titleLabel?.font = Theme.mediumFont(fontSizeL ?? fontSize)
    }
    
 
    
    
    class func getCKButtonStyle(_ button: UIButton, fontSizeL: CGFloat?){
        button.clipsToBounds = true
        button.layoutIfNeeded()
        button.layer.cornerRadius =  button.frame.height/2 //Theme.BUTTON_CORNER_RADIUS
        button.backgroundColor = CKColor.themeAnotherColor;
        button.setTitleColor(UIColor.white, for: UIControlState())
        let fontSize = button.frame.height/2
        button.titleLabel?.font = Theme.mediumFont(fontSizeL ?? fontSize)
    }
    
    class func setTextFieldStyle(_ textField: UITextField, placeholder: String){
        textField.font = regularFont(16.0)
        textField.textColor = CKColor.textFieldColor;
        textField.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                             attributes:[NSAttributedStringKey.foregroundColor: CKColor.textFieldPlaceholderColor])
    }
    
    class func setTextFieldErrorStyle(_ textField: UITextField?){
        if(textField != nil){
            textField!.font = regularFont(16.0)
            textField!.attributedPlaceholder = NSAttributedString(string: textField!.placeholder!,
                                                                  attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
        }
    }
    
    class func setTextViewStyle(_ textView: UITextView, placeholder: UILabel){
        textView.font = regularFont(16.0)
        placeholder.textColor = CKColor.textFieldPlaceholderColor
        placeholder.font = regularFont(16.0)
    }
    
    
    class func enableOrDisableButton(button: UIButton, enable: Bool){
        if(enable == true){
            button.alpha = 1.0
            button.isUserInteractionEnabled = true
        }else{
            button.alpha = 0.6
            button.isUserInteractionEnabled = false
        }
    }
    

    
}
