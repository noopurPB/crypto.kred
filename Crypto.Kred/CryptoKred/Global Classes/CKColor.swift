//
//  CKColor.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 09/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

struct CKColor {
    
    static let themeDarkColor:UIColor = UIColor.black
    
    static let defaultCoinColor: String = "FAB832"
    
    static let titleDark :UIColor = UIColor(red: (147 / 255.0), green: (133 / 255.0), blue: (184 / 255.0), alpha: 1.0)
    
    static let barTintColor :UIColor = UIColor.gray; 
    
    static let tableviewCellColor2 :UIColor = UIColor(red: (247 / 255.0), green: (247 / 255.0), blue: (247 / 255.0), alpha: 1.0)
    
    static let tableviewCellColor1 :UIColor = UIColor.white
    
    static let titleGreyColor:UIColor = UIColor.gray; //UIColor(red: (202 / 255.0), green: (202 / 255.0), blue: (202 / 255.0), alpha: 1.0)
    
    static let themeAnotherColor:UIColor = UIColor(red: (55 / 255.0), green: (126 / 255.0), blue: (247 / 255.0), alpha: 1.0)
    
    static let themePinkColor: UIColor = UIColor(red: (224 / 255.0), green: (36 / 255.0), blue: (94 / 255.0), alpha: 1.0)
    
    static let dividerColor:UIColor = UIColor(red: (225 / 255.0), green: (225 / 255.0), blue: (225 / 255.0), alpha: 1.0)
    
    static let textFieldColor:UIColor = UIColor(red: (78 / 255.0), green: (78 / 255.0), blue: (78 / 255.0), alpha: 1.0)
    
    static let textFieldPlaceholderColor:UIColor = UIColor(red: (142 / 255.0), green: (142 / 255.0), blue: (147 / 255.0), alpha: 1.0)
    
    static let placeholderColor:UIColor = UIColor(red: (241 / 255.0), green: (241 / 255.0), blue: (241 / 255.0), alpha: 1.0)
    
    static let verifiedGreen:UIColor = UIColor(red: (0 / 255.0), green: (175 / 255.0), blue: (59 / 255.0), alpha: 1.0)
    
    static let darkGrayColor: UIColor = Color.darkGreyColor;//UIColor(red: (142 / 255.0), green: (142 / 255.0), blue: (147 / 255.0), alpha: 1.0)
    
    ///0 175 59
    
    /*static let coinDefaultColor:UIColor = UIColor(red: (250 / 255.0), green: (184 / 255.0), blue: (50 / 255.0), alpha: 1.0)
    
    static let themeDarkColor:UIColor = UIColor.black
    
    
    
    static let themeColorLight :UIColor = UIColor(red: (147 / 255.0), green: (133 / 255.0), blue: (184 / 255.0), alpha: 1.0)
    
    static let indicatorColor: UIColor = UIColor.gray*/

}
