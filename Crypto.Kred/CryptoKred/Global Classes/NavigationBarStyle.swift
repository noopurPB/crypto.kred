//
//  NavigationBarStyle.swift
//  Grab
//
//  Created by Noopur Virmani on 21/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol NavigationBarStyleDelegate{
    @objc optional func backBtnClickedDelegateMethod()
    @objc optional func searchIconClickedDelegateMethod()
    @objc optional func titleBarClickedDelegateMethod()
    @objc optional func nextBtnClickedDelegateMethod()
    @objc optional func userProfileClickedDelegateMethod()
    @objc optional func tickBtnClickedDelegateMethod()
    @objc optional func settingBtnClickedDelegateMethod()
    @objc optional func filterBtnClickedDelegateMethod()
    @objc optional func grabIconBtnClickedDelegateMethod()
    @objc optional func userIconBtnClickedDelegateMethod()
 }


class NavigationBarStyle {

    var delegate: NavigationBarStyleDelegate?
    
    func setNavBarStyle (_ vc: UIViewController){
        vc.navigationController?.navigationBar.barTintColor = UIColor.white//Color.themeDarkColor
        vc.navigationController?.navigationBar.tintColor = CKColor.titleGreyColor
        vc.navigationController?.navigationBar.isTranslucent = true
        vc.navigationItem.setHidesBackButton(true, animated:false);
       //vc.navigationController?.isNavigationBarHidden = false
        //vc.navigationController?.navigationBar.alpha = 1.0;
       // UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        UINavigationBar.appearance().backgroundColor = UIColor.white//CKColor.themeDarkColor
        //vc.navigationController?.setSecondaryColor(Color.pinkColor)
    }
    
    func showNavigationTitle(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = nil;
        vc.navigationItem.leftBarButtonItem = nil;
    }
    
    func showNavigationSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
    }
    
    func showNavigationGrabIconAndSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationGrabIconBarItem(vc)
    }
    
    func showNavigationSettingAndSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationSettingBarItem(vc)
        
       // let segmentControl = setNavigationSegmentItem(vc)
       // vc.navigationController?.navigationBar.addSubview(segmentControl)
        //return segmentControl
    }
    
    
    func showNavigationFilterAndSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationFilterBarItem(vc)
        
        // let segmentControl = setNavigationSegmentItem(vc)
        // vc.navigationController?.navigationBar.addSubview(segmentControl)
        //return segmentControl
    }
    
    func showNavigationRightSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
    }
    
    func showNavigationSearchIconAndSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationSearchBarItem(vc)
    }

    
    func showNavigationSearchIconAndUserBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationUserBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationSearchBarItem(vc)
    }
    
    
    
    func showNavigationLeftBackBarItemAndSideMenuBarItem(_ vc: UIViewController,navTitle : String){
        if(navTitle != ""){
            setNavigationTitle(vc, navTitle: navTitle)
        }
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSidemenuBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationBackArrowBarItem(vc)
    }

    
    
    func showNavigationLeftBackBarItemAndUserProfileItem(_ vc: UIViewController,navTitle : String){
        if(navTitle != ""){
            setNavigationTitle(vc, navTitle: navTitle)
        }
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getUserProfileBarItem(vc)
        vc.navigationItem.leftBarButtonItem = getNavigationBackArrowBarItem(vc)
    }

    func showNavigationLeftBackBarItemAndSettingItem(_ vc: UIViewController,navTitle : String){
        if(navTitle != ""){
            setNavigationTitle(vc, navTitle: navTitle)
        }
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationSettingBarItem(vc)
        vc.navigationItem.leftBarButtonItem = self.getNavigationBackArrowBarItem(vc)
    }
    

    
    func showNavigationBackBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationItem.rightBarButtonItem = nil
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.leftBarButtonItem = getNavigationBackArrowBarItem(vc)
    }
    
    
    func showNavigationLeftTitleItem(_ vc: UIViewController,navTitle : String, barItemTitle: String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.leftBarButtonItem = getNavigationTitleBarItem(vc, buttonTitle: barItemTitle)
    }
    
    func showNavigationRightTitleItem(_ vc: UIViewController,navTitle : String, barItemTitle: String)
    {
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.rightBarButtonItem = getNavigationTitleBarItem(vc, buttonTitle: barItemTitle)
    }

    
    func showNavigationLeftBackBarItemandRightTitleBarItem(_ vc: UIViewController,navTitle : String, barItemTitle: String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.navigationBar.isHidden = false
        vc.navigationItem.leftBarButtonItem = getNavigationBackArrowBarItem(vc)
        vc.navigationItem.rightBarButtonItem = getNavigationTitleBarItem(vc, buttonTitle: barItemTitle)
    }
    
   func showNavigationLeftBackBarItemandRightNextBarItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.leftBarButtonItem = getNavigationBackArrowBarItem(vc)
        vc.navigationItem.rightBarButtonItem = getNavigationNextArrowBarItem(vc)
    }
    
   func showNavigationLeftTitleBarItemandBackBarItem(_ vc: UIViewController,navTitle : String, barItemTitle: String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.leftBarButtonItem = getNavigationTitleBarItem(vc, buttonTitle:barItemTitle)
        vc.navigationItem.rightBarButtonItem = getNavigationNextArrowBarItem(vc)
    }
    
     func showNavigationLeftBackBarItemandRightTickItem(_ vc: UIViewController,navTitle : String){
        setNavigationTitle(vc, navTitle: navTitle)
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.leftBarButtonItem = getNavigationBackArrowBarItem(vc)
        vc.navigationItem.rightBarButtonItem = getNavigationTickBarItem(vc)
    }

     func setNavigationTitle(_ vc: UIViewController,navTitle: String){
        vc.navigationItem.title = navTitle
        vc.navigationController?.navigationBar.alpha = 1.0
        vc.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: CKColor.barTintColor,
             NSAttributedStringKey.font: CKTheme.regularFont(20.0)]
    }
    
    
     func getTitleLabel(_ vc: UIViewController,navTitle: String)  -> UILabel{
            let navigationBar = vc.navigationController?.navigationBar
            let width = navigationBar!.frame.width - 100
            let firstFrame = CGRect(x: 50, y: 0, width: width, height: 30)
            let firstLabel = UILabel(frame: firstFrame)
            firstLabel.text = navTitle
            Theme.navigationTitleStyle(firstLabel)
            return firstLabel
    }
    
    func getSubTitleLabel(_ vc: UIViewController,navTitle: String) -> UILabel{
         let navigationBar = vc.navigationController?.navigationBar
         let width = navigationBar!.frame.width - 100
         let secondLabel = UILabel(frame: CGRect(x: 50, y: 24, width: width, height: 17))
         secondLabel.textAlignment = NSTextAlignment.center
         secondLabel.textColor = UIColor.white
         secondLabel.text = navTitle
         secondLabel.font = Theme.mediumFont(13.0)
         return secondLabel
    }
    
    
     func getNavigationSidemenuBarItem(_ vc: UIViewController) -> UIBarButtonItem{
        let menuBarButton = UIBarButtonItem(customView: getSidemenuBarButton(vc))
        return menuBarButton
    }
    
    
     func getSidemenuBarButton(_ vc: UIViewController) -> UIButton{
        let menuImage = UIImage(named: "menu.png")?.withRenderingMode(.alwaysTemplate)
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setImage(menuImage, for: UIControlState())
        menuButton.frame = CGRect(x: 0, y: 0,width: (menuImage?.size.width)!+10, height: (menuImage?.size.height)!)
        menuButton.addTarget(vc.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: UIControlEvents.touchUpInside)
        menuButton.tintColor = CKColor.barTintColor;
       // vc.view.addGestureRecognizer(vc.revealViewController().panGestureRecognizer())
        return menuButton
    }

    
    func getNavigationUserBarItem(_ vc: UIViewController) -> UIBarButtonItem{
       
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setFAIcon(icon: .FAUser, iconSize: 20.0, forState: UIControlState.normal)
        menuButton.frame = CGRect(x: 0, y: 0,width: 20.0, height: 20.0)
        menuButton.addTarget(self, action: #selector(NavigationBarStyle.userIconBtnClickedCalled), for: UIControlEvents.touchUpInside)
        menuButton.setFATitleColor(color: CKColor.barTintColor)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        return menuBarButton
    }
    
    
    
     func getNavigationGrabIconBarItem(_ vc: UIViewController) -> UIBarButtonItem{
        let menuImage = UIImage(named: "grab_medium_logo.png")
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setImage(menuImage, for: UIControlState())
        menuButton.frame = CGRect(x: 0, y: 0,width: (menuImage?.size.width)!, height: (menuImage?.size.height)!)
        menuButton.addTarget(self, action: #selector(NavigationBarStyle.grabIconBtnClickedCalled), for: UIControlEvents.touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        return menuBarButton
    }
    
    
     func getNavigationSettingBarItem(_ vc: UIViewController) -> UIBarButtonItem{
        let menuImage = UIImage(named: "grab_setting.png")
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setImage(menuImage, for: UIControlState())
      //  menuButton.frame = CGRect(x: 0, y: 0, width: (menuImage?.size.width)!, height: (menuImage?.size.height)!)
        menuButton.addTarget(self, action: #selector(NavigationBarStyle.settingBtnClickedCalled) , for: UIControlEvents.touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        return menuBarButton
    }
    

    func getNavigationFilterBarItem(_ vc: UIViewController) -> UIBarButtonItem{
        let menuImage = UIImage(named: "ck_sort_icon")?.withRenderingMode(.alwaysTemplate)
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setImage(menuImage, for: UIControlState())
        menuButton.frame = CGRect(x: 0, y: 0, width: (menuImage?.size.width)!, height: (menuImage?.size.height)!)
        menuButton.setTitle("", for: UIControlState.normal) //sort-amount-down
        menuButton.titleLabel?.font = Theme.regularFont(11.0)
        menuButton.addTarget(self, action: #selector(NavigationBarStyle.filterBtnClickedCalled) , for: UIControlEvents.touchUpInside)
        menuButton.tintColor = CKColor.barTintColor;
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        return menuBarButton
    }
    
    
     func getNavigationTickBarItem(_ vc: UIViewController) -> UIBarButtonItem{
        let menuImage = UIImage(named: "white_tick.png")
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setImage(menuImage, for: UIControlState())
      //  menuButton.frame = CGRect(x: 0, y: 0,width: (menuImage?.size.width)!+10, height: (menuImage?.size.height)!)
        menuButton.addTarget(self, action: #selector(NavigationBarStyle.tickBtnClickedCalled), for: UIControlEvents.touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        return menuBarButton
    }
    
    
     func getUserProfileBarItem(_ vc: UIViewController) -> UIBarButtonItem{
        let menuImage = UIImage(named: "user_profile_icon.png")
        let menuButton = UIButton(type:UIButtonType.custom) as UIButton
        menuButton.setImage(menuImage, for: UIControlState())
        menuButton.frame = CGRect(x: 0, y: 0,width: (menuImage?.size.width)!, height: (menuImage?.size.height)!)
        menuButton.addTarget(self, action: #selector(NavigationBarStyle.userProfileClickedCalled), for: UIControlEvents.touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        return menuBarButton
    }
    


    func getNavigationBackArrowBarItem(_ vc: UIViewController)-> UIBarButtonItem{
        let backBarButton = UIBarButtonItem(image: UIImage(named:"back_btn.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NavigationBarStyle.backBtnClickedCommonCalled))
        return backBarButton
    }
    
    //"backBtnClickedCommonCalled
     func getNavigationNextArrowBarItem(_ vc: UIViewController)-> UIBarButtonItem{
        let backBarButton = UIBarButtonItem(image: UIImage(named:"next_btn.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NavigationBarStyle.nextBtnClickedCommonCalled))
        return backBarButton
    }
    
     func getNavigationTitleBarItem(_ vc: UIViewController,buttonTitle: String)-> UIBarButtonItem{
        let barButton = UIBarButtonItem(title: buttonTitle, style: UIBarButtonItemStyle.plain, target: self, action:#selector(NavigationBarStyle.titleBarButtonClickedCalled))
        return barButton
    }
    
     func getNavigationSearchBarItem(_ vc: UIViewController)-> UIBarButtonItem{
        let backBarButton = UIBarButtonItem(image: UIImage(named:"search_white_icon.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NavigationBarStyle.searchIconClickedCalled))
        return backBarButton
    }
    
   
    
    @objc func backBtnClickedCommonCalled(){
        delegate?.backBtnClickedDelegateMethod!()
    }
    
    @objc func searchIconClickedCalled(){
        delegate?.searchIconClickedDelegateMethod!()
    }
    
    @objc func titleBarButtonClickedCalled(){
        delegate?.titleBarClickedDelegateMethod!()
    }
    
    @objc func nextBtnClickedCommonCalled(){
        delegate?.nextBtnClickedDelegateMethod!()
    }
    
    @objc func userProfileClickedCalled(){
        delegate?.userProfileClickedDelegateMethod!()
    }
    
    @objc func tickBtnClickedCalled(){
        delegate?.tickBtnClickedDelegateMethod!()
    }
   
    @objc func settingBtnClickedCalled(){
        delegate?.settingBtnClickedDelegateMethod!()
    }
    
    @objc func filterBtnClickedCalled(){
        delegate?.filterBtnClickedDelegateMethod!()
    }
    
    @objc func grabIconBtnClickedCalled(){
        delegate?.grabIconBtnClickedDelegateMethod!()
    }
    
    
    @objc func userIconBtnClickedCalled(){
        delegate?.userIconBtnClickedDelegateMethod!()
    }
    
}
