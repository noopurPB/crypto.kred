//
//  CKGlobal.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 25/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import SDWebImage

class CKGlobalMethods {
    
    
    class func initialiseCoin(_ nilCurrentCoin: Bool) -> Coin{
        if(nilCurrentCoin == true){
            setNilCurrentCoin()
        }
        let coin = Coin()
        return coin;
    }
    
    
    class func setNilCurrentCoin() {
        CKGlobalVariables.sharedManager.coin = nil;
    }
    
    class func setCurrentCoinDefaultValues(coin: Coin?) -> Coin? {
        
        coin?.issuerName = "NOOPUR.KRED"
        coin?.value = 1;  //"1 KRED - 12"
        coin?.count = 1;
        coin?.circulation = 1;
        coin?.coinColor = CKColor.defaultCoinColor;
        return coin;
    }
    
    class func setCoinFrontValue(coin: Coin?) -> String? {
        let unicodeL = String(format: "\\%@" , NSLocalizedString("circulation_code", comment: ""))
        let coinFrontValue =  String(format: "%.2f %@ - %@ %d", coin?.value ?? 0, NSLocalizedString("kred_name", comment: "") , unicodeL ,coin?.circulation ?? 0)
        return coinFrontValue;
    }
    
    
    class func setCoinBackValue(coin: Coin?) -> String? {
        let coinbackValue =  String(format: "%.2f %@ - %d/%d", coin?.value ?? 0, NSLocalizedString("kred_name", comment: ""), coin?.sequence ?? 1 ,coin?.count ?? 1);
        return coinbackValue;
    }
    
    
    class func addSubviewSingleCoin(coinframe: CGRect, view: UIView, coin: Coin?, showFrontCoin: Bool) -> CoinViewSingle{
        let coinviewL = CoinViewSingle(frame: coinframe)
        coinviewL.coin = coin;
        coinviewL.showFrontCoin = showFrontCoin;
        coinviewL.loadCoin(webView: coinviewL.webView!)
        view.addSubview(coinviewL)
        return coinviewL
    }
    
   
    //MARK: Date Methods
    class func getFormatter()-> DateFormatter{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        //dateFormatter.timeZone = NSTimeZone(abbreviation: Constants.DATE_TIME_ZONE)
        return dateFormatter
    }
    
    
    
    class func convertDateFromString(dateL: String) -> Date
    {
        print("before date", dateL)
        var actualDateL = dateL;
        if(dateL.contains(".000000")){
            actualDateL = dateL.replacingOccurrences(of: ".000000", with: "")
        }
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        //dateFormatter.locale = tempLocale;
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: actualDateL)!
        print("after date", date)
        /*dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(date)")*/
        return date;
    }
    
    
    class func convertTimestampToDate(timestamp: Double, format: String) -> String{
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current//Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = format //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    
    
    class func setTimeLeft(date: Date) -> String{
        let timeNow = Date()
        
        // Only keep counting if timeEnd is bigger than timeNow
       // if timeNow.compare(date) == ComparisonResult.orderedAscending {
            let calendar = Calendar.current
            let components = calendar.dateComponents([.day, .hour, .minute, .second], from: timeNow, to: date)
            
            //let components = calendar.components([.Day, .Hour, .Minute, .Second], fromDate: timeNow, toDate: timeEnd, options: [])
            
            let cDay = components.day ?? 0;
            let cHour = components.hour ?? 0;
            
            var dayText = String(format: "%d", cDay) + "D "
            var hourText = String(format: "%d", cHour) + "H "
            // Hide day and hour if they are zero
            if cDay <= 0 {
                dayText = ""
                if cHour <= 0 {
                    hourText = ""
                }
            }
            return dayText + hourText + String(format: "%d", components.minute ?? 0) + "M";
           // return dayText + hourText + String(format: "%d", components.minute ?? 0) + "M " + String(format: "%d", components.second ?? 0) + "s";
            
        /*} else {
            return "Ended";
        }*/
    }
    
    
     //MARK: Loadmore
    class func getPageId(_ arrayCount: Int?) -> Double{
        let pageId = (Double(arrayCount!) / Double(CKServiceConstants.ITEM_PER_PAGE)) + 1
        return pageId
    }
    
    
    class func loadMoreWebservice(_ pageId: Double) -> Bool{
        //print(floor(pageId))
        return  floor(pageId) == pageId
    }
    
    
    class func showErrorALertWithMessage(_ title: String? ,message: String?)
    {
        let errorAlert = LNRNotificationManager()
        errorAlert.notificationsPosition = LNRNotificationPosition.top
        errorAlert.notificationsBackgroundColor = UIColor.red//UIColor.init(red: 165/255.0, green: 42/255.0, blue: 42/255.0, alpha: 1)
        errorAlert.notificationsTitleTextColor = UIColor.white
        errorAlert.notificationsBodyTextColor = UIColor.white
        errorAlert.notificationsSeperatorColor = UIColor.gray
        errorAlert.notificationsIcon = UIImage(named: "error")
        errorAlert.notificationsTitleFont = Theme.mediumFont(17.0)
        errorAlert.notificationsBodyFont = Theme.regularFont(15.0)
        
        errorAlert.showNotification(notification: LNRNotification(title: title!, body: message, duration: LNRNotificationDuration.default.rawValue, onTap: { () -> Void in
            
            
        }, onTimeout: { () -> Void in
            //print("Notification Timed Out")
        }))
    }
    
    class func showSuccessALertWithMessage(_ title: String? ,message: String?)
    {
        /*
        TAlertView.hideAll()
        let alert = TAlertView(title: title, andMessage: message)
        
        if(title == NSLocalizedString("woops", comment: "")){
            alert?.style = TAlertViewStyle.error
            //alert.setTitleColor(UIColor.redColor(), forAlertViewStyle:TAlertViewStyle.Error)
        }else{
            alert?.setTitleColor(Color.themeDarkColor, for:TAlertViewStyle.warning)
        }
        alert?.show()*/
        
        let errorAlert = LNRNotificationManager()
        errorAlert.notificationsPosition = LNRNotificationPosition.top
        errorAlert.notificationsBackgroundColor = CKColor.verifiedGreen; //UIColor.init(red: 165/255.0, green: 42/255.0, blue: 42/255.0, alpha: 1)
        errorAlert.notificationsTitleTextColor = UIColor.white
        errorAlert.notificationsBodyTextColor = UIColor.white
        errorAlert.notificationsSeperatorColor = UIColor.gray
        errorAlert.notificationsIcon = UIImage(named: "success")
        errorAlert.notificationsTitleFont = Theme.mediumFont(17.0)
        errorAlert.notificationsBodyFont = Theme.regularFont(15.0)
        
        errorAlert.showNotification(notification: LNRNotification(title: title!, body: message, duration: LNRNotificationDuration.default.rawValue, onTap: { () -> Void in
            
            
        }, onTimeout: { () -> Void in
            //print("Notification Timed Out")
        }))
        
    }
    
    
    class func isValidEmail(_ string: String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
    class func trimString(_ string: String?)-> String {
        if(string != nil || string != ""){
            return string?.trimmingCharacters(in: CharacterSet.whitespaces) ?? ""
        }else{
            return ""
        }
    }
    
    
    class func showAlertWithTitleandMessage(_ title: String? ,message: String?){
        if(title == NSLocalizedString("woops", comment: "")){
            self.showErrorALertWithMessage(title ,message: message)
            
        }else{
            self.showSuccessALertWithMessage(title ,message: message)
        }
    }
    
    class func loginTextFieldStyle(_ textField: UITextField, placeholder: String?){
        textField.textColor = UIColor.white
        textField.tintColor = UIColor.white
        textField.font = Theme.regularFont(16.0)
        textField.attributedPlaceholder = NSAttributedString(string: placeholder!,
                                                             attributes:[NSAttributedStringKey.foregroundColor: Color.loginTextColor])
    }
    
    
    
    class func saveStringValueInPrefrences(_ value: String?, key: String?){
        let defaults = UserDefaults.standard
        defaults.set(CKGlobalMethods.trimString(value), forKey:key ?? "")
        defaults.synchronize()
        if(key == CKConstants.PREF_TOKEN_KEY){
            CKGlobalVariables.sharedManager.loggedInToken = value ?? ""
           
        }else if (key == CKConstants.PREF_USERNAME){
            CKGlobalVariables.sharedManager.loggedInUsername = value ?? ""
        }else if (key == CKConstants.PREF_AVATAR){
            CKGlobalVariables.sharedManager.loggedInAvatar = value ?? ""
        }else if (key == CKConstants.PREF_EMAIL_KEY){
            CKGlobalVariables.sharedManager.loggedInEmail = value ?? ""
        }else if (key == CKConstants.PREF_PASSWORD_KEY){
           CKGlobalVariables.sharedManager.loggedInPassword = value ?? ""
        }else if (key == CKConstants.PREF_SESSION_ID){
            CKGlobalVariables.sharedManager.sessionId = value ?? ""
        }
    }
    
    class func getStringValueFromPrefrences(_ key: String?) -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: key!) ?? ""
    }
    
    
    
    //MARK: Set Image
    
    class func setImageViewWithCache(_ imageURL: String?, imageView: UIImageView, contentMode: UIViewContentMode){
        
        imageView.backgroundColor = CKColor.placeholderColor
        imageView.image = nil

        if(imageURL != nil && imageURL != "" ){
            
            let indicator:UIActivityIndicatorView  = UIActivityIndicatorView(activityIndicatorStyle: .white)
            indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            //indicator.center = imageView.center
            imageView.layoutIfNeeded()
            var frame:CGRect = indicator.frame;
            frame.origin.x = imageView.frame.size.width / 2 - frame.size.width / 2;
            frame.origin.y = imageView.frame.size.height / 2 - frame.size.height / 2;
            indicator.frame = frame;
            
            imageView.addSubview(indicator)
            imageView.contentMode = contentMode;
            
            let escapedString = imageURL!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            if(escapedString != nil){
                //imageView.sd_setImageWithURL(NSURL(string: escapedString!), placeholderImage: UIImage(named: Constants.USER_PLACEHOLDER), options: SDWebImageOptions.RefreshCached)
                
                indicator.startAnimating()
                
                imageView.sd_setImage(
                    with: URL(string: escapedString!),
                    placeholderImage: nil, //UIImage(named: Constants.USER_PLACEHOLDER)
                    options: SDWebImageOptions.refreshCached,
                    progress: nil,
                    completed: { (image, error, cacheType, imageURL) in
                        
                        indicator.stopAnimating()
                        indicator.hidesWhenStopped = true
                })
            }
            imageView.clipsToBounds = true
            //imageView.setNeedsLayout()
            // imageView.layoutIfNeeded()
            
            
        }
        
    }


    class func urlIsImage(string: String) -> Bool{
        if( string.hasSuffix(".png") ||
            string.hasSuffix(".jpg") ||
            string.hasSuffix(".jpeg") ||
            string.hasSuffix(".gif") ){
            return true;
        }else{
            return false;
        }
        
    }

}
