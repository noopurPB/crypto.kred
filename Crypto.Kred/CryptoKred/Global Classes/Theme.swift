//
//  Theme.swift
//  Grab
//
//  Created by Noopur Virmani on 21/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import UIKit
import Foundation

class Theme {
    
    static let BUTTON_CORNER_RADIUS: CGFloat = 20.0;// seconds

    
    static let CELL_TITLE_LABEL_FONT: CGFloat = 17.0;// seconds
    
   
    class func mediumFont(_ size: CGFloat) -> UIFont{
     return UIFont(name: "HelveticaNeue-Medium", size: size)!
    }
    
    class func boldFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Bold", size: size)!
    }
    
    class func lightFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Light", size: size)!
    }
    
    class func thinFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Thin", size: size)!
    }
    
    class func regularFont(_ size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue", size: size)!
    }
    
    class func getConnectedButtonStyle(_ button: UIButton, color: UIColor){
        button.clipsToBounds = true
        button.layoutIfNeeded()
        button.layer.cornerRadius =  button.frame.height/2 //Theme.BUTTON_CORNER_RADIUS
        button.backgroundColor = color
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.font = Theme.mediumFont(18.0)
    }
    
    
    class func getConnectedButtonStyleLight(_ button: UIButton, color: UIColor){
        button.clipsToBounds = true
        button.layoutIfNeeded()
        button.layer.cornerRadius =  button.frame.height/2
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.darkGray, for: UIControlState())
        button.titleLabel?.font = Theme.mediumFont(18.0)
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    
    
    
  
    
    class func enableBarItem(_ barItem: UIBarButtonItem, enable: Bool?){
        if(enable == true){
            barItem.isEnabled = true
            barItem.tintColor = UIColor.white.withAlphaComponent(1.0)
        }else{
            barItem.isEnabled = false
            barItem.tintColor = UIColor.white.withAlphaComponent(0.5)
        }
    }
    
    
    class func themeTitleStyle(_ label: UILabel){
        label.font = Theme.regularFont(CELL_TITLE_LABEL_FONT)
        label.textColor = Color.themeDarkColor
    }
    
    
   
    class func navigationTitleStyle(_ firstLabel: UILabel){
            firstLabel.textAlignment = NSTextAlignment.center
            firstLabel.textColor = UIColor.white
            firstLabel.font = Theme.mediumFont(17.0)
    }
    
  
    
    
    
    
}
