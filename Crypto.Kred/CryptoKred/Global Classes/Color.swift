//
//  Color.swift
//  Grab
//
//  Created by Noopur Virmani on 20/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import UIKit
import Foundation

struct Color {
    
    static let coinDefaultColor:UIColor = UIColor(red: (250 / 255.0), green: (184 / 255.0), blue: (50 / 255.0), alpha: 1.0)
    
    static let themeDarkColor:UIColor = UIColor.black
    
    static let themeAnotherColor:UIColor = UIColor(red: (55 / 255.0), green: (126 / 255.0), blue: (247 / 255.0), alpha: 1.0)
    
    static let themeColorLight :UIColor = UIColor(red: (147 / 255.0), green: (133 / 255.0), blue: (184 / 255.0), alpha: 1.0)
    
    static let indicatorColor: UIColor = UIColor.gray
    
    static let themeDarkHexColor:String = "000000"
    
    
    static let darkGreyColor:UIColor = UIColor(red: (78 / 255.0), green: (78 / 255.0), blue: (78 / 255.0), alpha: 1.0)
    
    static let domainRegGreyColor:UIColor = UIColor(red: (142 / 255.0), green: (142 / 255.0), blue: (147 / 255.0), alpha: 1.0)
    
    static let createGrabColor:UIColor = UIColor(red: (247 / 255.0), green: (247 / 255.0), blue: (247 / 255.0), alpha: 1.0)
    
    static let hashTagColor:UIColor = UIColor(red: (142 / 255.0), green: (142 / 255.0), blue: (142 / 255.0), alpha: 1.0)
    
    static let customGrabHeaderColor:UIColor = UIColor(red: (246 / 255.0), green: (247 / 255.0), blue: (248 / 255.0), alpha: 1.0)
    
    static let filterCellLightColor:UIColor = UIColor(red: (86 / 255.0), green: (86 / 255.0), blue: (86 / 255.0), alpha: 1.0)

    static let filterCellDarkColor:UIColor = UIColor(red: (43 / 255.0), green: (43 / 255.0), blue: (43 / 255.0), alpha: 1.0)
    
    static let facebookBackColor:UIColor = UIColor(red: (59 / 255.0), green: (89 / 255.0), blue: (152 / 255.0), alpha: 1.0)

    static let twitterBackColor:UIColor = UIColor(red: (64 / 255.0), green: (153 / 255.0), blue: (255 / 255.0), alpha: 1.0)


    static let sideMenuDarkColor:UIColor = UIColor(red: (45 / 255.0), green: (48 / 255.0), blue: (52 / 255.0), alpha: 1.0)
    
    static let sideMenuLightColor:UIColor = UIColor(red: (61 / 255.0), green: (64 / 255.0), blue: (70 / 255.0), alpha: 1.0)
    
    static let greenGrabColor:UIColor = UIColor(red: (0 / 255.0), green: (203 / 255.0), blue: (0 / 255.0), alpha: 1.0)
    
     static let greyGrabColor:UIColor = UIColor(red: (189 / 255.0), green: (193 / 255.0), blue: (201 / 255.0), alpha: 1.0)
    
     static let progressBarColor:UIColor = UIColor(red: (190 / 255.0), green: (194 / 255.0), blue: (200 / 255.0), alpha: 1.0)
    
     static let grabButtonBackColor:UIColor = UIColor(red: (237 / 255.0), green: (237 / 255.0), blue: (237 / 255.0), alpha: 1.0)
    
    static let leaderboardColor:UIColor = UIColor(red: (95 / 255.0), green: (95 / 255.0), blue: (95 / 255.0), alpha: 1.0)
    
     static let memberOwnerColor:UIColor = UIColor(red: (14 / 255.0), green: (153 / 255.0), blue: (213 / 255.0), alpha: 1.0)
    
    static let memberAdminColor:UIColor = UIColor(red: (148 / 255.0), green: (213 / 255.0), blue: (122 / 255.0), alpha: 1.0)
    
    static let memberModeratorColor:UIColor = UIColor(red: (255 / 255.0), green: (144 / 255.0), blue: (8 / 255.0), alpha: 1.0)
    
    static let loginTextColor:UIColor = UIColor(red: (163 / 255.0), green: (163 / 255.0), blue: (163 / 255.0), alpha: 1.0)

    static let connectBtnColor:UIColor = UIColor(red: (148 / 255.0), green: (135 / 255.0), blue: (185 / 255.0), alpha: 1.0)
    
    
    
}


