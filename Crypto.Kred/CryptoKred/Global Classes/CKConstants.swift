//
//  CKConstants.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 12/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
struct CKConstants {
    
    
    
    enum AppErrorType{
        case ERROR_CANCELLED
        case AUTHENTICATION_ERROR
        case UNEXPECTED_ERROR
        case POST_NO_NETWORK
        case ERRORTYPE(Error)
    }

    
    enum MESSAGES_TYPE{
        case COMMENTS
        case PROFILE
        case NEWSFEED
    }
    
    
    enum FILTER_TYPE : String{
        
        case MOST_RECENT
        case LEAST_RECENT
        case LOWEST_VALUE
        case HIGHEST_VALUE
        case MOST_CIRCULATED
        case LEAST_CIRCULATED
        case MOST_LIKED
        case LEAST_LIKED
        case ENDING_LATEST
        case ENDING_SOONEST
        case COIN
        case ELSE
        
        
        var description : String {
            switch self {
                
            case .MOST_RECENT:
                return "-created"
                
            case .LEAST_RECENT:
                return "created"
                
            case .LOWEST_VALUE:
                return "value"
                
            case .HIGHEST_VALUE:
                return "-value"
                
            case .MOST_CIRCULATED:
                return "-circulation"
                
            case .LEAST_CIRCULATED:
                return "circulation"
                
            case .MOST_LIKED:
                return "-likes"
                
            case .LEAST_LIKED:
                return "likes"
                
            case .ENDING_LATEST:
                return "end"
                
            case .ENDING_SOONEST:
                return "-end"
                
            case .COIN:
                return "-coin"
                
            case .ELSE:
                return "";
            }
        }
    }

    
    static let publishableKey = "pk_test_g6do5S237ekq10r65BnxO6S0"//"pk_live_IIRiNucjNDQx0UANbgxsQqsM"
    static let baseURLString = "http://localhost:4567"
    static let defaultCurrency = "usd"
    static let defaultDescription = "Purchase from RWPuppies iOS"
    
    
    
    static let PREF_TOKEN_KEY = "token"
    
    static let APP_GROUP = "group.com.peoplebrowsr.app.ios"
    
    static let PREF_USER_ID = "userId"
    
    static let PREF_USERNAME = "username"
    
    static let PREF_AVATAR = "avatar"
    
    static let PREF_EMAIL_KEY = "email"
    
    static let PREF_WALLET_KEY = "wallet"
    
    static let PREF_RESET_EMAIL_KEY = "reset_email"
    
    static let PREF_PASSWORD_KEY = "password"
    
    static let PREF_SESSION_ID = "session_id"
    
    
}
