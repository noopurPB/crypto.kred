//
//  CKGlobalVariables.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 25/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKGlobalVariables {
    
    var coin: Coin?
    var loggedInToken: String = ""
    var loggedInUsername: String = ""
    var loggedInAvatar: String = ""
    var loggedInEmail: String = ""
    var loggedInUserId: String = ""
    var loggedInPassword: String = ""
    var sessionId: String = ""
    var walletId: Int? = 0
    var walletBalance: Int? = 0
    
    class var sharedManager: CKGlobalVariables {
        struct Static {
            static let instance = CKGlobalVariables()
        }
        return Static.instance
    }
}
