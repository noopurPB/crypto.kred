//
//  CoinProfileHeaderTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

@objc protocol CoinProfileHeaderTVCellDelegate{
    @objc optional func backPressedDelegateMethod()
    
    @objc optional func auctionPressedDelegateMethod()
    @objc optional func sellPressedDelegateMethod()
    @objc optional func givePressedDelegateMethod()
    
    @objc optional func buyPressedDelegateMethod()
    
    @objc optional func requestPressedDelegateMethod(button: UIButton)
    
    @objc optional func flagCoinPressedDelegateMethod()
    @objc optional func likeHeaderDelegateMethod(button: UIButton)
    @objc optional func circulationDelegateMethod()
}


class CoinProfileHeaderTVCell: CKBaseTVCell{
    
    var delegate: CoinProfileHeaderTVCellDelegate?
    
    @IBOutlet weak var issueView: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var heldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heldUserLbl: UILabel!
    @IBOutlet weak var heldByLbl: UILabel!
    @IBOutlet weak var heldImageView: UIImageView!
    @IBOutlet weak var heldView: UIView!
    @IBOutlet weak var flagCoinBtn: UIButton!
    @IBOutlet weak var coinBackView: UIView!
    
    @IBOutlet weak var issuedPersonLbl: UILabel!
    @IBOutlet weak var issuedByLbl: UILabel!
    @IBOutlet weak var issuedImageView: UIImageView!
   
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var marketValueLbl: UILabel!
    @IBOutlet weak var circulationLbl: UILabel!
    @IBOutlet weak var marketLbl: UILabel!
    
    @IBOutlet weak var circulationBtn: UIButton!
    
    @IBOutlet weak var rightActionBtn: UIButton!
    @IBOutlet weak var centreActionBtn: UIButton!
    @IBOutlet weak var leftActionBtn: UIButton!
   
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var rightActionWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftActionWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var timerLblWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionView: UIView!
    @IBOutlet weak var actionViewWidthConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        if(issuedByLbl != nil){
            self.issuedByLbl.font = CKTheme.regularFont(14.0)
            self.issuedPersonLbl.font = CKTheme.boldFont(16.0)
            self.issuedImageView.clipsToBounds = true
            self.issuedImageView.layer.cornerRadius = self.issuedImageView.frame.width/2;
        }
        
        if(heldByLbl != nil){
            self.heldByLbl.font = CKTheme.regularFont(14.0)
            self.heldUserLbl.font = CKTheme.boldFont(16.0)
            self.heldImageView.clipsToBounds = true
            self.heldImageView.layer.cornerRadius = self.heldImageView.frame.width/2;
        
        }
        if(flagCoinBtn != nil){
            flagCoinBtn.setFAText(prefixText: "", icon: .FAFlagO, postfixText: " Flag Coin", size: 11.0, forState: UIControlState.normal)
            flagCoinBtn.titleLabel?.font = CKTheme.regularFont(11.0)
            //flagCoinBtn.addTarget(self, action: #selector(CoinHeaderStyleBar.flagCoinPressed(_:)), for: .touchUpInside)
        }
        
        if(circulationLbl != nil){
            circulationLbl.font = CKTheme.regularFont(13.0)
            circulationLbl.textColor = CKColor.darkGrayColor;
            marketLbl.font = CKTheme.regularFont(13.0)
            marketLbl.textColor = CKColor.darkGrayColor;
            marketValueLbl.font = CKTheme.regularFont(18.0)
            marketValueLbl.textColor = CKColor.darkGrayColor
            circulationBtn.setFATitleColor(color: CKColor.darkGrayColor)
        }
        
        if(backBtn != nil){
            backBtn.setFAIcon(icon: .FAAngleLeft, iconSize: 40.0, forState: UIControlState.normal)
        }
        
        if(timerLabel != nil){
            timerLabel.font = CKTheme.mediumFont(14.0)
            timerLabel.textAlignment = NSTextAlignment.center;
            timerLabel.textColor = CKColor.titleGreyColor;
        }
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        delegate?.backPressedDelegateMethod!()
    }
    
    @IBAction func circulationBtnClicked(_ sender: UIButton) {
        delegate?.circulationDelegateMethod!()
    }
    
    @IBAction func likeBtnClicked(_ sender: UIButton) {
        delegate?.likeHeaderDelegateMethod!(button: sender)
    }
    
    @IBAction func flagCoinBtnClicked(_ sender: UIButton) {
        delegate?.flagCoinPressedDelegateMethod!()
    }
    
    
    @IBAction func leftActionBtnClicked(_ sender: UIButton) {
        
    }
    
    
    @IBAction func rightActionBtnClicked(_ sender: UIButton) {
        
    }
    
    
    @IBAction func centreActionBtnClicked(_ sender: UIButton) {
        
    }
    
    
    //MARK: Action Header
    func loadActionHeader(coin: Coin){
        if(coin.likes != nil && coin.likes != 0){
            self.likeBtn.setFAText(prefixText: "", icon: .FAHeart, postfixText: String(format: " %d", coin.likes ?? 0), size: 18.0, forState: UIControlState.normal, iconSize: 18.0)
        }else{
            self.likeBtn.setFAText(prefixText: "", icon: .FAHeart, postfixText: "", size: 18.0, forState: UIControlState.normal, iconSize: 18.0)
        }
        if(coin.liked != nil && coin.liked == true){
           
           // self.likeBtn.tintColor = CKColor.themePinkColor
            self.likeBtn.setFATitleColor(color: CKColor.themePinkColor)
        }else{
            self.likeBtn.setFATitleColor(color: CKColor.titleGreyColor)
        }
        
        self.circulationBtn.setFAText(prefixText: "", icon: .FARotateRight, postfixText: String(format: " %d", coin.circulation ?? 0) , size: 18.0, forState: UIControlState.normal, iconSize: 18.0)
       
        
        
        let isIntegerP = coin.value?.truncatingRemainder(dividingBy: 1) == 0
        if(isIntegerP){
            self.marketValueLbl.text = String(format: "%0.0f %@", coin.value ?? 0, NSLocalizedString("kred_name", comment: ""))
        }else{
            self.marketValueLbl.text = String(format: "%0.2f %@", coin.value ?? 0, NSLocalizedString("kred_name", comment: ""))
        }
        
       
    }
    
    
    //MARK: Top Header
    func loadHeader(coin: Coin, issuedBio: Bio?, heldBio: Bio?){
        self.setCoin(coin: coin)
        var color: UIColor = CKColor.tableviewCellColor2;
        if(coin.coinColor?.contains("#"))!{
            color = UIColor(hexString: (coin.coinColor)!)
        }else{
            color = UIColor(hexString: "#" + (coin.coinColor)!)
        }
        self.backgroundColor = color
        if color.isLight {
            self.setUIColor(color: CKColor.titleGreyColor)
        } else {
            self.setUIColor(color: UIColor.white)
        }
        
        if(issuedBio != nil){
           CKGlobalMethods.setImageViewWithCache(issuedBio?.avatar ?? "", imageView: self.issuedImageView, contentMode: UIViewContentMode.scaleAspectFit)
            self.issuedPersonLbl.text = issuedBio?.name;
        }
        
        if(heldBio != nil){
             self.heldHeightConstraint.constant = 60.0
             self.heldView.isHidden = false
             CKGlobalMethods.setImageViewWithCache(heldBio?.avatar ?? "", imageView: self.heldImageView, contentMode: UIViewContentMode.scaleAspectFit)
            self.heldUserLbl.text = heldBio?.name;
        }else{
            self.heldView.isHidden = true
            self.heldHeightConstraint.constant = 0.0
        }
        
        
        self.heldUserLbl.sizeToFit()
        self.issuedPersonLbl.sizeToFit()
        
        
        
      
        
    }
    
    
    func setCoin(coin: Coin){
        self.coinBackView.backgroundColor = UIColor.clear;
        let coinView = CoinView(frame: CGRect(x: 0, y: 0, width: self.coinBackView.frame.width - 10, height: self.coinBackView.frame.height - 40))
        coinView.backgroundColor = UIColor.clear;
        coinView.coin = coin;
        coinView.loadCoin(webView: coinView.frontWebView!)
        self.coinBackView.addSubview(coinView)
    }
    
    
    func setUIColor(color: UIColor){
        if(flagCoinBtn != nil){
            flagCoinBtn.setFATitleColor(color: color, forState: UIControlState.normal)
            self.issuedByLbl.textColor = color
            self.issuedPersonLbl.textColor = color
            self.heldByLbl.textColor = color
            self.heldUserLbl.textColor = color
        }
        if(backBtn != nil){
            self.backBtn.setFATitleColor(color: color)
        }
    }
    
    
    func showMyCollectionActionBtn(){
        let viewWidth = 3*self.centreActionBtn.frame.width + 3*5 + 50;
        self.actionViewWidthConstraint.constant = viewWidth
        CKTheme.getCKLightButtonStyle(self.leftActionBtn, fontSizeL: 15.0)
        self.leftActionBtn.setTitle("AUCTION", for: UIControlState.normal)
        self.leftActionBtn.addTarget(self, action:  #selector(CoinProfileHeaderTVCell.auctionPressedDelegateMethod), for: UIControlEvents.touchUpInside)
        
        CKTheme.getCKLightButtonStyle(self.centreActionBtn, fontSizeL: 15.0)
        self.centreActionBtn.setTitle("SELL", for: UIControlState.normal)
        self.centreActionBtn.addTarget(self, action:  #selector(CoinProfileHeaderTVCell.sellPressedDelegateMethod), for: UIControlEvents.touchUpInside)
        
        CKTheme.getCKButtonStyle(self.rightActionBtn, fontSizeL: 15.0)
        self.rightActionBtn.setTitle("GIVE", for: UIControlState.normal)
        self.rightActionBtn.addTarget(self, action:  #selector(CoinProfileHeaderTVCell.givePressedDelegateMethod), for: UIControlEvents.touchUpInside)
        
        self.timerLabel.isHidden = true;
        ////cell.rightsideBtn.addTarget(self, action:  #selector(CKExploreVC.rightBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
    }
    
  
    func showRequestedActionBtn(requested: Bool){
        if(requested){
            self.leftActionWidthConstraint.constant = 120;
            self.leftActionBtn.setTitle("REQUESTED", for: UIControlState.normal)
            self.leftActionBtn.alpha = 0.5
        }else{
            self.leftActionWidthConstraint.constant = 90;
            self.leftActionBtn.setTitle("REQUEST", for: UIControlState.normal)
            self.leftActionBtn.alpha = 1.0
            self.leftActionBtn.addTarget(self, action:  #selector(CoinProfileHeaderTVCell.requestPressedDelegateMethod(button:)), for: UIControlEvents.touchUpInside)
        }
        let viewWidth = 1*self.leftActionBtn.frame.width + 1*5 + 50;
        self.actionViewWidthConstraint.constant = viewWidth
        
        CKTheme.getCKButtonStyle(self.leftActionBtn, fontSizeL: 15.0)
        self.centreActionBtn.isHidden = true;
        self.rightActionBtn.isHidden = true;
        self.timerLabel.isHidden = true;
        
        
        
        ////cell.rightsideBtn.addTarget(self, action:  #selector(CKExploreVC.rightBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
    }
    
    func showBuyActionBtn(coin: Coin){
        var viewWidth: CGFloat?
        if(coin.type == "auction"){
            viewWidth = 2*self.leftActionBtn.frame.width + 2*5 + 50;
            self.timerLabel.isHidden = false;
            CKTheme.getCKButtonStyle(self.centreActionBtn, fontSizeL: 15.0)
            self.centreActionBtn.setTitle("BUY", for: UIControlState.normal)
            self.centreActionBtn.isHidden = false;
            self.rightActionBtn.isHidden = true;
            self.leftActionBtn.isHidden = true;
             self.centreActionBtn.addTarget(self, action:  #selector(CoinProfileHeaderTVCell.buyPressedDelegateMethod), for: UIControlEvents.touchUpInside)
            
            let date = CKGlobalMethods.convertDateFromString(dateL: coin.auctionCreated!)
            let timeleft = CKGlobalMethods.setTimeLeft(date: date)
            self.timerLabel.text = timeleft
            
        }else{
            viewWidth = 1*self.leftActionBtn.frame.width + 1*5 + 50;
            self.timerLabel.isHidden = true;
            CKTheme.getCKButtonStyle(self.leftActionBtn, fontSizeL: 15.0)
            self.leftActionBtn.setTitle("BUY", for: UIControlState.normal)
            self.centreActionBtn.isHidden = true;
            self.rightActionBtn.isHidden = true;
            self.leftActionBtn.isHidden = false;
             self.leftActionBtn.addTarget(self, action:  #selector(CoinProfileHeaderTVCell.buyPressedDelegateMethod), for: UIControlEvents.touchUpInside)
        }
        self.actionViewWidthConstraint.constant = viewWidth!
        
        
 
    }
    
    
    
    @objc func auctionPressedDelegateMethod(){
        self.delegate?.auctionPressedDelegateMethod!()
    }
    @objc func sellPressedDelegateMethod(){
        self.delegate?.sellPressedDelegateMethod!()
    }
    @objc func givePressedDelegateMethod(){
        self.delegate?.givePressedDelegateMethod!()
    }
    
    @objc func buyPressedDelegateMethod(){
        self.delegate?.buyPressedDelegateMethod!()
    }
    @objc func requestPressedDelegateMethod(button: UIButton){
        self.delegate?.requestPressedDelegateMethod!(button: button)
    }
    
 
    
}
