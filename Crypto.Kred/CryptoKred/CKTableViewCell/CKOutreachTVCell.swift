//
//  CKOutreachTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 26/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//


import Foundation
import UIKit


class CKOutreachTVCell: CKBaseTVCell{
    
    @IBOutlet var outreachDividerView: [UIView]!
    
    @IBOutlet weak var joinBtn: UIButton!
    @IBOutlet weak var tweetBtn: UIButton!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var joinTelegramLbl: UILabel!
    @IBOutlet weak var tweetAboutusLbl: UILabel!
    @IBOutlet weak var twitterFollowLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(followBtn != nil){
            self.followBtn.titleLabel?.font = CKTheme.regularFont(14.0)
            self.tweetBtn.titleLabel?.font = CKTheme.regularFont(14.0)
            self.joinBtn.titleLabel?.font = CKTheme.regularFont(14.0)
            
            self.followBtn.setTitleColor(CKColor.themeAnotherColor, for: UIControlState.normal)
            self.tweetBtn.setTitleColor(CKColor.themeAnotherColor, for: UIControlState.normal)
            self.joinBtn.setTitleColor(CKColor.themeAnotherColor, for: UIControlState.normal)
            
        }
        
        
        if(twitterFollowLbl != nil){
            self.twitterFollowLbl.textColor = CKColor.titleGreyColor;
            self.tweetAboutusLbl.textColor = CKColor.titleGreyColor;
            self.joinTelegramLbl.textColor = CKColor.titleGreyColor;
            
            self.twitterFollowLbl.font = CKTheme.regularFont(14.0)
            self.tweetAboutusLbl.font = CKTheme.regularFont(14.0)
            self.joinTelegramLbl.font = CKTheme.regularFont(14.0)
            
        }
       
    }
    @IBAction func joinBtnClicked(_ sender: UIButton) {
    }
    
    @IBAction func tweetBtnClicked(_ sender: UIButton) {
    }
    
    @IBAction func followBtnClicked(_ sender: UIButton) {
    }
    
    
    
    
    
    
    
    
}
