//
//  CKTransactionTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 03/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKTransactionTVCell: CKBaseTVCell{
    
    @IBOutlet var headingLbl: [UILabel]!
    @IBOutlet var transLbl: UILabel!
    @IBOutlet var collectionLbl: UILabel!
    @IBOutlet var balanceLbl: UILabel!
    
    @IBOutlet var collectionAdjLbl: UILabel!
    @IBOutlet var balanceAdjLbl: UILabel!
    
    override func awakeFromNib() {
        for label in headingLbl{
            label.textColor = CKColor.titleGreyColor
            label.font = CKTheme.boldFont(15.0)
        }
        
        transLbl.textColor = CKColor.titleGreyColor
        transLbl.font = CKTheme.regularFont(15.0)
        
        collectionLbl.textColor = CKColor.titleGreyColor
        collectionLbl.font = CKTheme.regularFont(15.0)
        
        balanceLbl.textColor = CKColor.titleGreyColor
        balanceLbl.font = CKTheme.regularFont(15.0)
        
        collectionAdjLbl.font = CKTheme.regularFont(15.0)
        balanceAdjLbl.font = CKTheme.regularFont(15.0)
        
        
    }
}
