//
//  CKTextFieldTVCell.swift
//  Grab
//
//  Created by Noopur Virmani on 22/01/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CKTextFieldTVCellDelegate{
    @objc optional func textFieldValueChanged(_ sender: UITextField)
}



class CKTextFieldTVCell: CKBaseTVCell {

    var delegate: CKTextFieldTVCellDelegate?
    
    @IBOutlet var smallIconBtn: UIButton!
    @IBOutlet var textField: UITextField!
    
    @IBOutlet var topDividerView: UIView!
    @IBOutlet var centreView: [UIView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.preservesSuperviewLayoutMargins = false
        self.contentView.preservesSuperviewLayoutMargins = false
        if(textField != nil){
            //textField.tintColor = UIColor.blackColor()
            textField.font = Theme.regularFont(16.0)
            textField.textColor = CKColor.textFieldColor;
           

        }
       
        if(topDividerView != nil){
            topDividerView.backgroundColor = CKColor.dividerColor
        }
    }

    @IBAction func textFieldValueChanged(_ sender: AnyObject) {
        delegate?.textFieldValueChanged!(sender as! UITextField )
    }
    
}
