//
//  CKExportCKrTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 26/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CKExportCKrTVCellDelegate{
    @objc optional func sliderClickedDelegateMethod(slider: UISlider)
    @objc optional func selectedSwitchValue(switchOn: Bool)
}


class CKExportCKrTVCell: CKBaseTVCell{
    
    var delegate: CKExportCKrTVCellDelegate?
    
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var valueSlider: UISlider!
    @IBOutlet weak var valueSwitch: UISwitch!
    @IBOutlet weak var customSwitch: UISwitchCustom!
    
    @IBOutlet weak var amountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if(valueTextField != nil){
            CKTheme.setTextFieldStyle(self.valueTextField, placeholder: self.valueTextField.placeholder!)
        }
        
        if(customSwitch != nil){
            customSwitch.OffTint = CKColor.themeAnotherColor;
            customSwitch.onTintColor = CKColor.themeAnotherColor;
        }
        
        if(self.valueSlider != nil){
            self.valueSlider.value = 1;
            self.valueSlider.minimumValue = 1;
            self.valueSlider.maximumValue = Float(CKGlobalVariables.sharedManager.walletBalance ?? 0);
        }
        if(amountLabel != nil){
            self.amountLabel.textColor = CKColor.titleGreyColor;
            self.amountLabel.font = CKTheme.regularFont(16.0)
        }
        
    }
    
    
  
    @IBAction func switchValueChanged(_ sender: UISwitchCustom) {
        delegate?.selectedSwitchValue!(switchOn: sender.isOn)
        
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        delegate?.sliderClickedDelegateMethod!(slider: sender)
    }
}


