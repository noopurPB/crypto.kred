//
//  CKAdvancedCoinAddTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 25/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CKAdvancedCoinAddTVCellDelegate{
    @objc optional func pickerDoneBtnClickedDelegateMethod()
    @objc optional func sliderClickedDelegateMethod(slider: UISlider)
   
    @objc optional func privateClickedDelegateMethod(sender: UIButton)
    @objc optional func privateInfoClickedDelegateMethod(sender: UIButton)
    @objc optional func nsfwClickedDelegateMethod(sender: UIButton)
    @objc optional func nsfwInfoClickedDelegateMethod(sender: UIButton)
  
}


class CKAdvancedCoinAddTVCell: CKBaseTVCell{
    
    @IBOutlet weak var coinView: UIView!
    @IBOutlet weak var ccSubheadingLabel: UILabel!
    @IBOutlet weak var ccHeadingLabel: UILabel!
    @IBOutlet weak var pickerDoneBtn: UIBarButtonItem!
    var delegate: CKAdvancedCoinAddTVCellDelegate?
    @IBOutlet weak var coinPickerView: UIPickerView!
    @IBOutlet weak var valueSlider: UISlider!
    @IBOutlet weak var ccTextField: UITextField!
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var coinViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var privateBtn: UIButton!
    @IBOutlet weak var privateInfoBtn: UIButton!
    
    @IBOutlet weak var nsfwBtn: UIButton!
    @IBOutlet weak var nsfwInfoBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    
    @IBAction func pickerDoneBtnClicked(_ sender: UIBarButtonItem) {
        delegate?.pickerDoneBtnClickedDelegateMethod!()
    }
    
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        delegate?.sliderClickedDelegateMethod!(slider: sender)
    }
    
    @IBAction func privateBtnClicked(_ sender: UIButton) {
        delegate?.privateClickedDelegateMethod!(sender: sender)
    }
    
    @IBAction func privateInfoBtnClicked(_ sender: UIButton) {
        delegate?.privateInfoClickedDelegateMethod!(sender: sender)
    }
    
    @IBAction func nsfwBtnClicked(_ sender: UIButton) {
        delegate?.nsfwClickedDelegateMethod!(sender: sender)
    }
    
    @IBAction func nsfwInfoBtnClicked(_ sender: UIButton) {
        delegate?.nsfwInfoClickedDelegateMethod!(sender: sender)
    }
    
}
