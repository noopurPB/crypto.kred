//
//  CKLoadMoreTVCell.swift
//  Grab
//
//  Created by Noopur Virmani on 22/03/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation

class CKLoadMoreTVCell: CKBaseTVCell  {
    
    @IBOutlet var loadMoreIndicator: UIActivityIndicatorView!
   
    @IBOutlet var loadMoreLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        if(loadMoreIndicator != nil){
            loadMoreIndicator.color = Color.themeDarkColor
        }
        
        if(loadMoreLabel != nil){
            loadMoreLabel.isHidden = true
            loadMoreLabel.textColor = Color.domainRegGreyColor
            loadMoreLabel.font = Theme.boldFont(13.0)
        }
        
    }
}
