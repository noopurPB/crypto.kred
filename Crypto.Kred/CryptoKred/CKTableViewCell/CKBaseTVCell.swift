//
//  CKBaseTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 23/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//


import Foundation
import UIKit

@objc protocol CKBaseTVCellDelegate{
    @objc optional func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton)
    @objc optional func leftBtnClickedTVCellDelegateMethod(_ sender: UIButton)
}


class CKBaseTVCell: UITableViewCell{
    
    var baseDelegate: CKBaseTVCellDelegate?
    @IBOutlet var dividerView: UIView!
    
    @IBOutlet var rightsideBtn: UIButton!
    @IBOutlet weak var leftsideBtn: UIButton!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var anotherDividerView: UIView!
    @IBOutlet var baseView: UIView!
    @IBOutlet weak var anotherTitleLabel: UILabel!
    @IBOutlet weak var baseImageView: UIImageView!
    
    
    @IBOutlet weak var baseSwitch: UISwitch!
    
    @IBOutlet weak var baseCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        if(dividerView != nil){
            self.dividerView.backgroundColor = CKColor.dividerColor
        }
        if(anotherDividerView != nil){
            self.anotherDividerView.backgroundColor = CKColor.dividerColor
        }
    }
    
    @IBAction func leftSideBtnClicked(_ sender: UIButton) {
        baseDelegate?.leftBtnClickedTVCellDelegateMethod!(sender)
    }
    
    @IBAction func rightBtnClicked(_ sender: UIButton) {
        baseDelegate?.rightBtnClickedTVCellDelegateMethod!(sender)
    }
}
