//
//  CKOutreachVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 26/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKOutreachVC: CKBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarStyle?.showNavigationSideMenuBarItem(self, navTitle: "Be an Outreacher and earn Coins")
        //navBarStyle?.delegate = self;
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row == 0){
            return 230;
            
        }else{
            return 230;
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTweetTVCell", for: indexPath) as! CKOutreachTVCell
            cell.baseView.backgroundColor = UIColor.clear
            cell.titleLabel.font = CKTheme.regularFont(16.0)
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            self.setHeading(label: cell.titleLabel, boldText: "Connect with us ", normalText: "for the Outreacher Coin worth 1 CKr")
           
            cell.twitterFollowLbl.setFAText(prefixText: "", icon: .FATwitter, postfixText: " Follow us on Twitter", size: 14.0)
            cell.tweetAboutusLbl.setFAText(prefixText: "", icon: .FATwitter, postfixText: " Tweet about us", size: 14.0)
            cell.joinTelegramLbl.setFAText(prefixText: "", icon: .FATelegram, postfixText: " Join our Telegram", size: 14.0)
            
            
            
            self.setOutreacherCoin(view: cell.baseView)
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTextFieldTVCell", for: indexPath) as! CKTextFieldTVCell
             cell.titleLabel.font = CKTheme.regularFont(16.0)
             cell.titleLabel.textColor = CKColor.titleGreyColor;
             cell.baseView.backgroundColor = UIColor.clear
             CKTheme.getCKButtonStyle(cell.rightsideBtn, fontSizeL: 14.0)
            cell.anotherTitleLabel.font = CKTheme.regularFont(14.0)
            cell.anotherTitleLabel.textColor = CKColor.titleGreyColor;
            CKTheme.setTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
            
            if(indexPath.row == 1){
                self.setHeading(label: cell.titleLabel, boldText: "Tell a Story about us ", normalText: "for the Storyteller Coin worth 20 Ckr")
                self.setStorytellerCoin(view: cell.baseView)
                cell.anotherTitleLabel.text = "Share a link to a Blog Post, Video or Infographic featuring Crypto.Kred"
                
             }else{
                self.setHeading(label: cell.titleLabel, boldText: "Translate the Whitepaper ", normalText: "for the Translator Coin worth 100 Ckr")
                self.setWhitepaperCoin(view: cell.baseView)
                cell.anotherTitleLabel.text = "Submit a certified translation of our whitepaper here. Once verified you will receive the 100 Ckr Translator Coin."
                
                
            }
            cell.anotherTitleLabel.sizeToFit()
            return cell
            
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    
    func setHeading(label: UILabel, boldText: String, normalText: String){
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold(boldText, fontSize: 16.0)
            .normal(normalText)
        label.attributedText =  formattedString
    }
    
    
    func setOutreacherCoin(view: UIView){
        setCoin(name: "OUTREACHER", value: 1, circulation: 1, front: "https://imgcdn.socialos.io/web/files/55779177b7725ded30d065ae/1522123791998_outreacher coin (1)-small.jpg", view: view)
       
    }
    
    
    func setStorytellerCoin(view: UIView){
        setCoin(name: "STORYTELLER", value: 20, circulation: 1, front: "https://imgcdn.socialos.io/web/files/55779177b7725ded30d065ae/1522124940317_storyteller coin-small.jpg", view: view)
        
    }
    
    
    func setWhitepaperCoin(view: UIView){
      setCoin(name: "TRANSLATOR", value: 100, circulation: 1, front: "https://imgcdn.socialos.io/web/files/55779177b7725ded30d065ae/1522124827715_translator coin-small.jpg", view: view)
        
    }
    
    
    func setCoin(name: String, value: CGFloat, circulation: Int, front: String, view: UIView){
        let coinL = Coin()
        coinL.coinName = name;
        coinL.value = value;
        coinL.coinColor = "FFFFFF";
        coinL.circulation = circulation;
        coinL.front = front;
        
        _ = CKGlobalMethods.addSubviewSingleCoin(coinframe: CGRect(x: -10, y: 0, width: 120, height: 120), view: view, coin: coinL, showFrontCoin: true)
        
    }
    
}
