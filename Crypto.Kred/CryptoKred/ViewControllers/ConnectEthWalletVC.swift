//
//  ConnectEthWalletVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class ConnectEthWalletVC: CKCommonVC{
    
    @IBOutlet weak var subHeadingLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subHeadingLbl.textColor = CKColor.titleGreyColor;
        self.subHeadingLbl.font = CKTheme.regularFont(15.0)
    }
    
    @IBAction func saveBtnClicked(_ sender: UIButton) {
        self.connectETHServiceMethod()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func connectETHServiceMethod(){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        params["name"] = "ethereum"
        params["platform"] = "ethereum"
        params["address"] = txtField.text;
        
        
        let cks = CKPostService()
        self.showLoadingDialog("")
        cks.connectETHWallet(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.error != nil){
                self.showErrorMessage(response.error ?? "")
            }else{
                CKGlobalMethods.showSuccessALertWithMessage(NSLocalizedString("yipee", comment: ""), message: NSLocalizedString("wallet_saved", comment: ""))
                self.dismiss(animated: true, completion: nil)
                
            }
            
        }, errorHandler: { error in
            self.hideLoadingDialog();
            self.errorHandler(error);
        })
        
    }
    
    
}
