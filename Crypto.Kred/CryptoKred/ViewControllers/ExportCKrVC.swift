//
//  ExportCKrVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 26/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class ExportCKrVC: CKBaseVC, UITableViewDelegate, UITableViewDataSource, CKExportCKrTVCellDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveBtn: UIButton!
    
    var titleArray = ["",
                    "Wallet",
                    "Address",
                    "Export Amount",
                    "3% Fee",
                    "Gas",
                    "You will Pay"]
    
    var exportAmountLabel: UILabel?
    var exportAmountValue: Float = 1;
    
    var walletType = ["Stellar", "Ethereum"];
    var selectedWallet: String = "Stellar";
    
    var walletAddressTxtField: UITextField?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedWallet = walletType[0]
        navBarStyle?.delegate = self;
        navBarStyle?.showNavigationSideMenuBarItem(self, navTitle: NSLocalizedString("export_ckr", comment: ""))
        CKTheme.getCKButtonStyle(self.saveBtn, fontSizeL: 18.0)
    }
    
    
    @IBAction func saveBtnClicked(_ sender: UIButton) {
        print(selectedWallet);
        print(walletAddressTxtField?.text ?? "")
        print(exportAmountValue)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    //MARK: Tableview Datasource and Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 100;
        }else if(indexPath.row == 2){
            return 60;
        }else if(indexPath.row == 3){
            return 100;
        }else if(indexPath.row == 5){
            if(selectedWallet == walletType[0]){
                return 0;
            }else{
                return 50;
            }
        }
        else{
            return 50;
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count;
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = "";
        if(indexPath.row == 0){
            identifier = "CKBalanceTVCell";
        }else if(indexPath.row == 1){
            identifier = "CKWalletTVCell";
        }else if(indexPath.row == 2){
            identifier = "CKAddressTVCell";
        }else if(indexPath.row == 3){
            identifier = "CKAmountTVCell";
        }else if(indexPath.row == 4 || indexPath.row == 5){
            identifier = "CKFeeTVCell";
        }else {
            identifier = "CKPayTVCell";
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CKExportCKrTVCell
        cell.delegate = self;
        if(indexPath.row == 2){
            cell.valueTextField.delegate = self;
            self.walletAddressTxtField = cell.valueTextField
        }
        if(indexPath.row != 0){
            cell.titleLabel.font = CKTheme.boldFont(16.0);
            cell.titleLabel.text = titleArray[indexPath.row];
            cell.titleLabel.textColor = CKColor.titleGreyColor;
        }else{
            cell.titleLabel.font = CKTheme.boldFont(20.0);
            cell.titleLabel.textColor = CKColor.themeAnotherColor;
            cell.titleLabel.text = String(format: "Available balance: %d CKr", CKGlobalVariables.sharedManager.walletBalance ?? 0)
        }
        
        if(cell.customSwitch != nil){
            if(selectedWallet == walletType[0]){
                cell.customSwitch.isOn = false;
            }else{
                cell.customSwitch.isOn = true;
            }
        }
        
        
        if(cell.valueSlider != nil){
            cell.valueSlider.value = self.exportAmountValue;
            cell.titleLabel.text = getExportAmountText(value: cell.valueSlider.value)
            self.exportAmountLabel = cell.titleLabel;
        }
        
        
        if(indexPath.row == 4){
           // self.feeValue =
            cell.amountLabel.text = String(format: "+ %0.2f Ckr", getFeeValue());
        }else if(indexPath.row == 5){
            //self.gasValue =
            cell.amountLabel.text = String(format: "+ %0.2f Ckr", getGasValue());
        }else if(indexPath.row == 6){
            //self.payValue =
            cell.amountLabel.text = String(format: "%0.2f Ckr", getPayValue());
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    //MARK: Get values
    
    func getFeeValue() -> Float{
        let fee = self.exportAmountValue * 0.01;
        return fee;
    }
    
    func getGasValue() -> Float{
        if(selectedWallet == walletType[0]){
            return 0.0;
        }else{
            let pay = self.exportAmountValue * 0.76;
            return pay
        }
    }
    
    func getPayValue() -> Float{
        let pay = self.exportAmountValue + getGasValue() + getFeeValue()
        return pay
    }
    
    func getExportAmountText(value: Float) -> String{
        return String(format: "Export Amount: %0.f %@", value, NSLocalizedString("kred_name", comment: ""))
    }
    
    //MARK: CKExportCKrTVCellDelegate
    
    func sliderClickedDelegateMethod(slider: UISlider){
        self.exportAmountLabel?.text = getExportAmountText(value: slider.value)
        self.exportAmountValue = slider.value;
        
        let indexPath1 = IndexPath(row: 4, section: 0)
        let indexPath2 = IndexPath(row: 5, section: 0)
        let indexPath3 = IndexPath(row: 6, section: 0)
        
        self.tableView.reloadRows(at: [indexPath1, indexPath2, indexPath3], with: UITableViewRowAnimation.none)
        
    }
    
    func selectedSwitchValue(switchOn: Bool){
        if(switchOn == true){
            selectedWallet = walletType[1]
        }else{
             selectedWallet = walletType[0]
        }
        let indexPath2 = IndexPath(row: 5, section: 0)
        self.tableView.reloadRows(at: [indexPath2], with: UITableViewRowAnimation.none)
    }
    
}

extension ExportCKrVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ExportCKrVC: UITextFieldDelegate{
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
}
