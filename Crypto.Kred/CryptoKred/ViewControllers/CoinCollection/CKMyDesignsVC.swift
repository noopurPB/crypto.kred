//
//  CKMyDesignsVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 09/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKMyDesignsVC: CKCommonCollectionVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        navBarStyle?.showNavigationSideMenuBarItem(self, navTitle: NSLocalizedString("my_designs", comment: ""))
        
    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        coinDraftServiceMethod()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
  
    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(loadmore == true){
            return coinsList.count + 1
        }else{
            return coinsList.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if(indexPath.row == coinsList.count){
            
            let pageId = CKGlobalMethods.getPageId(self.coinsList.count)
            if(pageId != 1){
                //self.grabTrendingServiceMethod()
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKLoadMoreCVCell",for:indexPath) as! CKLoadMoreCVCell
            return cell;
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKCoinCVCell",for:indexPath) as! CKCoinCVCell
        let coin: Coin = self.coinsList[indexPath.row]
        cell.updateCoinView(coinL: coin)
        
        cell.actionBtn.tag = indexPath.row
        cell.actionBtn.setTitle("USE DESIGN", for: UIControlState.normal)
        cell.actionBtn.addTarget(self, action:  #selector(CKCoinCollectionVC.actionBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
        return cell;
    }
    
    
   
    
    
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCoinCellSize()
    }
    
    
    @objc func actionBtnClickedMethod(button: UIButton) {
        //give coin
        let coinL = self.coinsList[button.tag]
        CKGlobalVariables.sharedManager.coin = coinL
        if(coinL.coinName == nil || coinL.coinName == ""){
            self.createCoinName2VCCalled()
        }else if(coinL.front == nil || coinL.front == ""){
            self.createCoinImageFrontVCCalled()
        }else if(coinL.back == nil || coinL.back == ""){
            self.createCoinImageBackVCCalled()
        }else{
            self.createCoinDoneVCCalled()
        }
    }
    
    
    func coinDraftServiceMethod() {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["page"] = 1
        self.coinCollectionServiceMethod(params: params, path: CKServiceConstants.COIN_DRAFT_URL)
    }
    
    
}




