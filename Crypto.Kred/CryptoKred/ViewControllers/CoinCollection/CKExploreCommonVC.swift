//
//  CKExploreCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKExploreCommonVC: CKCommonCollectionVC{
    
    var requestedCoinIds = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    func setButtonRequested(button: UIButton, requested: Bool){
        if(requested == true){
            button.setTitle("REQUESTED", for: UIControlState.normal)
            button.alpha = 0.5
            button.isEnabled = false
        }else{
            button.setTitle("REQUEST", for: UIControlState.normal)
            button.alpha = 1.0
            button.isEnabled = true
        }
        
    }
    
    
    
    //Explore Method
    
    func exploreActionBtnView(coinL: Coin?, actionBtn: UIButton){
        //explore
        if(requestedCoinIds.contains((coinL?.coin)!) == true){
            self.setButtonRequested(button: actionBtn, requested: true)
        }else{
            self.setButtonRequested(button: actionBtn, requested: false)
        }
    }
    
    func requestActionBtnClickedMethod(coin: Coin, button: UIButton) {
        //explore
        if(userIsLoggedIn() == false){
            self.loginViewControllerCalled(login: true)
            return
        }
        self.requestACoin(coinId: coin.coin ?? 0, requestButton: button)
    }
    
    
    func requestACoin(coinId: Int?, requestButton: UIButton){
        let cks = CKPostService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = coinId ?? 0
        params["wallet"] = CKGlobalVariables.sharedManager.walletId ?? 0
        
        self.showLoadingDialog("")
        
        cks.requestACoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            self.requestedCoinIds.append(coinId ?? 0)
            self.setButtonRequested(button: requestButton, requested: true)
            //self.requestedCoins()
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    
    func requestedCoins(){
        self.requestedCoinIds.removeAll()
        let cks = CKGetService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["user"] = CKGlobalVariables.sharedManager.loggedInUserId
        
        cks.getCoinsRequest(params: params, responseHandler: { response in
            for requestL in response.requests{
                self.requestedCoinIds.append(requestL.coin?.coin ?? 0)
            }
        }, cacheHandler: { response in
            
        }, errorHandler: {
            error in
        })
    }
    
}
