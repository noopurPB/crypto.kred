//
//  CKCommonCollectionVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKCommonCollectionVC: CKBaseVC{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    
    var selectedFilter: String?
    var saleTypeFilter: Bool?
    
    var loadmore: Bool = false;
    
    var coinsList = [Coin]()
    
    
    var coinsValue: CGFloat = 0;
    var coinCellHeight: CGFloat = 330;
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(collectionView != nil){
            self.registerCoinCollectionViewCell(self.collectionView)
            self.registerLoadmoreCollectionViewCell(self.collectionView)
        }
        if(navBarStyle != nil){
            navBarStyle?.delegate = self;
        }
        
        if(self.headerView != nil){
            self.initSearchHeaderView(self.headerView)
        }
        if(searchHeaderView != nil){
            searchHeaderView?.delegate = self
            //searchHeaderView?.textField.text = searchText ?? ""
            searchHeaderView?.textField.returnKeyType = UIReturnKeyType.search
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    }
    
    func getCoinCellSize() -> CGSize{
        let picDimension = self.view.frame.size.width - 20; //(self.view.frame.size.width / 3.0) + 15
        return CGSize(width: picDimension, height: coinCellHeight) //CGSize(width: picDimension+35, height: 195)
    }
    
    
    func showAction(coinparameter: CoinParameters , coin: Coin?){
        
        if(coin?.coins != nil && (coin?.coins)! > 1){
           // showCoinCountCollectionVC(selectedCoin: coin!, coinParameters: coinparameter)
          
        }else{
            self.coinProfileVCCalled(selectedCoin: coin!)
            //showActionsheet(userCollection: userCollection, marketplace: marketplace, coin: coin)
        }
        
    }
    
    
    func showActionsheet(userCollection: Bool, marketplace: Bool, coin: Coin?){
        
        let actionSheetC: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheetC.view.tintColor = Color.themeDarkColor;
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
        }
        actionSheetC.addAction(cancelActionButton)
        
        let giveCoinActionButton = UIAlertAction(title: "Give a Coin", style: .default)
        { _ in
             self.giveCoinCalledVC(coin: coin)
        }
        
        
        let buyCoinActionButton = UIAlertAction(title: "Buy", style: .default)
        { _ in
            
        }
        
        
        if(marketplace == false){
            if(userCollection == true){
                actionSheetC.addAction(buyCoinActionButton)
            }else{
                actionSheetC.addAction(giveCoinActionButton)
            }
        }
        
       
        
        let viewCoinActionButton = UIAlertAction(title: "View Coin", style: .default)
        { _ in
            self.coinProfileVCCalled(selectedCoin: coin!)
        }
        
        actionSheetC.addAction(viewCoinActionButton)
        self.present(actionSheetC, animated: true, completion: nil)
        
    }
    
 
    func coinsCommonHandler(_ coins: [Coin]){
        self.hideSearchView()
        self.coinsList = coins
        var value: CGFloat = 0.0;
        for coin in self.coinsList{
           value = value + coin.value!;
        }
        self.coinsValue = value;
        self.collectionView.reloadData()
        
        if(self.coinsList.count == 0){
            
            if(segmentControl != nil){
                if(segmentControl.selectedSegmentIndex == 0){
                     self.showNoDataView(view: self.collectionView, image: "no_givecoin", title: "Create your first Coin!", subTitle: "Personalize and create your own Crypto Coin", buttonTitle: "CREATE A COIN", buttonWidth: 200);
                }else{
                     self.showNoDataView(view: self.collectionView, image: "no_givecoin", title: "Create and Give your first Coin!", subTitle: "Personalize a Coin and Give it to a Friend", buttonTitle: "CREATE AND GIVE A COIN", buttonWidth: 230);
                    
                    /*"Here you will see Conversations and Notifications from COins in your Collection. Buy or create a Coin to get started."*/
                }
            }
            //   CREATE A COIN
            
           
            self.noDataView?.delegate = self;
        }else{
            self.hideNoDataView()
        }
        
    }
    
    
    
    func loadMoreWebservice(_ coins: [Coin]) -> Bool{
        let pageIdLocal = CKGlobalMethods.getPageId(coins.count)
        if(coins.count == 0 || CKGlobalMethods.loadMoreWebservice(pageIdLocal) == false){
            return false
        }else{
            return true
        }
    }
    
    
    func refreshView(){
        self.coinsList.removeAll();
        self.coinsValue = 0;
        self.collectionView.reloadData()
    }
    
    
    func coinCollectionServiceMethod(params: [String: Any], path: String) {
        
        let pageId = CKGlobalMethods.getPageId(self.coinsList.count)
        
        let cks = CKGetService()
        //params["count"] = CKServiceConstants.ITEM_PER_PAGE;
        //params["page"] = pageId
        if(pageId == 1){
            showSearchView(self.view, searchText: "")
        }
        
        
        cks.getCoinsList(params: params, path: path, responseHandler: { response  in
            self.loadmore = self.loadMoreWebservice(self.coinsList)
            if(pageId == 1){
                self.coinsCommonHandler(response.coins)
            }else{
                self.coinsCommonHandler(self.coinsList + response.coins)
            }
        }, cacheHandler: { response  in
            if(pageId == 1){
                self.coinsCommonHandler(response.coins)
            }
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    
    func filterSelectedMethodCalled(){
        
    }
    
    
  
    
   
   
   
}


extension CKCommonCollectionVC: CKTextFieldTVCellDelegate{
    
    func textFieldValueChanged(_ sender: UITextField) {
        
    }
}


extension CKCommonCollectionVC: CKFilterCoinCollectionVCDelegate{
    func filterOptionSelectedDelegateMethod(selectedType: String?) {
        self.selectedFilter = selectedType!;
        self.filterSelectedMethodCalled();
    }
}


extension CKCommonCollectionVC: NavigationBarStyleDelegate{
    func filterBtnClickedDelegateMethod(){
        let storyboard = UIStoryboard(name:"CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKFilterCoinCollectionVC") as? CKFilterCoinCollectionVC
        controllerObejct?.delegate = self;
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        controllerObejct?.selectedFilter = self.selectedFilter
        controllerObejct?.saleTypeFilter = saleTypeFilter ?? false
        self.present(controllerObejct!, animated: true, completion: nil)
    }
    
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}


extension CKCommonCollectionVC: CKNoDataCellDelegate{
    func noCoinBtnClickedDelegateMethod(_ sender: UIButton) {
        self.createCoinVCCalled()
    }
}

class CoinParameters{
    
    enum VIEW_TYPE{
        case EXPLORE
        case MARKETPLACE
        case COLLECTION
    }
    var viewType: VIEW_TYPE?
    var coinRequested: Bool?
}


