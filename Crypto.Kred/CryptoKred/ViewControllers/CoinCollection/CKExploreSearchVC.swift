//
//  CKExploreSearchVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 20/06/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKExploreSearchVC: CKExploreCommonVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate{
    
    @IBOutlet weak var searchCollectionView: UICollectionView!
    
    var selectedCoin: Coin?
    enum VIEW_TYPE{
        case SEARCH
        case COINS_COUNT
        case SORT_TYPE
    }
    
    var viewType: VIEW_TYPE?

    var navTitle: String?
    var sort: String?
    
    var popularSearches = [CKTag]()
    var searchFont: UIFont = CKTheme.regularFont(15.0)
    
    @IBOutlet weak var popularSearchesHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchbarHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarStyle?.showNavigationBackBarItem(self, navTitle: navTitle!)
        navBarStyle?.delegate = self;
        
        if(searchHeaderView != nil){
            searchHeaderView?.textField.delegate = self
            searchHeaderView?.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            if(viewType == VIEW_TYPE.COINS_COUNT){
               self.searchbarHeightConstraint.constant = 0
                self.searchHeaderView?.isHidden = true;
            }else{
                self.searchbarHeightConstraint.constant = 50.0
                self.searchHeaderView?.isHidden = false;
            }
        }
        self.popularSearches(show: false)
        
        
        if(userIsLoggedIn() && viewType == VIEW_TYPE.SEARCH){
            searchHeaderView?.textField.becomeFirstResponder();
            self.getPopularSearchesTags()
        }else{
            self.refreshView(tagSelected: false, tag: nil, searchText: nil)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(userIsLoggedIn()){
            self.requestedCoins()
        }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(collectionView ==  self.searchCollectionView){
            return popularSearches.count;
        }else{
            return coinsList.count;
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if(collectionView == searchCollectionView){
            //CKBaseCVCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKBaseCVCell",for:indexPath) as! CKBaseCVCell
            let tag = popularSearches[indexPath.row]
            cell.titleLabel.text = tag.tag;
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = searchFont
            cell.titleLabel.clipsToBounds = true;
            cell.titleLabel.layer.cornerRadius = 10.0
            cell.titleLabel.backgroundColor = CKColor.tableviewCellColor2;
            //cell.titleLabel.layer.borderColor = CKColor.titleGreyColor.cgColor
            //cell.titleLabel.layer.borderWidth = 1.0
            return cell;
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKCoinCVCell",for:indexPath) as! CKCoinCVCell
            let coinL: Coin?
            coinL = self.coinsList[indexPath.row];
            cell.updateCoinView(coinL: coinL!)
            //explore
            cell.actionBtn.tag = indexPath.row
            self.exploreActionBtnView(coinL: coinL, actionBtn: cell.actionBtn)
            if(self.selectedCoin != nil && self.requestedCoinIds.contains((self.selectedCoin?.coin)!)){
                self.setButtonRequested(button: cell.actionBtn, requested: true)
            }
            cell.actionBtn.addTarget(self, action:  #selector(CKExploreVC.actionBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
            
            return cell;
            
        }
    }
    
    
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == self.searchCollectionView){
            let tag = self.popularSearches[indexPath.row];
            self.refreshView(tagSelected: true, tag: tag.tag, searchText: "")
        }else{
            let coin = self.coinsList[indexPath.row]
            if(self.selectedCoin != nil && self.requestedCoinIds.contains((self.selectedCoin?.coin)!)){
                coin.coinRequested = true;
            }else{
                coin.coinRequested = false;
            }
            self.coinProfileVCCalled(selectedCoin: coin)
        }
        
        /*
         let coinL: Coin?
         if(segmentControl.selectedSegmentIndex == 0){
         let sale = self.salesList[indexPath.row]
         coinL = getSaleCoin(sale: sale)
         }else{
         coinL = self.coinsList[indexPath.row]
         //coinL = getAuctionCoin(auction: auction)
         }
         //let cell = collectionView.cellForItem(at: indexPath) as! CKCoinCVCell
         self.showAction(userCollection: false, marketplace: true, coin: coinL)*/
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == self.searchCollectionView){
            let tag = popularSearches[indexPath.row]
            let width = widthWithConstrainedHeight(35, font: searchFont , string: tag.tag)
            return CGSize(width: width, height: 35)
        }else{
            return getCoinCellSize()
        }
    }
    
    
    func widthWithConstrainedHeight(_ height: CGFloat, font: UIFont, string: String?) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = string!.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.width + 15
    }
    
    @objc func actionBtnClickedMethod(button: UIButton) {
         let coin = self.coinsList[button.tag]
         self.requestActionBtnClickedMethod(coin: coin, button: button)
    }
  
    @objc func textFieldDidChange(_ textField: UITextField) {
        if(textField.text?.count == 0){
            self.refreshView(tagSelected: false, tag: nil, searchText: textField.text)
        }
    }
    
    //MARK: TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        self.refreshView(tagSelected: false, tag: nil, searchText: textField.text)
        //refreshMarketplaceView()
        return true;
    }
    
    
    func popularSearches(show: Bool){
        self.view.layoutIfNeeded()
        if(show == true){
            let heightL = (self.popularSearches.count > 3) ? 90.0 : 50.0
            self.popularSearchesHeightConstraint.constant = CGFloat(heightL);
        }else{
            self.popularSearchesHeightConstraint.constant = 0.0;
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
    
    func refreshView(tagSelected: Bool, tag: String?, searchText: String?) {
        self.coinsList.removeAll()
        self.collectionView.reloadData()
        if(tagSelected){
            searchHeaderView?.textField.text = "";
        }
        self.coinSearchServiceMethod(tag: tag);
    }
    
     func coinSearchServiceMethod(tag: String?) {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["user"] = "";
        params["status"] = "active";
        
        params["flagged"] = true;
        params["nsfw"] = true;
        params["search"] = searchHeaderView?.textField.text ?? ""
        params["showcase"] = "sort"
        params["sort"] =  sort ?? CKConstants.FILTER_TYPE.MOST_RECENT.description
        params["minted"] = true
        params["tag"] = tag ?? ""
        
        if(selectedCoin != nil){
            params["batched"] = false
            params["batch"] = selectedCoin?.batch ?? ""
        }else{
            params["batched"] = true
            
            
        }
        //params["count"] = 5
        let path = CKServiceConstants.COIN_URL;
        
        let cks = CKGetService()
        showSearchView(self.view, searchText: "")
        
        cks.getCoinsList(params: params, path: path, responseHandler: { response  in
            self.coinSearchCommonHandler(response.coins)
        }, cacheHandler: { response  in
            self.coinSearchCommonHandler(response.coins)
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    //No result for crypto
  
    func coinSearchCommonHandler(_ coins: [Coin]) {
        self.coinsList = coins;
        
        if(self.coinsList.count == 0){
            showNoDataTextView(text: "We found 0 coins")
        }else{
            self.hideNoDataTextView()
        }
        
        self.collectionView.reloadData();
        self.hideSearchView();
    }
    
    
    func getPopularSearchesTags(){
        self.popularSearches.removeAll()
        let cks = CKGetService()
        
        func searchCommonH(response: CKCoinsResponseObject){
            self.popularSearches = response.tags;
            let showL = (self.popularSearches.count != 0 && userIsLoggedIn()) ? true : false
            self.popularSearches(show: showL)
            self.searchCollectionView.reloadData();
        }
        
        
        cks.getPopluarTags(responseHandler: { response in
            searchCommonH(response: response)
        }, cacheHandler: { response in
            searchCommonH(response: response)
        }, errorHandler: { error in })
        
    }
    
    
   
    
   
    
}




