//
//  CKExploreVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 20/06/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKExploreVC: CKExploreCommonVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var collectionTableview: UITableView!
    
    var mostLiked = [Coin]()
    var recentlyMinted = [Coin]() //-created
    var mostCirculated = [Coin]()
   
    var mostLikedCV: UICollectionView?
    var recentlyMintedCV: UICollectionView?
    var mostCirculatedCV: UICollectionView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarStyle?.delegate = self;
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewAppearMethods()
    }
    
    
    func viewAppearMethods(){
        self.navigationController?.navigationItem.rightBarButtonItem = nil;
        self.navigationController?.navigationItem.leftBarButtonItem = nil;
        if(CKGlobalVariables.sharedManager.loggedInToken == "")
        {
            navBarStyle?.showNavigationSearchIconAndUserBarItem(self, navTitle: NSLocalizedString("ck_explore", comment: ""))
        }else{
            navBarStyle?.showNavigationSearchIconAndSideMenuBarItem(self, navTitle: NSLocalizedString("ck_explore", comment: ""))
        }
        if(userIsLoggedIn()){
            self.requestedCoins()
        }
        self.coinMarketplaceServiceMethod(filterType: CKConstants.FILTER_TYPE.MOST_LIKED)
        self.coinMarketplaceServiceMethod(filterType: CKConstants.FILTER_TYPE.MOST_RECENT)
        self.coinMarketplaceServiceMethod(filterType: CKConstants.FILTER_TYPE.MOST_CIRCULATED)
    }
    
    
    override func loginSignupSuccessMethod(){
        self.viewAppearMethods()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return coinCellHeight;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKCollectionTVCell") as! CKBaseTVCell
        
        cell.baseCollectionView.delegate = self
        cell.baseCollectionView.dataSource = self
        self.registerCoinCollectionViewCell(cell.baseCollectionView)
        
        if(cell.rightsideBtn != nil){
            cell.rightsideBtn.setFAIcon(icon: .FAAngleRight, iconSize: 20.0, forState: UIControlState.normal)
            cell.rightsideBtn.setFATitleColor(color: CKColor.titleGreyColor)
        }
        if(cell.leftsideBtn != nil){
            cell.leftsideBtn.setFAIcon(icon: .FAAngleLeft, iconSize: 20.0, forState: UIControlState.normal)
            cell.leftsideBtn.setFATitleColor(color: CKColor.titleGreyColor)
        }
        
        cell.rightsideBtn.tag = indexPath.section
        cell.leftsideBtn.tag = indexPath.section
        if(indexPath.section == 0){
            self.mostLikedCV = cell.baseCollectionView
        }else if(indexPath.section == 1){
            self.recentlyMintedCV = cell.baseCollectionView
        }else{
            self.mostCirculatedCV = cell.baseCollectionView
        }
        
       
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
        cell.contentView.backgroundColor = CKColor.tableviewCellColor2
        cell.titleLabel.font = CKTheme.regularFont(17.0)
        cell.titleLabel.textColor = CKColor.darkGrayColor
        if(section == 0){
            cell.titleLabel.text = "Popular"
        }else if(section == 1){
            cell.titleLabel.text = "Recently Minted"
        }else{
            cell.titleLabel.text = "Most Circulated"
        }
        cell.rightsideBtn.addTarget(self, action:  #selector(CKExploreVC.rightBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
        cell.rightsideBtn.tag = section;
        cell.rightsideBtn.setFAIcon(icon: .FAAngleRight, forState: UIControlState.normal)
        cell.rightsideBtn.setTitleColor(CKColor.darkGrayColor, for: UIControlState.normal)
        cell.rightsideBtn.setFAIcon(icon: .FAAngleRight, iconSize: 20.0, forState: UIControlState.normal)
        return cell.contentView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45;
    }
    
    @objc func rightBtnClickedMethod(button: UIButton) {
        if(button.tag == 0){
            self.coinSearchVCCalled(selectedCoin: nil, navTitle: "Popular", sort: CKConstants.FILTER_TYPE.MOST_LIKED.description, viewType: CKExploreSearchVC.VIEW_TYPE.SORT_TYPE)
        }else if(button.tag == 1){
            self.coinSearchVCCalled(selectedCoin: nil, navTitle: "Recently Minted", sort: CKConstants.FILTER_TYPE.MOST_RECENT.description, viewType: CKExploreSearchVC.VIEW_TYPE.SORT_TYPE)
        }else{
            self.coinSearchVCCalled(selectedCoin: nil, navTitle: "Most Circulated", sort: CKConstants.FILTER_TYPE.MOST_CIRCULATED.description, viewType: CKExploreSearchVC.VIEW_TYPE.SORT_TYPE)
        }
        
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(collectionView ==  self.mostLikedCV){
            return mostLiked.count;
        }else if(collectionView == self.recentlyMintedCV){
            return recentlyMinted.count;
        }else{
            return mostCirculated.count;
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKCoinCVCell",for:indexPath) as! CKCoinCVCell
        let coinL: Coin?
        if(collectionView ==  self.mostLikedCV){
            coinL = mostLiked[indexPath.row];
            
            cell.actionBtn.tag = (0 * 1000) + indexPath.row
            
       }else if(collectionView == self.recentlyMintedCV){
            coinL = recentlyMinted[indexPath.row];
            cell.actionBtn.tag = (1 * 1000) + indexPath.row
        }else{
            coinL = mostCirculated[indexPath.row];
            cell.actionBtn.tag = (2 * 1000) + indexPath.row
        }
        cell.updateCoinView(coinL: coinL!)
        //explore
        self.exploreActionBtnView(coinL: coinL, actionBtn: cell.actionBtn)
        cell.actionBtn.addTarget(self, action:  #selector(CKExploreVC.actionBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
        
        return cell;
    }
    
   
    
    @objc func actionBtnClickedMethod(button: UIButton) {
        let row = button.tag % 1000
        let section = button.tag / 1000
   
        var coin: Coin?
        if(section == 0){
            coin = self.mostLiked[row];
            
        }else if(section == 1){
            coin = self.recentlyMinted[row];
        }
        else{
            coin = self.mostCirculated[row];
        }
        print(coin?.coinName ?? "");
        self.requestActionBtnClickedMethod(coin: coin!, button: button)
    }
   
    
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let coinL: Coin?
        if(collectionView ==  self.mostLikedCV){
            coinL = mostLiked[indexPath.row];
            
        }else if(collectionView == self.recentlyMintedCV){
            coinL = recentlyMinted[indexPath.row];
        }else{
            coinL = mostCirculated[indexPath.row];
        }
        
        if(self.requestedCoinIds.contains((coinL?.coin)!) == true){
            coinL?.coinRequested = true;
        }else{
            coinL?.coinRequested = false;
        }
        
        
        if(coinL?.coins != nil && (coinL?.coins)! > 1){
            self.coinSearchVCCalled(selectedCoin: coinL, navTitle: coinL?.coinName, sort: CKConstants.FILTER_TYPE.COIN.description, viewType: CKExploreSearchVC.VIEW_TYPE.COINS_COUNT)
            
        }else{
            self.coinProfileVCCalled(selectedCoin: coinL!)
            //showActionsheet(userCollection: userCollection, marketplace: marketplace, coin: coin)
        }
     }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCoinCellSize()
        
    }
    
  
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
    }
    
   
    
    func refreshMarketplaceView(filterType: CKConstants.FILTER_TYPE) {
        self.recentlyMinted.removeAll()
        self.mostCirculated.removeAll()
        self.mostLiked.removeAll()
        self.collectionView.reloadData()
        coinMarketplaceServiceMethod(filterType: filterType)
    }
    
    
    func coinMarketplaceServiceMethod(filterType: CKConstants.FILTER_TYPE) {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["user"] = "";
        params["status"] = "active";
        params["batched"] = true
        params["flagged"] = true;
        params["nsfw"] = true;
        params["search"] = searchHeaderView?.textField.text ?? ""
        params["showcase"] = "sort"
        params["sort"] = filterType.description //selectedFilter ?? "-created"
        params["minted"] = true
        params["count"] = 20
       
        let path = CKServiceConstants.COIN_URL;
        self.coinServiceMethod(params: params, path: path, filterType: filterType)
    }
    
    
    func coinServiceMethod(params: [String: Any], path: String, filterType: CKConstants.FILTER_TYPE) {
       
        let cks = CKGetService()
        showSearchView(self.view, searchText: "")
       
        cks.getCoinsList(params: params, path: path, responseHandler: { response  in
            
           self.coinExploreCommonHandler(response.coins, filterType: filterType)
           
        }, cacheHandler: { response  in
            self.coinExploreCommonHandler(response.coins, filterType: filterType)
            
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    func coinExploreCommonHandler(_ coins: [Coin], filterType: CKConstants.FILTER_TYPE) {
        if(filterType == CKConstants.FILTER_TYPE.MOST_LIKED){
            self.mostLiked = coins;
            if(mostLikedCV != nil){
                self.mostLikedCV?.reloadData()
            }
        }
        else if(filterType == CKConstants.FILTER_TYPE.MOST_CIRCULATED){
            self.mostCirculated = coins
            if(mostCirculatedCV != nil){
                self.mostCirculatedCV?.reloadData()
            }
        }
        else{
            self.recentlyMinted = coins
            if(recentlyMintedCV != nil){
                self.recentlyMintedCV?.reloadData()
            }
        }
        self.hideSearchView()
    }
}



extension CKExploreVC: UINavigationBarDelegate{
    
    func searchIconClickedDelegateMethod() {
        self.coinSearchVCCalled(selectedCoin: nil, navTitle: NSLocalizedString("ck_explore", comment: ""), sort: CKConstants.FILTER_TYPE.MOST_RECENT.description, viewType: CKExploreSearchVC.VIEW_TYPE.SEARCH)
    }
    
    func userIconBtnClickedDelegateMethod() {
        self.loginViewControllerCalled(login: true)
    }
    
}

