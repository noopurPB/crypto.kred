//
//  CKFilterCoinCollectionVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CKFilterCoinCollectionVCDelegate{
    @objc optional func filterOptionSelectedDelegateMethod(selectedType: String?)
}


class CKFilterCoinCollectionVC: CKBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: CKFilterCoinCollectionVCDelegate?
    
    var filterType: [String] =       [  "Most Recent",
                                        "Least Recent",
                                        "Highest Value",
                                        "Lowest Value",
                                        "Most Circulated",
                                        "Least Circulated",
                                        "Most Liked",
                                        "Least Liked"]
    
   /*var filterAuctionType: [String] = [  "Ending Soonest",
                                         "Ending Latest",
                                         "Most Circulated",
                                         "Least Circulated",
                                         "Most Liked",
                                         "Least Liked" ]*/
    
   
    var selectedFilter: String?
    var saleTypeFilter: Bool?
    
    @IBOutlet weak var filterHeightConstraint: NSLayoutConstraint!
    var tableViewCellHeight: CGFloat = 40;
    var tableViewHeaderHeight: CGFloat = 45;
    var saleTypeCount: Int = 2;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(selectedFilter == nil){
            selectedFilter = getFilterKey(row: 0)
            self.tableView.reloadData();
        }
        self.view.isOpaque = false
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    
        self.tableView.clipsToBounds = true
        self.tableView.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        
        if(saleTypeFilter == true){
            self.filterHeightConstraint.constant = (tableViewCellHeight*CGFloat(saleTypeCount)) + tableViewHeaderHeight;
        }else{
            self.filterHeightConstraint.constant = (tableViewCellHeight*CGFloat(filterType.count)) + tableViewHeaderHeight;
        }
    }
    
    
    
    func getFilterKey(row: Int) -> String{
        var key: String?
        /*if(auctionTypeFilter == true){
            key = filterAuctionType[row];
        }else{
            key = filterType[row];
        }*/
        key = filterType[row];
        return getFilterValue(title: key!);
    }
    
    
    func getFilterValue(title: String) -> String{
        switch title {
        case "Most Recent":
            return CKConstants.FILTER_TYPE.MOST_RECENT.description
            
        case "Least Recent":
            return CKConstants.FILTER_TYPE.LEAST_RECENT.description
            
        case "Lowest Value":
            return CKConstants.FILTER_TYPE.LOWEST_VALUE.description
            
        case "Highest Value":
            return CKConstants.FILTER_TYPE.HIGHEST_VALUE.description
            
        case "Most Circulated":
            return CKConstants.FILTER_TYPE.MOST_CIRCULATED.description
            
        case "Least Circulated":
            return CKConstants.FILTER_TYPE.LEAST_CIRCULATED.description
            
        case "Most Liked":
            return CKConstants.FILTER_TYPE.MOST_LIKED.description
            
        case "Least Liked":
            return CKConstants.FILTER_TYPE.LEAST_LIKED.description
            
        case "Ending Latest":
            return CKConstants.FILTER_TYPE.ENDING_LATEST.description
            
        case "Ending Soonest":
            return CKConstants.FILTER_TYPE.ENDING_SOONEST.description
            
        default:
            return CKConstants.FILTER_TYPE.ELSE.description;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewCellHeight;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(saleTypeFilter == true){
            return saleTypeCount;
        }else{
            return filterType.count;
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
        cell.titleLabel.font = Theme.regularFont(14.0)
        cell.titleLabel.textColor = UIColor.black
        cell.selectionStyle = UITableViewCellSelectionStyle.blue
        
        let title = filterType[indexPath.row];
        if(selectedFilter == getFilterValue(title: title)){
            cell.rightsideBtn.isHidden = false
        }else{
            cell.rightsideBtn.isHidden = true
        }
        cell.titleLabel.text = title
       
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedFilter = getFilterKey(row: indexPath.row)
        self.tableView.reloadData()
        
        self.dismiss(animated: true, completion: {
            self.delegate?.filterOptionSelectedDelegateMethod!(selectedType: self.selectedFilter)
        })
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKFilterTVCell") as! CKHeadingTVCell
        cell.headingLbl1.text = NSLocalizedString("filter_sort_heading", comment: "")
        cell.baseDelegate = self;
        cell.contentView.backgroundColor = Color.createGrabColor
        cell.dividerView.isHidden = true
        cell.headingLbl1.font = Theme.mediumFont(16.0)
        cell.headingLbl1.textColor = Color.domainRegGreyColor
        cell.rightsideBtn.addTarget(self, action:  #selector(CKFilterCoinCollectionVC.rightBtnClickedMethod), for: UIControlEvents.touchUpInside)
       
        return cell.contentView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return tableViewHeaderHeight;
    }
    
    @objc func rightBtnClickedMethod() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension CKFilterCoinCollectionVC: CKBaseTVCellDelegate{
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
