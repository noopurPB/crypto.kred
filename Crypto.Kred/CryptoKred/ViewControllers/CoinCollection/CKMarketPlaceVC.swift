//
//  CKMarketPlaceVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation




class CKMarketPlaceVC: CKCommonCollectionVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {

    @IBOutlet weak var searchHeightConstraint: NSLayoutConstraint!
    var salesList = [Sale]()
    var selectedCoin: Coin?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if(selectedCoin == nil){
            navBarStyle?.showNavigationFilterAndSideMenuBarItem(self, navTitle: "Marketplace")
            self.searchHeightConstraint.constant = 50.0
        }else{
            navBarStyle?.showNavigationBackBarItem(self, navTitle: selectedCoin?.coinName ?? "")
            self.searchHeightConstraint.constant = 0.0
        }
         if(searchHeaderView != nil){
            searchHeaderView?.textField.delegate = self
            if(selectedCoin != nil){
                searchHeaderView?.isHidden = true;
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshMarketplaceView();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    @IBAction func segmentControlValueChanged(_ sender: Any) {
        self.refreshMarketplaceView()
        if(segmentControl.selectedSegmentIndex == 0){
            saleTypeFilter = true
        }else{
            saleTypeFilter = false
        }
    }
    
    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        if(loadmore == true){
            return salesList.count + 1
        }else{
            return salesList.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let loadIndex = salesList.count;
        
        if(indexPath.row == loadIndex){
            let pageId = CKGlobalMethods.getPageId(loadIndex)
            if(pageId != 1){
                self.coinMarketplaceServiceMethod();
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKLoadMoreCVCell",for:indexPath) as! CKLoadMoreCVCell
            return cell;
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKCoinCVCell",for:indexPath) as! CKCoinCVCell
        let coinL: Coin?
        let sale = self.salesList[indexPath.row]
        coinL = getSaleCoin(sale: sale)
        cell.updateCoinView(coinL: coinL!)
        
        if(coinL?.coins != nil && (coinL?.coins)! > 1){
            cell.timerLabel.isHidden = false;
            cell.timerLabel.text = String(format: "%d FOR SALE", coinL?.coins ?? 0)
            cell.timerLabelWidthConstraint.constant = 90.0
        }
        self.marketplaceActionBtn(sale: coinL!, cell: cell)
        cell.actionBtn.tag = indexPath.row;
        cell.actionBtn.addTarget(self, action:  #selector(CKMarketPlaceVC.actionBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
        
        return cell;
    }
    
  
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let sale = self.salesList[indexPath.row]
        let coinL = getSaleCoin(sale: sale)
        if(coinL.coins != nil && (coinL.coins)! > 1){
            self.marketPlaceCalledVC(animation: true, selectedCoin: coinL)
            
        }else{
            self.coinProfileVCCalled(selectedCoin: coinL)
            //showActionsheet(userCollection: userCollection, marketplace: marketplace, coin: coin)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCoinCellSize()
    }
    
    
    func marketplaceActionBtn(sale: Coin, cell: CKCoinCVCell){
        var maxString:String?
        var minString:String?
        
        if sale.min_price != nil {
            let isIntegerMinP = sale.min_price?.truncatingRemainder(dividingBy: 1) == 0
            if(isIntegerMinP){
                minString = String(format: "%0.0f", sale.min_price!)
            }else{
                minString = String(format: "%0.1f", sale.min_price!)
            }
        }
        
        if sale.max_price != nil {
            let isIntegerMaxP = sale.max_price?.truncatingRemainder(dividingBy: 1) == 0
            if(isIntegerMaxP){
                maxString = String(format: "%0.0f", sale.max_price!)
            }else{
                maxString = String(format: "%0.1f", sale.max_price!)
            }
        }
        var amount: String?
        if(sale.max_price == nil || sale.min_price == sale.max_price){
            amount = String(format: "BUY %@%@", minString! ,NSLocalizedString("kred_name", comment: ""))
            cell.actionBtnWidthConstraint.constant = 120
        }else{
            amount = String(format: "BUY %@ TO %@%@", minString!, maxString! ,NSLocalizedString("kred_name", comment: ""))
            cell.actionBtnWidthConstraint.constant = 160
        }
        cell.actionBtn.setFAText(prefixText: "", icon: .FATag, postfixText: amount!, size: cell.actionBtnFontSize, forState: UIControlState.normal)
        cell.actionBtn.setFATitleColor(color: CKColor.themeAnotherColor)
        //cell.actionBtn.addTarget(self, action:  #selector(CKCommonCollectionVC.marketplaceActionBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
    }
    
    
    @objc func actionBtnClickedMethod(button: UIButton) {
        //buy coin
    }
    
    
    func getSaleCoin(sale: Sale) -> Coin{
        let coinL = sale.coin!
        coinL.value =  sale.price;//String(format: "%d", sale.price ?? 0);
        coinL.user = sale.user;
        coinL.type = sale.type;
        coinL.max_price = sale.max_price;
        coinL.min_price = sale.min_price;
        coinL.auctionCreated = sale.end;
        return coinL;
    }
    
   
    //MARK: TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
    func textFieldDidEndEditing(_ textField: UITextField) {}
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        refreshMarketplaceView()
        return true;
    }
    
    
    override func filterSelectedMethodCalled() {
        refreshMarketplaceView();
    }
    
    
    
    func refreshMarketplaceView() {
        self.salesList.removeAll()
        self.coinsList.removeAll()
        self.collectionView.reloadData()
        saleTypeFilter = true;
      
        coinMarketplaceServiceMethod()
    }
    
    
    func coinMarketplaceServiceMethod() {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["user"] = "";
        params["status"] = "active";
        
        params["flagged"] = true;
        params["nsfw"] = true;
        params["search"] = searchHeaderView?.textField.text ?? ""
        params["showcase"] = "sort"
        if(selectedCoin != nil){
            params["sort"] = CKConstants.FILTER_TYPE.COIN.description
            params["batch"] = selectedCoin?.batch ?? 0
            params["batched"] = false
        }else{
            params["sort"] = selectedFilter ?? "-created"
            params["batched"] = true
        }
      
        var path = "";
        path = CKServiceConstants.COIN_MARKET_URL;
        self.coinServiceMethod(params: params, path: path)
    }
    
    
    
    func coinServiceMethod(params: [String: Any], path: String) {
        let pageId: Double = CKGlobalMethods.getPageId(self.salesList.count)
        let cks = CKGetService()
        if(pageId == 1){
            showSearchView(self.view, searchText: "")
        }
        
        
        cks.getCoinsList(params: params, path: path, responseHandler: { response  in
             self.loadmore = self.loadMoreSalesWebservice(self.salesList)
           
            if(pageId == 1){
                self.segment0CommonHandler(response.sales)
                
            }else{
                self.segment0CommonHandler(self.salesList + response.sales)
            }
        }, cacheHandler: { response  in
            if(pageId == 1){
                self.segment0CommonHandler(response.sales)
                
                
            }
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    
    func segment0CommonHandler(_ sales: [Sale]){
        self.hideSearchView();
        self.salesList = sales;
        self.collectionView.reloadData();
    }
    
    
    
    func loadMoreSalesWebservice(_ sales: [Sale]) -> Bool{
        let pageIdLocal = CKGlobalMethods.getPageId(sales.count)
        if(sales.count == 0 || CKGlobalMethods.loadMoreWebservice(pageIdLocal) == false){
            return false
        }else{
            return true
        }
    }
    
  
    
}


