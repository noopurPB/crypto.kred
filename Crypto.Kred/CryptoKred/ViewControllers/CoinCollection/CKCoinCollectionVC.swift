//
//  CKCoinCollectionVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKCoinCollectionVC: CKCommonCollectionVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UITextFieldDelegate{
    
    var userCollection: Bool?
    
    var selectedCoin: Coin?
    @IBOutlet weak var searchHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var giveAwayHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var giveawayRequestLinkBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
        coinUserCollectionServiceMethod();
        if(selectedCoin == nil){
            self.giveawayRequestLinkBtn.isHidden = true;
            self.giveAwayHeightConstraint.constant = 0.0
          navBarStyle?.showNavigationFilterAndSideMenuBarItem(self, navTitle: NSLocalizedString("my_collection", comment: ""))
            self.searchHeightConstraint.constant = 50.0
            self.giveAwayHeightConstraint.constant = 0.0
        }else{
            self.showGiveAwayBtn()
            self.giveawayRequestLinkBtn.isHidden = false;
            self.giveAwayHeightConstraint.constant = 50.0
            self.searchHeightConstraint.constant = 0.0
            navBarStyle?.showNavigationBackBarItem(self, navTitle: selectedCoin?.coinName ?? "")
        }
        if(searchHeaderView != nil){
            searchHeaderView?.textField.delegate = self
            if(selectedCoin != nil){
                searchHeaderView?.isHidden = true;
            }
        }
    }
    
    
    func showGiveAwayBtn(){
        if(giveawayRequestLinkBtn != nil){
            CKTheme.getCKLightButtonStyle(self.giveawayRequestLinkBtn, fontSizeL: 16.0)
            let btnText = NSLocalizedString("giveaway_and_request_links", comment: "") + " "
            self.giveawayRequestLinkBtn.setFATitleColor(color: CKColor.titleGreyColor)
            self.giveawayRequestLinkBtn.setFAText(prefixText: btnText, icon: .FALink, postfixText: "", size: 16.0, forState: UIControlState.normal)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func filterSelectedMethodCalled() {
        refreshView()
        coinUserCollectionServiceMethod();
    }
    
    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(loadmore == true){
            return coinsList.count + 1
        }else{
            return coinsList.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if(indexPath.row == coinsList.count){
           
            let pageId = CKGlobalMethods.getPageId(self.coinsList.count)
            if(pageId != 1){
                //self.grabTrendingServiceMethod()
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKLoadMoreCVCell",for:indexPath) as! CKLoadMoreCVCell
            return cell;
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKCoinCVCell",for:indexPath) as! CKCoinCVCell
        let coin: Coin = getCoin(indexPath: indexPath)
        cell.updateCoinView(coinL: coin)
        
        cell.actionBtn.tag = indexPath.row
        cell.actionBtn.setTitle("GIVE", for: UIControlState.normal)
        cell.actionBtn.addTarget(self, action:  #selector(CKCoinCollectionVC.actionBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
        return cell;
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize{
        if(selectedCoin == nil){
            return CGSize(width: self.view.frame.width, height: 45)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        //1
        switch kind {
        //2
        case UICollectionElementKindSectionHeader:
            //3
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "CKHeaderCVCell",
                                                                             for: indexPath) as! CKHeaderCVCell
            headerView.backgroundColor = Color.createGrabColor
            headerView.titleLabel.font =  Theme.mediumFont(15.0)
            headerView.titleLabel.textColor = Color.domainRegGreyColor
            
            headerView.subTitleLabel.font =  Theme.mediumFont(10.0)
            headerView.subTitleLabel.textColor = Color.domainRegGreyColor
            
            headerView.thirdTitleLabel.font =  Theme.mediumFont(10.0)
            headerView.thirdTitleLabel.textColor = Color.domainRegGreyColor
            
           
            headerView.titleLabel.text = "Currently Held"
            
            if(coinsList.count == 1){
                headerView.subTitleLabel.text = String(format: "%d COIN", self.coinsList.count)
            }else{
                headerView.subTitleLabel.text = String(format: "%d COINS", self.coinsList.count)
            }
            headerView.thirdTitleLabel.text = String(format: "%0.2f %@", self.coinsValue, NSLocalizedString("kred_name", comment: ""));
            
            return headerView
        default:
            //4
            //assert(false, "Unexpected element kind")
            fatalError("Unexpected element kind")
        }
    }
    
    
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row != self.coinsList.count){
            let coinL: Coin = getCoin(indexPath: indexPath)
            if(coinL.coins != nil && (coinL.coins)! > 1){
                self.collectionViewVCCalled(userCollection: false, selectedCoin: coinL)
                
            }else{
                self.coinProfileVCCalled(selectedCoin: coinL)
                //showActionsheet(userCollection: userCollection, marketplace: marketplace, coin: coin)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCoinCellSize()
    }
    
    
    @objc func actionBtnClickedMethod(button: UIButton) {
        //give coin
        let coin = self.coinsList[button.tag]
        
    }
    
    
    //MARK: TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        refreshView()
        coinUserCollectionServiceMethod();
        return true;
        
    }
    
    
    
    func coinUserCollectionServiceMethod() {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["user"] = CKGlobalVariables.sharedManager.loggedInUserId
        params["search"] = searchHeaderView?.textField.text ?? ""
        
        if(selectedCoin == nil){
            params["sort"] = selectedFilter ?? "-created"
            params["batched"] = true
        }else{
            params["batch"] = selectedCoin?.batch ?? 0
            params["sort"] = CKConstants.FILTER_TYPE.COIN.description
        }
        params["count"] = 200;
        params["page"] = 1;
        
        params["minted"] = true
        params["nsfw"] = true
        params["flagged"] = true
        params["hidden"] = true
        
        self.coinCollectionServiceMethod(params: params, path: CKServiceConstants.COIN_URL)
    }
    
    
    func getCoin(indexPath: IndexPath) -> Coin{
        return self.coinsList[indexPath.row];
    }
    
    @IBAction func giveawayRequestLinkClicked(_ sender: UIButton) {
        self.giveawayRequestLinkVC(coinL: self.selectedCoin)
    }
    
}




