//
//  SellCoinSurrenderVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 11/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class SellCoinSurrenderVC: SellCoinCommonVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var surrenderView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var coin: Coin?
    
    @IBOutlet weak var coinSubview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.surrenderView.backgroundColor = UIColor.white;
        self.surrenderView.clipsToBounds = true;
        self.surrenderView.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        self.setValues();
        navBarStyle?.showNavigationBackBarItem(self, navTitle: NSLocalizedString("surrender_this_coin", comment: ""))
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setValues(){
        let xAxis = (self.coinSubview.frame.width - 270)/2
        let yAxis = (self.coinSubview.frame.height - 180)/2
        
        let coinView = CoinView(frame: CGRect(x: xAxis, y: yAxis, width: 270 , height: 180))
        coinView.backgroundColor = UIColor.clear;
        coinView.coin = self.coin;
        self.coinSubview.addSubview(coinView)
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
   
    }
    
    @IBAction func crossBtnClicked(_ sender: UIButton) {
       
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0;
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuymorePriceTVCell") as! BuymorePriceTVCell
        
        let titleArray =  ["Face Value", "10% Fee", "You will Receive"]
        cell.titleLabel.text = titleArray[indexPath.row]
        cell.titleLabel.font = CKTheme.regularFont(15.0)
        cell.priceLabel.font = CKTheme.regularFont(15.0)
        
        if(indexPath.row == 0){
            cell.priceLabel.text = "1 Ckr"
            cell.anotherDividerView.isHidden = false;
            
        }else if(indexPath.row == 1){
            cell.priceLabel.text = "- 0.1 Ckr"
            cell.anotherDividerView.isHidden = true;
        }else {
           cell.priceLabel.text = "0.9 Ckr"
            cell.anotherDividerView.isHidden = true;
            cell.titleLabel.font = CKTheme.boldFont(15.0)
            cell.priceLabel.font = CKTheme.boldFont(15.0)
        }
        return  cell;
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    
}
