//
//  SellCoinDoneVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 11/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class SellCoinDoneVC: SellCoinCommonVC {
    
    @IBOutlet weak var sellHeadingLabel: UILabel!
    @IBOutlet weak var sellPriceLabel: UILabel!

    var coin: Coin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sellHeadingLabel.textColor = CKColor.titleGreyColor;
        self.sellHeadingLabel.font = CKTheme.boldFont(25.0)
        
        self.sellPriceLabel.textColor = CKColor.titleGreyColor;
        self.sellPriceLabel.font = CKTheme.mediumFont(17.0)
        
        self.setAuctionValues();
        //// self.auctionDurationLabel.text = sendTo;
        // self.auctionDurationLabel.text = sendTo;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setAuctionValues(){
        let priceStr = String(format: "Listed Price: %d %@", 2 , NSLocalizedString("kred_name", comment: ""))
        self.sellPriceLabel.text = priceStr;
        
        let xAxis = (self.view.frame.width - 270)/2
        let coinView = CoinView(frame: CGRect(x: xAxis, y: 140, width: 270 , height: 180))
        coinView.backgroundColor = UIColor.clear;
        coinView.coin = self.coin;
        self.view.addSubview(coinView)
    }
    
    
    @IBAction func viewAuctionClicked(_ sender: UIButton) {
        self.auctionCoin()
    }
    
    @IBAction func viewSalesClicked(_ sender: UIButton) {
        self.marketPlaceCalledVC(animation: true, selectedCoin: nil)
    }
    
    
    func auctionCoin(){
        /*var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = mainVC?.auction?.coin?.coin ?? 0;
        params["mode"] = "reverse";
        params["start"] = mainVC?.auction?.start ?? 0;
        params["end"] = mainVC?.auction?.end ?? 0;
        params["maximum"] = mainVC?.auction?.maximum ?? 0;
        params["minimum"] = mainVC?.auction?.minimum ?? 0;
        
        self.showLoadingDialog("")
        
        let cks = CKPostService()
        cks.auctionCoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.auction != nil){
                self.mainVC?.crossBtnClicked();
            }
            
            if(response.error != nil){
                self.showErrorMessage(response.error ?? "")
            }
            
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error);
        })*/
    }
    
    
    
}

