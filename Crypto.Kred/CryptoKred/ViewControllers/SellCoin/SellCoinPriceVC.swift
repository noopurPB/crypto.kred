//
//  SellCoinPriceVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 11/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class SellCoinPriceVC: SellCoinCommonVC , UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    var priceQuantites = Array(1...1000)
    var pricePicker: UIPickerView?
    var currentTextField: UITextField?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detailLabel: UILabel!
    
    var selectedRow: Int?
    var coin: Coin?
    
    @IBOutlet weak var surrenderBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subHeadingLbl.font = Theme.mediumFont(15.0)
        self.subHeadingLbl.textColor = CKColor.themeAnotherColor;
        self.subHeadingLbl.text = String(format: "SUGGESTED PRICE: %.2f %@", (coin?.value)! + 1, NSLocalizedString("kred_name", comment: ""))
        
        if(self.detailLabel != nil){
            self.detailLabel.textColor = CKColor.titleGreyColor;
            self.detailLabel.font = Theme.regularFont(subHeadingLbl_fontsize)
        }
        
        //self.surrenderBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        self.surrenderBtn.titleLabel?.font = CKTheme.regularFont(15.0)
        CKTheme.getCKLightButtonStyle(self.surrenderBtn, fontSizeL: 18.0)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func surrenderCoinClicked(_ sender: UIButton) {
        self.sellcoinSurrenderVC(coinL: self.coin!)
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as! TextFieldTableViewCell
        cell.textField.placeholder = "Price";
        self.initPickerView(cell.textField)
        cell.textField.delegate = self;
        cell.textField.text = self.setPickerValue(row: 0)
        CKTheme.setTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
        return cell;
    }
    
    
    //MARK: Picker
    func initPickerView(_ textField: UITextField){
        if(pricePicker == nil){
            pricePicker = UIPickerView()
            pricePicker!.backgroundColor = UIColor.white
            
            pricePicker!.showsSelectionIndicator = true
            pricePicker!.delegate = self
            pricePicker!.dataSource = self
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: NSLocalizedString("done", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateCoinName2VC.donePickerMethod))
            
            toolBar.setItems([doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            
            textField.inputView = pricePicker;
            textField.inputAccessoryView = toolBar
            
        }
    }
    
    
    @objc func donePickerMethod(){
        self.currentTextField?.resignFirstResponder()
    }
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    
    //MARK:  Picker View Delegate  and Datasource Method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return priceQuantites.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return setPickerValue(row: row);
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.currentTextField?.text = setPickerValue(row: row)
    }
    
    
    func setPickerValue(row: Int) -> String{
        let value = priceQuantites[row];
        self.selectedRow = row;
        return String(format: "%d %@", value, NSLocalizedString("kred_name", comment: ""))
    }
    
    
    
    @IBAction func confirmBtnClicked(_ sender: Any) {
        self.sellCoin();
        
    }
    
    
    func sellCoin(){
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = self.coin?.coin ?? 0;
        params["price"] = priceQuantites[selectedRow!]
        
         
         self.showLoadingDialog("")
         
         let cks = CKPostService()
         cks.sellCoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.coin != nil){
                self.sellcoinDoneVC(coinL: self.coin!);
            }
         
            if(response.error != nil){
                self.showErrorMessage(response.error ?? "")
            }
         
            }, errorHandler: { error in
                self.hideLoadingDialog()
                self.errorHandler(error);
         })
    }
    
    
}

