//
//  SellCoinCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 11/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class SellCoinCommonVC: CKBaseVC {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var subHeadingLbl: UILabel!
    var mainVC: SellCoinMainVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(headingLbl != nil){
            self.headingLbl.textColor = CKColor.titleGreyColor;
            self.headingLbl.font = Theme.boldFont(19.0);
        }
        
        if(dividerView != nil){
            self.dividerView.backgroundColor = CKColor.dividerColor;
        }
        
        if(nextBtn != nil){
            CKTheme.getCKButtonStyle(self.nextBtn, fontSizeL: 18.0)
        }
        
        if(self.subHeadingLbl != nil){
            self.subHeadingLbl.textColor = CKColor.titleGreyColor;
            self.subHeadingLbl.font = Theme.mediumFont(16.0)
        }
        
        navBarStyle?.delegate = self;
        navBarStyle?.showNavigationBackBarItem(self, navTitle: NSLocalizedString("sell_this_coin", comment: ""))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func sellcoinSurrenderVC(coinL: Coin){
        let demoVC = self.storyboard?.instantiateViewController(withIdentifier: "SellCoinSurrenderVC") as? SellCoinSurrenderVC
        demoVC?.coin = coinL;
        self.navigationController?.pushViewController(demoVC!, animated: true)
    }
    
    
    func sellcoinDoneVC(coinL: Coin) {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "SellCoinDoneVC") as?
        SellCoinDoneVC
        controllerObejct?.coin = coinL;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
}

extension SellCoinCommonVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
}
