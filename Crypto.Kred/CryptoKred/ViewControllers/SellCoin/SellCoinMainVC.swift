//
//  SellCoinMainVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 11/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit



class SellCoinMainVC: CKBaseVC{
    
    var pageViewController: UIPageViewController?
    var screenCount = 3;
    
    var controllerObjectFrame: CGRect?
    var currentPageIndex: Int = -1;
    var coin: Coin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controllerObjectFrame = CGRect(x: 3.0, y: self.view.frame.height - 397, width: self.view.frame.width, height: 400)
        createPageViewController()
        setupPageControl()
        NotificationCenter.default.addObserver(self, selector: #selector(SellCoinMainVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SellCoinMainVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func createPageViewController() {
        let pageController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        //pageController.dataSource = self
        pageController.view.frame = controllerObjectFrame! //self.view.frame
        pageController.view.clipsToBounds = true
        pageController.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        pageViewController = pageController
        
        if screenCount > 0 {
            demoNextClicked()
        }
        
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
    }
    
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.clear //Color.themeDarkColor
        appearance.currentPageIndicatorTintColor = UIColor.clear //Color.themeDarkColor
        appearance.backgroundColor = UIColor.clear
    }
    
    
    private func getItemController(_ itemIndex: Int) -> UIViewController? {
        if(itemIndex == 0){
            return sellcoinPriceVC();
        }
        else if(itemIndex == 1){
            return sellcoinDoneVC();
        }else{
            return sellcoinSurrenderVC();
        }
        
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return screenCount;
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    //MARK: Keyboard methods
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //258
        // if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        // {
        var frame = self.pageViewController?.view.frame;
        frame?.origin.y = (pageViewController?.view.frame.origin.y)! - 200;
        self.pageViewController?.view.frame = frame!;
        //}
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        self.pageViewController?.view.frame = controllerObjectFrame!
        // }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Button Method
    
    @objc func crossBtnClicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func demoNextClicked(){
        self.currentPageIndex = currentPageIndex+1;
        let firstController = getItemController(self.currentPageIndex)!
        let startingViewControllers: NSArray = [firstController]
        pageViewController?.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.forward ,animated: false, completion: nil)
    }
    
    
    @objc func surrenderClicked(){
        let firstController = getItemController(2)!
        let startingViewControllers: NSArray = [firstController]
        pageViewController?.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.forward ,animated: false, completion: nil)
    }
    
    
    func sellcoinPriceVC() -> UIViewController {
        let demoVC = self.storyboard?.instantiateViewController(withIdentifier: "SellCoinPriceVC") as? SellCoinPriceVC
        demoVC?.view.clipsToBounds = true
        demoVC?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        demoVC?.nextBtn.addTarget(self, action: #selector(SellCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        demoVC?.crossBtn.addTarget(self, action: #selector(SellCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
        // demoVC?.surrenderBtn.addTarget(self, action: #selector(SellCoinMainVC.surrenderClicked), for: UIControlEvents.touchUpInside)
        demoVC?.mainVC = self;
        return demoVC!
    }
    
    func sellcoinSurrenderVC() -> UIViewController {
        let demoVC = self.storyboard?.instantiateViewController(withIdentifier: "SellCoinSurrenderVC") as? SellCoinSurrenderVC
        demoVC?.view.clipsToBounds = true
        demoVC?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        //demoVC?.nextBtn.addTarget(self, action: #selector(SellCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        demoVC?.crossBtn.addTarget(self, action: #selector(SellCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
        demoVC?.mainVC = self;
        return demoVC!
    }
    
    
    func sellcoinDoneVC() -> UIViewController {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "SellCoinDoneVC") as?
        SellCoinDoneVC
        controllerObejct?.view.clipsToBounds = true
        controllerObejct?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        
        // controllerObejct?.nextBtn.addTarget(self, action: #selector(SellCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        controllerObejct?.crossBtn.addTarget(self, action: #selector(SellCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
        controllerObejct?.mainVC = self;
        controllerObejct?.setAuctionValues();
        return controllerObejct!
    }
}

/*
extension SellCoinMainVC: GiveCoinToVCDelegate{
    func nextBtnClickedDelegateMethod(sendTo: String?) {
        self.sendTo = sendTo
        self.demoNextClicked()
    }
}


extension SellCoinMainVC: GiveCoinChooseVCDelegate{
    func chooseNextBtnClickedDelegateMethod(coin: Coin?) {
        self.coin = coin;
        self.demoNextClicked()
    }
}


extension SellCoinMainVC: GiveCoinMessageVCDelegate{
    
    func giveCoinBtnClickedDelegateMethod() {
        demoNextClicked()
    }
}*/

