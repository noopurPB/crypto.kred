//
//  BuymoreDogeVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class BuymoreDogeVC: BuymoreCommonVC, UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate, BuymoreBuyTVCellDelegate, BuymoreTVCellDelegate{
    
    var priceTxtField: UITextField?
    var ckrTxtField: UITextField?
    var ckr: Float = 1;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Tableview Datasource and Delegate
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return headerViewCellHeight;
        }else if(indexPath.row == 1 ){
            return buyViewCellHeight;
        }else if(indexPath.row == 2 || indexPath.row == 3){
            return priceViewCellHeight;
        }else {
            return 100.0;
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreHeaderTVCell") as! BuymoreHeaderTVCell
            cell.headerTitle.text = "Buy CƘr with Doge";
            cell.headerIconBtn.setImage(UIImage(named: "ck_doge"), for: UIControlState.normal)
            return  cell;
            
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreBuyTVCell") as! BuymoreBuyTVCell
            cell.titleLbl1.text = "DOGE to Spend";
            cell.txtField1.delegate = self;
            cell.txtField2.delegate = self;
            
            addToolBar(textField: cell.txtField1);
            addToolBar(textField: cell.txtField2);
            self.priceTxtField = cell.txtField1;
            self.ckrTxtField = cell.txtField2;
            
            cell.delegate = self;
            
            self.ckrTxtField?.text = String(format: "%.0f", ckr);
            self.priceTxtField?.text = getCaclulatedPrice(ckr: ckr);
            
            return  cell;
            
        }else if(indexPath.row == 2 || indexPath.row == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymorePriceTVCell") as! BuymorePriceTVCell
            if(indexPath.row == 2){
                cell.priceLabel.text =  getCaclulatedPrice(ckr: ckr) + " DOGE"
            }else{
                cell.priceLabel.text = getCaclulatedPrice(ckr: ckr) + " DOGE"
            }
            cell.setValue(row: indexPath.row)
            return  cell;
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreTVCell") as! BuymoreTVCell
            cell.delegate = self
            return  cell;
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func getCaclulatedPrice(ckr: Float) -> String{
        return String(format: "%.4f", ckr * 2185.9070);
    }
    
    func getCaclulatedCkr(price: Float) -> String{
        return String(format: "%.4f", price / 2185.9070);
    }
    
    
    //MARK: BuymoreTVCellDelegate
    
    func payBtnClickedDelegateMethod(sender: UIButton) {
        if(ckrTxtField?.text != ""){
            showErrorMessage("API under implementation")
        }else{
            showErrorMessage(NSLocalizedString("enter_valid_amount", comment: ""))
        }
        
        
        
    }
    
    
    
    //MARK: BuymoreBuyTVCellDelegate
    
    func textFieldDidChangeDelegateMethod(sender: UITextField) {
        //if(sender.text?.count != 0){
        if(sender == priceTxtField){
            let price = NSString(string: sender.text!).floatValue
            self.ckrTxtField?.text = getCaclulatedCkr(price: price);
            self.ckr = NSString(string: getCaclulatedCkr(price: price)).floatValue
            
            //self.ckrTxtField?.text = self.priceTxtField?.text;
        }else{
            self.ckr = NSString(string: sender.text!).floatValue
            self.priceTxtField?.text = getCaclulatedPrice(ckr: ckr);
        }
        
        let indexPath1 = IndexPath(row: 2, section: 0)
        let indexPath2 = IndexPath(row: 3, section: 0)
        self.tableView.reloadRows(at: [indexPath1, indexPath2], with: UITableViewRowAnimation.none)
    }
    
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    
}
