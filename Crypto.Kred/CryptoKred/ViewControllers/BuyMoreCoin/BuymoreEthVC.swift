//
//  BuymoreEthVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class BuymoreEthVC: BuymoreCommonVC , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, BuymoreBuyTVCellDelegate, BuymoreTVCellDelegate, CKBaseTVCellDelegate{
    
    //0.0105
    var priceTxtField: UITextField?
    var ckrTxtField: UITextField?
    var ckr: Float = 1;
    var segmentTransferSelected: Bool = false;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return headerViewCellHeight;
        }else if(indexPath.row == 1 ){
            return buyViewCellHeight;
        }else if(indexPath.row == 2 || indexPath.row == 3){
            return priceViewCellHeight;
        }else if(indexPath.row == 4){
            if(segmentTransferSelected == true){
                return 50;
            }else{
                return 100; //150
            }
        }else {
            
            //transfer segment
            if(segmentTransferSelected == true){
                if(indexPath.row == 5){
                    return 90;
                }else{
                    return 130;
                }
                
            }else{
                return 0;
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreHeaderTVCell") as! BuymoreHeaderTVCell
            cell.headerTitle.text = "Buy CƘr with Ether";
            cell.headerIconBtn.setImage(UIImage(named: "ck_ether"), for: UIControlState.normal)
            return  cell;
            
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreBuyTVCell") as! BuymoreBuyTVCell
            cell.titleLbl1.text = "ETH to Spend";
            cell.txtField1.delegate = self;
            cell.txtField2.delegate = self;
            
            addToolBar(textField: cell.txtField1);
            addToolBar(textField: cell.txtField2);
            self.priceTxtField = cell.txtField1;
            self.ckrTxtField = cell.txtField2;
            
            cell.delegate = self;
            
            self.ckrTxtField?.text = String(format: "%.0f", ckr);
            self.priceTxtField?.text = getCaclulatedPrice(ckr: ckr);
            
            return  cell;
            
        }else if(indexPath.row == 2 || indexPath.row == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymorePriceTVCell") as! BuymorePriceTVCell
            if(indexPath.row == 2){
                cell.priceLabel.text =  getCaclulatedPrice(ckr: ckr) + " ETH"
            }else{
                cell.priceLabel.text = getCaclulatedPrice(ckr: ckr) + " ETH"
            }
            cell.setValue(row: indexPath.row)
            return  cell;
        }else if(indexPath.row == 4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreTVCell") as! BuymoreTVCell
             cell.delegate = self
            return  cell;
        }else if(indexPath.row == 6){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
            let img = Barcode.fromString(string: "GBXWATLEYHCDLDKBE54R7WLHIBNWEJ4L2URK66KMFMTIWAMYTZ6VUTG5")
            //let img = generateBarcode(from: "GBXWATLEYHCDLDKBE54R7WLHIBNWEJ4L2URK66KMFMTIWAMYTZ6VUTG5")
            cell.baseImageView.image = img;
            return  cell;
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTextFieldTVCell") as! CKTextFieldTVCell
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = CKTheme.mediumFont(15.0)
            cell.titleLabel.text = "Send " + getCaclulatedPrice(ckr: ckr) + " ETH to this address";
            cell.rightsideBtn.setFAIcon(icon: .FACopy, iconSize: 20.0, forState: UIControlState.normal)
            cell.rightsideBtn.setTitleColor(CKColor.dividerColor, for: UIControlState.normal)
            cell.baseDelegate = self;
            return  cell;
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func getCaclulatedPrice(ckr: Float) -> String{
        return String(format: "%.4f", ckr * 0.0105);
    }
    
    func getCaclulatedCkr(price: Float) -> String{
        return String(format: "%.4f", price / 0.0105);
    }
    
    
    //MARK: BuymoreTVCellDelegate
    
    func payBtnClickedDelegateMethod(sender: UIButton) {
        if(ckrTxtField?.text != ""){
            showErrorMessage("API under implementation")
        }else{
            showErrorMessage(NSLocalizedString("enter_valid_amount", comment: ""))
        }
    }
    
    func segmentControlClickedDelegateMethod(sender: UISegmentedControl){
        self.segmentTransferSelected = !self.segmentTransferSelected;
        self.tableView.reloadData();
    }
    
    //MARK: BuymoreBuyTVCellDelegate
    
    func textFieldDidChangeDelegateMethod(sender: UITextField) {
        //if(sender.text?.count != 0){
        if(sender == priceTxtField){
            let price = NSString(string: sender.text!).floatValue
            self.ckrTxtField?.text = getCaclulatedCkr(price: price);
            self.ckr = NSString(string: getCaclulatedCkr(price: price)).floatValue
            
            //self.ckrTxtField?.text = self.priceTxtField?.text;
        }else{
            self.ckr = NSString(string: sender.text!).floatValue
            self.priceTxtField?.text = getCaclulatedPrice(ckr: ckr);
        }
        
        let indexPath2 = IndexPath(row: 2, section: 0)
        let indexPath3 = IndexPath(row: 3, section: 0)
        let indexPath5 = IndexPath(row: 5, section: 0)
        self.tableView.reloadRows(at: [indexPath2, indexPath3, indexPath5], with: UITableViewRowAnimation.none)
    }
    
    
    
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
}
