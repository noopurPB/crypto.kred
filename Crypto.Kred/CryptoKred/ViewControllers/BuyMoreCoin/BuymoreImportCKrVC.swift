//
//  BuymoreImportCKrVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class BuymoreImportCKrVC: BuymoreCommonVC , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CKBaseTVCellDelegate{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Tableview Datasource and Delegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return headerViewCellHeight;
        }else if(indexPath.row == 2){
            return 120;
        }else{
            return 100;
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreHeaderTVCell") as! BuymoreHeaderTVCell
            cell.headerTitle.text = "Import CƘr from another Wallet";
            cell.headerIconBtn.setImage(UIImage(named: "ck_grab"), for: UIControlState.normal)
            return  cell;
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
            let img = Barcode.fromString(string: "GBXWATLEYHCDLDKBE54R7WLHIBNWEJ4L2URK66KMFMTIWAMYTZ6VUTG5")
            //let img = generateBarcode(from: "GBXWATLEYHCDLDKBE54R7WLHIBNWEJ4L2URK66KMFMTIWAMYTZ6VUTG5")
            cell.baseImageView.image = img;
            return  cell;
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTextFieldTVCell") as! CKTextFieldTVCell
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = CKTheme.regularFont(15.0)
            cell.rightsideBtn.setFAIcon(icon: .FACopy, iconSize: 20.0, forState: UIControlState.normal)
            cell.rightsideBtn.setTitleColor(CKColor.dividerColor, for: UIControlState.normal)
            cell.baseDelegate = self;
            return  cell;
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    //MARK: CKBaseTVCellDelegate
    
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        
    }
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    
}


class Barcode {
    
    class func fromString(string : String) -> UIImage? {
        
        let data = string.data(using: .ascii)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        return UIImage(ciImage: (filter?.outputImage)!)
    }
    
}


