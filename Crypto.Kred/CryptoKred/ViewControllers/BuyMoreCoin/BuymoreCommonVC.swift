//
//  BuymoreCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation


class BuymoreCommonVC: CKBaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    var headerViewCellHeight: CGFloat = 160.0;
    var buyViewCellHeight: CGFloat = 80.0;
    var priceViewCellHeight: CGFloat = 50.0;
    
    var limits: CKLimits?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(tableView != nil){
            self.registerHeaderTVCell(tableView)
            self.registerBuyTVCell(tableView)
            self.registerPriceTVCell(tableView)
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
           // tableView.isScrollEnabled = false;
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func valueToString(value: Float?) -> String{
        return String(format: "%.6f", value ?? 0.0);
    }
    
    
    func getDiscountValue(price: Float, discount: Float) -> Float{
        return (discount/100)*price
    }
    
    
    func getTotal(price: Float, discount: Float) -> Float{
        return price - getDiscountValue(price: price, discount: discount)
    }
    
    
    //MARK: Register UITableViewCell
    func registerHeaderTVCell(_ tableView: UITableView){
        tableView.register(UINib(nibName: "BuymoreHeaderTVCell", bundle: nil), forCellReuseIdentifier: "BuymoreHeaderTVCell")
    }
    
    func registerBuyTVCell(_ tableView: UITableView){
        tableView.register(UINib(nibName: "BuymoreBuyTVCell", bundle: nil), forCellReuseIdentifier: "BuymoreBuyTVCell")
    }
    
    func registerPriceTVCell(_ tableView: UITableView){
        tableView.register(UINib(nibName: "BuymorePriceTVCell", bundle: nil), forCellReuseIdentifier: "BuymorePriceTVCell")
    }
    
    
    //MARK: Keyboard notification
    func showKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(BuymoreCommonVC.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BuymoreCommonVC.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                if(tableView != nil){
                    self.tableView.contentInset = contentInsets
                    self.tableView.scrollIndicatorInsets = contentInsets
                   // self.tableView.isScrollEnabled = true;
                }
            }
        }
    }
    
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let contentIntent = UIEdgeInsetsMake(0,0,0,0);
        if(tableView != nil){
            self.tableView.contentInset = contentIntent
            self.tableView.scrollIndicatorInsets = contentIntent
           // self.tableView.isScrollEnabled = false;
        }
    }
    
    
    func addToolBar(textField: UITextField){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true;
        toolBar.tintColor = CKColor.themeAnotherColor;
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.inputAccessoryView = toolBar
    }
    
    
    @objc func donePressed(){
        self.view.endEditing(true)
    }
    
    @objc func cancelPressed(){
        self.view.endEditing(true) // or do something
    }
    
    
    
    
    
}


