//
//  BuymoreCardVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class BuymoreCardVC: BuymoreCommonVC, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, BuymoreBuyTVCellDelegate, BuymoreTVCellDelegate {
    
    var priceTxtField: UITextField?
    var ckrTxtField: UITextField?
    var price: String = "1";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    //MARK: Tableview Datasource and Delegate
    
   /* func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row == 0){
            return headerViewCellHeight;
        }
        return 200
    }*/
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return headerViewCellHeight;
        }else if(indexPath.row == 1 ){
            return buyViewCellHeight;
        }else if(indexPath.row == 2 || indexPath.row == 3){
            return priceViewCellHeight;
        }else {
            return 150.0;
        }
    }
    
    
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreHeaderTVCell") as! BuymoreHeaderTVCell
            cell.headerTitle.text = "Buy CƘr with a Credit Card";
            cell.headerIconBtn.setImage(UIImage(named: "ck_card"), for: UIControlState.normal)
            return  cell;
            
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreBuyTVCell") as! BuymoreBuyTVCell
            cell.titleLbl1.text = "USD to Spend";
            cell.txtField1.delegate = self;
            cell.txtField2.delegate = self;
            self.priceTxtField = cell.txtField1;
            self.ckrTxtField = cell.txtField2;
            addToolBar(textField: cell.txtField1);
            addToolBar(textField: cell.txtField2);
            cell.delegate = self;
            
            self.priceTxtField?.text = price;
            self.ckrTxtField?.text = price;
            return  cell;
            
        }else if(indexPath.row == 2 || indexPath.row == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymorePriceTVCell") as! BuymorePriceTVCell
            if(indexPath.row == 2){
                cell.priceLabel.text = price + " USD"
            }else{
                cell.priceLabel.text = price + " USD"
            }
            cell.setValue(row: indexPath.row)
            return  cell;
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreTVCell") as! BuymoreTVCell
            CKTheme.getCKButtonStyle(cell.payBtn, fontSizeL: nil)
            cell.titleLabel.font = CKTheme.regularFont(13.0)
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.rightsideBtn.setFAIcon(icon: .FACcStripe, iconSize: 40.0, forState: UIControlState.normal)
            cell.rightsideBtn.setFATitleColor(color: CKColor.dividerColor, forState: .normal);
            cell.delegate = self;
            //cell.rightsideBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
           // cell.rightsideBtn.set
            //cell.rightsideBtn.set
            
            return  cell;
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    //MARK: BuymoreTVCellDelegate
    
    func payBtnClickedDelegateMethod(sender: UIButton) {
        if(price != ""){
            showErrorMessage("API under implementation")
        }else{
            showErrorMessage(NSLocalizedString("enter_valid_amount", comment: ""))
        }
        
        
        
    }
    
    //MARK: BuymoreBuyTVCellDelegate
    
    func textFieldDidChangeDelegateMethod(sender: UITextField) {
        //if(sender.text?.count != 0){
            if(sender == priceTxtField){
                self.ckrTxtField?.text = self.priceTxtField?.text;
            }else{
                self.priceTxtField?.text = self.ckrTxtField?.text;
            }
            price = sender.text ?? "";
        /*}else{
            self.ckrTxtField?.text = "1";
            self.priceTxtField?.text = "1";
            self.price = "1";
        }*/
        
        
        
        let indexPath1 = IndexPath(row: 2, section: 0)
        let indexPath2 = IndexPath(row: 3, section: 0)
            self.tableView.reloadRows(at: [indexPath1, indexPath2], with: UITableViewRowAnimation.none)
    }
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    
    
    
}
