//
//  CoinPaymentVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 12/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class CoinPaymentVC: BuymoreCommonVC, WKUIDelegate, WKNavigationDelegate{
    
    @IBOutlet weak var webView: WKWebView!
    var coinparam: CoinPaymentParameters?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarStyle?.delegate = self
        navBarStyle?.showNavigationBackBarItem(self, navTitle: "Coin Payments")
        
        let myURL = URL(string:"https://www.coinpayments.net/index.php")
        
        var request = URLRequest(url: myURL!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
       
        
        
        var params = [String: Any]()
       
        params["cmd"] = "_pay_simple"   //Ckr to buy
        params["reset"] = 1
        params["merchant"] = "aa714a6929c2af5e23860a7302efe1d7"
        params["currency"] = coinparam?.payment_currency
        params["allow_currencies"] = coinparam?.payment_currency
        params["amountf"] = coinparam?.payment_amount
        params["item_name"] =  "Buyu More Ckr"
        params["item_desc"] = String(format: "%0.6f ", (coinparam?.ckr)!) + NSLocalizedString("kred_name", comment: "")
        params["invoice"] = coinparam?.invoice
        params["success_url"] = "https://app.crypto.kred/buy?purchase=1"
        
        let postString = convertParamsToString(params)
        
         
       // let postString = reset=1&merchant=&currency=DOGE&allow_currencies=DOGE&amountf=206.18556701&item_name=BuyMore CƘr&invoice=zwsfcjaoajfkjf6u6vkfiryhse&item_desc=1CƘr"
        
        request.httpBody = postString.data(using: .utf8)
        webView.navigationDelegate =  self;
        webView.uiDelegate = self
        webView.load(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_: WKWebView, didStartProvisionalNavigation: WKNavigation!){
        self.showSearchView(self.webView, searchText: "")
    }
    
    func webView(_: WKWebView, didFailProvisionalNavigation: WKNavigation!, withError: Error){
        self.hideSearchView()
    }
    
    

       
    
    
    func convertParamsToString(_ params: [String: Any])-> String{
        var paramString = ""
        for(key, value) in params{
            paramString = paramString + key + "=" + String(describing: value) + "&"
        }
        return paramString
    }
    
    

}


extension CoinPaymentVC:NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

class CoinPaymentParameters{
    
    var payment_currency: String?
    var payment_amount: Float?
    var ckr: Float?
    var invoice: String?
}


