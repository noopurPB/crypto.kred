//
//  BuymoreBuyTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol BuymoreBuyTVCellDelegate{
    @objc optional func textFieldDidChangeDelegateMethod(sender: UITextField)
}


class BuymoreBuyTVCell: CKBaseTVCell {
    
    @IBOutlet weak var titleLbl2: UILabel!
    @IBOutlet weak var titleLbl1: UILabel!
    @IBOutlet weak var txtField2: UITextField!
    @IBOutlet weak var txtField1: UITextField!
    @IBOutlet weak var equalToLbl: UILabel!
   
    var delegate: BuymoreBuyTVCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(equalToLbl != nil){
            self.equalToLbl.textColor = CKColor.themeAnotherColor;
            self.equalToLbl.font = CKTheme.boldFont(20.0)
        }
        
        if(titleLbl1 != nil){
            self.titleLbl1.textColor = CKColor.titleGreyColor;
            self.titleLbl1.font = CKTheme.boldFont(10.0)
        }
        
        if(titleLbl2 != nil){
            self.titleLbl2.textColor = CKColor.titleGreyColor;
            self.titleLbl2.font = CKTheme.boldFont(10.0)
            self.titleLbl2.text =   String(format: "%@ to Buy", NSLocalizedString("kred_name", comment: ""));
        }
        
        if(txtField1 != nil){
            CKTheme.setTextFieldStyle(txtField1, placeholder: txtField1.placeholder!)
            self.txtField1.addTarget(self, action: #selector(BuymoreBuyTVCell.textFieldDidChange(_:)), for: .editingChanged)
            
           // self.txtField1.clipsToBounds = true;
           // self.txtField1.layer.cornerRadius = 10.0;
        }
        
        if(txtField2 != nil){
            CKTheme.setTextFieldStyle(txtField2, placeholder: txtField2.placeholder!)
           self.txtField2.addTarget(self, action: #selector(BuymoreBuyTVCell.textFieldDidChange(_:)), for: .editingChanged)
           
           // self.txtField2.clipsToBounds = true;
           // self.txtField2.layer.cornerRadius = 10.0;
        }
    }
    
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        delegate?.textFieldDidChangeDelegateMethod!(sender: sender);
    }
    
    
    
}


