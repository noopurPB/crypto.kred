//
//  BuymorePriceTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class BuymorePriceTVCell: CKBaseTVCell {
 
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(priceLabel != nil){
            self.priceLabel.textColor = CKColor.titleGreyColor;
        }
        
        if(titleLabel != nil){
            
            self.titleLabel.textColor = CKColor.titleGreyColor;
        }
    }
    
    func setValue(row: Int?){
        if(row == 2){
            titleLabel.text = "Regular Price"
            titleLabel.font = CKTheme.regularFont(15.0)
            priceLabel.font = CKTheme.regularFont(15.0)
        }else if(row == 3){
            titleLabel.text = "Discount"
            titleLabel.font = CKTheme.regularFont(15.0)
            priceLabel.font = CKTheme.regularFont(15.0)
            priceLabel.textColor = CKColor.verifiedGreen
        }else{
            titleLabel.text = "Total"
            titleLabel.font = CKTheme.boldFont(15.0)
            priceLabel.font = CKTheme.boldFont(15.0)
        }
    }
    
}
