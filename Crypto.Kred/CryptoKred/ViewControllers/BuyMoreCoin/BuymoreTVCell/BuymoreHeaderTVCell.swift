//
//  BuymoreHeaderTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class BuymoreHeaderTVCell: CKBaseTVCell {
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerIconBtn: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        if(headerTitle != nil){
            self.headerTitle.textColor = CKColor.titleGreyColor;
            self.headerTitle.font = CKTheme.boldFont(16.0);
        }
    }
}
