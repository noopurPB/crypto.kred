//
//  BuymoreTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


@objc protocol BuymoreTVCellDelegate{
    @objc optional func payBtnClickedDelegateMethod(sender: UIButton)
    @objc optional func segmentControlClickedDelegateMethod(sender: UISegmentedControl)
}

class BuymoreTVCell: CKBaseTVCell, UITextViewDelegate {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var payBtn: UIButton!
    var delegate: BuymoreTVCellDelegate?
    
    
    @IBOutlet weak var metaMaskTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(metaMaskTextView != nil){
            self.metaMaskTextView.delegate = self;
            self.metaMaskTextView.font = CKTheme.regularFont(14.0);
            self.metaMaskTextView.textColor = UIColor.red;
            
            
            let str = "<div style=\"text-align:center;font-size:15px;color:red\">You need to install <a href=\"https://metamask.io\">MetaMask</a> to use this feature.\n<a href=\"https://metamask.io\">https://metamask.io.check</a></div>";
            self.metaMaskTextView.attributedText = str.html2AttributedString;
            
            
        }
        
        if(payBtn != nil){
            self.payBtn.isHidden = false;
        }
        
        if(metaMaskTextView != nil){
            self.metaMaskTextView.isHidden = true;
        }
        
    }
    
    @IBAction func payBtnClicked(_ sender: UIButton) {
        delegate?.payBtnClickedDelegateMethod!(sender: sender)
    }
    
    
    @IBAction func segmentControlValueChanged(_ sender: UISegmentedControl) {
        
        if(sender.selectedSegmentIndex == 0){
            self.payBtn.isHidden = false;
            self.metaMaskTextView.isHidden = true;
        }else if(sender.selectedSegmentIndex == 1){
            self.payBtn.isHidden = true;
            self.metaMaskTextView.isHidden = true;
           
        }else{
            self.payBtn.isHidden = true;
            self.metaMaskTextView.isHidden = false;
            
        }
        delegate?.segmentControlClickedDelegateMethod!(sender: sender);
    
    }
    
    
    //MARK: UITextView Delegate
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if let url = URL(string:UIApplicationOpenSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        return false;
    }
    
    
}

/*
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
*/
