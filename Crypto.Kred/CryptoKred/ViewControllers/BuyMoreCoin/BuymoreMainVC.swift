//
//  BuymoreMainVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit
import Stripe


class BuymoreMainVC: CKBaseVC{
    
    @IBOutlet weak var notifLabel: UILabel!
    @IBOutlet weak var notifView: UIView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var controllerObjectFrame: CGRect?
    var cardVC: BuymoreCardVC?
    var btcVC: BuymoreBtcVC?
    var dogeVC: BuymoreDogeVC?
    var ethVC: BuymoreEthVC?
    var importCrVC: BuymoreImportCKrVC?
    
    var yAxis: CGFloat?
    var limits: CKLimits?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUserLimits()
        navBarStyle?.showNavigationBackBarItem(self, navTitle: "Pay with")
        navBarStyle?.delegate = self;
        initNotifView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initNotifView(){
        self.notifView.isHidden = true;
        self.notifView.backgroundColor = CKColor.verifiedGreen;
        self.notifLabel.textColor = UIColor.white
        self.notifLabel.font = CKTheme.boldFont(15.0)
    }
    
    
    @IBAction func segmentControlClicked(_ sender: UISegmentedControl) {
        removeAllViews();
        if(sender.selectedSegmentIndex == 0){
            //cardVCView();
             btcVCView(paymentType: BuymoreBtcVC.PAYMENT_TYPE.CARD);
        }else if(sender.selectedSegmentIndex == 1){
            btcVCView(paymentType: BuymoreBtcVC.PAYMENT_TYPE.BTC);
        }else if(sender.selectedSegmentIndex == 2){
           // dogeVCView();
            btcVCView(paymentType: BuymoreBtcVC.PAYMENT_TYPE.DOGE);
        }else if(sender.selectedSegmentIndex == 3){
            //ethVCView();
            btcVCView(paymentType: BuymoreBtcVC.PAYMENT_TYPE.ETH);
        }else if(sender.selectedSegmentIndex == 4){
            importCrVCView();
        }
    }
    
    
    
    // MARK: - Button Method
    func cardVCView(){
        cardVC = self.storyboard?.instantiateViewController(withIdentifier: "BuymoreCardVC") as? BuymoreCardVC
        cardVC?.view.frame = controllerObjectFrame!
        cardVC?.limits = self.limits;
        self.view.addSubview((cardVC?.view)!)
    }
   
    func btcVCView(paymentType: BuymoreBtcVC.PAYMENT_TYPE){
        btcVC = self.storyboard?.instantiateViewController(withIdentifier: "BuymoreBtcVC") as? BuymoreBtcVC
        btcVC?.view.frame = controllerObjectFrame!
        btcVC?.paymentType = paymentType;
        btcVC?.limits = limits;
        btcVC?.delegate = self
        self.view.addSubview((btcVC?.view)!)
    }
    
    
    func dogeVCView(){
        dogeVC = self.storyboard?.instantiateViewController(withIdentifier: "BuymoreDogeVC") as? BuymoreDogeVC
        dogeVC?.view.frame = controllerObjectFrame!
        self.view.addSubview((dogeVC?.view)!)
    }
    
    
    func ethVCView(){
        ethVC = self.storyboard?.instantiateViewController(withIdentifier: "BuymoreEthVC") as? BuymoreEthVC
        ethVC?.view.frame = controllerObjectFrame!
        self.view.addSubview((ethVC?.view)!)
    }
    
   
    func importCrVCView(){
        importCrVC = self.storyboard?.instantiateViewController(withIdentifier: "BuymoreImportCKrVC") as? BuymoreImportCKrVC
        importCrVC?.view.frame = controllerObjectFrame!
        self.view.addSubview((importCrVC?.view)!)
    }
    
    
    func removeAllViews(){
        if(cardVC != nil){
            cardVC?.view.removeFromSuperview();
            cardVC = nil;
        }
        if(btcVC != nil){
            btcVC?.view.removeFromSuperview();
            btcVC = nil;
        }
        if(dogeVC != nil){
            dogeVC?.view.removeFromSuperview();
            dogeVC = nil;
        }
        if(ethVC != nil){
            ethVC?.view.removeFromSuperview();
            ethVC = nil;
        }
        
        if(importCrVC != nil){
            importCrVC?.view.removeFromSuperview();
            importCrVC = nil;
        }
    }
    
    
    func getUserLimits(){
        
        //self.showSearchView(self.view, searchText: "")
        self.showLoadingDialog("")
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        let cks = CKGetService()
        cks.getUserLimits(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            self.limits = response.limits;
            
            if(self.limits?.discount != nil){
                
                
                let date = CKGlobalMethods.convertTimestampToDate(timestamp: self.limits?.discount?.expires ?? 0.0, format: "dd MMM")
                
                self.notifLabel.text = String(format: "Congratulations! You are eligible for the %@ %d percent discount until %@",self.self.limits?.discount?.reason ?? "", self.self.limits?.discount?.percent ?? 0,  date )
                self.notifLabel.sizeToFit()
                self.notifView.isHidden = false;
            }else{
                
                self.notifLabel.text = ""
                self.notifLabel.sizeToFit()
                self.notifView.isHidden = true;
                
            }
            self.setViewFrame()
            
        }, errorHandler: { error in
            self.setViewFrame()
             self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    
    func setViewFrame(){
        self.view.layoutIfNeeded()
        self.yAxis = self.notifView.frame.height+130;
        controllerObjectFrame = CGRect(x: 0.0, y: yAxis!, width: self.view.frame.width, height: self.view.frame.height - yAxis!)
        btcVCView(paymentType: BuymoreBtcVC.PAYMENT_TYPE.CARD);
    }
    
    
    func showStripe(){
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        self.navigationController?.pushViewController(addCardViewController, animated: true)
    }
    
   
    
}

extension BuymoreMainVC: STPAddCardViewControllerDelegate {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        StripeClient.shared.completeCharge(with: token, amount: 0) { result in
            switch result {
            // 1
            case .success:
                completion(nil)
                
                let alertController = UIAlertController(title: "Congrats", message: "Your payment was successful!", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true)
            // 2
            case .failure(let error):
                completion(error)
            }
        }
    }
}





extension BuymoreMainVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BuymoreMainVC: BuymoreBtcVCDelegate{
    
    func coinPaymentDelegateMethod(coinparams: CoinPaymentParameters) {
        self.coinPaymentsVCCalled(coinParams: coinparams)
    }
    
    func stripeSDKCalledDelegateMethod(){
        showStripe()
    }
    
}


