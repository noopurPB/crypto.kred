//
//  BuymoreBtcVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation


protocol BuymoreBtcVCDelegate{
    func coinPaymentDelegateMethod(coinparams: CoinPaymentParameters)
    func stripeSDKCalledDelegateMethod()
}


class BuymoreBtcVC: BuymoreCommonVC, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, BuymoreBuyTVCellDelegate, BuymoreTVCellDelegate {
    
    var delegate: BuymoreBtcVCDelegate?
    
    var priceTxtField: UITextField?
    var ckrTxtField: UITextField?
    var ckr: Float = 1;
    
    var btc : Float = 0.000157276;
    var doge: Float = 400.58118135;
    var eth : Float = 0.002214068619727552;
    var card: Float = 1;
    
    enum PAYMENT_TYPE{
        case BTC
        case DOGE
        case ETH
        case CARD
    }
    
    var paymentType: PAYMENT_TYPE?

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return headerViewCellHeight;
        }else if(indexPath.row == 1 ){
            return buyViewCellHeight;
        }else if(indexPath.row == 2 || indexPath.row == 4){
            return priceViewCellHeight;
        }else if(indexPath.row == 3){
            if(limits?.discount != nil){
                return priceViewCellHeight;
            }else{
                return 0
            }
        }
        else {
            return 100.0;
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreHeaderTVCell") as! BuymoreHeaderTVCell
            
            if(paymentType == PAYMENT_TYPE.BTC){
                cell.headerTitle.text = "Buy CƘr with Bitcoin";
                //cell.headerIconBtn.setFAIcon(icon: .FABitcoin, iconSize: 30.0, forState: UIControlState.normal)
                cell.headerIconBtn.setImage(UIImage(named: "ck_btc"), for: UIControlState.normal)
            }else if(paymentType == PAYMENT_TYPE.DOGE){
                cell.headerTitle.text = "Buy CƘr with Doge";
                cell.headerIconBtn.setImage(UIImage(named: "ck_doge"), for: UIControlState.normal)
            }else if(paymentType == PAYMENT_TYPE.CARD){
                cell.headerTitle.text = "Buy CƘr with a Credit Card";
                cell.headerIconBtn.setImage(UIImage(named: "ck_card"), for: UIControlState.normal)
            }else{
                cell.headerTitle.text = "Buy CƘr with Ether";
                cell.headerIconBtn.setImage(UIImage(named: "ck_ether"), for: UIControlState.normal)
            }
            
            //cell.headerIconBtn.setFATitleColor(color: CKColor.titleGreyColor)
            
            return  cell;
            
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreBuyTVCell") as! BuymoreBuyTVCell
            if(paymentType == PAYMENT_TYPE.BTC){
                cell.titleLbl1.text = "BTC to Spend";
            }else if(paymentType == PAYMENT_TYPE.DOGE){
               cell.titleLbl1.text = "DOGE to Spend";
            }else if(paymentType == PAYMENT_TYPE.CARD){
                cell.titleLbl1.text = "USD to Spend";
            }else{
               cell.titleLbl1.text = "ETH to Spend";
            }
            
            cell.txtField1.delegate = self;
            cell.txtField2.delegate = self;
            addToolBar(textField: cell.txtField1);
            addToolBar(textField: cell.txtField2);
            self.priceTxtField = cell.txtField1;
            self.ckrTxtField = cell.txtField2;
            
            cell.delegate = self;
            
            self.ckrTxtField?.text = String(format: "%.0f", ckr);
            
            self.priceTxtField?.text = valueToString(value: getPrice(ckrL: ckr))
            
            return  cell;
            
        }else if(indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuymorePriceTVCell") as! BuymorePriceTVCell
            var priceStr = ""
            if(paymentType == PAYMENT_TYPE.BTC){
                priceStr = " BTC";
                //cell.headerIconBtn.setImage(UIImage(named: "ck_btc"), for: UIControlState.normal)
            }else if(paymentType == PAYMENT_TYPE.DOGE){
                priceStr = " DOGE";
            }else if(paymentType == PAYMENT_TYPE.CARD){
                priceStr  = " USD";
            }else{
                priceStr = " ETH";
            }
            
            if(indexPath.row == 2){
                cell.priceLabel.text =  valueToString(value: getPrice(ckrL: ckr)) + priceStr
            
            
            }else if(indexPath.row == 3){
                
                let disCountValue = valueToString(value: getDiscountValue(price: getPrice(ckrL: ckr), discount: Float(self.limits?.discount?.percent ?? 0)))
                
                cell.priceLabel.text = "-" + disCountValue + priceStr;
            }else{
                
                let total = valueToString(value: getTotal(price: getPrice(ckrL: ckr), discount: Float(self.limits?.discount?.percent ?? 0)))
                
                cell.priceLabel.text =  total + priceStr
            }
            cell.setValue(row: indexPath.row)
            return  cell;
            
        }else {
            if(paymentType == PAYMENT_TYPE.CARD){
                let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreCardCell") as! BuymoreTVCell
                CKTheme.getCKButtonStyle(cell.payBtn, fontSizeL: nil)
                cell.titleLabel.font = CKTheme.regularFont(13.0)
                cell.titleLabel.textColor = CKColor.titleGreyColor;
                cell.rightsideBtn.setFAIcon(icon: .FACcStripe, iconSize: 40.0, forState: UIControlState.normal)
                cell.rightsideBtn.setFATitleColor(color: CKColor.dividerColor, forState: .normal);
                cell.delegate = self;
                return  cell;
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "BuymoreTVCell") as! BuymoreTVCell
                //CKTheme.getCKButtonStyle(cell.payBtn, fontSizeL: nil)
                cell.delegate = self
                return  cell;
            }          
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    
    
    func getPrice(ckrL: Float) -> Float{
        var pr: Float = 0.0
        if(paymentType == PAYMENT_TYPE.BTC){
            pr = ckrL * btc
        }else if(paymentType == PAYMENT_TYPE.DOGE){
            pr = ckrL * doge
        }else if(paymentType == PAYMENT_TYPE.CARD){
            pr  = ckrL * card
        }else{
            pr = ckrL * eth
        }
        return pr
        
    }
    
    
  
    
    func getCaclulatedCkr(price: Float) -> String{
        if(paymentType == PAYMENT_TYPE.BTC){
             return valueToString(value: price / btc)
        }else if(paymentType == PAYMENT_TYPE.DOGE){
            return valueToString(value: price / doge);
        }else if(paymentType == PAYMENT_TYPE.CARD){
            return valueToString(value: price / card);
        }
        else{
            return valueToString(value: price / eth);
        }
        
    }
    
    
    //MARK: BuymoreTVCellDelegate
    
    func payBtnClickedDelegateMethod(sender: UIButton) {
        
        
        
        if(ckrTxtField?.text != ""){
            if(paymentType == PAYMENT_TYPE.CARD){
                self.delegate?.stripeSDKCalledDelegateMethod()
            }
            else{
                purchaseAPI()
            }
        }else{
            showErrorMessage(NSLocalizedString("enter_valid_amount", comment: ""))
        }
        
        
        
    }
    
    //MARK: BuymoreBuyTVCellDelegate
    
    func textFieldDidChangeDelegateMethod(sender: UITextField) {
        //if(sender.text?.count != 0){
        if(sender == priceTxtField){
            let price = NSString(string: sender.text!).floatValue
            self.ckrTxtField?.text = getCaclulatedCkr(price: price);
            self.ckr = NSString(string: getCaclulatedCkr(price: price)).floatValue
            
            //self.ckrTxtField?.text = self.priceTxtField?.text;
        }else{
            self.ckr = NSString(string: sender.text!).floatValue
            self.priceTxtField?.text =  valueToString(value: getPrice(ckrL: ckr))  //getCaclulatedPrice(ckr: ckr);
        }
        
        let indexPath1 = IndexPath(row: 2, section: 0)
        let indexPath2 = IndexPath(row: 3, section: 0)
        self.tableView.reloadRows(at: [indexPath1, indexPath2], with: UITableViewRowAnimation.none)
    }
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
   
    
    func purchaseAPI(){
        
        if(limits?.kyc?.accept_crypto == false){
            showErrorMessage("Cannot accept crypto currency")
            return
        }
        
        self.showLoadingDialog("")
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["amount"] = ckrTxtField?.text   //Ckr to buy
        params["currency"] = 1
        params["wallet"] = CKGlobalVariables.sharedManager.walletId;
        params["payment_method"] = "coinpayments"
        
        if(paymentType == PAYMENT_TYPE.BTC){
            params["payment_currency"] = "btc"
        }else if(paymentType == PAYMENT_TYPE.DOGE){
            params["payment_currency"] = "doge"
        }else if(paymentType == PAYMENT_TYPE.ETH){
            params["payment_currency"] = "eth"
        }
        params["payment_amount"] = getTotal(price: getPrice(ckrL: ckr), discount: Float(self.limits?.discount?.percent ?? 0))
        
        let cks = CKPostService()
        cks.purchase(params: params, responseHandler: {
            response in
            self.hideLoadingDialog()
            let coinP = CoinPaymentParameters()
            coinP.invoice = response.invoice?.uuid;
            coinP.payment_amount = self.getTotal(price: self.getPrice(ckrL: self.ckr), discount: Float(self.limits?.discount?.percent ?? 0))//response.invoice?.payment_amount;
            coinP.ckr = response.invoice?.amount;
            coinP.payment_currency = response.invoice?.payment_currency;
            self.delegate?.coinPaymentDelegateMethod(coinparams: coinP)
            
        }, errorHandler: { error in
            //self.errorHandler(error)
            self.showErrorMessage("Cannot accept crypto currency")
            self.hideLoadingDialog()
        })
        
    }
}
