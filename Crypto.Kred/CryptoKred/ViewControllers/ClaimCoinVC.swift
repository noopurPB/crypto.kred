//
//  ClaimCoinVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class ClaimCoinVC: CKCommonVC{
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func claimBtnClicked(_ sender: UIButton) {
        if(txtField.text?.count != 0){
            self.claimCoinServiceMethod();
        }
    }
    
    
    
    func claimCoinServiceMethod(){
        let cks = CKPostService()
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["wallet"] = CKGlobalVariables.sharedManager.walletId ?? 0
        params["code"] = self.txtField.text ?? "";
       
        self.showLoadingDialog("")
        cks.claimCoin(params: params, responseHandler: { response in
            self.hideLoadingDialog();
            if(response.error != nil){
                self.showErrorMessage(response.error ?? "")
            }else{
                self.txtField.resignFirstResponder();
                self.dismiss(animated: true, completion: nil)
                CKGlobalMethods.showSuccessALertWithMessage("", message: NSLocalizedString("coin_claimed_alert", comment: ""))
            }
            
        }, errorHandler: { error in
            self.hideLoadingDialog();
            self.errorHandler(error);
        })
        
    }
    
    
    
}
