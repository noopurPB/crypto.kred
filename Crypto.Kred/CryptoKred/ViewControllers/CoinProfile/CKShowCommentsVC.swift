//
//  CKShowCommentsVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 27/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class CKShowCommentsVC: CKBaseVC, CKCoinMessagesListTVCDelegate, CKAddCommentVCDelegate, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var messagesListTableVC: CKCoinMessagesListTVC?
    var addCommentObject: AddCommentObject?
    var messages =  [CKMessage]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coinMessagesServiceMethod()
       // initCoinProfileListTableView()
        navBarStyle?.showNavigationBackBarItem(self, navTitle: "Comments");
        navBarStyle?.delegate = self;
    }
    
 
    func initCoinProfileListTableView(){
        if(messagesListTableVC == nil){
            messagesListTableVC = self.storyboard?.instantiateViewController(withIdentifier: "CKCoinMessagesListTVC") as? CKCoinMessagesListTVC
            messagesListTableVC?.messageType = CKConstants.MESSAGES_TYPE.COMMENTS;
            messagesListTableVC!.view.frame = CGRect(x: 0,y: 64,width: (self.view.frame.size.width),height: (self.view.frame.size.height))
            messagesListTableVC?.baseVC = self;
            messagesListTableVC?.commentsVC = self;
           
            messagesListTableVC?.delegate = self;
            if #available(iOS 11.0, *) {
                messagesListTableVC!.tableView.contentInsetAdjustmentBehavior = .never
            }
            messagesListTableVC?.coinMessagesServiceMethod(dataHandler: {
                
            })
            self.view.addSubview((messagesListTableVC?.view)!)
        }
    }
    
    
    //MARK:
    func composePressedDelegateMethod(){
        let vc = coinAddCommentVCCalled(addComment: true, commentObjectL: self.addCommentObject!);
        vc.delegate = self;
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //MARK: CKAddCommentVCDelegate
    
    func postDoneDelegateMethod(addComment: Bool ,message: CKMessage?) {
        message?.ago = "Now";
        self.messages.append(message!);
        self.tableView.reloadData();
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  130
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension;
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count;
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoinCommentTVCell") as! CoinProfileMessageTVCell
        let message = messages[indexPath.row]
        cell.setMessageView(message: message, tableView: tableView, indexPath: indexPath)
        
        if(indexPath.row % 2 == 0){
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
        }else{
            cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
        }
        cell.messageLbl.isHidden = false
        cell.messageLbl.text = message.text ?? "";
        cell.messageLbl.sizeToFit()
        cell.messageLbl.backgroundColor = UIColor.clear
        return  cell;
     
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
        
        CKTheme.getCKButtonStyle(cell.rightsideBtn, fontSizeL: 18.0)
        
        //cell.contentView.backgroundColor = CKColor.tableviewCellColor2
        //cell.rightsideBtn.setTitleColor(CKColor.darkGrayColor, for: UIControlState.normal)
        cell.rightsideBtn.addTarget(self, action:  #selector(CKShowCommentsVC.rightBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
        return cell.contentView
        
    }
    
    
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 60;
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*if(messageType == CKConstants.MESSAGES_TYPE.COMMENTS && indexPath.row == 0){
         delegate?.composePressedDelegateMethod!()
         }*/
    }
    
    @objc func rightBtnClickedMethod(button: UIButton) {
        self.composePressedDelegateMethod()
    }
    
    //MARK: Get messages
    func coinMessagesServiceMethod() {
        var params = [String: Any]()
        
        params["id"] = addCommentObject?.messageId
        
        
        params["format"] = "json";
        params["count"] = CKServiceConstants.ITEM_PER_PAGE;
        params["page"] = 1;
        
        
        
        let cks = CKGetService()
        
        self.showSearchView(self.view, searchText: "")
        
        
        func commonHandler(response: CKCoinsResponseObject){
            self.messages = response.messages;
            self.tableView.reloadData();
        }
        
        
        cks.getCoinMessages(showCache: true, messageType: CKConstants.MESSAGES_TYPE.COMMENTS, grab: "", params: params, responseHandler: { response in
            commonHandler(response: response)
            self.hideSearchView()
           
            
        }, cacheHandler: { response in
            commonHandler(response: response)
            
        }, errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
        
    }

}


extension CKShowCommentsVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.dismiss(animated: true, completion: nil)
    }
}
