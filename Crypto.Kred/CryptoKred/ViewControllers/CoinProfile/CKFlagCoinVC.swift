//
//  CKFlagCoinVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKFlagCoinVC: CKCommonVC, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    var queryArray = [  "It displays a sensitive image",
                        "It's abusive or harmful",
                        "They're pretending to be me or someone else"]
    
    var selectedIndex: Int = 0;
    var coinId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 2){
           return 55.0;
        }
        return 40.0;
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return queryArray.count;
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
        if(indexPath.row % 2 == 0){
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
        }else{
            cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
        }
        cell.titleLabel.font = CKTheme.mediumFont(14.0);
        cell.titleLabel.textColor = CKColor.titleGreyColor;
        cell.titleLabel.text = queryArray[indexPath.row]
        if(selectedIndex == indexPath.row){
            cell.rightsideBtn.isHidden = false;
        }else{
             cell.rightsideBtn.isHidden = true;
        }
        return  cell;
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row;
        self.tableView.reloadData();
        
    }
    
    @IBAction func submitBtnClicked(_ sender: UIButton) {
       flagCoinServiceMethod()
    }
    
    
    func flagCoinServiceMethod(){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = coinId ?? ""
        params["text"] = self.queryArray[selectedIndex];
        
        let cks = CKPostService()
        self.showLoadingDialog("")
        cks.flagCoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.error != nil){
                self.showErrorMessage(response.error ?? "")
            }else{
                CKGlobalMethods.showSuccessALertWithMessage(NSLocalizedString("yipee", comment: ""), message: NSLocalizedString("coin_flagged_success", comment: ""))
                self.dismiss(animated: true, completion: nil)
                
            }
            
        }, errorHandler: { error in
            self.hideLoadingDialog();
            self.errorHandler(error);
        })
        
       }
    
    
}
