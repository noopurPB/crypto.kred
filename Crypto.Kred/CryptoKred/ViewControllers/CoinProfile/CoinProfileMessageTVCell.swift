//
//  CoinProfileMessageTVCell.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 09/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import SDWebImage

protocol CoinProfileMessageTVCellDelegate{
    func composePressedDelegateMethod()
    func commentPressedDelegateMethod(index: Int?)
    func likePressedDelegateMethod(button: UIButton)
    func connectPressedDelegateMethod(button: UIButton, index: Int)
    //func likeHeaderDelegateMethod(button: UIButton)
    func deletePressedDelegateMethod(index: Int?)
    func morePressedDelegateMethod(index: Int?)
    func circulationDelegateMethod()
    func linkClickedDelegateMethod(index: Int, linkUrl: String)
}

class CoinProfileMessageTVCell: CKBaseTVCell , UITextViewDelegate {
    
    var delegate: CoinProfileMessageTVCellDelegate?
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var userThumbnailImageView: UIImageView!
    @IBOutlet weak var usernameBtn: UIButton!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var connectBtn: UIButton!
    @IBOutlet weak var connectBtnHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var secDividerView: UIView!
    @IBOutlet weak var composeBtn: UIButton!
    
    @IBOutlet weak var mediaImageView: UIImageView!
    
    @IBOutlet weak var messageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaHeightConstraint: NSLayoutConstraint!
    
  
    @IBOutlet weak var circulationImageView: UIImageView!
    
    @IBOutlet weak var marketpriceLbl: UILabel!
    @IBOutlet weak var circulationLabel: UILabel!
    
    @IBOutlet weak var circulationBtn: UIButton!
    
    @IBOutlet weak var actionView: UIView!
    @IBOutlet weak var actionViewHeightConstraint: NSLayoutConstraint!
    
    var indexClicked: Int?
    var tableView: UITableView?
  
    override func prepareForReuse() {
        super.prepareForReuse()
       // mediaHeightConstraint = nil;
    }
    
     /*
    override func layoutSubviews() {
        super.layoutSubviews()
      //  self.contentView.layoutIfNeeded()
    }*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(self.userThumbnailImageView != nil){
            self.userThumbnailImageView.clipsToBounds = true;
            self.userThumbnailImageView.layer.cornerRadius = self.userThumbnailImageView.frame.width/2;
        }
        if(self.usernameBtn != nil){
            usernameBtn.titleLabel?.font = CKTheme.boldFont(17.0)
            usernameBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        }
        
        if(self.messageLbl != nil){
            messageLbl.font = CKTheme.regularFont(15.0);
            messageLbl.textColor = CKColor.titleGreyColor;
        }
        
        if(self.messageTextView != nil){
            self.messageTextView.delegate = self;
            self.messageTextView.font = CKTheme.regularFont(15.0);
            self.messageTextView.textColor = CKColor.titleGreyColor;
        }
        
        
        if(self.circulationLabel != nil){
            circulationLabel.font = CKTheme.regularFont(15.0);
            circulationLabel.textColor = CKColor.titleGreyColor;
        }
        
        if(circulationBtn != nil){
            let image = UIImage(named: "ck_circulation")?.withRenderingMode(.alwaysTemplate)
            self.circulationImageView.image = image;
            self.circulationImageView.tintColor = CKColor.titleGreyColor;
        }
        
        if(self.timeLbl != nil){
            timeLbl.font = CKTheme.regularFont(12.0);
            timeLbl.textColor = CKColor.titleGreyColor;
        }
        
        if(self.connectBtn != nil){
            CKTheme.getCKButtonStyle(self.connectBtn, fontSizeL: nil)
        }
        
        if(likeBtn != nil){
            
            let likeImage = self.likeBtn.imageView?.image?.withRenderingMode(.alwaysTemplate);
            likeBtn.setImage(likeImage, for: UIControlState.normal);
            likeBtn.setImage(likeImage, for: UIControlState.selected);
            self.likeBtn.titleLabel?.font = CKTheme.mediumFont(15.0)
            self.likeBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        }
        
        if(secDividerView != nil){
            self.secDividerView.backgroundColor = CKColor.dividerColor
        }
        
        if(mediaImageView != nil){
            self.mediaImageView.layer.masksToBounds = true;
            self.mediaImageView.clipsToBounds = true;
            self.mediaImageView.backgroundColor = CKColor.placeholderColor;
        }
    }
    
   
    @IBAction func moreBtnClicked(_ sender: UIButton) {
        delegate?.morePressedDelegateMethod(index: sender.tag)
    }
    
    @IBAction func likeHeaderClicked(_ sender: UIButton) {
      //  delegate?.likeHeaderDelegateMethod(button: sender)
    }
    
    
    @IBAction func likeBtnClicked(_ sender: UIButton) {
        delegate?.likePressedDelegateMethod(button: sender)
    }
    
    @IBAction func commentBtnClicked(_ sender: UIButton) {
        delegate?.commentPressedDelegateMethod(index: sender.tag)
    }
    
    @IBAction func composeBtnClicked(_ sender: UIButton) {
        delegate?.composePressedDelegateMethod()
    }
    
    @IBAction func deleteBtnClicked(_ sender: UIButton) {
        delegate?.deletePressedDelegateMethod(index: sender.tag)
    }
    
    
    @IBAction func connectBtnClicked(_ sender: UIButton) {
        delegate?.connectPressedDelegateMethod(button: sender, index: sender.tag)
    }
    
    
    @IBAction func circulationBtnClicked(_ sender: Any) {
        delegate?.circulationDelegateMethod()
    }
    
    
    func setSecondHeaderView(coinL: Coin?){
        self.contentView.backgroundColor = CKColor.tableviewCellColor1;
        self.messageLbl.font = CKTheme.mediumFont(14.0)
        self.messageLbl.text = String(format: "%d", coinL?.circulation ?? 0); //People have Held this Coin
        
        if(coinL?.liked != nil && coinL?.liked == true){
            self.likeBtn.tintColor = CKColor.themeAnotherColor;
        }else{
            self.likeBtn.tintColor = CKColor.titleGreyColor;
        }
        
        
       
        let likeCount = coinL?.likes ?? 0//
        if(likeCount == 0){
            self.likeBtn.setTitle("", for: UIControlState.normal)
        }else{
            self.likeBtn.setTitle(String(format: "%d", likeCount), for: UIControlState.normal)
        }
        
        if(composeBtn != nil){
            if(coinL?.user == CKGlobalVariables.sharedManager.loggedInUserId){
                self.composeBtn.isHidden = false;
            }else{
                self.composeBtn.isHidden = true;
            }
        }
    }
    
    
    func setMessageView(message: CKMessage?, tableView: UITableView, indexPath: IndexPath){
        self.tableView = tableView;
        self.usernameBtn.setTitle(getMessageUsername(message), for: UIControlState.normal)
        self.indexClicked = indexPath.row;
        
        if(self.connectBtn != nil){
            if(message?.user?.id != CKGlobalVariables.sharedManager.loggedInUserId){
                self.connectBtn.isHidden = false;
                self.connectBtnHeightConstraint.constant = 25.0;
            }else{
                self.connectBtn.isHidden = true;
                self.connectBtnHeightConstraint.constant = 0.0;
            }
        }
        
        CKGlobalMethods.setImageViewWithCache(message?.user?.bio?.avatar, imageView: self.userThumbnailImageView, contentMode: UIViewContentMode.scaleAspectFill)
        
        
        
        
         let newFtext = String(format: "<div style=\"text-align:left;font-size:15px;color:grey\">%@</div>", message?.text ?? "");
        
        
        self.messageLbl.attributedText = newFtext.html2AttributedString
        
        if(messageTextView != nil){
            self.messageTextView.attributedText = newFtext.html2AttributedString
        }
        
        
        self.messageLbl.sizeToFit()
        self.messageLbl.isHidden = true;
        
       
        
        
        
        if(self.timeLbl != nil){
            self.timeLbl.text = message?.ago ?? ""
            self.timeLbl.sizeToFit()
        }
        
        
        if(mediaImageView != nil){
            self.mediaImageView.clipsToBounds = true
            self.mediaImageView.layer.masksToBounds = true;
            
            self.setMediaImage(message: message, tableView: tableView, indexPath: indexPath)
        }
        
        if(likeBtn != nil){
            if(message?.liked != nil && message?.liked == true){
                self.likeBtn.tintColor = CKColor.themeAnotherColor;
            }else{
                self.likeBtn.tintColor = CKColor.titleGreyColor;
            }
            let likes = message?.likes ?? 0
            if(likes == 0){
                self.likeBtn.setTitle("", for: UIControlState.normal)
            }else{
                self.likeBtn.setTitle(String(format: "%d",  likes), for: UIControlState.normal)
            }
        }
        
        
        if(deleteBtn != nil){
            if(message?.user?.id == CKGlobalVariables.sharedManager.loggedInUserId){
                self.deleteBtn.isHidden = false;
            }else{
                self.deleteBtn.isHidden = true;
            }
        }
        
        if(commentBtn != nil){
          if(message?.commentsCount != nil && message?.commentsCount != 0){
             self.commentBtn.setTitle(String(format: "%d", message?.commentsCount ?? 0), for: UIControlState.normal)
        }else{
            self.commentBtn.setTitle("", for: UIControlState.normal)
          }
        }
    }
    
    
    //DispatchQueue.main.async {}
    
    func setMediaImage(message: CKMessage?, tableView: UITableView, indexPath: IndexPath){
            if(message?.mediaStr != nil){
                self.mediaHeightConstraint?.constant = 200;//self.frame.size.width - ;
                
                self.setMediaAttachmentSize(imageURL: message?.mediaStr, imageView: self.mediaImageView, tableView: tableView, indexPath: indexPath)
                
            }else if (message?.media.count != 0){
                self.mediaHeightConstraint?.constant = 200;//self.frame.size.width;
                let mediaL = message?.media[0];
                    self.setMediaAttachmentSize(imageURL: mediaL?.media_url, imageView: self.mediaImageView, tableView: tableView, indexPath: indexPath)
            }
            else{
                //no media
                //self.setNoMediaConstraint()
                self.mediaHeightConstraint?.constant = 0.0;
            }
    }
    
    
    /*DispatchQueue.main.async {
       }*/
    
    func setNoMediaConstraint(){
        let aspect: CGFloat = 0.0
        let constraint = NSLayoutConstraint(item: self.mediaImageView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self.mediaImageView, attribute: NSLayoutAttribute.height, multiplier: aspect, constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        self.mediaHeightConstraint = constraint
    }
    
    
    func setMediaAttachmentSize(imageURL: String?,imageView: UIImageView, tableView: UITableView, indexPath: IndexPath){
        imageView.backgroundColor = CKColor.placeholderColor
        imageView.image = nil
       // imageView.contentMode = UIViewContentMode.scaleAspectFill;
       
        if(imageURL != nil && imageURL != "" ){
            
            let indicator:UIActivityIndicatorView  = UIActivityIndicatorView(activityIndicatorStyle: .white)
            indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            //indicator.center = imageView.center
            imageView.layoutIfNeeded()
            var frame:CGRect = indicator.frame;
            frame.origin.x = imageView.frame.size.width / 2 - frame.size.width / 2;
            frame.origin.y = imageView.frame.size.height / 2 - frame.size.height / 2;
            indicator.frame = frame;
            
            imageView.addSubview(indicator)
            
            let escapedString = imageURL!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            if(escapedString != nil){
                //imageView.sd_setImageWithURL(NSURL(string: escapedString!), placeholderImage: UIImage(named: Constants.USER_PLACEHOLDER), options: SDWebImageOptions.RefreshCached)
                
                indicator.startAnimating()
                
                imageView.sd_setImage(
                    with: URL(string: escapedString!),
                    placeholderImage: nil, //UIImage(named: Constants.USER_PLACEHOLDER)
                    options: SDWebImageOptions.refreshCached,
                    progress: nil,
                    completed: { (image, error, cacheType, imageURL) in
                        
                        if(image != nil && self.mediaHeightConstraint != nil){
                            self.getNewHeight(image: image!)
                        }
                        
                       
                       
                        
                        indicator.stopAnimating()
                        indicator.hidesWhenStopped = true
                })
                
                 imageView.clipsToBounds = true
                // imageView.layoutIfNeeded()
            }
           
            //imageView.setNeedsLayout()
            // imageView.layoutIfNeeded()
            
            
        }
    }
    
    
    func getNewHeight(image: UIImage){
        let screen_width = self.frame.width
        // Ratio Width / Height
        let ratio =  image.size.height / image.size.width
        
        // Calculated Height for the picture
        let newHeight = screen_width * ratio
        
        
        self.tableView?.beginUpdates()
        
        // METHOD 1
        self.mediaHeightConstraint.constant = newHeight
        
        // METHOD 2
        //self.selfieImageView.bounds = CGRectMake(0,0,screen_width,newHeight)
        
        self.mediaImageView.image = image
        self.tableView?.endUpdates()
        
        
    }
    
    func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
        let widthOffset = downloadedImage.size.width - cellImageFrame.width
        let widthOffsetPercentage = (widthOffset*100)/downloadedImage.size.width
        let heightOffset = (widthOffsetPercentage * downloadedImage.size.height)/100
        let effectiveHeight = downloadedImage.size.height - heightOffset
        return(effectiveHeight)
    }

    
    
    
    
    func getMessageUsername(_ message: CKMessage?) -> String{
        return  message?.parent?.user?.bio?.name ?? message?.user?.bio?.name ?? message?.face?.bio?.name ?? message?.user?.username ?? message?.screen_name ?? ""
    }
    
    
    //MARK: UITextView Delegate
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        //if let url = URL(string:UIApplicationOpenSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                
                
                if(url.absoluteString.contains("https://app.crypto.kred/coin/") ||
                    url.absoluteString.contains("/collection") ){
                    self.delegate?.linkClickedDelegateMethod(index: self.indexClicked ?? 0, linkUrl: url.absoluteString)
                }else{
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        //}
        return false;
    }
    
  
    /*+(NSString *)convertHtmlToText: (NSString *)string{
     NSString *htmlString = string;
     NSAttributedString *attributedString = [[NSAttributedString alloc]
     initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
     options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
     documentAttributes: nil
     error: nil];
     return attributedString.string;
     }*/

    
    
    
}


extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
