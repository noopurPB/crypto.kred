//
//  CKCirculateListVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 28/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKCirculateListVC: CKBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var coinId: Int?
    var circulationList = [CKHistory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBarStyle?.delegate = self;
        self.navBarStyle?.showNavigationBackBarItem(self, navTitle: "Circulation")
        self.circulationServiceMethod()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0;
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.circulationList.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
        cell.titleLabel.font = CKTheme.boldFont(15.0)
        cell.titleLabel.textColor = CKColor.titleGreyColor;
        cell.baseImageView.clipsToBounds = true;
        cell.baseImageView.layer.cornerRadius = cell.baseImageView.frame.width/2;
        if(indexPath.row % 2 == 0){
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
        }else{
            cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
        }
        let historyL = self.circulationList[indexPath.row];
       
        cell.titleLabel.text = historyL.name;
        CKGlobalMethods.setImageViewWithCache(historyL.avatar, imageView: cell.baseImageView, contentMode: UIViewContentMode.scaleAspectFill)
        
        return  cell;
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    
    
    func circulationServiceMethod() {
        
        let pageId = CKGlobalMethods.getPageId(self.circulationList.count)
       
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = self.coinId ?? ""
        //params["count"] = CKServiceConstants.ITEM_PER_PAGE;
        //params["page"] = pageId
        
        let cks = CKGetService()
        
        
        
        
        //

        //
        
        
        
        if(pageId == 1){
            showSearchView(self.view, searchText: "")
        }
        
        cks.getCirculationList(params: params, responseHandler: { response  in
            /*self.loadmore = self.loadMoreWebservice(self.coinsList)
            if(pageId == 1){
                self.coinsCommonHandler(response.coins)
            }else{
                self.coinsCommonHandler(self.coinsList + response.coins)
            }*/
            self.circulationCommonHandler(circulations:  response.history)
        }, cacheHandler: { response  in
            /*if(pageId == 1){
                self.coinsCommonHandler(response.coins)
            }*/
            self.circulationCommonHandler(circulations:  response.history)
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    
    func circulationCommonHandler(circulations: [CKHistory]){
        
        self.hideSearchView()
        self.circulationList = circulations
        self.tableView.reloadData()
    }
    
    
}


extension CKCirculateListVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
