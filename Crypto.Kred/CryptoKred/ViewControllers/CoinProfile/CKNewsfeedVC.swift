//
//  CKNewsfeedVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 19/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKNewsfeedVC: CKBaseVC, CKCoinMessagesListTVCDelegate{
    
    var messagesListTableVC: CKCoinMessagesListTVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        initCoinProfileListTableView()
        navBarStyle?.showNavigationSideMenuBarItem(self, navTitle: NSLocalizedString("ck_newsfeeds" , comment: ""))
         navBarStyle?.delegate = self;
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initCoinProfileListTableView(){
        if(messagesListTableVC == nil){
            messagesListTableVC = self.storyboard?.instantiateViewController(withIdentifier: "CKCoinMessagesListTVC") as? CKCoinMessagesListTVC
            messagesListTableVC!.delegate = self
            messagesListTableVC!.baseVC = self;
            messagesListTableVC!.view.frame = CGRect(x: 0,y: 80 ,width: (self.view.frame.size.width),height: (self.view.frame.size.height - 64))
            messagesListTableVC?.messageType = CKConstants.MESSAGES_TYPE.NEWSFEED;
            if #available(iOS 11.0, *) {
                messagesListTableVC!.tableView.contentInsetAdjustmentBehavior = .never
            }
            messagesListTableVC?.coinMessagesServiceMethod(dataHandler: {
                if(self.messagesListTableVC?.messages.count == 0){
                     self.showNoDataView(view: self.view, image: "givecoin_to_image2", title: "Your Newsfeed is Empty!", subTitle: "Here you will see Conversations and Notifications from COins in your Collection. Buy or create a Coin to get started.", buttonTitle: "SEE COINS FOR SALE", buttonWidth: 200);
                     self.noDataView?.delegate = self;
                }
            })
            self.view.addSubview((messagesListTableVC?.view)!)
        
        }
    }
    
    
    //MARK:
    
    func linkClickedDelegateMethod(coin: Coin?, linkUrl: String) {
        if(linkUrl.contains("/buy=")){
            
        }else{
            if(coin != nil){
                self.coinProfileVCCalled(selectedCoin: coin!)
            }
            //self.collectionViewVCCalled(userCollection: false, selectedCoin: nil)
        }
    }
    
    
    func commentPressedDelegateMethod(message: CKMessage){
        let commentObj = AddCommentObject();
        commentObj.grabId = message.data?.coin?.grab ?? "";
        commentObj.messageId = message.id ?? "";
        if(message.commentsCount != nil && message.commentsCount != 0){
            self.coinShowCommentVCCalled(addCommentObj: commentObj);
        }else{
            let vc = coinAddCommentVCCalled(addComment: true, commentObjectL: commentObj);
            vc.delegate = self;
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension CKNewsfeedVC: CKNoDataCellDelegate{
    func noCoinBtnClickedDelegateMethod(_ sender: UIButton) {
        self.marketPlaceCalledVC(animation: true,selectedCoin: nil)
    }
}




extension CKNewsfeedVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CKNewsfeedVC: CKAddCommentVCDelegate{
    func postDoneDelegateMethod(addComment: Bool ,message: CKMessage?){
       self.messagesListTableVC?.coinMessagesServiceMethod(dataHandler: {})
        
    }
}
