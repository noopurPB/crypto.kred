//
//  CKAddCommentVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 14/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

protocol CKAddCommentVCDelegate{
    func postDoneDelegateMethod(addComment: Bool ,message: CKMessage?)
}

class CKAddCommentVC: CKBaseVC, UITextViewDelegate {
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var commentPlaceHolder: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var dividerView: UIView!
    
    var addComment: Bool?
    var commentObject: AddCommentObject?
    var delegate: CKAddCommentVCDelegate?
    
    @IBOutlet weak var attachmentCrossBtn: UIButton!
    @IBOutlet weak var attachmentImageView: UIImageView!
    
    @IBOutlet weak var cameraBtn: UIButton!
    var attachedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI();
        NotificationCenter.default.addObserver(self, selector: #selector(CKAddCommentVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CKAddCommentVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func initUI(){
        
        self.dividerView.backgroundColor = CKColor.dividerColor;
        
        self.commentView.clipsToBounds = true
        self.commentView.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        
        if(headerLabel != nil){
            self.headerLabel.textColor = CKColor.titleGreyColor;
            self.headerLabel.font = Theme.boldFont(19.0);
            self.headerLabel.text = returnHeaderText();
        }
        self.commentTextView.clipsToBounds = true
        self.commentTextView.layer.borderColor = CKColor.dividerColor.cgColor
        self.commentTextView.layer.borderWidth = 1.0;
        self.commentTextView.layer.cornerRadius = 6.0;
        self.commentTextView.font = CKTheme.regularFont(15.0)
        self.commentTextView.tintColor = CKColor.titleGreyColor;
        self.commentTextView.textColor = CKColor.textFieldColor;
        
        
        self.commentPlaceHolder.textColor = CKColor.textFieldPlaceholderColor;
        self.commentPlaceHolder.font = CKTheme.regularFont(15.0)
        self.commentPlaceHolder.text = returnHeaderText();
        
        userImageView.clipsToBounds = true;
        userImageView.layer.cornerRadius = userImageView.frame.width/2;
        
        CKGlobalMethods.setImageViewWithCache(CKGlobalVariables.sharedManager.loggedInAvatar, imageView: self.userImageView, contentMode: UIViewContentMode.scaleAspectFill)
        
        CKTheme.getCKButtonStyle(self.doneBtn, fontSizeL: nil)
        
        self.showOrHideAttachment(showAttachment: false)
        hidePlaceholder(self.commentTextView, placeholder: self.commentPlaceHolder)
        
        
        self.attachmentImageView.clipsToBounds = true;
        self.attachmentImageView.layer.cornerRadius = 3.0
        self.attachmentImageView.contentMode = UIViewContentMode.scaleAspectFill;
        
        if(addComment != nil && addComment == true){
            self.cameraBtn.isHidden = true;
        }
        
    }
    
    
    func returnHeaderText()-> String?{
        if(addComment == true){
            return "Write a Comment";
        }else{
            return "New Post";
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        if(attachedImage != nil){
            uploadImageToServer()
        }else {
            postMessage(imageUrl: nil);
        }
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func crossBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
    
    //MARK: Keyboard methods
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //258
        // if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        // {
        var frame = self.commentView.frame;
        frame.origin.y = (commentView.frame.origin.y) - 200;
        self.commentView.frame = frame;
        //}
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        self.commentView.frame.origin.y = (commentView.frame.origin.y) + 200;
        // }
    }
    
    
    //MARK: TextView Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        hidePlaceholder(textView, placeholder: self.commentPlaceHolder)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //self.commentView.frame.origin.y -= keyboardSize.height
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        hidePlaceholder(textView, placeholder: self.commentPlaceHolder)
    }
    
    
    func hidePlaceholder(_ textView: UITextView, placeholder: UILabel){
        if(textView.text.count == 0){
            placeholder.isHidden = false
            CKTheme.enableOrDisableButton(button: doneBtn, enable: false)
        }else{
            placeholder.isHidden = true
            CKTheme.enableOrDisableButton(button: doneBtn, enable: true)
        }
    }

    
    
    
    //MARK: POST Message
    
    func postMessage(imageUrl: String?){
        
        if(imageUrl == nil){
            self.showLoadingDialog("");
        }
        
        let cks = CKPostService();
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["text"] = self.commentTextView.text;
        
        if(imageUrl != nil){
            params["media"] = imageUrl;
        }
        if(addComment == true){
            params["parent"] = commentObject?.messageId ?? "";
        }else{
            params["id"] = commentObject?.grabId ?? "";
        }
        //"tags": [],
        
        cks.writeAPost(addComment: addComment!, params: params, responseHandler: { response in
            self.hideLoadingDialog();
            if(response.messages.count > 0){
                self.delegate?.postDoneDelegateMethod(addComment: self.addComment! ,message: response.messages[0]);
                self.dismiss(animated: true, completion: nil)
            }else{
                self.showErrorMessage(response.error ?? "")
            }
            
            
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    
    @IBAction func attachmentCrossClicked(_ sender: UIButton) {
        self.showOrHideAttachment(showAttachment: false)
        self.attachedImage = nil;
    }
    
    
    @IBAction func cameraBtnClicked(_ sender: UIButton) {
         self.showImageSelector(false, showSquareCropL: false)
    }
    
    
    //MARK: Set Image Notification
    
    override func setImageNotifictaionReceived(_ notification: Notification) {
        if notification.object is UIImage {
            let image: UIImage = notification.object as! UIImage
            self.attachmentImageView.image = image;
            self.attachedImage = image;
           self.showOrHideAttachment(showAttachment: true)
           
            
        }
    }
    
    
    func uploadImageToServer(){
        let cks = CKPostService()
        
        self.showLoadingDialog("")
        
        DispatchQueue.main.async {
            
            let imageData = UIImageJPEGRepresentation(self.attachedImage!, 0.5)
            let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
            
            cks.uploadImageServiceMethod( base64String , attachmentExt: "png", responseHandler: { response in
                self.postMessage(imageUrl: response.url);
                
            }, errorHandler: { error in
                self.hideLoadingDialog()
                self.errorHandler(error)
            })
        }
    }
    
    
    
    
    func showOrHideAttachment(showAttachment: Bool){
        self.attachmentImageView.isHidden = !showAttachment;
        self.attachmentCrossBtn.isHidden = !showAttachment;
    }
    
}

class AddCommentObject: NSObject {
    
    var messageId: String?
    var grabId: String?
    
    override init() {
        super.init()
        
    }

    
}




