//
//  CKContactsListVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 04/05/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//


import Foundation
import UIKit


class CKContactsListVC: CKBaseVC, UITableViewDelegate, UITableViewDataSource, CKBaseTVCellDelegate , UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var coinId: Int?
    var contactsList = [GrabUser]()
     @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBarStyle?.showNavigationSideMenuBarItem(self, navTitle: NSLocalizedString("ck_contacts", comment: ""))
        
        if(self.headerView != nil){
            self.initSearchHeaderView(self.headerView)
        }
        
        if(searchHeaderView != nil){
            searchHeaderView?.delegate = self
            //searchHeaderView?.textField.text = searchText ?? ""
            searchHeaderView?.textField.returnKeyType = UIReturnKeyType.search
             searchHeaderView?.textField.delegate = self
        }
        self.refreshContactsView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0;
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactsList.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseTVCell") as! CKBaseTVCell
        if(indexPath.row % 2 == 0){
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
        }else{
             cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
        }
        
        cell.titleLabel.font = CKTheme.boldFont(15.0)
        cell.titleLabel.textColor = CKColor.titleGreyColor;
        cell.baseImageView.clipsToBounds = true;
        cell.baseImageView.layer.cornerRadius = cell.baseImageView.frame.width/2;
        
        CKTheme.getCKButtonStyle(cell.rightsideBtn, fontSizeL: 12.0)
        cell.baseDelegate = self;
        cell.rightsideBtn.tag = indexPath.row;
        
        let contact = self.contactsList[indexPath.row];
        
        
        cell.titleLabel.text = contact.user?.bio?.name ?? "";
        CKGlobalMethods.setImageViewWithCache(contact.user?.bio?.avatar ?? "", imageView: cell.baseImageView, contentMode: UIViewContentMode.scaleAspectFill)
      
        return  cell;
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    //MARK: CKBaseTVCellDelegate method
    
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        self.giveCoinCalledVC(coin: nil)
    }
    
    
    func contactsServiceMethod() {
        
        let pageId = CKGlobalMethods.getPageId(self.contactsList.count)
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        //params["count"] = CKServiceConstants.ITEM_PER_PAGE;
        params["page"] = Int(pageId)
        params["mode"] = "widget"
        params["annotate"] = false
        params["require_email"] = true
            
        
        let cks = CKGetService()
        if(pageId == 1){
            showSearchView(self.view, searchText: "")
        }
        
        cks.getContactsList (params: params, responseHandler: { response  in
            /*self.loadmore = self.loadMoreWebservice(self.coinsList)
             if(pageId == 1){
             self.coinsCommonHandler(response.coins)
             }else{
             self.coinsCommonHandler(self.coinsList + response.coins)
             }*/
            self.contactsCommonHandler(circulations:  response.contacts)
        }, cacheHandler: { response  in
            /*if(pageId == 1){
             self.coinsCommonHandler(response.coins)
             }*/
            self.contactsCommonHandler(circulations:  response.contacts)
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    
    func contactsCommonHandler(circulations: [GrabUser]){
        self.hideSearchView()
        self.contactsList = circulations
        self.tableView.reloadData()
    }
    
    
    
    //MARK: TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
    func textFieldDidEndEditing(_ textField: UITextField) {}
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        self.refreshContactsView()
        return true;
    }
    
   
    
    
    func refreshContactsView() {
        self.contactsList.removeAll()
        self.tableView.reloadData()
        contactsServiceMethod()
    }
    
    
    
}


extension CKContactsListVC: NavigationBarStyleDelegate{
    
    /*func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }*/
    
}

extension CKContactsListVC: CKTextFieldTVCellDelegate{
    
    func textFieldValueChanged(_ sender: UITextField) {
        
    }
}

