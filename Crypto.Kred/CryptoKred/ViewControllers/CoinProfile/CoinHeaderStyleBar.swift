//
//  CoinHeaderStyleBar.swift
//  Grab
//
//  Created by Noopur Virmani on 01/06/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//


import Foundation
import BLKFlexibleHeightBar
import Font_Awesome_Swift

extension BLKDelegateSplitter: UITableViewDelegate{
}

@objc protocol CoinHeaderStyleBarDelegate{
    @objc optional func backPressedDelegateMethod()
    @objc optional func auctionPressedDelegateMethod()
    @objc optional func sellPressedDelegateMethod()
    @objc optional func givePressedDelegateMethod()
    @objc optional func buyPressedDelegateMethod()
    @objc optional func requestPressedDelegateMethod()
    @objc optional func flagCoinPressedDelegateMethod()
}

class CoinHeaderStyleBar: BLKFlexibleHeightBar {
    
    var delegate: CoinHeaderStyleBarDelegate?
    var coinProfile: CKCoinProfileVC?
    
    var valueDynamicLabel: UILabel?
    var valueStaticLabel: UILabel?
    
    var mainActionView: UIView?
    var shadowView: UIView?
    
  //  var webViewMainView: UIView?
  
    var backButton: UIButton?
    
    var ownerView: UIView?
    var auctionButton: UIButton?
    var sellButton: UIButton?
    var giveButton: UIButton?
    
    var ownByElseView: UIView?
    var buyButton: UIButton?
    
    var notSaleView: UIView?
    var requestButton: UIButton?
    
    var topViewHeight: CGFloat = 355;
    var actionViewScrollTo: CGFloat = 82.0;
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        self.configureBar()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.configureBar()
    }
    
   
    func configureBar() {
        
        self.backgroundColor = CKColor.tableviewCellColor2;
        
        let width = self.frame.width - 120
        valueDynamicLabel = UILabel(frame: CGRect(x: 60, y: 20, width: width, height: 44))
        valueDynamicLabel?.textColor = CKColor.titleGreyColor;
        //valueDynamicLabel?.text = "50 Kred";
        valueDynamicLabel?.font = Theme.regularFont(20.0)
        valueDynamicLabel?.textAlignment = NSTextAlignment.center;
        self.addSubview(valueDynamicLabel!)
        
       
        valueStaticLabel = UILabel(frame: CGRect(x: 60, y: 46, width: width, height: 30))
        valueStaticLabel?.textColor = CKColor.titleGreyColor;
        valueStaticLabel?.text = "MARKET PRICE";
        valueStaticLabel?.font = Theme.boldFont(10.0)
        valueStaticLabel?.textAlignment = NSTextAlignment.center;
        self.addSubview(valueStaticLabel!)
        
       /* menuButton = UIButton(frame: CGRect(x: self.frame.width-60, y: 20, width: 50, height: 44))
        let menuImage = UIImage(named: "menu.png")?.withRenderingMode(.alwaysTemplate)
        menuButton?.setImage(menuImage, for: UIControlState())
        menuButton?.tintColor = CKColor.titleGreyColor
        self.addSubview(menuButton!)*/
        
        
        backButton = UIButton(frame: CGRect(x: 0, y: 20 , width: 50, height: 44))
        let backImage = UIImage(named: "back_btn.png")?.withRenderingMode(.alwaysTemplate)
        backButton?.setImage(backImage, for: UIControlState())
        backButton?.addTarget(self, action: #selector(CoinHeaderStyleBar.backPressed(_:)), for: .touchUpInside)
        backButton?.tintColor = CKColor.titleGreyColor
        self.addSubview(backButton!)
        
        
        
        let mainViewHeight: CGFloat = 50.0;
        
        mainActionView = UIView(frame: CGRect(x: 0, y: topViewHeight - mainViewHeight, width:self.frame.width , height: mainViewHeight))
        mainActionView?.backgroundColor = UIColor.white;
        
        
        let initialProfileImageViewLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes()
        initialProfileImageViewLayoutAttributes.frame = mainActionView!.frame
        mainActionView!.add(initialProfileImageViewLayoutAttributes, forProgress: 0.0)
        
        
        let midwayNameLabelLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: initialProfileImageViewLayoutAttributes)
        midwayNameLabelLayoutAttributes.frame = CGRect(x: mainActionView!.frame.origin.x, y: 90.0, width: mainActionView!.frame.width, height: mainActionView!.frame.height)
       
        mainActionView!.add(midwayNameLabelLayoutAttributes, forProgress: 0.3)
        
        let finalNameLabelLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: midwayNameLabelLayoutAttributes)
        finalNameLabelLayoutAttributes.frame = CGRect(x: mainActionView!.frame.origin.x, y: actionViewScrollTo, width: mainActionView!.frame.width, height: mainActionView!.frame.height)
        //finalNameLabelLayoutAttributes.alpha = 0.0;
        mainActionView!.add(finalNameLabelLayoutAttributes, forProgress: 1.0)
       
        self.addSubview(mainActionView!)
        
        
        
        shadowView = UIView(frame: CGRect(x: 0, y: (mainActionView?.frame.height)!-1, width:(mainActionView?.frame.width)! , height: 1))
        shadowView?.backgroundColor = CKColor.dividerColor;
        setShadow(shadowToView: shadowView!, view: mainActionView!);
        mainActionView?.addSubview(shadowView!)
        
       
        
    }
    
    //MARK: Set action view
    
    func setActionView(coinL: Coin?){
        
        let buttonWidth: CGFloat = 80.0;
        let buttonHeight: CGFloat = 30.0;
        let buttonY: CGFloat = 0.0;
        let buttonFontSize: CGFloat = 12.0;
        
        if(coinL?.user == CKGlobalVariables.sharedManager.loggedInUserId){
            self.addSubViewOwnerView(buttonWidth: buttonWidth, buttonHeight: buttonHeight, buttonY: buttonY, buttonFontSize: buttonFontSize)
        }else{
            //Owned by someone else
            if(coinL?.sale_price != nil){
                
                let value = String(format: "%@ %@", coinL?.sale_price ?? "0", NSLocalizedString("kred_name", comment: ""));
                
                 self.addSubViewOwnByElseView(buttonWidth: buttonWidth, buttonHeight: buttonHeight, buttonY: buttonY, buttonFontSize: buttonFontSize, value: value)
            }else if(coinL?.auction_price != nil){
                
                let date = CKGlobalMethods.convertDateFromString(dateL: (coinL?.auctionCreated)!)
                let timeleft = CKGlobalMethods.setTimeLeft(date: date)
                
                self.addSubViewOwnByElseView(buttonWidth: buttonWidth, buttonHeight: buttonHeight, buttonY: buttonY, buttonFontSize: buttonFontSize, value: timeleft);
            }
            else{
                //for sale.- Request
               self.addSubViewNotSaleView(buttonWidth: buttonWidth, buttonHeight: buttonHeight, buttonY: buttonY, buttonFontSize: buttonFontSize)
            }
        }
    }
    
    
    //MARK: Set coin view
    func setCoinView(coinL: Coin?){
        
        let webViewMainViewViewX: CGFloat = (self.frame.width - 270)/2
        
        let coinView = CoinView(frame: CGRect(x: webViewMainViewViewX, y: 70, width: 270 , height: 180))
        coinView.backgroundColor = UIColor.clear;
        coinView.coin = coinL;
      //  coinView.loadCoin(webView: coinView.frontWebView!)
       // coinView.loadCoin(webView: coinView.backWebView!)
        
        
        let initialProfileImageViewLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes()
        initialProfileImageViewLayoutAttributes.frame = coinView.frame
        initialProfileImageViewLayoutAttributes.alpha = 1.0
        coinView.add(initialProfileImageViewLayoutAttributes, forProgress: 0.0)
        
        
        let midwayNameLabelLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: initialProfileImageViewLayoutAttributes)
        midwayNameLabelLayoutAttributes.alpha = 0.0
        coinView.add(midwayNameLabelLayoutAttributes, forProgress: 0.3)
        
        let finalNameLabelLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: midwayNameLabelLayoutAttributes)
        finalNameLabelLayoutAttributes.alpha = 0.0;
        coinView.add(finalNameLabelLayoutAttributes, forProgress: 1.0)
       
        self.addSubview(coinView)
    }
    
    
    func setIssuerView(coinL: Coin?){
        
        let actualWidth = self.frame.width;
        let viewWidth: CGFloat = 200;
        let xAxis: CGFloat = (self.frame.width - viewWidth)/2 + 20
        
        let issuerView = UIView(frame: CGRect(x: 0, y: 252, width: actualWidth, height: 45))
        //issuerView.backgroundColor = UIColor.red;
        
        let initialProfileImageViewLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes()
        initialProfileImageViewLayoutAttributes.frame = issuerView.frame
        initialProfileImageViewLayoutAttributes.alpha = 1.0
        issuerView.add(initialProfileImageViewLayoutAttributes, forProgress: 0.0)
        
        let midwayNameLabelLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: initialProfileImageViewLayoutAttributes)
        midwayNameLabelLayoutAttributes.alpha = 0.0
        issuerView.add(midwayNameLabelLayoutAttributes, forProgress: 0.3)
        
        let finalNameLabelLayoutAttributes: BLKFlexibleHeightBarSubviewLayoutAttributes = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: midwayNameLabelLayoutAttributes)
        finalNameLabelLayoutAttributes.alpha = 0.0;
        issuerView.add(finalNameLabelLayoutAttributes, forProgress: 1.0)
       
        self.addSubview(issuerView)
        
        
        let avatarImageview = UIImageView(frame: CGRect(x: xAxis, y: 0, width: 45, height: 45))
        avatarImageview.clipsToBounds = true;
        avatarImageview.layer.cornerRadius = avatarImageview.frame.width/2;
        CKGlobalMethods.setImageViewWithCache(coinL?.history?.avatar, imageView: avatarImageview, contentMode: UIViewContentMode.scaleAspectFill);
        issuerView.addSubview(avatarImageview)
        
        let issLabel = UILabel(frame: CGRect(x: xAxis+avatarImageview.frame.width+5, y: 3, width: 270 - 55, height: 20))
        issLabel.textColor = CKColor.titleGreyColor;
        issLabel.text = "ISSUED BY";
        issLabel.font = Theme.regularFont(11.0)
        issuerView.addSubview(issLabel)
        
        let issnameLabel = UILabel(frame: CGRect(x: xAxis+avatarImageview.frame.width+5, y: 20, width: 270 - 55, height: 20))
        issnameLabel.textColor = CKColor.titleGreyColor;
        issnameLabel.text = coinL?.history?.name ?? ""
        issnameLabel.font = Theme.boldFont(16.0)
        issuerView.addSubview(issnameLabel)
        
        
        let flagButton = UIButton(frame: CGRect(x: actualWidth - 70 , y: 0 , width: 70, height: 45))
        //flagButton.setTitle("Flag Coin", for: UIControlState.normal)
       // flagButton.setFAIcon(icon: .FAFlag, iconSize: 10, forState: UIControlState.normal)
        
        
        flagButton.setFAText(prefixText: "", icon: .FAFlagO, postfixText: " Flag Coin", size: 11.0, forState: UIControlState.normal)
        flagButton.setFATitleColor(color: CKColor.titleGreyColor, forState: UIControlState.normal)
        flagButton.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        flagButton.titleLabel?.font = CKTheme.regularFont(11.0)
        flagButton.addTarget(self, action: #selector(CoinHeaderStyleBar.flagCoinPressed(_:)), for: .touchUpInside)
        
        issuerView.addSubview(flagButton)
        
        
    }
    
    
    
    func setShadow(shadowToView: UIView ,view: UIView){
        shadowToView.layer.shadowColor = CKColor.dividerColor.cgColor
        shadowToView.layer.shadowRadius = 0.1
        shadowToView.layer.shadowOpacity = 1.0
        shadowToView.layer.shadowOffset = CGSize(width: 1, height: 1)//CGSize.zero
        shadowToView.layer.masksToBounds = false;
        view.addSubview(shadowToView)
    }
    
    
    func addSubViewOwnerView(buttonWidth: CGFloat, buttonHeight: CGFloat, buttonY: CGFloat, buttonFontSize: CGFloat){
        
        let viewWidth: CGFloat = (buttonWidth*3)+15;
        let viewX: CGFloat = (self.frame.width - viewWidth)/2
        let viewY: CGFloat = ((self.mainActionView?.frame.height)! - buttonHeight)/2
        
        ownerView = UIView(frame: CGRect(x: viewX, y: viewY, width: viewWidth, height: buttonHeight))
        ownerView?.backgroundColor = UIColor.clear
        mainActionView?.addSubview(ownerView!)
        
        
        auctionButton = UIButton(frame: CGRect(x: 0, y: buttonY, width: buttonWidth, height: buttonHeight))
        auctionButton?.addTarget(self, action: #selector(CoinHeaderStyleBar.auctionPressed(_:)), for: .touchUpInside)
        auctionButton?.setTitle("AUCTION", for: UIControlState.normal)
        CKTheme.getCKLightButtonStyle(auctionButton!, fontSizeL: buttonFontSize)
        ownerView?.addSubview(auctionButton!)
        
        sellButton = UIButton(frame: CGRect(x: buttonWidth+5, y: buttonY, width: buttonWidth, height: buttonHeight))
        sellButton?.addTarget(self, action: #selector(CoinHeaderStyleBar.sellPressed(_:)), for: .touchUpInside)
        sellButton?.setTitle("SELL", for: UIControlState.normal)
        CKTheme.getCKLightButtonStyle(sellButton!, fontSizeL: buttonFontSize)
        ownerView?.addSubview(sellButton!)
        
        giveButton = UIButton(frame: CGRect(x: (buttonWidth*2)+10, y: buttonY, width: buttonWidth, height: buttonHeight))
        giveButton?.addTarget(self, action: #selector(CoinHeaderStyleBar.givePressed(_:)), for: .touchUpInside)
        giveButton?.setTitle("GIVE", for: UIControlState.normal)
        CKTheme.getCKButtonStyle(giveButton!, fontSizeL: buttonFontSize)
        ownerView?.addSubview(giveButton!)
    }
    
    
    
    func addSubViewOwnByElseView(buttonWidth: CGFloat, buttonHeight: CGFloat, buttonY: CGFloat, buttonFontSize: CGFloat, value: String?){
        
        let viewWidth: CGFloat = (buttonWidth*2)+10;
        let viewX: CGFloat = (self.frame.width - viewWidth)/2
        let viewY: CGFloat = ((self.mainActionView?.frame.height)! - buttonHeight)/2
        
        ownByElseView = UIView(frame: CGRect(x: viewX, y: viewY, width: viewWidth, height: buttonHeight))
        ownByElseView?.backgroundColor = UIColor.clear
        mainActionView?.addSubview(ownByElseView!)
        
        
        
        let kredLabel = UILabel(frame: CGRect(x: 5, y: 0, width: buttonWidth, height: buttonHeight));
       
        kredLabel.text = value;
        kredLabel.font = CKTheme.mediumFont(14.0)
        kredLabel.textAlignment = NSTextAlignment.right;
        kredLabel.textColor = CKColor.titleGreyColor;
        ownByElseView?.addSubview(kredLabel)
        
        
        buyButton = UIButton(frame: CGRect(x: buttonWidth+10, y: buttonY, width: buttonWidth, height: buttonHeight))
        buyButton?.addTarget(self, action: #selector(CoinHeaderStyleBar.buyPressed(_:)), for: .touchUpInside)
        buyButton?.setTitle("BUY", for: UIControlState.normal)
        CKTheme.getCKButtonStyle(buyButton!, fontSizeL: buttonFontSize)
        ownByElseView?.addSubview(buyButton!)
        
      
    }
    
    func addSubViewNotSaleView(buttonWidth: CGFloat, buttonHeight: CGFloat, buttonY: CGFloat, buttonFontSize: CGFloat){
        
        let viewWidth: CGFloat = buttonWidth;
        let viewX: CGFloat = (self.frame.width - viewWidth)/2
        let viewY: CGFloat = ((self.mainActionView?.frame.height)! - buttonHeight)/2
        
        notSaleView = UIView(frame: CGRect(x: viewX, y: viewY, width: viewWidth, height: buttonHeight))
        notSaleView?.backgroundColor = UIColor.clear
        mainActionView?.addSubview(notSaleView!)
        
        
        requestButton = UIButton(frame: CGRect(x: 0, y: buttonY, width: buttonWidth, height: buttonHeight))
        requestButton?.addTarget(self, action: #selector(CoinHeaderStyleBar.requestPressed(_:)), for: .touchUpInside)
        requestButton?.setTitle("REQUEST", for: UIControlState.normal)
        CKTheme.getCKButtonStyle(requestButton!, fontSizeL: buttonFontSize)
        notSaleView?.addSubview(requestButton!)
        
    }
    
    @objc func flagCoinPressed(_ sender: UIButton!) {
        delegate?.flagCoinPressedDelegateMethod!()
    }
    
    @objc func auctionPressed(_ sender: UIButton!) {
        delegate?.auctionPressedDelegateMethod!()
    }
    
    @objc func sellPressed(_ sender: UIButton!) {
        delegate?.sellPressedDelegateMethod!()
    }
    
    @objc func givePressed(_ sender: UIButton!) {
        delegate?.givePressedDelegateMethod!()
    }
    
    @objc func buyPressed(_ sender: UIButton!) {
        delegate?.buyPressedDelegateMethod!()
    }
    
    @objc func requestPressed(_ sender: UIButton!) {
        delegate?.requestPressedDelegateMethod!()
    }
    
    @objc func backPressed(_ sender: UIButton!) {
        delegate?.backPressedDelegateMethod!()
    }
    
}


struct StopWatch {
    
    var totalSeconds: Int
    
    var years: Int {
        return totalSeconds / 31536000
    }
    
    var days: Int {
        return (totalSeconds % 31536000) / 86400
    }
    
    var hours: Int {
        return (totalSeconds % 86400) / 3600
    }
    
    var minutes: Int {
        return (totalSeconds % 3600) / 60
    }
    
    var seconds: Int {
        return totalSeconds % 60
    }
    
    //simplified to what OP wanted
    var hoursMinutesAndSeconds: (hours: Int, minutes: Int, seconds: Int) {
        return (hours, minutes, seconds)
    }
}
//action = create; action = hold;



