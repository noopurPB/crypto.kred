//
//  CKCoinMessagesListTVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 09/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CKCoinMessagesListTVCDelegate{
    @objc optional func composePressedDelegateMethod()
    @objc optional func commentPressedDelegateMethod(message: CKMessage)
    @objc optional func morePressedDelegateMethod(message: CKMessage)
    @objc optional func circulationDelegateMethod()
    @objc optional func linkClickedDelegateMethod(coin: Coin?, linkUrl: String)
    @objc optional func backPressedDelegateMethod()
   
    @objc optional func auctionPressedDelegateMethod()
    @objc optional func sellPressedDelegateMethod()
    @objc optional func givePressedDelegateMethod()
    
    @objc optional func buyPressedDelegateMethod()
    @objc optional func requestPressedDelegateMethod()
    
    @objc optional func flagCoinPressedDelegateMethod()
    @objc optional func loginSignupPressedDelegateMethod()
}


class CKCoinMessagesListTVC: UITableViewController, CoinProfileMessageTVCellDelegate {
    
    
    var baseVC: CKBaseVC?
    var coinProfileVC: CKCoinProfileVC?
    var commentsVC: CKShowCommentsVC?
    
    var delegate: CKCoinMessagesListTVCDelegate?
    
    var messageType: CKConstants.MESSAGES_TYPE?
    var messages =  [CKMessage]()
    var issuedByBio: Bio?
    var heldByBio: Bio?
    
    enum COIN_TYPE{
        case REQUEST
        case OWNER
        case MARKET
    }
    
    var coinType: COIN_TYPE?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    func getCoinType() -> COIN_TYPE{
        if(CKGlobalVariables.sharedManager.loggedInUserId == self.coinProfileVC?.coin?.user){
            return COIN_TYPE.OWNER
        }else if (self.coinProfileVC?.coin?.type == "sale" || self.coinProfileVC?.coin?.type == "auction"){
            return COIN_TYPE.MARKET
        }else{
            return COIN_TYPE.REQUEST
        }
    }
    
    //MARK: Tableview Datasource and Delegate
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED && (indexPath.section == 0 || indexPath.section == 1)){
            return  0;
        }else{
            if(indexPath.section == 0 && indexPath.row == 0){
                return  300;
            }else if(indexPath.section == 0 && indexPath.row == 1){
                return 115
            }else if(indexPath.section == 0 && indexPath.row == 2){
                if(self.coinProfileVC != nil && self.coinProfileVC?.userIsLoggedIn() == false){
                    return 115
                }else{
                    return 0
                }
            }
            else if(indexPath.section == 1){
                //sale history
                if(getCoinType() == COIN_TYPE.REQUEST){
                    return 300;
                }else{
                    return 0;
                }
            }else{
                return 300;
            }
            
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        
        if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED && (indexPath.section == 0 || indexPath.section == 1)){
            return  0;
        }else{
            if(indexPath.section == 0 && indexPath.row == 0){
                return UITableViewAutomaticDimension;
            }else if(indexPath.section == 0 && indexPath.row == 1){
                return 115
            }else if(indexPath.section == 0 && indexPath.row == 2){
                if(self.coinProfileVC != nil && self.coinProfileVC?.userIsLoggedIn() == false){
                    return UITableViewAutomaticDimension;
                }else{
                    return 0
                }
            }else if(indexPath.section == 1){
                //sale history
                if(getCoinType() == COIN_TYPE.REQUEST){
                    return 300;
                }else{
                    return 0;
                }
            }else{
                return UITableViewAutomaticDimension;
            }
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
                return 0
            }else{
                if(self.coinProfileVC != nil && self.coinProfileVC?.userIsLoggedIn() == false){
                    return 3;
                }else{
                    return 2;
                }
                
            }
        }else if(section == 1){
            //sale history
            return 1;
        }
        else{
            return self.messages.count;
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.section == 0 && indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoinProfileHeaderTVCell") as! CoinProfileHeaderTVCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.loadHeader(coin: (self.coinProfileVC?.coin)!, issuedBio: self.issuedByBio, heldBio: heldByBio)
            cell.delegate = self
            return  cell;
    
        }else if(indexPath.section == 0 && indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoinHeaderTVCell") as! CoinProfileHeaderTVCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.delegate = self
            cell.loadActionHeader(coin: (self.coinProfileVC?.coin)!)
            
            if(getCoinType() == COIN_TYPE.OWNER){
                cell.showMyCollectionActionBtn()
            }else if (getCoinType() == COIN_TYPE.MARKET){
                cell.showBuyActionBtn(coin: (self.coinProfileVC?.coin)!)
            }else{
                cell.showRequestedActionBtn(requested: coinProfileVC?.coin?.coinRequested ?? false)
            }
            return cell
            
        }else if(indexPath.section == 0 && indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKLoginTVCell") as! CKBaseTVCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.anotherTitleLabel.sizeToFit()
            CKTheme.getCKButtonStyle(cell.rightsideBtn, fontSizeL: 15.0)
            cell.baseDelegate = self;
            return cell
        }
        else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKChartViewTVCell") as! CKChartViewTVCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoinGrabTVCell") as! CoinProfileMessageTVCell
            if(indexPath.row % 2 == 0){
                cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
            }else{
                cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
            }
            
                cell.delegate = self;
                let message = getMessage(index: indexPath.row)
                cell.likeBtn.tag = indexPath.row;
                cell.deleteBtn.tag = indexPath.row
                cell.commentBtn.tag = indexPath.row
                cell.setMessageView(message: message, tableView: tableView, indexPath: indexPath)
                
                if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED || getCoinType() == COIN_TYPE.OWNER){
                    cell.actionViewHeightConstraint.constant = 50.0;
                    cell.actionView.isHidden = false;
                }else{
                    
                    cell.actionViewHeightConstraint.constant = 50.0;
                    cell.actionView.isHidden = false;
                }
           
                
            return  cell;
                
          
        }
        
        /*if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoinHeaderTVCell") as! CoinProfileMessageTVCell
            cell.setSecondHeaderView(coinL: self.coinProfileVC?.coin)
            cell.delegate = self;
            
            if(messageType == CKConstants.MESSAGES_TYPE.COMMENTS){
                cell.messageLbl.text = "Write a comment"
                cell.messageLbl.textAlignment = NSTextAlignment.center;
                cell.likeBtn.isUserInteractionEnabled = false;
            }
            
            return  cell;
        }else{
            
            if(messageType == CKConstants.MESSAGES_TYPE.PROFILE || messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
                let cell = tableView.dequeueReusableCell(withIdentifier: "CoinGrabTVCell") as! CoinProfileMessageTVCell
                if(indexPath.row % 2 == 0){
                    cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
                }else{
                    cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
                }
                
                
                cell.delegate = self;
                let message = getMessage(index: indexPath.row)
                cell.likeBtn.tag = indexPath.row;
                cell.deleteBtn.tag = indexPath.row
                cell.commentBtn.tag = indexPath.row
                cell.setMessageView(message: message, tableView: tableView, indexPath: indexPath)
                
                if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
                    cell.actionViewHeightConstraint.constant = 0.0;
                    cell.actionView.isHidden = true;
                }else{
                    cell.actionViewHeightConstraint.constant = 50.0;
                    cell.actionView.isHidden = false;
                }
                
                
                return  cell;
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CoinCommentTVCell") as! CoinProfileMessageTVCell
                let message = getMessage(index: indexPath.row)
                cell.setMessageView(message: message, tableView: tableView, indexPath: indexPath)
                
                if(indexPath.row % 2 == 0){
                    cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
                }else{
                    cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
                }
                return  cell;
                
            }
            
        }*/
    }
    
    
    override func numberOfSections(in tableView: UITableView?) -> Int {
        if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
            return 3;
        }else{
            return 4
        }
        
        
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(section == 0){
            return nil
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKBaseHeadingTVCell") as! CKBaseTVCell
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2
            cell.titleLabel.font = CKTheme.regularFont(17.0)
            cell.titleLabel.textColor = CKColor.darkGrayColor
            if(section == 1){
                cell.titleLabel.text = "Sale History";
                cell.rightsideBtn.isHidden = true;
                cell.leftsideBtn.isHidden = true;
            }else if(section == 2){
                cell.titleLabel.text = "Comments";
            }else {
                cell.titleLabel.text = "Tags";
            }
            
            cell.rightsideBtn.isHidden = true;
            cell.leftsideBtn.isHidden = true;
            
            cell.rightsideBtn.setTitleColor(CKColor.darkGrayColor, for: UIControlState.normal)
            cell.leftsideBtn.setTitleColor(CKColor.darkGrayColor, for: UIControlState.normal)
            
            cell.rightsideBtn.setFAIcon(icon: .FAEdit, iconSize: 20.0, forState: UIControlState.normal)
            cell.leftsideBtn.setFAIcon(icon: .FAAngleRight, iconSize: 20.0, forState: UIControlState.normal)
            //cell.rightsideBtn.addTarget(self, action:  #selector(CKExploreVC.rightBtnClickedMethod(button:)), for: UIControlEvents.touchUpInside)
            return cell.contentView
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(section == 0 || messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
            return 0;
        }else {
            if(getCoinType() != COIN_TYPE.REQUEST && section == 1){
                return 0;
            }else{
                return 45;
            }
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*if(messageType == CKConstants.MESSAGES_TYPE.COMMENTS && indexPath.row == 0){
            delegate?.composePressedDelegateMethod!()
        }*/
    }
    
    func getMessage(index: Int) -> CKMessage?{
        return self.messages[index];
    }
    
    //MARK: CoinProfileMessageTVCellDelegate
    
    func auctionPressedDelegateMethod(){
        delegate?.auctionPressedDelegateMethod!()
    }
    
    
    func sellPressedDelegateMethod(){
        delegate?.sellPressedDelegateMethod!()
    }
    
    func givePressedDelegateMethod(){
        delegate?.givePressedDelegateMethod!()
    }
    
    func buyPressedDelegateMethod(){
        delegate?.buyPressedDelegateMethod!()
    }
    
    func requestPressedDelegateMethod(button: UIButton) {
        self.requestACoin(requestButton: button)
    }
    
    //MARK: Request A Coin
    func requestACoin(requestButton: UIButton){
        let cks = CKPostService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = self.coinProfileVC?.coin?.coin ?? 0
        params["wallet"] = CKGlobalVariables.sharedManager.walletId ?? 0
        
        self.coinProfileVC?.showLoadingDialog("")
        
        cks.requestACoin(params: params, responseHandler: { response in
            self.coinProfileVC?.hideLoadingDialog()
            self.coinProfileVC?.coin?.coinRequested = true;
            self.tableView.reloadData()
            
            //self.requestedCoins()
        }, errorHandler: { error in
            self.coinProfileVC?.hideLoadingDialog()
            self.coinProfileVC?.errorHandler(error)
        })
    }
    
    
    func circulationDelegateMethod(){
        self.delegate?.circulationDelegateMethod!()
    }
    
    
    
    
    func connectPressedDelegateMethod(button: UIButton, index: Int) {
        
    }
    
    
    
    func likeHeaderDelegateMethod(button: UIButton) {
        
        if(coinProfileVC?.coin?.liked != nil){
            coinProfileVC?.coin?.liked = !(coinProfileVC?.coin?.liked)!;
            
        }else{
            coinProfileVC?.coin?.liked = true;
        }
        
        let like = coinProfileVC?.coin?.liked;
        
        self.likeUnlikeCoinServiceCalled(like: like!)
        
        let likeL = coinProfileVC?.coin?.likes ?? 0
        
        if(like == true){
            coinProfileVC?.coin?.likes = likeL + 1
        }else{
            if(likeL == 0){
                coinProfileVC?.coin?.likes = 0
            }else{
                coinProfileVC?.coin?.likes = likeL - 1
            }
        }
        
        button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        let likes = coinProfileVC?.coin?.likes ?? 0
        if(likes == 0){
            //button.setTitle("", for: UIControlState.normal)
            button.setFAText(prefixText: "", icon: .FAHeart, postfixText: "", size: 18.0, forState: UIControlState.normal, iconSize: 18.0)
        }else{
            button.setFAText(prefixText: "", icon: .FAHeart, postfixText: String(format: "%d",  likes), size: 18.0, forState: UIControlState.normal, iconSize: 18.0)
            //button.setTitle(String(format: "%d",  likes), for: UIControlState.normal)
        }
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        
                        if(self.coinProfileVC?.coin?.liked != nil && self.coinProfileVC?.coin?.liked == true){
                            button.setFATitleColor(color: CKColor.themePinkColor)
                           // button.tintColor = CKColor.themeAnotherColor;
                        }else{
                            button.setFATitleColor(color: CKColor.titleGreyColor)
                           // button.tintColor = CKColor.titleGreyColor;
                        }
                        
                        button.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func composePressedDelegateMethod() {
        delegate?.composePressedDelegateMethod!()
    }
    
    func commentPressedDelegateMethod(index: Int?){
        delegate?.commentPressedDelegateMethod!(message: getMessage(index: index!)!)
    }
    
    
    func likePressedDelegateMethod(button: UIButton){
        let message = getMessage(index: button.tag)
        if(message?.liked != nil){
            message?.liked = !(message?.liked)!;
            
        }else{
            message?.liked = true;
        }
        
        let like = message?.liked;
        
        self.likeMessageServiceCalled(like: like!, message: message!);
        
        let likeL = message?.likes ?? 0
        
        if(like == true){
            message?.likes = likeL + 1
        }else{
            if(likeL == 0){
                message?.likes = 0
            }else{
                message?.likes = likeL - 1
            }
        }
        
        let newMessage = message;
        let indexOfMessage = self.messages.index(of: message!);
        self.messages.remove(at: indexOfMessage!);
        self.messages.insert(newMessage!, at: indexOfMessage!);
        
        if(message?.liked != nil && message?.liked == true){
            //unlike
            
        }else{
            //like
        }
        
        button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        
        
        let likes = message?.likes ?? 0
        if(likes == 0){
            button.setTitle("", for: UIControlState.normal)
        }else{
            button.setTitle(String(format: "%d",  likes), for: UIControlState.normal)
        }
        
        
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        
                        if(message?.liked != nil && message?.liked == true){
                            button.tintColor = CKColor.themeAnotherColor;
                        }else{
                            button.tintColor = CKColor.titleGreyColor;
                        }
                        
                        button.transform = CGAffineTransform.identity
        }, completion: nil)
        
        // delegate?.likePressedDelegateMethod!(message: getMessage(index: index!)!)
    }
    
    func deletePressedDelegateMethod(index: Int?){
        
        let alert = UIAlertController(title: NSLocalizedString("delete", comment: ""), message: NSLocalizedString("confirm_delete_message", comment: "")  , preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = CKColor.themeAnotherColor;
        alert.addAction(UIAlertAction(title: NSLocalizedString("alert_yes", comment: ""), style: .default, handler: { (action: UIAlertAction!) in
            self.deleteMessage(index: index)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("alert_no", comment: ""), style: .default, handler: { (action: UIAlertAction!) in }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func morePressedDelegateMethod(index: Int?){
        delegate?.morePressedDelegateMethod!(message: getMessage(index: index!)!)
    }
    
    
    func linkClickedDelegateMethod(index: Int, linkUrl: String) {
        let message = getMessage(index: index)
        self.delegate?.linkClickedDelegateMethod!(coin: message?.data?.coin, linkUrl: linkUrl)
    }
    
    //MARK: Like Unlike Coin
    
    func likeUnlikeCoinServiceCalled(like: Bool){
        let cks = CKPostService()
        DispatchQueue.main.async {
            var params = [String: Any]()
            params = CKServiceConstants.initializeParams()
            params["coin"] = self.coinProfileVC?.coin?.coin ?? 0
            
            cks.coinLikeUnlike(like, params: params, responseHandler: { response in }, errorHandler: { error in })
        }
    }
    
    
    //MARK: Like Unlike Messages
    
    func likeMessageServiceCalled(like: Bool, message: CKMessage){
        let cks = CKPostService()
        DispatchQueue.main.async {
            
            var params = [String: Any]()
            params = CKServiceConstants.initializeParams()
            params["parent"] = message.id;
            
            cks.likeUnlikeMessages(like, params: params, responseHandler: { response in }, errorHandler: { error in })
        }
    }
    
    //MARK: Delete Messages
    
    func deleteMessage(index: Int?){
        let message = getMessage(index: index!)
        self.deleteMessageServiceCalled(message: message!);
        let indexOfMessage = self.messages.index(of: message!);
        self.messages.remove(at: indexOfMessage!);
        self.tableView.reloadData()
    }
    
    
    func deleteMessageServiceCalled(message: CKMessage){
        let cks = CKPostService()
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams();
        
        var grabStr: String?
        if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
            grabStr = message.data?.coin?.grab ?? ""
        }else{
            grabStr = self.coinProfileVC?.coin?.grab ?? ""
        }
        
        
        DispatchQueue.main.async {
            let pathExt = grabStr ?? "" + "/" + message.id!
            cks.deleteMessages(params: params, pathExt: pathExt, responseHandler: { response in }, errorHandler: { error in })
        }
    }
    
    
    
    
    
    //MARK: Get messages
    func coinMessagesServiceMethod(dataHandler:@escaping () -> Void) {
        var params = [String: Any]()
        
        if(messageType == CKConstants.MESSAGES_TYPE.COMMENTS){
            params["id"] = commentsVC?.addCommentObject?.messageId;
        }else {
            params = CKServiceConstants.initializeParams()
            //new=70
        }
        
        params["format"] = "json";
        params["count"] = CKServiceConstants.ITEM_PER_PAGE;
        params["page"] = 1;
        
        var grab = "";
        if(messageType == CKConstants.MESSAGES_TYPE.NEWSFEED){
            grab = String(format: "collection.%@.grab" , CKGlobalVariables.sharedManager.loggedInUserId)
        }else if(messageType == CKConstants.MESSAGES_TYPE.PROFILE){
            grab = self.coinProfileVC?.coin?.grab ?? ""
        }
        
        let cks = CKGetService()
        
        self.baseVC?.showSearchView(self.view, searchText: "")
        
        
        func commonHandler(response: CKCoinsResponseObject){
            self.messages = response.messages;
            self.tableView.reloadData();
        }
        
        
        cks.getCoinMessages(showCache: true, messageType: self.messageType!, grab: grab, params: params, responseHandler: { response in
            commonHandler(response: response)
            self.baseVC?.hideSearchView()
            dataHandler();
            
        }, cacheHandler: { response in
            commonHandler(response: response)
            
        }, errorHandler: { error in
            self.baseVC?.hideSearchView()
            self.baseVC?.errorHandler(error)
        })
        
    }
    
    //MARK: User Info
    func userInfoProfile(user: String?, creator: Bool?){
        
        let cks = CKGetService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["user"] = user ?? ""
        
        func cH(response: CKUserValidationResponseObject){
            if(creator == true){
                self.issuedByBio = response.user?.bio;
            }else{
                self.heldByBio = response.user?.bio;
            }
        }
        cks.getUserDomainProfile(params: params, responseHandler: { response in
            cH(response: response)
           // self.tableview.reloadData()
            
        }, cacheHandler: { response in
            cH(response: response)
           // self.tableview.reloadData()
        },
           errorHandler: { error in
            self.coinProfileVC?.errorHandler(error)
        })
    }
    
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            let a = scrollView.contentOffset
            if a.y <= 0 {
                scrollView.contentOffset = CGPoint.zero // this is to disable tableview bouncing at top.
            }
        }
    }
    
    
    
    
}

extension CKCoinMessagesListTVC: CoinProfileHeaderTVCellDelegate{
    
    func flagCoinPressedDelegateMethod() {
        delegate?.flagCoinPressedDelegateMethod!()
    }
    
    func backPressedDelegateMethod() {
        delegate?.backPressedDelegateMethod!()
    }
    
    
}




extension CKCoinMessagesListTVC: CKBaseTVCellDelegate{
    
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        self.delegate?.loginSignupPressedDelegateMethod!()
    }
    
    func leftBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        
    }
    
}
