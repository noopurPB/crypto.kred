//
//  CKCoinProfileVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 09/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit
import BLKFlexibleHeightBar

class CKCoinProfileVC: CKBaseVC, CoinHeaderStyleBarDelegate, CKCoinMessagesListTVCDelegate, CKAddCommentVCDelegate {
    
    @IBOutlet weak var iPhoneXView: UIView!
    var messagesListTableVC: CKCoinMessagesListTVC?
    
    var myCustomBar: CoinHeaderStyleBar?
    var delegateSplitter: BLKDelegateSplitter?
    
    
    var headerMinHeightWithoutCTA: CGFloat =  120;
    var headerMaxHeightWithoutCTA: CGFloat =  355; 
    
    var coin: Coin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.isHidden = true;
        setColor()  
        getCoinHistory(coinId: self.coin?.coin)
        initCoinProfileListTableView()
        navBarStyle?.showNavigationBackBarItem(self, navTitle: "")
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if (self.navigationController?.topViewController != self)
        {
            self.navigationController?.isNavigationBarHidden = false;
        }
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false;
    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initCoinProfileListTableView(){
        if(messagesListTableVC == nil){
            messagesListTableVC = self.storyboard?.instantiateViewController(withIdentifier: "CKCoinMessagesListTVC") as? CKCoinMessagesListTVC
            messagesListTableVC?.coinProfileVC = self;
            messagesListTableVC!.delegate = self
            messagesListTableVC!.baseVC = self;
            messagesListTableVC!.view.frame = CGRect(x: 0,y: 0,width: (self.view.frame.size.width),height: (self.view.frame.size.height))
             messagesListTableVC?.messageType = CKConstants.MESSAGES_TYPE.PROFILE;
           
            if #available(iOS 11.0, *) {
                 messagesListTableVC!.tableView.contentInsetAdjustmentBehavior = .never
            }
            messagesListTableVC?.coinMessagesServiceMethod(dataHandler: {
                
            })
            
            if(coin?.user != nil && coin?.creator != nil && coin?.user != coin?.creator){
                messagesListTableVC?.userInfoProfile(user: coin?.user ?? "", creator: false)
                messagesListTableVC?.userInfoProfile(user: coin?.creator ?? "", creator: true)
            }else{
                messagesListTableVC?.userInfoProfile(user: coin?.creator ?? "", creator: true)
            }
            self.iPhoneXView.addSubview((messagesListTableVC?.view)!)
           // initTopImageViewPost()
            
        }
    }
    
    func setColor(){
        var color: UIColor = CKColor.tableviewCellColor2;
        if(self.coin?.coinColor?.contains("#"))!{
            color = UIColor(hexString: (self.coin?.coinColor)!)
        }else{
            color = UIColor(hexString: "#" + (self.coin?.coinColor)!)
        }
        //self.myCustomBar?.backgroundColor = color
        self.iPhoneXView.backgroundColor = color
        self.view.backgroundColor = color;
    }
    
    
    func initTopImageViewPost(){
        self.myCustomBar = CoinHeaderStyleBar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: headerMinHeightWithoutCTA))
        let  behaviorDefiner = SquareCashStyleBehaviorDefiner()
        behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart: 0.0, end: 0.5)
        behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart: 0.5, end: 1.0)
        behaviorDefiner.isSnappingEnabled = false
        behaviorDefiner.isElasticMaximumHeightAtTop = false
        self.myCustomBar!.behaviorDefiner = behaviorDefiner
        self.myCustomBar?.maximumBarHeight = headerMaxHeightWithoutCTA
        self.myCustomBar?.minimumBarHeight = headerMinHeightWithoutCTA //64.0 + 50
        self.myCustomBar?.coinProfile = self;
        self.myCustomBar?.delegate = self
        
       // setCoinHeaderValues()
        
       // self.myCustomBar?.menuButton!.addTarget(revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: UIControlEvents.touchUpInside)

        // Configure a separate UITableViewDelegate and UIScrollViewDelegate (optional)
        self.delegateSplitter = BLKDelegateSplitter(firstDelegate: behaviorDefiner, secondDelegate: messagesListTableVC)
        self.messagesListTableVC?.tableView.delegate = self.delegateSplitter
    }
    
    
    func setCoinHeaderValues(){
        self.myCustomBar?.setActionView(coinL: self.coin)
        self.myCustomBar?.setCoinView(coinL: self.coin);
        self.myCustomBar?.setIssuerView(coinL: self.coin);
        
        self.setColor()
        
        /*if color.isLight {
            self.myCustomBar?.valueDynamicLabel?.textColor = CKColor.titleGreyColor
        } else {
            self.myCustomBar?.valueDynamicLabel?.textColor = UIColor.white
        }*/
        
        
        let price = self.coin?.auction_price ?? String(format: "%.2f", self.coin?.value ?? 0);
        self.myCustomBar?.valueDynamicLabel?.text = String(format: "%@ %@", price, NSLocalizedString("kred_name", comment: ""))
    }
    
    
    
    //MARK: CoinHeaderStyleBarDelegate
    
    func flagCoinPressedDelegateMethod(){
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "CKFlagCoinVC") as?
        CKFlagCoinVC
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        controllerObejct?.coinId = self.coin?.coin;
        self.present(controllerObejct!, animated: true, completion: nil)
    }
    
     func auctionPressedDelegateMethod(){
        self.auctionCoinCalledVC(coin: self.coin)
     }
    
    
     func sellPressedDelegateMethod(){
        self.sellCoinCalledVC(coinL: self.coin)
     }
    
     func givePressedDelegateMethod(){
        self.giveCoinCalledVC(coin: self.coin)
     }
    
     func buyPressedDelegateMethod(){
        
     }
    
    
    func backPressedDelegateMethod(){
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    
    func loginSignupPressedDelegateMethod() {
        self.loginViewControllerCalled(login: true)
    }
    
    
    //MARK: CKCoinMessagesListTVCDelegate
    
    func circulationDelegateMethod() {
        self.coinCirculationVCCalled(coinIdL: self.coin?.coin ?? 0)
    }
    
    func composePressedDelegateMethod() {
        let commentObj = AddCommentObject();
        commentObj.grabId = self.coin?.grab;
        let vc = coinAddCommentVCCalled(addComment: false, commentObjectL: commentObj);
        vc.delegate = self;
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func commentPressedDelegateMethod(message: CKMessage){
        let commentObj = AddCommentObject();
        commentObj.grabId = self.coin?.grab;
        commentObj.messageId = message.id ?? "";
        
        if(message.commentsCount != nil && message.commentsCount != 0){
            self.coinShowCommentVCCalled(addCommentObj: commentObj);
        }else{
            let vc = coinAddCommentVCCalled(addComment: true, commentObjectL: commentObj);
            vc.delegate = self;
            self.present(vc, animated: true, completion: nil)
        }
    }
    
   
    func morePressedDelegateMethod(message: CKMessage){
        
    }
    
  
    //MARK: CKAddCommentVCDelegate
    
    func postDoneDelegateMethod(addComment: Bool ,message: CKMessage?) {
        if(addComment == false){
            self.messagesListTableVC?.messages.insert(message!, at: 0)   //.append(message!);
            self.messagesListTableVC?.tableView.reloadData();
        }else{
            self.messagesListTableVC?.coinMessagesServiceMethod(dataHandler: {
                
            })
        }
        
    }
    
    
    override func loginSignupSuccessMethod(){
        self.messagesListTableVC?.tableView.reloadData()
    }
    
    
    
    //MARK: Service methods
    
    func getCoinHistory(coinId: Int?) {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = coinId;
        self.coinHistoryServiceMethod(params: params)
    }
    
    
    func coinHistoryServiceMethod(params: [String: Any]) {
        let cks = CKGetService()
        cks.getCoinHistory(params: params,
                responseHandler: { response in
                    if(response.history.count != 0){
                        self.coin?.history = response.history[0];
                    }
                    self.setCoinHeaderValues();
                },
                errorHandler: { error in
                    self.setCoinHeaderValues();
        })
        
    }
    
    
}

extension UIColor {
    var isLight: Bool {
        var white: CGFloat = 0
        getWhite(&white, alpha: nil)
        return white > 0.5
    }
}




