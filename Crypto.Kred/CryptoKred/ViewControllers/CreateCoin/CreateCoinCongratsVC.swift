//
//  CreateCoinCongratsVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CreateCoinCongratsVC: CreateCoinCommonVC, UIWebViewDelegate {
    
    @IBOutlet weak var giveToFrndBtn: UIButton!
    @IBOutlet weak var congratsLabel: UILabel!
    var draft: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCoinView()
    
        if(giveToFrndBtn != nil){
            Theme.getConnectedButtonStyleLight(giveToFrndBtn, color: UIColor.white)
        }
        self.congratsLabel.textColor = CKColor.titleGreyColor;
        self.congratsLabel.font = CKTheme.regularFont(25.0)
        
        if(draft == true){
            self.headingLabel.text = "Your new Kred Coin is now in your Draft"
            self.giveToFrndBtn.setTitle("USE DRAFT", for: UIControlState.normal)
        }else{
            self.headingLabel.text = "Your new Kred Coin is now in your Collection";
            self.giveToFrndBtn.setTitle("GIVE TO A FRIEND", for: UIControlState.normal)
        }
        self.navigationItem.leftBarButtonItem = nil;
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func giveToFriendClicked(_ sender: UIButton) {
        if(draft == true){
            self.givecoinChooseVC(coin: CKGlobalVariables.sharedManager.coin, draftCoin: true)
        }else{
            self.giveCoinCalledVC(coin: CKGlobalVariables.sharedManager.coin)
        }
        
    }
    
    @IBAction func doneClicked(_ sender: UIButton) {
        self.collectionViewVCCalled(userCollection: false, selectedCoin: nil)
    }
    
    
    func setCoinView(){
        let viewWidth: CGFloat = 340.0;
        let viewHeight: CGFloat = 200.0;
        let x = (self.view.frame.width - viewWidth)/2
        let y = (self.view.frame.height - viewHeight)/2
        let coinView = CoinView(frame: CGRect(x: x, y: y, width: viewWidth , height: viewHeight))
        coinView.coin = CKGlobalVariables.sharedManager.coin;
        coinView.backgroundColor = UIColor.clear;
        self.view.addSubview(coinView)
        
    }
    
    
   
   
}

