//
//  CKUserValidationVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 20/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


protocol CKUserValidationVCDelegate{
    func crossClickedDelegateMethod()
    func verificationCompletedDelegateMethod()
}


class CKUserValidationVC: CKCommonVC, UITableViewDelegate, UITableViewDataSource{
  
    @IBOutlet weak var subHeadingLbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var delegate: CKUserValidationVCDelegate?
    var responseOverall: CKUserValidationResponseObject?
    
    var profileUrl: String?
    var phoneno: String?
    var smsCode: String?
    
    var smsCodeSent: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.subHeadingLbl != nil){
            self.subHeadingLbl.textColor = CKColor.titleGreyColor;
            self.subHeadingLbl.font = CKTheme.mediumFont(15.0)
        }
        userDomainProfile();
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   
    @IBAction func userCrossClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate?.crossClickedDelegateMethod()
        })
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0 && indexPath.row == 0){
            //phone
            if(responseOverall?.phone != nil && responseOverall?.phone == true){
                return 50;
            }else{
                return 100;
                
            }
        }else if(indexPath.section == 0 && indexPath.row == 1){
            //Manual Call
            if(smsCodeSent == true){
                return 0;
            }
            return 100;
        }
        else{
            //domain
            if(responseOverall?.kreddomain != nil && responseOverall?.kreddomain == true){
                return 50;
            }else{
                if(self.profileUrl == nil || self.profileUrl == ""){
                    //enter profile url
                    return 100;
                }else{
                    //only verify
                    return 50;
                }
                
            }
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 0 && indexPath.row == 0){
            //phone
            if(responseOverall?.phone != nil && responseOverall?.phone == true){
                return 50;
            }else{
                return 100;
                
            }
        }else if(indexPath.section == 0 && indexPath.row == 1){
            //Manual Call
            if(smsCodeSent == true){
                return 0;
            }
            return UITableViewAutomaticDimension
        }
        else{
            //domain
            if(responseOverall?.kreddomain != nil && responseOverall?.kreddomain == true){
                return 50;
            }else{
                if(self.profileUrl == nil || self.profileUrl == ""){
                    //enter profile url
                   return 100;
                }else{
                    //only verify
                   return 50;
                }
                
                
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            //phone
            return 2
        }else{
            //domain
            return 1;
        }
        
        
       
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0 && indexPath.row == 0){
            //phone
            if(responseOverall?.phone != nil && responseOverall?.phone == true){
                return verifiedCell(tableView: tableview, indexPath: indexPath, verifiedPhone: true)
            }else{
                if(smsCodeSent == true){
                    return enterValueCell(tableView: tableview, indexPath: indexPath, phoneField: true, smsField: true)
                }else{
                    return enterValueCell(tableView: tableview, indexPath: indexPath, phoneField: true, smsField: false)
                }
                
            }
        }else if(indexPath.section == 0 && indexPath.row == 1){
            //Manual Call
            //CKManualCallVCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKManualCallVCell", for: indexPath) as! CKBaseTVCell
            self.setTitleFont(titleLabel: cell.titleLabel)
            cell.titleLabel.textColor = CKColor.themeAnotherColor;
            cell.titleLabel.text = "Can't receive SMS? Request a manual verification and an agent will call you";
            cell.titleLabel.sizeToFit()
            return cell
        }
        else{
            //domain
            if(responseOverall?.kreddomain != nil && responseOverall?.kreddomain == true){
                return verifiedCell(tableView: tableview, indexPath: indexPath, verifiedPhone: false)
            }else{
                if(self.profileUrl == nil || self.profileUrl == ""){
                    //enter profile url
                   return enterValueCell(tableView: tableview, indexPath: indexPath, phoneField: false, smsField: false)
                }else{
                    //only verify
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CKDomainTVCell", for: indexPath) as! CKBaseTVCell
                    self.setTitleFont(titleLabel: cell.titleLabel)
                    CKTheme.getCKButtonStyle( cell.rightsideBtn, fontSizeL: 15.0)
                    cell.baseDelegate = self;
                    cell.titleLabel.text = String(format: "Domain: %@", self.profileUrl!)
                    cell.rightsideBtn.tag = indexPath.section
                    return cell
                }
                
                
            }
            
        }
        
        
        
     
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0 && indexPath.row == 1){
            //manual calling
            CKGlobalMethods.showSuccessALertWithMessage("", message:"We will contact you shortly!" )
        }
    }
    
    func setTitleFont(titleLabel: UILabel){
       titleLabel.font = CKTheme.boldFont(15.0)
       titleLabel.textColor = CKColor.titleGreyColor;
    }
    
    
    
    func verifiedCell(tableView: UITableView,indexPath: IndexPath, verifiedPhone: Bool) -> CKBaseTVCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKVerifiedTVCell", for: indexPath) as! CKBaseTVCell
        self.setTitleFont(titleLabel: cell.titleLabel)
        cell.anotherTitleLabel.textColor = CKColor.verifiedGreen;
        cell.anotherTitleLabel.font = CKTheme.boldFont(15.0)
        cell.anotherTitleLabel.setFAText(prefixText: "", icon: .FACheck, postfixText: " Verified", size: 15.0)
        if(verifiedPhone == true){
            cell.titleLabel.text = String(format: "Phone: %@", self.phoneno ?? "")
        }else{
            cell.titleLabel.text =  String(format: "Domain: %@", self.profileUrl ?? "")
        }
        return cell
    }
    
    
    func enterValueCell(tableView: UITableView,indexPath: IndexPath, phoneField: Bool, smsField: Bool) -> CKTextFieldTVCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKTextFieldTVCell", for: indexPath) as! CKTextFieldTVCell
        self.setTitleFont(titleLabel: cell.titleLabel)
        cell.textField.delegate = self
        cell.textField.tag = indexPath.row;
        cell.textField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
        
        if(phoneField == false && smsField == false){
            //Domain
            cell.titleLabel.text = "Domain:"
            cell.textField.placeholder = "YourName.Kred";
            cell.rightsideBtn.setTitle("CREATE DOMAIN", for: UIControlState.normal)
            cell.textField.text = self.profileUrl ?? ""
            cell.dividerView.isHidden = false;
        }else{
            if(smsField == false){
                cell.titleLabel.text = "Phone:"
                cell.textField.placeholder = "+1234567890";
                cell.rightsideBtn.setTitle("SEND SMS CODE", for: UIControlState.normal)
                cell.textField.text = self.phoneno ?? ""
                cell.dividerView.isHidden = true;
                
            }else{
                cell.titleLabel.font = CKTheme.boldFont(13.0)
                cell.titleLabel.text = "SMS Code:"
                cell.textField.placeholder = "123456";
                cell.rightsideBtn.setTitle("VALIDATE PHONE", for: UIControlState.normal)
                cell.textField.text = self.smsCode ?? ""
                cell.dividerView.isHidden = false;
            }
            
        }
        CKTheme.setTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
        CKTheme.getCKButtonStyle(cell.rightsideBtn, fontSizeL: 15.0)
        cell.baseDelegate = self;
        cell.textField.delegate = self;
        cell.rightsideBtn.tag = indexPath.section
        return cell
    }
    
    
    @IBAction func textFieldEditingDidChange(_ sender: UITextField){
        if(sender.tag == 0 && smsCodeSent == false){
            self.phoneno = sender.text;
        }else if(sender.tag == 0 && smsCodeSent == true){
            self.smsCode = sender.text;
        }else if (sender.tag == 2){
            self.profileUrl = sender.text;
        }
    }
    
    
   
    
    
    //MARK: 1. Services method
    
    func userValidationCheck(path: String, params: [String: Any], responseHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void){
        let cks = CKGetService()
        self.showLoadingDialog("")
       
        cks.userValidationAPI(path: path, params: params, responseHandler:
            { response in
                self.hideLoadingDialog()
                if(response.error != nil){
                    self.showErrorMessage(response.message ?? response.error ?? "")
                }else{
                   responseHandler(response)
                }
                
        }, errorHandler: { error in
            self.hideLoadingDialog();
            self.errorHandler(error);
        })
    }
    
    //MARK: 2. Get domain profile
    
    func userDomainProfile(){
        let cks = CKGetService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        cks.getUserDomainProfile(params: params, responseHandler: { response in
            self.profileUrl = response.home;
            self.phoneno = response.login?.bio?.phone ?? ""
            self.tableview.reloadData()
            
        }, cacheHandler: { response in
            self.profileUrl = response.home;
            self.tableview.reloadData()
        },
            errorHandler: { error in
                self.errorHandler(error)
        })
    }
    
    
    func sendSMSCode(){
        
        if(self.phoneno == nil || CKGlobalMethods.trimString(self.phoneno) == ""){
            self.showErrorMessage(NSLocalizedString("enter_valid_phoneno", comment: ""))
            return
        }
        
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        params["method"] = "phone"
        params["user_id"] = CKGlobalVariables.sharedManager.loggedInUserId
        params["address"] = self.phoneno ?? ""
        
        userValidationCheck(path: CKServiceConstants.PHONE_SMS_SEND_URL, params: params, responseHandler: { response in
            self.smsCodeSent = true;
            self.tableview.reloadData()
        })
        
    }
    
    func verifySMSCode(){
        
        if(self.smsCode == nil || CKGlobalMethods.trimString(self.smsCode) == ""){
            self.showErrorMessage(NSLocalizedString("enter_valid_code", comment: ""))
            return
        }
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["code"] = self.smsCode ?? ""
        userValidationCheck(path: CKServiceConstants.PHONE_SMS_ACCEPT_URL, params: params, responseHandler: { response in
            
            if(response.message == nil){
                self.showErrorMessage(NSLocalizedString("enter_valid_code", comment: ""))
            }else{
                self.responseOverall?.phone = true;
                self.tableview.reloadData()
            }
            self.bothVerified()
        })
    }
    
    
    func verifyDomain(){
        if(self.profileUrl == nil || CKGlobalMethods.trimString(self.profileUrl) == ""){
            self.showErrorMessage(NSLocalizedString("enter_valid_domain", comment: ""))
            return
        }
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["domain"] = self.profileUrl ?? ""
        
        userValidationCheck(path: CKServiceConstants.USER_DOMAIN_CHECK_URL, params: params, responseHandler: { response in
            self.responseOverall?.kreddomain = true;
            self.tableview.reloadData();
            self.bothVerified()
        })
        
    }
    
    func bothVerified(){
        if(self.responseOverall?.phone == true && self.responseOverall?.kreddomain == true){
            self.dismiss(animated: true, completion: nil)
        }
    }

 
}

extension CKUserValidationVC :CKBaseTVCellDelegate{
    
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        if(sender.tag == 0){
            if(smsCodeSent == true){
                self.verifySMSCode()
            }else{
                self.sendSMSCode()
            }
        }else{
            //verify kred domain
            /*if((self.profileUrl != nil && self.profileUrl != "") ||
                (phoneTxtField != nil && phoneTxtField?.text?.count != 0)){
                 self.verifyDomain()
            }else{
                showErrorMessage("Please enter valid domain")
            }*/
            self.verifyDomain();
            
           
        }
        self.tableview.reloadData();
    }
    
}


