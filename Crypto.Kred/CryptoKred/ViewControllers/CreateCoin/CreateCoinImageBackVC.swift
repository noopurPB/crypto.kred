//
//  CreateCoinImageBackVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit
class CreateCoinImageBackVC: CreateCoinCommonVC {
    
    @IBOutlet weak var coinView: UIView!
    @IBOutlet weak var uploadImageBtn: UIButton!
    @IBOutlet weak var matchFrontBtn: UIButton!

    var singleCoinView: CoinViewSingle?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enableNextButton()
        CKGlobalVariables.sharedManager.coin?.back = coinDefaultImage;
        showUploadBtnIcon()
        addSubViewCoin()
        
        if(matchFrontBtn != nil){
            CKTheme.getCKLightButtonStyle(self.matchFrontBtn, fontSizeL: 18.0)
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func addSubViewCoin(){
        let xAxis = (self.coinView.frame.size.width - coinViewSize)/2
        let yAxis = (self.coinView.frame.size.height - coinViewSize)/2
        let coinFrame: CGRect = CGRect(x: xAxis, y: yAxis, width: coinViewSize, height: coinViewSize)
        
        self.singleCoinView = CKGlobalMethods.addSubviewSingleCoin(coinframe: coinFrame, view: self.coinView, coin: CKGlobalVariables.sharedManager.coin, showFrontCoin: false)
    }
    
    
    
    @IBAction func nextClicked(_ sender: UIButton) {
        createCoinDoneVCCalled()
    }
    
    @IBAction func matchFrontClicked(_ sender: UIButton) {
        
        if(CKGlobalVariables.sharedManager.coin?.front != nil){
           updateCoinBackImage(filepath: CKGlobalVariables.sharedManager.coin?.front)
        }
    }
    
    
    @IBAction func uploadImageClicked(_ sender: UIButton) {
        showImageSelector(true, showSquareCropL: true)
    }
    
    
    //MARK: Set Image Notification
    
    override func setImageNotifictaionReceived(_ notification: Notification) {
        if notification.object is String {
            let filepath: String = notification.object as! String
            updateCoinBackImage(filepath: filepath)
        }
    }
    
    
    func updateCoinBackImage(filepath: String?){
        CKGlobalVariables.sharedManager.coin?.back =  filepath ?? coinDefaultImage
        self.singleCoinView?.coin = CKGlobalVariables.sharedManager.coin;
        self.singleCoinView?.loadCoin(webView: (self.singleCoinView?.webView)!)
        enableNextButton()
        showUploadBtnIcon()
    }
    
    
    
    func enableNextButton(){
        if(CKGlobalVariables.sharedManager.coin?.back != nil && CKGlobalVariables.sharedManager.coin?.back != ""){
            CKTheme.enableOrDisableButton(button: nextBtn, enable: true)
        }else{
            CKTheme.enableOrDisableButton(button: nextBtn, enable: false)
        }
    }
    
    
    func showUploadBtnIcon(){
        if(CKGlobalVariables.sharedManager.coin?.back != nil && CKGlobalVariables.sharedManager.coin?.back == coinDefaultImage){
            self.uploadImageBtn.imageView?.image  = nil;
        }else{
            self.uploadImageBtn.imageView?.image = UIImage(named: uploadBtnDefaultImage)
        }
        
    }
}

