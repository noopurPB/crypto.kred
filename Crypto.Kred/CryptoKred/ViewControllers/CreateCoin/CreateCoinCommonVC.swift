//
//  CreateCoinCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CreateCoinCommonVC: CKBaseVC, UIScrollViewDelegate {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    
    var coinDefaultImage: String = "ck_transparent_upload.png";
    var uploadBtnDefaultImage: String = "upload_white_icon";
    var coinViewSize: CGFloat = 220;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarStyle?.showNavigationBackBarItem(self, navTitle: NSLocalizedString("create_a_coin", comment: ""))
        navBarStyle?.delegate = self;
        if(nextBtn != nil){
            CKTheme.getCKButtonStyle(nextBtn, fontSizeL: 18.0)
        }
        
        if(headingLabel != nil){
            self.formatHeadingTitle(label: self.headingLabel)
        }
        if(self.tableView != nil){
            self.registerCKHeadingTVCell(self.tableView)
            self.registerCKTextFieldTVCell(self.tableView)
        }
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         showKeyboardNotification()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Keyboard notification
    func showKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(CreateCoinCommonVC.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateCoinCommonVC.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: Keyboard show hide method
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                if(tableView != nil){
                    self.tableView.contentInset = contentInsets
                    self.tableView.scrollIndicatorInsets = contentInsets
                   // self.tableView.isScrollEnabled = true
                
                }
            }
        }
    }
    
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if(self.tableView != nil){
            self.tableView.contentInset = UIEdgeInsets.zero
            self.tableView.scrollIndicatorInsets = UIEdgeInsets.zero
            //self.tableView.isScrollEnabled = false
            
        }
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y <= -64){
            scrollView.contentOffset.y = -64
        }
    }
    
    
    
    func setUploadImageviewUI(uploadImageBtn: UIImageView){
        uploadImageBtn.clipsToBounds = true;
        uploadImageBtn.layer.masksToBounds = true;
        uploadImageBtn.layer.cornerRadius = uploadImageBtn.frame.width/2;
        uploadImageBtn.contentMode = UIViewContentMode.scaleAspectFill;
    }
    
    func coinTextGradientColor(){
        let gradient = CAGradientLayer()
        
        // gradient colors in order which they will visually appear
        gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // set the gradient layer to the same size as the view
        gradient.frame = view.bounds
        // add the gradient layer to the views layer for rendering
        view.layer.addSublayer(gradient)
    }
    
    
    func formatHeadingTitle(label: UILabel){
        label.font = Theme.mediumFont(subHeadingLbl_fontsize)
        label.textColor = CKColor.titleGreyColor
    }

    func setShadow(label: CircularLabel, view: UIView){
        let shadowColor:UIColor = UIColor(red: (162 / 255.0), green: (82 / 255.0), blue: (0 / 255.0), alpha: 1.0)
        label.layer.shadowColor = Color.coinDefaultColor.cgColor
        label.layer.shadowRadius = 0.1
        label.layer.shadowOpacity = 1.0
        label.layer.shadowOffset = CGSize(width: 1, height: 1)//CGSize.zero
        label.layer.masksToBounds = false;
        view.addSubview(label)
    }
    
    
   
    
    func setUpperandLowerTextTheme(view: UIView, label: CircularLabel, text: String?){
        label.text = text ?? ""
        label.font = Theme.boldFont(19.0)
        
       // let colorT = UIColor(patternImage: gradientImage(size: label.frame.size , color1: CIColor(color: Color.coinDefaultColor), color2: CIColor(color: UIColor.white)))
        label.textColor = .white //Color.coinDefaultColor;
        label.backgroundColor = UIColor.clear
        label.isUserInteractionEnabled = false;
        self.setShadow(label: label, view: view)
        
        
       /* label.attributedText = NSMutableAttributedString(string: text ?? "",
                                                         attributes: stroke(font: Theme.boldFont(19.0),
                                                                            strokeWidth: 10, insideColor: Color.coinDefaultColor, strokeColor: .black))*/

        
        
    }
    
    
    public func stroke(font: UIFont, strokeWidth: Float, insideColor: UIColor, strokeColor: UIColor) -> [NSAttributedStringKey: Any]{
        return [
            NSAttributedStringKey.strokeColor : strokeColor,
            NSAttributedStringKey.foregroundColor : insideColor,
            NSAttributedStringKey.strokeWidth : -strokeWidth,
            NSAttributedStringKey.font : font
        ]
    }

  
    
    
    func gradientImage(size: CGSize, color1: CIColor, color2: CIColor) -> UIImage {
        
        let context = CIContext(options: nil)
        
        let filter = CIFilter(name: "CILinearGradient")
        var startVector: CIVector
        var endVector: CIVector
        
        filter!.setDefaults()
        
        startVector = CIVector(x: size.width * 0.5, y: 0)
        endVector = CIVector(x: size.width * 0.5, y: size.height)
        
        /*switch direction {
         case .Up:
         startVector = CIVector(x: size.width * 0.5, y: 0)
         endVector = CIVector(x: size.width * 0.5, y: size.height)
         case .Left:
         startVector = CIVector(x: size.width, y: size.height * 0.5)
         endVector = CIVector(x: 0, y: size.height * 0.5)
         case .UpLeft:
         startVector = CIVector(x: size.width, y: 0)
         endVector = CIVector(x: 0, y: size.height)
         case .UpRight:
         startVector = CIVector(x: 0, y: 0)
         endVector = CIVector(x: size.width, y: size.height)
         }*/
        
        filter!.setValue(startVector, forKey: "inputPoint0")
        filter!.setValue(endVector, forKey: "inputPoint1")
        filter!.setValue(color1, forKey: "inputColor0")
        filter!.setValue(color2, forKey: "inputColor1")
        
        
        let image = UIImage(cgImage: context.createCGImage(filter!.outputImage!, from: CGRect(x: 0, y: 0, width: size.width, height: size.height))!)
        return image
        
    }
    

    
    
}


extension CreateCoinCommonVC: NavigationBarStyleDelegate{
    
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
}



