//
//  CreateCoinDoneVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class CreateCoinDoneVC: CreateCoinCommonVC , UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource,  UIPopoverPresentationControllerDelegate, CKAdvancedCoinAddTVCellDelegate, UIWebViewDelegate,PopoverViewDelegate, CreatePatternListVCDelegate {
    
    var hovereffectArray = ["None","Bounce","Pulse","Shake","Swing","Tada","Wobble", "Jello"]
    var hoverPicker: UIPickerView?
    var pickerOpen: Bool = false;
    var hoverEffectLabel: UILabel?
    
    var coinQuantityTextField: UITextField?
    var coinValueLabel: UILabel?
    
    var advancedOpen: Bool = false;
    
    var chooseColorIndexpath: IndexPath?
    
    
    @IBOutlet weak var saveDraftBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var meshBtn: UIButton!
    var nsfwBtn: UIButton!
    @IBOutlet weak var meshView: UIView!
    @IBOutlet weak var meshViewWidthConstraint: NSLayoutConstraint!
    
    var popoverVC: ColorPickerViewController?
    var coinView: CoinView?
   
    var pv: PopoverView?
    var patternSelected: Bool = false;
    
    
    var totalNoOfRows: Int = 7; //9
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.meshBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        resizeMeshBtnFont()
        self.infoBtn.setFATitleColor(color: CKColor.titleGreyColor, forState: UIControlState.normal)
        self.infoBtn.setFAIcon(icon: .FAQuestionCircle, iconSize: 14.0, forState: UIControlState.normal)
       
        if(saveDraftBtn != nil){
            CKTheme.getCKLightButtonStyle(self.saveDraftBtn, fontSizeL: 18.0)
        }
        
        self.showMeshView()
       
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addClicked(_ sender: UIButton) {
        self.saveOrAddCoin(draft: false)
       // self.createCoinCongratsVCCalled(draft: false)
    }
    
    @IBAction func saveDraftBtnClicked(_ sender: UIButton) {
        self.saveOrAddCoin(draft: true)
        //self.createCoinCongratsVCCalled(draft: true)
    }
    
    
    func resizeMeshBtnFont(){
        if(meshBtn.isSelected == true){
            self.meshBtn.titleLabel?.font = CKTheme.boldFont(13.0)
        }else{
            self.meshBtn.titleLabel?.font = CKTheme.boldFont(15.0)
        }
    }
    
    func showMeshView(){
        if((CKGlobalVariables.sharedManager.coin?.count)! < 2){
            self.meshBtn.isHidden = true
            self.meshViewWidthConstraint.constant = 0;
        }else{
            self.meshBtn.isHidden = false
            self.meshViewWidthConstraint.constant = 90;
        }
    }
    
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return coinViewCellHeight()
        }
        else if (indexPath.row == 1){
            return 45;
        }else if (indexPath.row == 8){
            if(pickerOpen == true){
                return 145
            }else{
                return 0;
            }
        }
        else{
            if(advancedOpen == true){
                if(indexPath.row == 6){
                    return 90;
                }else{
                    return 45;
                }
            }else{
                return 0;
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalNoOfRows;
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKHeadingAddTVCell", for: indexPath) as! CKAdvancedCoinAddTVCell
            self.formatHeadingTitle(label: cell.ccHeadingLabel)
            self.formatHeadingTitle(label: cell.ccSubheadingLabel)
            self.formatHeadingTitle(label: cell.coinLabel)
            cell.ccTextField.placeholder = "Quantity"
            cell.ccTextField.text = String(format: "%d", CKGlobalVariables.sharedManager.coin?.count ?? 0)
            cell.ccTextField.delegate = self;
            cell.ccTextField.tag = indexPath.row;
            CKTheme.setTextFieldStyle(cell.ccTextField, placeholder: cell.ccTextField.placeholder!)
            cell.coinLabel.text = self.setCoinLabel(coinCount: CKGlobalVariables.sharedManager.coin?.count ?? 0)
            self.toolBarToTextField(textField: cell.ccTextField)
            cell.coinViewHeightConstraint.constant = self.coinViewHeight();
            self.setCoinView(view: cell.coinView)
          
            return cell
            
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKAdvancedTVCell", for: indexPath) as! CKBaseTVCell
            if(advancedOpen == true){
                cell.rightsideBtn.setImage(UIImage(named: "arrow_up.png"), for: UIControlState.normal)
            }else{
                cell.rightsideBtn.setImage(UIImage(named: "arrow_down.png"), for: UIControlState.normal)
            }
            return cell
            
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKNSFWTVCell", for: indexPath) as! CKAdvancedCoinAddTVCell
            cell.delegate = self;
            cell.privateInfoBtn.setFAIcon(icon: .FAQuestionCircle, iconSize: 14.0, forState: UIControlState.normal)
            cell.privateInfoBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
            
            cell.nsfwInfoBtn.setFAIcon(icon: .FAQuestionCircle, iconSize: 14.0, forState: UIControlState.normal)
            cell.nsfwInfoBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
            
            cell.nsfwBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
            cell.nsfwBtn.titleLabel?.font = CKTheme.boldFont(15.0)
            
            cell.privateBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
            cell.privateBtn.titleLabel?.font = CKTheme.boldFont(15.0)
            
            cell.privateBtn.isSelected = CKGlobalVariables.sharedManager.coin?.privateCoin ?? false
            cell.nsfwBtn.isSelected = CKGlobalVariables.sharedManager.coin?.nsfw ?? false
            
            return cell
        }
        else if(indexPath.row == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCoinColorTVCell") as! CKAdvancedCoinAddTVCell
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2
            cell.titleLabel.text = "Coin Color"
            return cell
        }else if(indexPath.row == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCoinPatternTVCell") as! CKAdvancedCoinAddTVCell
            cell.baseDelegate = self;
            cell.rightsideBtn.tag = indexPath.row;
            if(patternSelected == true){
                cell.rightsideBtn.isHidden = false;
            }else{
                cell.rightsideBtn.isHidden = true;
            }
            return cell
        }else if(indexPath.row == 5){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCoinColorTVCell") as! CKAdvancedCoinAddTVCell
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2
            cell.titleLabel.text = "Pattern Color"
            return cell
        }else if(indexPath.row == 6){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCoinValueTVCell") as! CKAdvancedCoinAddTVCell
            cell.delegate = self
            cell.valueSlider.isContinuous = false;
            cell.valueSlider.value = Float(CKGlobalVariables.sharedManager.coin?.value ?? 0.0)
            self.coinValueLabel = cell.ccHeadingLabel;
            self.setCoinValueLabel(reload: false)
            return cell;
        }else if(indexPath.row == 7){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCoinColorTVCell") as! CKAdvancedCoinAddTVCell
            cell.contentView.backgroundColor = CKColor.tableviewCellColor2
            self.hoverEffectLabel = cell.titleLabel;
            setPickerValue(hovereffect: CKGlobalVariables.sharedManager.coin?.animation ?? hovereffectArray[0])
            return cell
        }else if(indexPath.row == 8){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKPickerTVCell") as! CKAdvancedCoinAddTVCell
            cell.delegate = self;
            initPickerView(coinPicker: cell.coinPickerView)
            return cell;
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCoinColorTVCell") as! CKAdvancedCoinAddTVCell
            
            return cell
        }
    }
    
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 1){
            //advanced clicked
            advancedOpen = !advancedOpen;
            self.tableView.reloadData()
            self.scrollToBottom()
            
        }else if(indexPath.row == 2){
           
            
        }else if(indexPath.row == 3){
            self.chooseColorIndexpath = indexPath;
            let cell = tableView.cellForRow(at: indexPath)
            colorPickerButton(cell!)
       
        }else if(indexPath.row == 4){
            patternSelected = !patternSelected;
            if(patternSelected == true){
                //show patterns
                showPatternView()
            }else{
                CKGlobalVariables.sharedManager.coin?.pattern = nil;
                reloadCoinView()
            }
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        }else if(indexPath.row == 5){
            self.chooseColorIndexpath = indexPath;
            let cell = tableView.cellForRow(at: indexPath)
            colorPickerButton(cell!)
        }else if(indexPath.row == 7){
            pickerOpen = true
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            self.scrollToBottom()
        }
        
    }
    
    
    func showPatternView(){
        
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreatePatternListVC") as?
        CreatePatternListVC
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .coverVertical;
        controllerObejct?.delegate = self;
        controllerObejct?.selectedPattern = CKGlobalVariables.sharedManager.coin?.pattern;
        self.present(controllerObejct!, animated: true, completion: {})
    }
    
    //MARK: CreatePatternListVCDelegate
    func patternSelectedDelegateMethod(pattern: String?) {
       CKGlobalVariables.sharedManager.coin?.pattern = pattern
       self.reloadCoinView()
    }
    
    
    func setCoinView(view: UIView){
        if(coinView != nil){
            coinView?.removeFromSuperview();
            coinView = nil;
        }
        
        
        if(advancedOpen == true){
            let xAxis = (view.frame.width - 250)/2
            let yAxis = (self.coinViewHeight() - 160)/2
            coinView = CoinView(frame: CGRect(x: xAxis, y: yAxis, width: 250.0 , height: 160.0))
        }else{
            let xAxis = (view.frame.width - 300)/2
            let yAxis = (self.coinViewHeight() - 230)/2 - 10
            coinView = CoinView(frame: CGRect(x: xAxis, y: yAxis, width: 300.0 , height: 200.0))
        }
        coinView?.backgroundColor = UIColor.clear;
        view.addSubview(coinView!)
        
        coinView?.coin = CKGlobalVariables.sharedManager.coin;
        coinView?.loadCoin(webView: (coinView?.frontWebView)!)
        coinView?.loadCoin(webView: (coinView?.backWebView)!)
        
    }
    
    
    //400 224
    func coinViewCellHeight() -> CGFloat{
        if(advancedOpen == false){
            return self.view.frame.height - 135 - 45 - 60
        }else{
            return 350;
        }
    }
    

    func coinViewHeight() -> CGFloat{
        let height = 0.56 * coinViewCellHeight() + 25;
        return height;
    }
   
    
    //MARK: CKBaseTVCellDelegate
    
    
    func toolBarToTextField(textField: UITextField){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: NSLocalizedString("done", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateCoinDoneVC.donePickerMethod))
       
        toolBar.setItems([ doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        textField.inputAccessoryView = toolBar
        
        
    }
    
    
    @objc func donePickerMethod(){
        self.view.endEditing(true)
    }
    
    @objc func advancedClicked(_ sender: UIButton) {
        advancedOpen = !advancedOpen
        self.tableView.reloadData()
    }
    
   
    func initPickerView(coinPicker: UIPickerView){
        coinPicker.showsSelectionIndicator = true
        coinPicker.delegate = self
        coinPicker.dataSource = self
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.totalNoOfRows-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    
    //MARK: CKAdvancedCoinAddTVCellDelegate
    
    
    func privateClickedDelegateMethod(sender: UIButton){
        sender.isSelected = !sender.isSelected;
        CKGlobalVariables.sharedManager.coin?.privateCoin = sender.isSelected;
        
        print(CKGlobalVariables.sharedManager.coin?.privateCoin ?? false);
    }
    
    func privateInfoClickedDelegateMethod(sender: UIButton){
        
        showNsfwPopOver(message: "Check this box if you'd like Coin comments to be private. Only users who have previously owned the Coin can view and leave comments.", sender: sender)
        
    }
    func nsfwClickedDelegateMethod(sender: UIButton){
        sender.isSelected = !sender.isSelected;
        CKGlobalVariables.sharedManager.coin?.nsfw = sender.isSelected;
        print(CKGlobalVariables.sharedManager.coin?.nsfw ?? false);
    }
    
    
    func nsfwInfoClickedDelegateMethod(sender: UIButton){
        showNsfwPopOver(message: "Not Safe For Work: Check this box if your Coin contains sensitive material. NSFW Coins are blurred by default and the viewer must click to unblur and reveal the Coin.", sender: sender)
    }
    
    
    func showNsfwPopOver(message: String, sender: UIButton){
        let indexPath = IndexPath(row: 2, section: 0)
        
        let rectOfCellInTableView: CGRect = tableView.rectForRow(at: indexPath)
        let rectOfCellInSuperview: CGRect = tableView.convert(rectOfCellInTableView, to: tableView.superview)
        
        let pointX: CGFloat = sender.frame.origin.x - 5
        let point = CGPoint(x: pointX, y: rectOfCellInSuperview.origin.y)
        
        self.showIntroPopOver(point, message: message)
        
    }
    
    
    func pickerDoneBtnClickedDelegateMethod() {
        pickerOpen = false;
        self.tableView.reloadRows(at: [IndexPath(row: totalNoOfRows-1, section: 0)], with: UITableViewRowAnimation.automatic)
        //self.tableView.reloadData()
        scrollToBottom()
        
        reloadCoinView()
        
    }
    
    
    func sliderClickedDelegateMethod(slider: UISlider){
        CKGlobalVariables.sharedManager.coin?.value = CGFloat(slider.value)
        setCoinValueLabel(reload: true)
    }
    
    func setCoinValueLabel(reload: Bool){
        self.coinValueLabel?.text = String(format: "Coin Value - %.2f %@", CKGlobalVariables.sharedManager.coin?.value ?? 0, NSLocalizedString("kred_name", comment: ""))
        if(reload){
            reloadCoinView()
        }
    }
   
    //MARK:  Picker View Delegate  and Datasource Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return hovereffectArray.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return hovereffectArray[row]
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        setPickerValue(hovereffect: hovereffectArray[row])
    }
    
    
    func setPickerValue(hovereffect: String){
        CKGlobalVariables.sharedManager.coin?.animation = hovereffect.lowercased()
        self.hoverEffectLabel?.text = String(format: "Hover Effect: %@", hovereffect.capitalizingFirstLetter())
        //reloadCoinView()
    }
    
    
    //MARK:
    func setCoinLabel(coinCount: Int?) -> String{
        
        if(coinCount == 1){
            return "Coin"//String(format: "%d Coin", coinCount ?? 0)
        }else{
            return "Coins" // String(format: "%d Coins", coinCount ?? 0)
        }
    }
    
   
    //MARK: Color picker
    func colorPickerButton(_ sender: UITableViewCell) {
        popoverVC = storyboard?.instantiateViewController(withIdentifier: "colorPickerPopover") as? ColorPickerViewController
        popoverVC?.modalPresentationStyle = .popover
        popoverVC?.preferredContentSize = CGSize(width: self.view.frame.width - 50, height: 446)
        if let popoverController = popoverVC!.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
            popoverController.permittedArrowDirections = .any
            popoverController.delegate = self
            popoverVC?.delegate = self
        }
        present(popoverVC!, animated: true, completion: nil)
    }
    
    
    // Override the iPhone behavior that presents a popover as fullscreen
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .none
    }
    
    
    func setButtonColor (_ color: UIColor, hexString: String) {
        if(self.chooseColorIndexpath?.row == 3){
            //coin color
            CKGlobalVariables.sharedManager.coin?.coinColor = hexString;
        }else{
            //pattern color
            CKGlobalVariables.sharedManager.coin?.pattern_color = hexString;
        }
        reloadCoinView()
        popoverVC?.dismiss(animated: true, completion: nil)
    }
    
    
    func reloadCoinView(){
        if(self.coinView != nil){
            self.coinView?.loadCoin(webView: (self.coinView?.frontWebView)!)
            self.coinView?.loadCoin(webView: (self.coinView?.backWebView)!)
        }
        
        let coinIndexPath = IndexPath(row: 0, section: 0)
        self.tableView.reloadRows(at: [coinIndexPath], with: UITableViewRowAnimation.none)
    }
    
    
    @IBAction func infoBtnClicked(_ sender: UIButton) {
        
        let pointX: CGFloat = (self.meshView.frame.origin.x + sender.frame.origin.x) - 5
        let point = CGPoint(x: pointX, y: self.meshView.frame.origin.y)
        
        showIntroPopOver(point, message: "Selecting Mesh links the conversation streams of all Coins in this batch. Anyone who holds any Coin in the batch will see the conversation from all Coins in the batch.")
        
    }
    
    
    @IBAction func meshBtnClicked(_ sender: UIButton) {
        self.meshBtn.isSelected = !self.meshBtn.isSelected
        resizeMeshBtnFont()
    }
    
    
    //MARK:  pop over view
    func popOverButtonClicked(_ point: CGPoint, message: String, button: UIButton){
        showIntroPopOver(button.frame.origin, message: message)
    }
    
    
    func showIntroPopOver(_ point: CGPoint, message: String){
        let pointL = CGPoint(x: point.x+20, y: point.y+9)
        pv = PopoverView.showPopover(at: pointL, in: self.view, withText: message, delegate: self)
        //pv?.isUserInteractionEnabled = false;
    }
    
    
    //MARK: PopoverViewDelegate Methods
    func popoverView(_ popoverView: PopoverView!, didSelectItemAt index: Int) {
        
    }
    
    func popoverViewDidDismiss(_ popoverView: PopoverView!) {
        
    }
    
    
    func saveOrAddCoin(draft: Bool){
        
        self.uploadFrontImage(completionHandler: {
            self.uploadBackImage(completionHandler: {
                self.getUnmintedCoin(draft: draft)
            })
        })
    }
    
    
    
    //MARK: Create coin Service
    
    func getUnmintedCoin(draft: Bool){
        self.showLoadingDialog("")
        let ckS = CKGetService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["wallet"] = CKGlobalVariables.sharedManager.walletId;
        params["minted"] = false
        
        ckS.getUnmintedCoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.coins.count > 0){
                let coin = response.coins[0]
                self.createCoinServiceMethod(coin: coin.coin ?? 0, draft: draft)
            }
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    
    
    func createCoinServiceMethod(coin: Int?, draft: Bool){
        let ckS = CKPostService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["name"] = CKGlobalVariables.sharedManager.coin?.coinName ?? ""
        params["currency"] = CKServiceConstants.COIN_CURRENCY;
        params["count"] =  CKGlobalVariables.sharedManager.coin?.count ?? 0
        params["wallet"] = CKGlobalVariables.sharedManager.walletId ?? 0
        if(draft == false){
            params["coin"] = coin
        }
        params["value"] = CKGlobalVariables.sharedManager.coin?.value ?? 0
        params["face"] = CKGlobalVariables.sharedManager.coin?.front ?? ""
        params["back"] = CKGlobalVariables.sharedManager.coin?.back ?? ""
        
        if(CKGlobalVariables.sharedManager.coin?.coinColor != nil && (CKGlobalVariables.sharedManager.coin?.coinColor?.contains("#"))!){
            let newColor = CKGlobalVariables.sharedManager.coin?.coinColor?.replacingOccurrences(of: "#", with: "")
             params["color"] = newColor
        }else{
             params["color"] = CKGlobalVariables.sharedManager.coin?.coinColor ?? ""
        }
        
       
        params["pattern_color"] = CKGlobalVariables.sharedManager.coin?.pattern_color ?? ""
        params["pattern"] = CKGlobalVariables.sharedManager.coin?.pattern ?? ""
        params["mesh"] = CKGlobalVariables.sharedManager.coin?.mesh ?? false
        params["nsfw"] = CKGlobalVariables.sharedManager.coin?.nsfw ?? false
        params["private"] = CKGlobalVariables.sharedManager.coin?.privateCoin ?? false
        // params["animation"] = CKGlobalVariables.sharedManager.coin?.animation ?? ""
        
        //params["payload"] = "%7B%22pattern%22%3Anull%2C%22patternColor%22%3Anull%7D"
        
        self.showLoadingDialog("")
       
        ckS.createAMintCoin(draft: draft, params: params, responseHandler: { response in
            self.hideLoadingDialog()
           
            if(response.error == nil){
                if(response.batch != nil){
                    CKGlobalVariables.sharedManager.coin = response.batch;
                }
                
                self.createCoinCongratsVCCalled(draft: draft)
            }else{
                self.showErrorMessage(response.message ?? response.error ?? "")
            }
            
            /*if(draft == true && response.error == nil){
                CKGlobalMethods.showSuccessALertWithMessage(NSLocalizedString("yipee", comment: ""), message: NSLocalizedString("coin_save_alert", comment: ""))
            }else{
                
 
            }
             self.hideLoadingDialog()*/
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
    }
    
    
    
    func uploadFrontImage(completionHandler:@escaping () -> Void){
        self.uploadImageToServer(imageStr: CKGlobalVariables.sharedManager.coin?.front ?? "", completionHandler: { response in
            CKGlobalVariables.sharedManager.coin?.front = response;
            completionHandler()
        })
    }
    
    
    
    func uploadBackImage(completionHandler:@escaping () -> Void){
        self.uploadImageToServer(imageStr: CKGlobalVariables.sharedManager.coin?.back ?? "", completionHandler: { response in
            CKGlobalVariables.sharedManager.coin?.back = response;
            completionHandler()
        })
    }
    
    
    //MARK: Upload images to server
    func uploadImageToServer(imageStr: String , completionHandler:@escaping (_ imageUrl: String) -> Void){
        if(imageStr.contains("http://") || imageStr.contains("https://")){
            completionHandler(imageStr)
        
        }else{
           
            DispatchQueue.main.async {
            
                let url = URL(string: imageStr)
                if(self.getAttachmentType(imageStr: imageStr) == ATTACHMENT_TYPE.VIDEO){
                    self.compressVideoMethod(videoURL: url!, completionHandler: completionHandler)
                }else{
                    if let base64String = try? Data(contentsOf: url!).base64EncodedString() {
                        self.uploadFinalService(imageStr: imageStr, base64: base64String, completionHandler: completionHandler)
                    }else{
                        self.hideLoadingDialog();
                        completionHandler("");
                    }
                }
            }
        }
     }
    
    
    
    func uploadFinalService(imageStr: String,base64: String ,completionHandler:@escaping (_ imageUrl: String) -> Void){
        self.showLoadingDialog("")
        let cks = CKPostService()
        cks.uploadImageServiceMethod(base64,
                                     attachmentExt: self.getExtension(imageStr: imageStr),
                                     responseHandler: { response in
                                        completionHandler(response.url ?? "")
                                        self.hideLoadingDialog();
        }, errorHandler: { error in
            completionHandler("")
            self.errorHandler(error)
            self.hideLoadingDialog();
        })
    }
    
    
    
    func compressVideoMethod(videoURL: URL, completionHandler:@escaping (_ imageUrl: String) -> Void){
        
        let data = NSData(contentsOf: videoURL)
        
        print("File size before compression: \(Double((data?.length)! / 1048576)) mb")
       
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
        
        compressVideo(inputURL: videoURL , outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                if let base64String = try? Data(contentsOf: compressedURL).base64EncodedString() {
                    self.uploadFinalService(imageStr: videoURL.absoluteString, base64: base64String, completionHandler: completionHandler)
                }else{
                    self.hideLoadingDialog();
                    completionHandler("");
                }
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetLowQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }

    
    
    
}


extension CreateCoinDoneVC: CKBaseTVCellDelegate{
    
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
       // sender = !sender;
        if(sender.tag == 2){
           
        }else if(sender.tag == 5){
            //pattern show
            
        }
    }
    
}


extension CreateCoinDoneVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
   /* func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /*if(textField.tag == 0){
            if(textField.text == ""){
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: "1")
                textField.text = newString
                return false
            }else if( Int(textField.text!)! < 1){
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: "1")
            
                textField.text = newString
                return false
                
            }else{
                return true
            }
        }else{
            return true
        }*/
        print(range)
        if(textField.tag == 0 && range.location <= 0 && range.length <= 1){
             print(textField.text ?? "0")
            if(Int(textField.text!)! < 1){
                //let newString = (textField.text! as NSString).replacingCharacters(in: range, with: "1")
                
                //textField.text = newString
                return false
            }else{
                return true
            }
            
            
           
            
            
           
        }else{
            return true
        }
    }*/
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if(textField.tag == 0){
            let value = Int(textField.text ?? "0") ?? 0
            self.coinCountvalidation(value: value)
        }
    }
    
    
    func coinCountvalidation(value: Int){
        if(value < 1){
            showErrorMessage("Atleast 1 coin is need to be added")
            CKGlobalVariables.sharedManager.coin?.count = 1
        }else{
            CKGlobalVariables.sharedManager.coin?.count = value
        }
        
        
        showMeshView()
        reloadCoinView()
        
    }
    
    
}


extension UIColor {
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    
    
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

