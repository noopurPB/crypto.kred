//
//  CreatePatternListVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

protocol CreatePatternListVCDelegate{
    func patternSelectedDelegateMethod(pattern: String?)
}


class CreatePatternListVC: CreateCoinCommonVC , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var coinPatterList = ["overlapping-diamonds",
                          "like",
                          "zig-zag",
                          "bee",
                          "aztec",
                          "steel-beams",
                          "bank-note",
                          "glamorous",
                          "boxes",
                          "Lipstick",
                          "hamburger",
                          "bear-face",
                          "butterfly",
                          "fancy-rectangles",
                          "apple-black-silhouette-with-a-leaf",
                          "circuit-board",
                          "connections",
                          "cow",
                          "topography",
                          "melt",
                          "overlapping-hexagons",
                          "bunny",
                          "death-star",
                          "signal",
                          "valentines-heart","lisbon","happy-intersection","heavy-rain","diagonal-lines","intersecting-circles","lion","sea-turtle","curtain","jupiter","stripes","dominos","piano-man","morphing-diamonds","wallpaper","circles-and-squares","floating-cogs","footprint","yyy","temple",
                          "line-in-motion","4-point-stars","elephant-alone","x-equals","squares","plus","tiny-checkers","polka-dots","cage","pet-hotel-sign","dog-face","duck","pie-factory","sainted-stars","bamboo","fish","rain","bevel-circle","kite","kiwi","squares-in-squares",
                          "light-bolt","overcast","parkay-floor","cutout","farm-sheep","graph-paper","pig","wiggle","stamp-collection","church-on-sunday","bubbles","leaf","network","eyes","autumn","cork-screw","star","sun","bone","current","rails","moroccan",
                          "jigsaw", "curved-banana", "monkey", "endless-clouds", "cute-giraffe", "crab", "texture", "i-like-food", "rounded-plus-connected", "anchors-away", "brick-wall", "floor-tile", "hexagons", "bathroom-floor", "houndstooth", "overlapping-circles", "random-shapes", "falling-triangles", "flipped-diamonds", "lips",
                          "tic-tac-toe", "cloud", "diagonal-stripes", "architect", "grapes", "charlie-brown", "formal-invitation", "hideout", "moon", "dove-of-peace", "pixel-dots", "skulls",
                          "volcano-lamp"]
    
    var selectedPattern: String?
    var previousIndexPath: IndexPath?
    
    var delegate: CreatePatternListVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func crossBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return coinPatterList.count;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKBaseCVCell",for:indexPath) as! CKBaseCVCell
        
        cell.webView.layer.borderColor = CKColor.dividerColor.cgColor
        cell.webView.layer.borderWidth = 2.0
        
        //
        
        
        let coinpattern = coinPatterList[indexPath.row]
        
        if(selectedPattern != nil && selectedPattern == coinpattern){
            cell.contentView.backgroundColor = CKColor.themeAnotherColor;
        }else{
            cell.contentView.backgroundColor = UIColor.white;
        }
        
        let path = Bundle.main.path(forResource: coinpattern, ofType: "svg")!
        if path != "" {
            let fileURL:URL = URL(fileURLWithPath: path)
            let req = URLRequest(url: fileURL, cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 60*100)
            cell.webView.scalesPageToFit = false
            cell.webView.loadRequest(req)
        }
        else {
            //handle here if path not found
        }
        
        return cell;
    }
    
   
  
    
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let coinpattern = coinPatterList[indexPath.row]
        self.selectedPattern = coinpattern;
        self.collectionView.reloadData()
        self.delegate?.patternSelectedDelegateMethod(pattern: coinpattern)
        //self.collectionView.reloadItems(at: [indexPath])
        //self.previousIndexPath = indexPath;
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }

}



