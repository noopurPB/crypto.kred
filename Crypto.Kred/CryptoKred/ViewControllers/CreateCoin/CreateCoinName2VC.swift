//
//  CreateCoinName1VC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CreateCoinName2VC: CreateCoinCommonVC, UITableViewDelegate, UITableViewDataSource, CKTextFieldTVCellDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var popularnames = [ "Thank You",
        "Launch Day",
        "Employee of the Month",
        "Welcome to the Club",
        "Thanks for Following Me",
        "Let’s Connect",
        "I Love You",
        "Our Wedding Day",
        "Graduation",
        "It’s a Boy",
        "It’s a Girl",
        "We’re Engaged",
        "My Birthday",
        "Just for Fun"]
    
    var popularNamesPicker: UIPickerView?
    var nameTextField: UITextField?
    var namePickerTextField: UITextField?
   
    override func viewDidLoad() {
        super.viewDidLoad()
         addTapGestureToView()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //164 94 0
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextClicked(_ sender: UIButton) {
        CKGlobalVariables.sharedManager.coin?.coinName = nameTextField?.text
        CKGlobalVariables.sharedManager.coin = CKGlobalMethods.setCurrentCoinDefaultValues(coin: CKGlobalVariables.sharedManager.coin);
        
        createCoinImageFrontVCCalled()
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 200
        }else{
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKHeadingTVCell", for: indexPath) as! CKHeadingTVCell
            cell.headingLbl1.text = NSLocalizedString("give_your_kred_coin_name_that", comment: "")
            cell.headingLbl2.text = NSLocalizedString("tells_world_what_represents", comment: "")
            self.formatHeadingTitle(label: cell.headingLbl1)
            self.formatHeadingTitle(label: cell.headingLbl2)
            return cell
            
        }else if(indexPath.row == 1){
           let cell = tableView.dequeueReusableCell(withIdentifier: "CKTextFieldTVCell", for: indexPath) as! CKTextFieldTVCell
            cell.textField.placeholder = NSLocalizedString("name_your_coin", comment: "")
           CKTheme.setTextFieldStyle(cell.textField, placeholder: NSLocalizedString("name_your_coin", comment: ""))
            cell.textField.delegate = self
            cell.delegate = self
            self.enableNextButton(textField: cell.textField)
            cell.smallIconBtn.setImage(UIImage(named: "grab_Aa.png"), for: UIControlState.normal)
            self.nameTextField = cell.textField
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTextPickerTVCell", for: indexPath) as! CKTextFieldTVCell
            self.initPickerView(cell.textField)
            cell.textField.tintColor = UIColor.white
            cell.textField.delegate = self
            self.namePickerTextField = cell.textField
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func enableNextButton(textField:UITextField){
        if(textField.text?.count != 0){
            CKTheme.enableOrDisableButton(button: nextBtn, enable: true)
        }else{
            CKTheme.enableOrDisableButton(button: nextBtn, enable: false)
        }
    }
    
    
    //MARK: CKTextFieldTVCellDelegate Method
    func textFieldValueChanged(_ sender: UITextField){
        enableNextButton(textField: sender)
    }
    
   //MARK: UITextfield Delegate
    
   /* func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == self.nameTextField){
            return allowOnlyAlphaNumeric(textField, string: string)
            
        }else{
            return true
        }
        
    }*/
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == namePickerTextField){
            setPickerValue(row: 0)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    // first letter uppercase
    /*func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //textField.textColor = Theme.textFieldColor()
        if(textField == self.nameTextField){
            return cardNumberAllowedCharacterText(range)
        }
        else{
            return true
        }
    }
    
    func cardNumberAllowedCharacterText(_ range: NSRange)-> Bool{
        if(range.location > 15 && range.length == 0){
            return false
        }else{
            return true
        }
    }*/
    
    
    //MARK: Picker
    
    func initPickerView(_ textField: UITextField){
        if(popularNamesPicker == nil){
            popularNamesPicker = UIPickerView()
            popularNamesPicker!.backgroundColor = UIColor.white
            
            popularNamesPicker!.showsSelectionIndicator = true
            popularNamesPicker!.delegate = self
            popularNamesPicker!.dataSource = self
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: NSLocalizedString("done", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateCoinName2VC.donePickerMethod))
           /* let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title:  NSLocalizedString("cancel", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateCoinName2VC.cancelPickerMethod))
            */
            //cancelButton, spaceButton,
            toolBar.setItems([ doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            
            textField.inputView = popularNamesPicker
            textField.inputAccessoryView = toolBar
            
            /*let x = (self.view.frame.width - 150)/2
            
            let label: UILabel = UILabel(frame: CGRect(x: x , y: 0, width: 150, height: 44))
            label.text = NSLocalizedString("choose_domain", comment: "")
            label.textAlignment = NSTextAlignment.center
            label.font = Theme.mediumFont(18.0)
            toolBar.addSubview(label)*/
        }
    }
    
    
    @objc func donePickerMethod(){
        self.namePickerTextField?.resignFirstResponder()
    }
    
    
   /* @objc func cancelPickerMethod(){
        self.namePickerTextField.resignFirstResponder()
    }*/
    
    //MARK:  Picker View Delegate  and Datasource Method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return popularnames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let domain = popularnames[row].uppercased()
        return domain;
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        setPickerValue(row: row)
    }
    
    
    func setPickerValue(row: Int){
        self.nameTextField?.text = popularnames[row].uppercased()
        self.enableNextButton(textField: self.nameTextField!)
    }
    
}

