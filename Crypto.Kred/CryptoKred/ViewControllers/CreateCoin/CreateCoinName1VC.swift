//
//  CreateCoinName1VC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CreateCoinName1VC: CreateCoinCommonVC, PopoverViewDelegate , UIPopoverPresentationControllerDelegate {
    
    var pv: PopoverView?
    
    @IBOutlet weak var namePoint: UIButton!
    @IBOutlet weak var frontImagePoint: UIButton!
    @IBOutlet weak var backImagePoint: UIButton!
    @IBOutlet weak var quantityPoint: UIButton!
    @IBOutlet weak var valuePoint: UIButton!
    @IBOutlet weak var scorePoint: UIButton!
    @IBOutlet weak var coinImageView: UIImageView!
    
    var selectedButton: UIButton?
    
    @IBOutlet weak var useDraftBtn: UIButton!
    @IBAction func namePointClicked(_ sender: UIButton) {
        popOverButtonClicked(sender.frame.origin, message: "Name", button: namePoint)
    }
    
    @IBAction func frontImagePointClicked(_ sender: UIButton) {
        popOverButtonClicked(sender.frame.origin, message: "Front Image", button: frontImagePoint)
    }
    
    @IBAction func backImagePointClicked(_ sender: UIButton) {
        popOverButtonClicked(sender.frame.origin, message: "Rear Image", button: backImagePoint)
    }
    
    @IBAction func quantityPointClicked(_ sender: UIButton) {
        popOverButtonClicked(sender.frame.origin, message: "Coin Value", button: quantityPoint)
    }
    
    @IBAction func scorePointClicked(_ sender: UIButton) {
        popOverButtonClicked(sender.frame.origin, message: "Live Circulation Counter", button: scorePoint)
    }
    
    @IBAction func valuePointClicked(_ sender: UIButton) {
        popOverButtonClicked(sender.frame.origin, message: "Numbering and Issue Count", button: valuePoint)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CKGlobalVariables.sharedManager.coin =  CKGlobalMethods.initialiseCoin(true)
        
       self.userCheckAPI();
        if(useDraftBtn != nil){
            coinDraftServiceMethod()
            CKTheme.getCKLightButtonStyle(self.useDraftBtn, fontSizeL: 18.0)
        }
        
        
        //self.popOverButtonClicked(namePoint.frame.origin, message: "Name your Coin", button: namePoint)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func nextClicked(_ sender: UIButton) {
        self.createCoinName2VCCalled()
    }
    
    
    @IBAction func useDraftBtnClicked(_ sender: UIButton) {
        self.givecoinChooseVC(coin: nil, draftCoin: true)
    }
    
    
    //MARK:  pop over view
    func popOverButtonClicked(_ point: CGPoint, message: String, button: UIButton){
        if(pv != nil){
           // pv?.dismiss()
        }
        button.isSelected = true
        self.selectedButton = button;
        showIntroPopOver(button.frame.origin, message: message, button: button)
    }
    
    
    func showIntroPopOver(_ point: CGPoint, message: String, button: UIButton){
        let pointL = CGPoint(x: point.x+20, y: point.y+9)
        pv = PopoverView.showPopover(at: pointL, in: self.view, withText: message, delegate: self)
        //pv?.isUserInteractionEnabled = false;
    }
    
    
    //MARK: PopoverViewDelegate Methods
    func popoverView(_ popoverView: PopoverView!, didSelectItemAt index: Int) {
        
    }
    
    func popoverViewDidDismiss(_ popoverView: PopoverView!) {
        self.selectedButton?.isSelected = false
    }
    
    
    func userCheckAPI(){
        CKTheme.enableOrDisableButton(button: self.nextBtn, enable: false)
        self.showLoadingDialog("")
        
        let cks = CKGetService();
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams();
        //params["user_id"] = CKGlobalVariables.sharedManager.loggedInUserId;
        cks.userCheck(params: params, responseHandler: { response in
            self.hideLoadingDialog();
            if(response.error != nil || response.phone == false || response.kreddomain == false){
                self.userValidationVC(responseL: response);
            }
            CKTheme.enableOrDisableButton(button: self.nextBtn, enable: true)
            
        }, cacheHandler: { response in }, errorHandler: { error in
            self.hideLoadingDialog();
            self.errorHandler(error);
        })
    }
    
    
    func userValidationVC(responseL: CKUserValidationResponseObject){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKUserValidationVC") as?
        CKUserValidationVC
        controllerObejct?.delegate = self;
        controllerObejct?.responseOverall = responseL;
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        self.present(controllerObejct!, animated: true, completion: {})
    }
    
    
    
    func coinDraftServiceMethod() {
        self.useDraftBtn.isHidden = true;
        let cks = CKGetService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        cks.getCoinsList(params: params, path: CKServiceConstants.COIN_DRAFT_URL, responseHandler: { response  in
            self.coinsDraftCommonHandler(response.coins)
        }, cacheHandler: { response  in
            self.coinsDraftCommonHandler(response.coins)
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
    }
    
    func coinsDraftCommonHandler(_ coins: [Coin]){
        if(coins.count > 0){
            self.useDraftBtn.isHidden = false
        }else{
            self.useDraftBtn.isHidden = true
        }
    }
    
    
    
    
}

extension CreateCoinName1VC:  CKUserValidationVCDelegate{
    
    func crossClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func verificationCompletedDelegateMethod() {
        CKTheme.enableOrDisableButton(button: self.nextBtn, enable: true)
    }
    
}


