//
//  CKBaseVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Haneke
import MBProgressHUD
import Font_Awesome_Swift
import MobileCoreServices

class CKBaseVC: UIViewController , UINavigationControllerDelegate,UIImagePickerControllerDelegate, PECropViewControllerDelegate{
    
    var imagePicker = UIImagePickerController()
    var showSquareCrop: Bool?
    var showCropper: Bool?
    
    var navBarStyle: NavigationBarStyle?
    
    var HUD: NVActivityIndicatorView?
    
    var searchLoadingView: NVActivityIndicatorView?
    
    var noDataView: CKNoDataCell?
    var noDataTextView: UILabel?
    
    var searchHeaderView : CKTextFieldTVCell?
    
    var subHeadingLbl_fontsize: CGFloat = 17.0;
    
    enum ATTACHMENT_TYPE {
        case GIF;
        case VIDEO;
        case IMAGE;
    }
    
    var attchementType: ATTACHMENT_TYPE?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.delegate = self;
        // Do any additional setup after loading the view, typically from a nib.
        navBarStyle = NavigationBarStyle()
        navBarStyle?.setNavBarStyle(self)
         self.imagePicker.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    //MARK : Init Search View
    func initSearchHeaderView(_ view: UIView){
        if(searchHeaderView == nil){
            searchHeaderView = UINib(nibName: "CKTextFieldTVCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CKTextFieldTVCell
            searchHeaderView?.smallIconBtn.setImage(UIImage(named: "search_icon.png"), for: UIControlState())
            searchHeaderView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width+3, height: 50)
            searchHeaderView?.textField.placeholder = NSLocalizedString("search", comment: "")
            CKTheme.setTextFieldStyle((searchHeaderView?.textField)!, placeholder: (searchHeaderView?.textField.placeholder!)!)
            searchHeaderView?.backgroundColor = UIColor.white
            //searchHeaderView?.topDividerView.isHidden = false;
            searchHeaderView?.textField.returnKeyType = UIReturnKeyType.done
            //searchHeaderView.textField.clearButtonMode = UITextFieldViewMode.Always
            view.addSubview(searchHeaderView!)
        }
    }

    
    
    //MARK: No Data View Methods
    func showNoDataView(view: UIView, image: String, title: String, subTitle: String, buttonTitle: String, buttonWidth: CGFloat){
        if(noDataView == nil){
            noDataView = UINib(nibName: "CKNoDataCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CKNoDataCell
            let width: CGFloat =  view.frame.width
            let height: CGFloat = 400
            noDataView?.frame = CGRect(x: 0, y: (view.frame.height - height)/2, width: width, height: height)
            noDataView?.backgroundColor = UIColor.clear
        }
        noDataView?.noCoinImageBtn.setImage(UIImage(named: image), for: UIControlState.normal)
        noDataView?.noCoinTitle1.text = title;
        noDataView?.noCoinTitle2.text = subTitle;
       
        
        let maxHeight = CGFloat.infinity;
        let rect = noDataView?.noCoinTitle2.attributedText?.boundingRect(with: CGSize(width: self.view.frame.width - 20, height: maxHeight), options: .usesLineFragmentOrigin, context: nil)
       
        var frame = noDataView?.noCoinTitle2.frame
        frame?.size.height = (rect?.size.height)!
        noDataView?.noCoinTitle2.frame = frame!
        
        
        //noDataView?.noCoinTitle2.sizeToFit()
        noDataView?.noCoinBtn.setTitle(buttonTitle, for: UIControlState.normal)
        noDataView?.btnWidthConstraint.constant = buttonWidth;
        view.addSubview(noDataView!)
    }
    
    
    func hideNoDataView(){
        if(noDataView != nil){
            noDataView?.removeFromSuperview()
        }
    }
    
    
    func showNoDataTextView(text: String){
        
        if(self.noDataTextView == nil){
            let height:CGFloat = 50.0;
            let yAxis =  (self.view.frame.height - height)/2
        
            noDataTextView = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: height))
            noDataTextView?.text = text;
            noDataTextView?.textColor = CKColor.themeAnotherColor;
            noDataTextView?.font = CKTheme.regularFont(20.0)
            noDataTextView?.textAlignment = NSTextAlignment.center;
        }
        self.view.addSubview(noDataTextView!)
        
    }
    
    func hideNoDataTextView(){
        if(noDataTextView != nil){
            self.noDataTextView?.removeFromSuperview()
        }
    }
    //MARK: Check login or not
    
    func userIsLoggedIn()-> Bool{
        if(CKGlobalVariables.sharedManager.loggedInToken != ""){
            //loggedin
            return true
        }else{
            return false
        }
    }
    
    
    //MARK: Register UITableViewCell
    
    func registerCKTextFieldTVCell(_ tableView: UITableView){
        tableView.register(UINib(nibName: "CKTextFieldTVCell", bundle: nil), forCellReuseIdentifier: "CKTextFieldTVCell")
    }
    
    func registerCKHeadingTVCell(_ tableView: UITableView){
        tableView.register(UINib(nibName: "CKHeadingTVCell", bundle: nil), forCellReuseIdentifier: "CKHeadingTVCell")
    }
    
    func registerCKLoadmoreTVCell(_ tableView: UITableView){
        tableView.register(UINib(nibName: "CKLoadMoreTVCell", bundle: nil), forCellReuseIdentifier: "CKLoadMoreTVCell")
    }
    
    
    //MARK: Register CollectionView Cell
    
    func registerCoinCollectionViewCell(_ collectionView: UICollectionView){
        //Set Main Cell in Collection View
        let nib = UINib(nibName: "CKCoinCVCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier:"CKCoinCVCell")
        collectionView.backgroundColor = UIColor.clear
    }
    
    func registerLoadmoreCollectionViewCell(_ collectionView: UICollectionView){
        //Set Main Cell in Collection View
        let nib = UINib(nibName: "CKLoadMoreCVCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier:"CKLoadMoreCVCell")
        collectionView.backgroundColor = UIColor.clear
    }
    
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if(viewController is CKCoinProfileVC){
            self.navigationController?.navigationBar.alpha = 0.0;
            //setNavigationBarHidden(true, animated: animated)
        }else{
            self.navigationController?.navigationBar.alpha = 1.0;
            //self.navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    
    
    //MARK: VC Called Methods
    
    func giveawayRequestLinkVC(coinL: Coin?) {
        let storyboard = UIStoryboard(name: "CKGiveCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "GiveawayRequestLinkVC") as?
        GiveawayRequestLinkVC
        controllerObejct?.selectedCoin = coinL;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    func createCoinVCCalled(){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinName1VC") as?
        CreateCoinName1VC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    func createCoinName1VCCalled(){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinName1VC") as?
        CreateCoinName1VC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    func createCoinName2VCCalled(){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinName2VC") as?
        CreateCoinName2VC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    func createCoinImageFrontVCCalled(){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinImageFrontVC") as?
        CreateCoinImageFrontVC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    func createCoinImageBackVCCalled(){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinImageBackVC") as?
        CreateCoinImageBackVC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    func createCoinDoneVCCalled(){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinDoneVC") as?
        CreateCoinDoneVC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    func createCoinCongratsVCCalled(draft: Bool){
        let storyboard = UIStoryboard(name: "CKCreateCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CreateCoinCongratsVC") as?
        CreateCoinCongratsVC
        controllerObejct?.draft = draft;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    func marketPlaceCalledVC(animation: Bool, selectedCoin: Coin?){
        //if(checkVCInStack(animation) == false){
            
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is CKMarketPlaceVC && selectedCoin == nil){
                self.navigationController!.popToViewController(aViewController, animated: animation);
                return
            }
        }
        let storyboard = UIStoryboard(name: "CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKMarketPlaceVC") as?
            CKMarketPlaceVC
        controllerObejct?.selectedCoin = selectedCoin;
        self.navigationController?.pushViewController(controllerObejct!, animated: animation)
       // }
    }
    
    func exploreCalledVC(animation: Bool){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is CKExploreVC){
                self.navigationController!.popToViewController(aViewController, animated: animation);
                return
            }
        }
        let storyboard = UIStoryboard(name: "CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKExploreVC") as?
            CKExploreVC
        self.navigationController?.pushViewController(controllerObejct!, animated: animation)
    
    }
    
    func myDesignCalledVC(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is CKMyDesignsVC){
                self.navigationController!.popToViewController(aViewController, animated: false);
                return
            }
        }
        let storyboard = UIStoryboard(name: "CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKMyDesignsVC") as?
        CKMyDesignsVC
        self.navigationController?.pushViewController(controllerObejct!, animated: false)
        // }
    }
    
    
    func newsfeedVCCalled(_ animated: Bool){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is CKNewsfeedVC){
                self.navigationController!.popToViewController(aViewController, animated: animated);
                return
            }
        }
            let storyboard = UIStoryboard(name: "CKCoinProfile", bundle: nil)
            let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKNewsfeedVC") as?
           CKNewsfeedVC
            self.navigationController?.pushViewController(controllerObejct!, animated: animated)
   
    }
    
    
    
    func checkVCInStack(_ animated: Bool) -> Bool{
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is CKMarketPlaceVC ||
                aViewController is CKCoinCollectionVC || aViewController is CKNewsfeedVC ||
                aViewController is BuymoreMainVC ||
                aViewController is CKOutreachVC || aViewController is CKExploreVC
                || aViewController is CKTransactionVC){
                self.navigationController!.popToViewController(aViewController, animated: animated);
                return true
            }
        }
        return false
    }
    
    
    func outreachVCCalled(){
        let storyboard = UIStoryboard(name: "CKMain", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKOutreachVC") as?
        CKOutreachVC
        self.navigationController?.pushViewController(controllerObejct!, animated: false)
    }
    
    func contactsVCCalled(){
        let storyboard = UIStoryboard(name: "CKCoinProfile", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKContactsListVC") as?
        CKContactsListVC
        self.navigationController?.pushViewController(controllerObejct!, animated: false)
    }
    
    func transactionVCCalled(){
         //if(checkVCInStack(true) == false){
            let storyboard = UIStoryboard(name: "CKMain", bundle: nil)
            let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKTransactionVC") as?
        CKTransactionVC
            self.navigationController?.pushViewController(controllerObejct!, animated: false)
       
       // }
    }
    
    func collectionViewVCCalled(userCollection: Bool, selectedCoin: Coin?){
        
       /* let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is CKCoinCollectionVC && selectedCoin == nil){
                self.navigationController!.popToViewController(aViewController, animated: true);
                return
            }
        }
        */
        let storyboard = UIStoryboard(name: "CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKCoinCollectionVC") as?
        CKCoinCollectionVC
        controllerObejct?.userCollection = userCollection;
        controllerObejct?.selectedCoin = selectedCoin
        self.navigationController?.pushViewController(controllerObejct!, animated: true)

    }
    
    func coinSearchVCCalled(selectedCoin: Coin?, navTitle: String?, sort: String?, viewType: CKExploreSearchVC.VIEW_TYPE){
        let storyboard = UIStoryboard(name: "CKCoinCollection", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKExploreSearchVC") as?
        CKExploreSearchVC
        controllerObejct?.navTitle = navTitle
        controllerObejct?.sort = sort
        controllerObejct?.viewType = viewType
        controllerObejct?.selectedCoin = selectedCoin
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
        
    }
    
    func giveCoinCalledVC(coin: Coin?){
        let storyboard = UIStoryboard(name: "CKGiveCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "GiveCoinDemoVC") as?
        GiveCoinDemoVC
        if(coin != nil){
            controllerObejct?.selectedCoin = coin!
        }
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    func givecoinChooseVC(coin: Coin?, draftCoin: Bool) {
         let storyboard = UIStoryboard(name: "CKGiveCoin", bundle: nil)
        let chooseVC = storyboard.instantiateViewController(withIdentifier: "GiveCoinChooseVC") as?
        GiveCoinChooseVC
        chooseVC?.selectedCoin = coin;
        chooseVC?.draftChooseCoin = draftCoin;
        self.navigationController?.pushViewController(chooseVC!, animated: true)
    }
    
    
    
    //MARK: Auction Coin
    func auctionCoinCalledVC(coin: Coin?){
        let storyboard = UIStoryboard(name: "CKAuctionCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "AuctionCoinDemoVC") as?
        AuctionCoinDemoVC
        controllerObejct?.auction = CKAuction()
        controllerObejct?.auction?.coin = coin!;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    //MARK: Export CKr
    func exportCKrVCCalled(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is ExportCKrVC){
                self.navigationController!.popToViewController(aViewController, animated: true);
                return
            }
        }
        let storyboard = UIStoryboard(name: "CKMain", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "ExportCKrVC") as?
        ExportCKrVC
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    //MARK: Sell Coin
    func sellCoinCalledVC(coinL: Coin?){
        let storyboard = UIStoryboard(name: "CKSellCoin", bundle: nil)
        let demoVC = storyboard.instantiateViewController(withIdentifier: "SellCoinPriceVC") as? SellCoinPriceVC
        demoVC?.coin = coinL;
        self.navigationController?.pushViewController(demoVC!, animated: true)
    }
    
    
    
    func coinProfileVCCalled(selectedCoin: Coin){
        let storyboard = UIStoryboard(name: "CKCoinProfile", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKCoinProfileVC") as?
        CKCoinProfileVC
        controllerObejct?.coin = selectedCoin;
        self.navigationController?.pushViewController(controllerObejct!, animated: true);
        //self.present(controllerObejct!, animated: true, completion: nil)
    }
    
    
    func coinCirculationVCCalled(coinIdL: Int?){
        let storyboard = UIStoryboard(name: "CKCoinProfile", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKCirculateListVC") as?
        CKCirculateListVC
        controllerObejct?.coinId = coinIdL
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    func coinAddCommentVCCalled(addComment: Bool?,commentObjectL: AddCommentObject) -> CKAddCommentVC{
        let storyboard = UIStoryboard(name: "CKCoinProfile", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKAddCommentVC") as?
        CKAddCommentVC
        controllerObejct?.addComment = addComment;
        controllerObejct?.commentObject = commentObjectL
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        return controllerObejct!;
        //self.present(controllerObejct!, animated: true, completion: nil)
    }
   
    func coinShowCommentVCCalled(addCommentObj: AddCommentObject){
        let storyboard = UIStoryboard(name: "CKCoinProfile", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CKShowCommentsVC") as?
        CKShowCommentsVC
        controllerObejct?.addCommentObject = addCommentObj;
        let navVC = UINavigationController(rootViewController: controllerObejct!);
        self.present(navVC, animated: true, completion: nil)
    }
    
    
    func webViewControllerCalled(_ navTitle: String? , url: String?){
        let storyboard = UIStoryboard(name: "CKLogin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "WebViewController") as?
        WebViewController
        controllerObejct?.navTitle = navTitle
        controllerObejct?.url = url
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    //MARK: Claim a Coin
    func claimCoinCalledVC(){
        let storyboard = UIStoryboard(name: "CKMain", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "ClaimCoinVC") as?
        ClaimCoinVC
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        self.present(controllerObejct!, animated: true, completion: nil)
    }
    
    
    //MARK: Claim a Coin
    func connectEthWalletCalledVC(){
        let storyboard = UIStoryboard(name: "CKMain", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "ConnectEthWalletVC") as?
        ConnectEthWalletVC
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        self.present(controllerObejct!, animated: true, completion: nil)
    }
    
    
  
    
    
    
    //MARK: Buy more VC Called
    func buymoreMainVCCalled(_animated: Bool){
        //if(checkVCInStack(_animated) == false){
            let storyboard = UIStoryboard(name: "CKBuymoreCoin", bundle: nil)
            let controllerObejct = storyboard.instantiateViewController(withIdentifier: "BuymoreMainVC") as?
            BuymoreMainVC
            self.navigationController?.pushViewController(controllerObejct!, animated: _animated)
            
       // }
    }
    
    
    func coinPaymentsVCCalled(coinParams: CoinPaymentParameters){
        let storyboard = UIStoryboard(name: "CKBuymoreCoin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "CoinPaymentVC") as?
        CoinPaymentVC
        controllerObejct?.coinparam = coinParams;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
        // }
    }
    
    
    
    //MARK: Login VC Called
    func loginViewControllerCalled(login: Bool){
        let storyboard = UIStoryboard(name: "CKLogin", bundle: nil)
        let controllerObejct = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        controllerObejct!.loginPressed = login
        controllerObejct?.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        controllerObejct?.modalPresentationStyle = .overCurrentContext
        controllerObejct?.modalTransitionStyle = .crossDissolve;
        controllerObejct?.delegate = self;
       // let navC = UINavigationController(rootViewController: controllerObejct!)
        self.present(controllerObejct!, animated: true, completion: nil)
        
        //self.navigationController?.pushViewController(controllerObejct!, animated: false)
        
    }
  
    //MARK: Tap gesture
    func addTapGestureToView(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CKBaseVC.DismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
    
    
    
    //MARK: Image Picker Methods
    
    func addSetImageNotification(_ vc: UIViewController){
        NotificationCenter.default.addObserver(self, selector: #selector(CKBaseVC.setImageNotifictaionReceived(_:)), name: NSNotification.Name(rawValue: "SetImageNotification"), object: nil)
    }
    
    func postSetImageNotificataion(_ file: String){
        NotificationCenter.default.post(name: Notification.Name(rawValue: "SetImageNotification"), object: file)
    }
    
    
    @objc func setImageNotifictaionReceived(_ notification: Notification) {
        
    }
    
    
    func showImageSelector(_ showCropperL: Bool? ,showSquareCropL: Bool?)
    {
        self.showSquareCrop = showSquareCropL
        self.showCropper = showCropperL
        
        let alert:UIAlertController=UIAlertController(title: NSLocalizedString("add_photo", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.view.tintColor = Color.themeDarkColor
        let cameraAction = UIAlertAction(title: NSLocalizedString("take_photo", comment: ""), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: NSLocalizedString("choose_library", comment: ""), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            alert.addAction(cameraAction)
        }
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.delegate = self;
            self .present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.imagePicker.allowsEditing = false;
        self.imagePicker.delegate = self;
        self.imagePicker.mediaTypes =  [kUTTypeMovie as String, kUTTypeImage as String, kUTTypeLivePhoto as String]  //[KUTTypeMovie, KUTTypeVideo, KUTTypeMPEG4]
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    //MARK: Image Picker Delegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true, completion: nil)
        var filename: String = "";
       // if #available(iOS 11.0, *) {
            if(info[UIImagePickerControllerMediaURL] != nil){
                let url = info[UIImagePickerControllerMediaURL] as! URL
                filename = url.absoluteString
                self.setImageAfterSelection(filename)
                
            }else if (info[UIImagePickerControllerImageURL] != nil){
                let url = info[UIImagePickerControllerImageURL] as! URL
                filename = url.absoluteString
                if(getAttachmentType(imageStr: filename) == ATTACHMENT_TYPE.IMAGE && showCropper == true)
                {
                    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                    cropImageSelected(image: image)
                }else{
                    self.setImageAfterSelection(filename)
                }
            }
       /* } else {
            // Fallback on earlier versions
        }*/
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: PECropViewController Delegate Methods
    
    func cropImageSelected(image: UIImage){
        let controller: PECropViewController = PECropViewController()
        controller.delegate = self
        
        controller.image = image
        controller.toolbarHidden = true
        if(showSquareCrop == true){
            controller.cropAspectRatio = 1.0
        }else{
            controller.cropAspectRatio = 16.0 / 9.0
        }
        controller.keepingCropAspectRatio = true;
        
        let navigationController: UINavigationController = UINavigationController(rootViewController: controller)
        self.present(navigationController, animated: true, completion: nil)
        
    }
    
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        DispatchQueue.main.async(execute: {
            controller.dismiss(animated: true, completion: nil)
        })
        
        let filename = croppedImage.save(getFilename())
        setImageAfterSelection(filename!)
    }
    
    
    func getFilename() -> String {
        let dateFormatter = CKGlobalMethods.getFormatter()
        dateFormatter.dateFormat = "yyyy-MM-ddhh:mm:ss"
        let dateStr = dateFormatter.string(from: Date())
        return String(format: "iosapp_user_image%@.png",dateStr)
    }
    
    
    func cropViewControllerDidCancel(_ controller: PECropViewController!) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func setImageAfterSelection(_ file: String){
        addSetImageNotification(self)
        postSetImageNotificataion(file)
    }
    
    
    
    func getExtension(imageStr: String) -> String{
        let imageArray = imageStr.components(separatedBy: ".")
        if(imageArray.count != 0){
            return imageArray[imageArray.count - 1];
        }else{
            return ""
        }
    }
    
    
    func getAttachmentType(imageStr: String) -> ATTACHMENT_TYPE{
        let extensionL = getExtension(imageStr: imageStr)
        if(extensionL == "gif"){
            return ATTACHMENT_TYPE.GIF;
        }else if(extensionL == "png" || extensionL == "jpeg" || extensionL == "jpg"){
            return ATTACHMENT_TYPE.IMAGE;
        }else{
            return ATTACHMENT_TYPE.VIDEO;
        }
    }
    
    
    
    
    //MARK: Loading Methods
    func showLoadingDialog(_ loadingText: String?){
        self.view.window?.isUserInteractionEnabled = false
        let width: CGFloat = 50.0
        let height: CGFloat = 50.0
        let x: CGFloat = (self.view.frame.width - width)/2
        let y: CGFloat = (self.view.frame.height - height)/2
        
        HUD = NVActivityIndicatorView(frame: CGRect(x: x, y: y, width: width, height: height))
        //HUD?.padding = 20
        HUD?.type = NVActivityIndicatorType.ballSpinFadeLoader
        HUD?.backgroundColor = UIColor.clear
        HUD?.color = Color.themeAnotherColor
        HUD?.startAnimating()
        self.view.addSubview(HUD!)
        /* HUD = MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
         HUD?.backgroundColor = UIColor.clear //Color.themeDarkColor
         //HUD?.indi
         // HUD?.activityIndicatorColor = UIColor.white
         HUD?.label.text = loadingText!
         HUD?.label.textColor = UIColor.white
         
         // HUD?.mode = MBProgressHUDMode.CustomView
         //HUD?.customView = UIImageView(image: UIImage(named: "Loading_icon.gif"))
         
         // TAOverlay.showOverlayWithLabel(loadingText ?? NSLocalizedString("please_wait", comment: ""), options:[.OverlaySizeRoundedRect, .OverlayTypeActivitySquare, .OpaqueBackground, .OverlayShadow])
         
         //,image: UIImage(named: "load_logo.png"), */
    }
    
    
    func hideLoadingDialog(){
        if(HUD != nil){
            self.view.window?.isUserInteractionEnabled = true
            HUD?.stopAnimating()
            HUD?.removeFromSuperview()
            HUD = nil
        }
        //TAOverlay.hideOverlay()
    }
    
    //MARK: ShowDoneHud
    
    func showDoneHUD(_ message: String?){
        let HUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        HUD.mode = MBProgressHUDMode.customView
        //HUD.backgroundView.  = Color.themeAnotherColor
        HUD.customView = UIImageView(image: UIImage(named: "done_checkmark.png"))
        HUD.backgroundColor = UIColor.clear
        HUD.bezelView.style = .solidColor
        HUD.bezelView.color = Color.themeAnotherColor
        HUD.bezelView.layer.cornerRadius = 10.0
        HUD.label.text = message ?? ""
        HUD.label.textColor = UIColor.white
        HUD.hide(animated: true, afterDelay: 1.0)
    }
    
    
    /*func gradientLayer(){
     let gradientLayer = CAGradientLayer()
     gradientLayer.frame = self.view.bounds
     
     // 3
     let color1 = UIColor.yellow.cgColor as CGColor
     let color2 = UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0).cgColor as CGColor
     let color3 = UIColor.clear.cgColor as CGColor
     let color4 = UIColor(white: 0.0, alpha: 0.7).cgColor as CGColor
     gradientLayer.colors = [color1, color2, color3, color4]
     gradientLayer.locations = [0.0, 0.25, 0.75, 1.0]
     
     // 5
     // HUD?.layer.addSublayer(gradientLayer)
     
     }*/
    
    //MARK: Search View Methods
    
    func showSearchView(_ view: UIView, searchText: String?){
        
        if(searchLoadingView == nil){
            
            let width: CGFloat = 40.0
            let height: CGFloat = 40.0
            let x: CGFloat = (view.frame.width - width)/2
            let y: CGFloat = (view.frame.height - height)/2 //200//
            
            
            let frame: CGRect?
            if(searchText == "GIVE_COIN"){
                frame = CGRect(x: x, y: 200, width: width, height: height)
            }else{
                frame = CGRect(x: x, y: y, width: width, height: height)
            }
            
            searchLoadingView = NVActivityIndicatorView(frame: frame!)
            searchLoadingView?.type = NVActivityIndicatorType.ballSpinFadeLoader
            
            searchLoadingView?.backgroundColor = UIColor.clear
            searchLoadingView?.color = Color.themeAnotherColor
            searchLoadingView?.startAnimating()
        }
        self.hideNoDataTextView()
        view.addSubview(searchLoadingView!)
        
        
        
        /*if(searchLoadingView == nil){
         searchLoadingView = UINib(nibName: "SearchViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? SearchViewCell
         searchLoadingView?.frame = CGRect(x: (view.frame.width - 180)/2, y: (view.frame.height/2), width: 180, height: 38)
         searchLoadingView?.backgroundColor = UIColor.clear
         
         if(searchText == Constants.CHANGE_LOADING_POSITION){
         let yAxs: CGFloat = view.frame.height - 100
         searchLoadingView?.frame = CGRect(x: (view.frame.width - 180)/2, y: yAxs, width: 180, height: 38)
         
         //searchLoadingView?.searchActivityIndicator.color = UIColor.redColor()
         }
         searchLoadingView?.searchLabel.text = searchText
         }
         view.addSubview(searchLoadingView!)*/
    }
    
    
    
    
    
    func hideSearchView(){
        if(searchLoadingView != nil){
            searchLoadingView?.stopAnimating()
            searchLoadingView?.removeFromSuperview()
            searchLoadingView = nil
        }
    }
    
    //MARK: Error Handler
    
    func errorHandler(_ errorResponse: CKConstants.AppErrorType){
        hideLoadingDialog()
        hideSearchView()
        
        switch errorResponse {
        case .AUTHENTICATION_ERROR: break
            //logoutActionCommonMethod()
        
        case .UNEXPECTED_ERROR:
            showErrorMessage(NSLocalizedString("server_error", comment: ""))
        case .POST_NO_NETWORK:
             CKGlobalMethods.showAlertWithTitleandMessage(NSLocalizedString("woops", comment: ""), message: NSLocalizedString("network_error", comment: ""))
        case .ERROR_CANCELLED: break
            
        case .ERRORTYPE(let error):
            let errorStr = error.localizedDescription
            showErrorMessage(errorStr) //(error as NSError).localizedDescription
        }
    }
    
    
    func showErrorMessage(_ message: String?){
        if(message?.lowercased().contains("internet connection appears to be offline") == true || message?.lowercased().contains("request timed out") == true){
            /*if(Global.appDelegate().showNetworkAlert == true){
                 CKGlobalMethods.showAlertWithTitleandMessage(NSLocalizedString("woops", comment: ""), message: NSLocalizedString("network_error", comment: ""))
                Global.appDelegate().showNetworkAlert = false
                showNetworkAlertTimer()
            }*/
        }else if (message?.lowercased().contains("unexpected server error") == true){
             CKGlobalMethods.showAlertWithTitleandMessage(NSLocalizedString("woops", comment: ""), message: NSLocalizedString("server_error", comment: ""))
            
        }else if (message?.lowercased().contains("server with the specified hostname could not be found") == true){
             CKGlobalMethods.showAlertWithTitleandMessage(NSLocalizedString("woops", comment: ""), message: NSLocalizedString("server_not_found", comment: ""))
        }
        else{
            CKGlobalMethods.showAlertWithTitleandMessage(NSLocalizedString("woops", comment: ""), message: message)
        }
    }
    
    func showNetworkAlertTimer(){
        //Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(BaseViewController.showNetworkAlert), userInfo: nil, repeats: false)
    }
    
    @objc func showNetworkAlert(){
       // Global.appDelegate().showNetworkAlert = true
    }
    
    
    func loginSignupSuccessMethod(){
        
    }
    
}

extension UIImage {
    /// Save PNG in the Documents directory
    func save(_ name: String) -> String? {
        let fileManager = FileManager.default
        let path: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let url = URL(fileURLWithPath: path).appendingPathComponent(name)
        if fileManager.fileExists(atPath: url.absoluteString){
            // print("File deleted")
            try! fileManager.removeItem(atPath: url.absoluteString)
        }
        
        try! UIImagePNGRepresentation(self)?.write(to: url)
        
        return url.absoluteString;
        
    }
}
extension URL {
    static var documentsDirectory: URL {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        return (try! URL(string: documentsDirectory))!
    }
    
    static func urlInDocumentsDirectory(with filename: String) -> URL {
        return documentsDirectory.appendingPathComponent(filename)
    }
}

extension CKBaseVC: LoginViewControllerDelegate{
    
    func loginSignupSuccessDelegateMethod() {
        self.loginSignupSuccessMethod()
    }
    
    
    
}



