//
//  CKCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 07/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKCommonVC: CKBaseVC, UITextFieldDelegate{
    
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var backView: UIView!
   
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var dividerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI();
       
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initUI(){
        
        if(backView != nil){
            self.backView.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS + 10.0;
            
        }
        //self.claimView.layer.borderColor = CKColor.dividerColor;
        //self.claimView.layer.borderWidth = 1.0
        
        if(headingLabel != nil){
            self.headingLabel.textColor = CKColor.titleGreyColor;
            self.headingLabel.font = CKTheme.mediumFont(16.0)
            
        }
        
        if(dividerView != nil){
            self.dividerView.backgroundColor = CKColor.dividerColor;
            
        }
        if(self.doneBtn != nil){
            CKTheme.getCKButtonStyle(self.doneBtn, fontSizeL: 15.0)
        }
       
        if(txtField != nil){
            CKTheme.setTextFieldStyle(self.txtField, placeholder: self.txtField.placeholder!)
            self.txtField.delegate = self;
            self.txtField.addTarget(self, action: #selector(CKCommonVC.textFieldDidChange(_:)), for: .editingChanged)
             enableDisableClaimBtn()
        }
        
    }
    
    func enableDisableClaimBtn(){
        if(self.txtField.text?.count == 0){
            CKTheme.enableOrDisableButton(button: self.doneBtn, enable: false)
        }else{
            CKTheme.enableOrDisableButton(button: self.doneBtn, enable: true)
        }
    }
    
    
    @IBAction func crossBtnClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        enableDisableClaimBtn()
    }
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        enableDisableClaimBtn()
    }
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    
}
