//
//  AuctionCoinDurationVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class AuctionCoinDurationVC: AuctionCoinCommonVC , UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    var priceQuantites = [1, 2, 3 ,4, 5, 6, 7]
    var pricePicker: UIPickerView?
    var currentTextField: UITextField?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detailLabel: UILabel!
    
    var selectedRow: Int?
    
    var auction: CKAuction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subHeadingLbl.font = Theme.regularFont(subHeadingLbl_fontsize)
        setHeading()
        if(self.detailLabel != nil){
            self.detailLabel.textColor = CKColor.titleGreyColor;
            self.detailLabel.font = Theme.regularFont(16.0)
        }
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setHeading(){
        let formattedString = NSMutableAttributedString()
        formattedString
            .normal("Set the ")
            .bold("Duration", fontSize: subHeadingLbl_fontsize)
        subHeadingLbl.attributedText =  formattedString
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as! TextFieldTableViewCell
        cell.textField.placeholder = "Duration";
        self.initPickerView(cell.textField)
        cell.textField.delegate = self;
        cell.textField.text = self.setPickerValue(row: 0)
        CKTheme.setTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
        return cell;
    }
    
    
    //MARK: Picker
    func initPickerView(_ textField: UITextField){
        if(pricePicker == nil){
            pricePicker = UIPickerView()
            pricePicker!.backgroundColor = UIColor.white
            
            pricePicker!.showsSelectionIndicator = true
            pricePicker!.delegate = self
            pricePicker!.dataSource = self
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: NSLocalizedString("done", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateCoinName2VC.donePickerMethod))
            
            toolBar.setItems([doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            
            textField.inputView = pricePicker;
            textField.inputAccessoryView = toolBar
            
        }
    }
    
    
    @objc func donePickerMethod(){
        self.currentTextField?.resignFirstResponder()
    }
    
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    
    //MARK:  Picker View Delegate  and Datasource Method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return priceQuantites.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return setPickerValue(row: row);
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.currentTextField?.text = setPickerValue(row: row)
    }
    
    
    func setPickerValue(row: Int) -> String{
        let value = priceQuantites[row];
        self.selectedRow = row;
        if(value == 1){
            return String(format: "%d Day", value)
        }else{
            return String(format: "%d Days", value)
        }
        
    }
    
    
    @IBAction func confirmBtnClicked(_ sender: Any) {
        
        let dayValue = priceQuantites[self.selectedRow ?? 0]
        let today = Date()
        let selectedDate = Calendar.current.date(byAdding: .day, value: dayValue, to: today)
        
        if(self.auction != nil){
            self.auction?.auctionDay = self.setPickerValue(row: self.selectedRow ?? 0)
            self.auction?.start = String(format: "%d", today.toMillis())
            self.auction?.end =  String(format: "%d", (selectedDate?.toMillis())!);
            self.auctionCoin();
        }
        
    }
    
    
    func auctionCoin(){
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["coin"] = self.auction?.coin?.coin ?? 0;
        params["mode"] = "reverse";
        params["start"] = self.auction?.start ?? 0;
        params["end"] = self.auction?.end ?? 0;
        params["maximum"] = self.auction?.maximum ?? 0;
        params["minimum"] = self.auction?.minimum ?? 0;
        
        self.showLoadingDialog("")
        
        let cks = CKPostService()
        cks.auctionCoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.auction != nil){
                self.auctioncoinDoneVC(auctionL: self.auction);
            }
            
            if(response.error != nil){
                self.showErrorMessage(response.error ?? "")
            }
            
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error);
        })
    }
    
   
  
    
}


extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
