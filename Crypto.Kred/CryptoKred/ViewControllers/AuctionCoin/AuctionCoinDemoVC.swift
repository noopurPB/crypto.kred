//
//  AuctionCoinDemoVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class AuctionCoinDemoVC: AuctionCoinCommonVC {
    
    var auction: CKAuction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        self.auctioncoinPriceVC(auctionL: auction)
    }
    
    
    
}
