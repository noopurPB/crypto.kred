//
//  AuctionCoinPriceVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class AuctionCoinPriceVC: AuctionCoinCommonVC, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var priceQuantites =  Array(1...1000) //[1, 2, 3 ,4, 5, 6, 7, 8, 9, 10]
    var pricePicker: UIPickerView?
    var currentTextField: UITextField?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detailLabel: UILabel!
    
    var highRowSelected: Int?
    var lowRowSelected: Int?
    
    var auction: CKAuction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subHeadingLbl.font = Theme.regularFont(subHeadingLbl_fontsize)
        setHeading()
        if(self.detailLabel != nil){
            self.detailLabel.textColor = CKColor.titleGreyColor;
            self.detailLabel.font = Theme.regularFont(15.0)
        }
    }
    
    
    func setHeading(){
        let formattedString = NSMutableAttributedString()
        formattedString
            .normal("Set your ")
            .bold("High and Low Prices", fontSize: subHeadingLbl_fontsize)
        subHeadingLbl.attributedText =  formattedString
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as! TextFieldTableViewCell
        cell.titleLabel.textColor = CKColor.titleGreyColor;
        cell.titleLabel.font = Theme.boldFont(16.0)
        if(indexPath.row == 0){
            cell.titleLabel.text = "High";
            cell.textField.placeholder = "Maximum Price";
            
        }else{
            cell.topDividerView.isHidden = true;
            cell.titleLabel.text = "Low";
            cell.textField.placeholder = "Minimum Price";
        }
        cell.textField.tag = indexPath.row;
        cell.textField.text = setPickerValue(row: 0)
        cell.textField.delegate = self;
        self.initPickerView(cell.textField)
        CKTheme.setTextFieldStyle(cell.textField, placeholder: cell.textField.placeholder!)
        return cell;
    }
    
    
    //MARK: Picker
    func initPickerView(_ textField: UITextField){
        
        if(pricePicker != nil){
            pricePicker = nil;
        }
        
        if(pricePicker == nil){
            pricePicker = UIPickerView()
            pricePicker!.backgroundColor = UIColor.white
            
            pricePicker!.showsSelectionIndicator = true
            pricePicker!.delegate = self
            pricePicker!.dataSource = self
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: NSLocalizedString("done", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateCoinName2VC.donePickerMethod))
       
            toolBar.setItems([doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            
            textField.inputView = pricePicker;
            textField.inputAccessoryView = toolBar
            
        }
    }
    
    
    @objc func donePickerMethod(){
        self.currentTextField?.resignFirstResponder()
    }
    
    
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        if(self.auction != nil){
            self.auction?.maximum = priceQuantites[highRowSelected ?? 0];
            self.auction?.minimum = priceQuantites[lowRowSelected ?? 0];
            self.auctioncoinDurationVC(auctionL: self.auction);
            //self.mainVC?.demoNextClicked();
        }
    }
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
    
    //MARK:  Picker View Delegate  and Datasource Method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return priceQuantites.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return setPickerValue(row: row);
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.currentTextField?.text = setPickerValue(row: row)
    }
    
    
    func setPickerValue(row: Int) -> String{
        
        if(self.currentTextField != nil){
            if(currentTextField?.tag == 0){
                self.highRowSelected = row;
            }else{
                self.lowRowSelected = row;
            }
        }else{
            self.highRowSelected = 0;
            self.lowRowSelected = 0;
        }
        
        let value = priceQuantites[row];
        return String(format: "%d %@", value, NSLocalizedString("kred_name", comment: ""))
    }
    
    
    
}


extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String, fontSize: CGFloat) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: CKTheme.boldFont(fontSize)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
