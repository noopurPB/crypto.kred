//
//  AuctionCoinCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

class AuctionCoinCommonVC: CKBaseVC {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var subHeadingLbl: UILabel!
    var mainVC: AuctionCoinMainVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(headingLbl != nil){
            self.headingLbl.textColor = CKColor.titleGreyColor;
            self.headingLbl.font = Theme.boldFont(18.0);
        }
        
        if(dividerView != nil){
            self.dividerView.backgroundColor = CKColor.dividerColor;
        }
        
        if(nextBtn != nil){
            CKTheme.getCKButtonStyle(nextBtn, fontSizeL: 18.0)
        }
        
        if(self.subHeadingLbl != nil){
            self.subHeadingLbl.textColor = CKColor.titleGreyColor;
            self.subHeadingLbl.font = Theme.mediumFont(subHeadingLbl_fontsize)
        }
        
        navBarStyle?.delegate = self;
        navBarStyle?.showNavigationBackBarItem(self, navTitle: NSLocalizedString("auction_this_coin", comment: ""));
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
   
    
    func auctioncoinPriceVC(auctionL: CKAuction?) {
        let chooseVC = self.storyboard?.instantiateViewController(withIdentifier: "AuctionCoinPriceVC") as?
        AuctionCoinPriceVC
        chooseVC?.auction = auctionL;
        self.navigationController?.pushViewController(chooseVC!, animated: true)
    }
    
    func auctioncoinDurationVC(auctionL: CKAuction?) {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "AuctionCoinDurationVC") as?
        AuctionCoinDurationVC
       controllerObejct?.auction = auctionL;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    
    
    func auctioncoinDoneVC(auctionL: CKAuction?){
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "AuctionCoinDoneVC") as?
        AuctionCoinDoneVC
        controllerObejct?.auction = auctionL;
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
}

extension AuctionCoinCommonVC: NavigationBarStyleDelegate{
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true);
    }
}
