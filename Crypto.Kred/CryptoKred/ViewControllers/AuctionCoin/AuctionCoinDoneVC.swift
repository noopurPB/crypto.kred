//
//  AuctionCoinDoneVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class AuctionCoinDoneVC: AuctionCoinCommonVC {
    
    @IBOutlet weak var auctionHeadingLabel: UILabel!
    
    @IBOutlet weak var auctionPriceLabel: UILabel!
    
    @IBOutlet weak var auctionDurationLabel: UILabel!
    
    var auction: CKAuction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.auctionHeadingLabel.textColor = CKColor.titleGreyColor;
        self.auctionHeadingLabel.font = CKTheme.boldFont(23.0)
        
        self.auctionPriceLabel.textColor = CKColor.titleGreyColor;
        self.auctionPriceLabel.font = CKTheme.mediumFont(17.0)
        
        self.auctionDurationLabel.textColor = CKColor.titleGreyColor;
        self.auctionDurationLabel.font = CKTheme.mediumFont(17.0)
       
        self.setAuctionValues();
        
        self.navBarStyle?.showNavigationTitle(self, navTitle: NSLocalizedString("auction_this_coin", comment: ""))
        
        //// self.auctionDurationLabel.text = sendTo;
        // self.auctionDurationLabel.text = sendTo;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setAuctionValues(){
        
        let priceStr = String(format: "High Price: %d %@ - Low Price: %d %@", self.auction?.maximum ?? 0, NSLocalizedString("kred_name", comment: ""), self.auction?.minimum ?? 0, NSLocalizedString("kred_name", comment: ""))
        self.auctionPriceLabel.text = priceStr;
        
        let durationStr = String(format: "Duration: %@", self.auction?.auctionDay ?? "0 Days")
        self.auctionDurationLabel.text = durationStr;
        
        let xAxis = (self.view.frame.width - 270)/2
        let yAxis = (self.view.frame.height - 180)/2
        let coinView = CoinView(frame: CGRect(x: xAxis, y: yAxis, width: 300 , height: 200))
        coinView.backgroundColor = UIColor.clear;
        coinView.coin = self.auction?.coin;
        self.view.addSubview(coinView)
        
    }
    
    
    
    @IBAction func viewAuctionClicked(_ sender: UIButton) {
        self.marketPlaceCalledVC(animation: true, selectedCoin: nil)
    }
    

    
    
}
