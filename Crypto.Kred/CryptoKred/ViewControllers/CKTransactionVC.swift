//
//  CKTransactionVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 03/07/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit


class CKTransactionVC: CKBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var tranasctions = [CKTransaction]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarStyle?.showNavigationSideMenuBarItem(self, navTitle: "Transaction History")
        
        //navBarStyle?.delegate = self;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getTransactionHistory()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       return UITableViewAutomaticDimension;
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tranasctions.count;
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTransactionTVCell", for: indexPath) as! CKTransactionTVCell
            if(indexPath.row % 2 == 0){
                cell.contentView.backgroundColor = CKColor.tableviewCellColor2;
            }else{
                cell.contentView.backgroundColor = CKColor.tableviewCellColor1;
            }
        
            let trans = self.tranasctions[indexPath.row];
            cell.transLbl.text = trans.descriptionL;
            cell.transLbl.sizeToFit()
       
        if(trans.after?.collection != nil){
            let isIntegerCollection = trans.after?.collection?.truncatingRemainder(dividingBy: 1) == 0
            if(isIntegerCollection){
                cell.collectionLbl.text = String(format: "%0.0f %@", trans.after?.collection ?? 0.0, NSLocalizedString("kred_name", comment: ""))
                
            }else{
                cell.collectionLbl.text = String(format: "%0.1f %@", trans.after?.collection ?? 0.0, NSLocalizedString("kred_name", comment: ""))
            }
        }else{
            cell.collectionLbl.text = ""
        }
        cell.collectionLbl.sizeToFit()
        
        if(trans.after?.balance != nil){
            let isIntegerBalance = trans.after?.balance?.truncatingRemainder(dividingBy: 1) == 0
            if(isIntegerBalance){
                cell.balanceLbl.text = String(format: "%0.0f %@", trans.after?.balance ?? 0.0, NSLocalizedString("kred_name", comment: ""))
                
            }else{
                cell.balanceLbl.text = String(format: "%0.1f %@", trans.after?.balance ?? 0.0, NSLocalizedString("kred_name", comment: ""))
            }
                
        }else{
            cell.balanceLbl.text = ""
        }
        cell.balanceAdjLbl.sizeToFit()
        
        
        
        if(trans.collection_adj != nil && trans.collection_adj != 0){
            if(trans.collection_adj! < 0){
                cell.collectionAdjLbl.textColor = UIColor.red
                cell.collectionAdjLbl.setFAText(prefixText: "", icon: .FAAngleDown, postfixText: String(format: " %d", 0 - trans.collection_adj!) , size: 15.0)
            }else{
                cell.collectionAdjLbl.textColor = CKColor.verifiedGreen
                cell.collectionAdjLbl.setFAText(prefixText: "", icon: .FAAngleUp, postfixText: String(format: " %d", trans.collection_adj ?? 0) , size: 15.0)
            }
            
        }else{
            cell.collectionAdjLbl.isHidden = true;
        }
        
        
        if(trans.balance_adj != nil && trans.balance_adj != 0){
            if(trans.balance_adj! < 0){
                 cell.balanceAdjLbl.textColor = UIColor.red
                 cell.balanceAdjLbl.setFAText(prefixText: "", icon: .FAAngleDown, postfixText: String(format: " %d", 0 - trans.balance_adj!) , size: 15.0)
            }else{
                cell.balanceAdjLbl.textColor = CKColor.verifiedGreen
                cell.balanceAdjLbl.setFAText(prefixText: "", icon: .FAAngleUp, postfixText: String(format: " %d", trans.balance_adj ?? 0) , size: 15.0)
            }
            
        }else{
                cell.balanceAdjLbl.isHidden = true;
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    //MARK: Transaction History
    func getTransactionHistory(){
        let cks = CKGetService()
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["page"] = 1
        params["count"] = 100
        
        self.showSearchView(self.view, searchText: "")
        
        cks.getTransactionHistory(params: params, responseHandler: { response in
            self.tranasctions = response.transactions;
            self.tableView.reloadData();
            if(self.tranasctions.count == 0){
                self.showNoDataTextView(text: "No transactions")
            }else{
                self.hideNoDataTextView();
            }
            self.hideSearchView()
        }, cacheHandler: { response in
            self.tranasctions = response.transactions;
            self.tableView.reloadData();
            //self.hideSearchView()
        }, errorHandler: { error in
            self.errorHandler(error)
            self.hideSearchView()
        })
        
        
        
    }
    
}
