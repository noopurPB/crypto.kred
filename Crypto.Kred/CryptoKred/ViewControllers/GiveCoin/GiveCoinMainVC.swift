//
//  GiveCoinMainVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit



class GiveCoinMainVC: CKBaseVC{
  
    private var pageViewController: UIPageViewController?
    var screenCount = 5;
    
    var controllerObjectFrame: CGRect?
    var currentPageIndex: Int = -1;
    
    var sendTo: String?
    var coin: Coin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controllerObjectFrame = CGRect(x: 0.0, y: self.view.frame.height - 397, width: self.view.frame.width, height: 400)
        createPageViewController()
        setupPageControl()
        NotificationCenter.default.addObserver(self, selector: #selector(GiveCoinMainVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GiveCoinMainVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func createPageViewController() {
        let pageController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        //pageController.dataSource = self
        pageController.view.frame = controllerObjectFrame! //self.view.frame
        pageController.view.clipsToBounds = true
        pageController.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        pageViewController = pageController
        
        if screenCount > 0 {
            demoNextClicked()
        }
        
        
       
        
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
    }
    
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.clear //Color.themeDarkColor
        appearance.currentPageIndicatorTintColor = UIColor.clear //Color.themeDarkColor
        appearance.backgroundColor = UIColor.clear
    }
    
    
    private func getItemController(_ itemIndex: Int) -> UIViewController? {
        if(itemIndex == 0){
            return givecoinDemoVC()
        } else if(itemIndex == 1){
            return givecoinChooseVC()
        } else if(itemIndex == 2){
            return givecoinToVC()
        } else if(itemIndex == 3){
            return givecoinMessageVC()
        }
        else{
            return givecoinDoneVC()
        }
       
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return screenCount;
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    //MARK: Keyboard methods
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //258
       // if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
       // {
            var frame = self.pageViewController?.view.frame;
            frame?.origin.y = (pageViewController?.view.frame.origin.y)! - 200;
            self.pageViewController?.view.frame = frame!;
        //}
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.pageViewController?.view.frame = controllerObjectFrame!
       // }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Button Method
    
    @objc func crossBtnClicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func demoNextClicked(){
        self.currentPageIndex = currentPageIndex+1;
        let firstController = getItemController(self.currentPageIndex)!
        let startingViewControllers: NSArray = [firstController]
        pageViewController?.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.forward ,animated: false, completion: nil)
    }
    
  
    
    func givecoinDemoVC() -> UIViewController {
        let demoVC = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinDemoVC") as? GiveCoinDemoVC
        demoVC?.view.clipsToBounds = true
        demoVC?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        demoVC?.nextBtn.addTarget(self, action: #selector(GiveCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        demoVC?.crossBtn.addTarget(self, action: #selector(GiveCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
        return demoVC!
    }
    
    func givecoinChooseVC() -> UIViewController {
        let chooseVC = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinChooseVC") as?
        GiveCoinChooseVC
        chooseVC?.selectedCoin = self.coin;
        chooseVC?.view.clipsToBounds = true
        chooseVC?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        chooseVC?.delegate = self;
        
        //chooseVC?.nextBtn.addTarget(self, action: #selector(GiveCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        chooseVC?.crossBtn.addTarget(self, action: #selector(GiveCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
      //  chooseVC?.giveCoinMainVC = self;
        
        return chooseVC!
    }
    
    func givecoinToVC() -> UIViewController {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinToVC") as?
        GiveCoinToVC
        controllerObejct?.view.clipsToBounds = true
        controllerObejct?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        controllerObejct?.delegate = self;
       // controllerObejct?.nextBtn.addTarget(self, action: #selector(GiveCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        controllerObejct?.crossBtn.addTarget(self, action: #selector(GiveCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
        return controllerObejct!
    }
    
    func givecoinMessageVC() -> UIViewController {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinMessageVC") as?
        GiveCoinMessageVC
        controllerObejct?.view.clipsToBounds = true
        controllerObejct?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        controllerObejct?.selectedCoin = self.coin;
        controllerObejct?.sendTo = self.sendTo ?? ""
        controllerObejct?.delegate = self
       // controllerObejct?.nextBtn.addTarget(self, action: #selector(GiveCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        controllerObejct?.crossBtn.addTarget(self, action: #selector(GiveCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
       return controllerObejct!
    }
    
    func givecoinDoneVC() -> UIViewController {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinDoneVC") as?
        GiveCoinDoneVC
        controllerObejct?.sendTo = self.sendTo ?? ""
        controllerObejct?.view.clipsToBounds = true
        controllerObejct?.view.layer.cornerRadius = CKTheme.ALERT_VIEW_FILTER_RADIUS;
        
       // controllerObejct?.nextBtn.addTarget(self, action: #selector(GiveCoinMainVC.demoNextClicked), for: UIControlEvents.touchUpInside)
        controllerObejct?.crossBtn.addTarget(self, action: #selector(GiveCoinMainVC.crossBtnClicked), for: UIControlEvents.touchUpInside)
        return controllerObejct!
    }
}

extension GiveCoinMainVC: GiveCoinToVCDelegate{
    func nextBtnClickedDelegateMethod(sendTo: String?) {
        self.sendTo = sendTo
        self.demoNextClicked()
    }
}
 

extension GiveCoinMainVC: GiveCoinChooseVCDelegate{
    func chooseNextBtnClickedDelegateMethod(coin: Coin?) {
        self.coin = coin;
        self.demoNextClicked()
    }
}


extension GiveCoinMainVC: GiveCoinMessageVCDelegate{
    
    func giveCoinBtnClickedDelegateMethod() {
        demoNextClicked()
    }
}

