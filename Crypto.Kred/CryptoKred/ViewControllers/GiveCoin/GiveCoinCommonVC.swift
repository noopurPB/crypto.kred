//
//  GiveCoinCommonVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 06/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class GiveCoinCommonVC: CKBaseVC {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var subHeadingLbl: UILabel!

    var giveCoinArray = ["give-1","give-2","give-3","give-4","give-5","give-6","give-7","give-8","give-9"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(headingLbl != nil){
            self.headingLbl.textColor = CKColor.titleGreyColor;
            self.headingLbl.font = Theme.boldFont(19.0);
        }
        
        if(dividerView != nil){
            self.dividerView.backgroundColor = CKColor.dividerColor;
        }
        
        if(nextBtn != nil){
            CKTheme.getCKButtonStyle(nextBtn, fontSizeL: 18.0)
        }
        
        if(self.subHeadingLbl != nil){
            self.subHeadingLbl.textColor = CKColor.titleGreyColor;
            self.subHeadingLbl.font = Theme.mediumFont(subHeadingLbl_fontsize)
        }
        
        navBarStyle?.delegate = self;
        navBarStyle?.showNavigationBackBarItem(self, navTitle: "Give a Coin");
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
   
   
    func givecoinToVC(coin: Coin?){
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinToVC") as?
        GiveCoinToVC
        controllerObejct?.selectedCoin = coin;
      //  controllerObejct?.delegate = self;
         self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    func givecoinMessageVC(coin: Coin?, sendTo: String?) {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinMessageVC") as?
        GiveCoinMessageVC
        controllerObejct!.selectedCoin = coin!;
        controllerObejct?.sendTo = sendTo ?? ""
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
    func givecoinDoneVC(sendTo: String?) {
        let controllerObejct = self.storyboard?.instantiateViewController(withIdentifier: "GiveCoinDoneVC") as?
        GiveCoinDoneVC
        controllerObejct?.sendTo = sendTo ?? ""
        self.navigationController?.pushViewController(controllerObejct!, animated: true)
    }
    
   
    
}


extension GiveCoinCommonVC: NavigationBarStyleDelegate{
    func backBtnClickedDelegateMethod() {
        self.navigationController?.popViewController(animated: true);
    }
}
