//
//  GiveCoinDoneVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class GiveCoinDoneVC: GiveCoinCommonVC {
    @IBOutlet weak var giveHeadingLabel: UILabel!
    
    @IBOutlet weak var giveSubHeadingLabel: UILabel!
    
    var sendTo: String?
    @IBOutlet weak var giveEmailLabel: UILabel!
    @IBOutlet weak var givecoinImageBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.giveHeadingLabel.textColor = CKColor.titleGreyColor;
        self.giveHeadingLabel.font = CKTheme.boldFont(25.0)
        
        self.giveSubHeadingLabel.textColor = CKColor.titleGreyColor;
        self.giveSubHeadingLabel.font = CKTheme.mediumFont(17.0)
        
        self.giveEmailLabel.textColor = CKColor.titleGreyColor;
        self.giveEmailLabel.font = CKTheme.boldFont(17.0)
        self.giveEmailLabel.text = sendTo;
        
        
        let n = Int(arc4random_uniform(UInt32(giveCoinArray.count)))
        
        let image = UIImage(named: giveCoinArray[n])
        self.givecoinImageBtn.setImage(image, for: UIControlState.normal)
        
        if(doneBtn != nil){
            CKTheme.getCKButtonStyle(self.doneBtn, fontSizeL: 18.0)
        }
        
    }
    
    @IBAction func doneBtnClicked(_ sender: UIButton) {
        self.collectionViewVCCalled(userCollection: false, selectedCoin: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
