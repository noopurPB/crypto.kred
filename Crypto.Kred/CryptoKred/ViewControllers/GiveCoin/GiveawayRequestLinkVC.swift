//
//  GiveawayRequestLinkVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 02/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class GiveawayRequestLinkVC: GiveCoinCommonVC, UITableViewDataSource, UITableViewDelegate {
    
    var selectedCoin: Coin?
    @IBOutlet weak var tableView: UITableView!
   
    enum URL_TYPE {
        case NO_GIVEAWAY_URL;
        case GIVEAWAY_CLICKED;
        case DEFAULT;
    }
    
    var url_type: URL_TYPE?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.url_type = URL_TYPE.DEFAULT;
        
        navBarStyle?.showNavigationBackBarItem(self, navTitle: NSLocalizedString("giveaway_and_request_links", comment: ""))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview Datasource and Delegate
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row == 0){
            return 135;
        }else if(indexPath.row == 1){
            return 70;
        }
        else if(indexPath.row == 2 || indexPath.row == 3){
            return 70;
        }/*else if(indexPath.row == 4){
            if(url_type == URL_TYPE.NO_GIVEAWAY_URL || url_type == URL_TYPE.GIVEAWAY_CLICKED){
                return 0;
            }
            else{
                return 0; //return 70;
            }
        }else if(indexPath.row == 5){
            
            if(url_type == URL_TYPE.GIVEAWAY_CLICKED){
                return 135;
            }else{
                return 40.0;
            }
            
        }else if(indexPath.row == 6){
            
            return 0;
            
        }*/
        else{
            return 40.0
        }
    }
    
    
   
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
          return UITableViewAutomaticDimension;
        }else if(indexPath.row == 1){
            return 80;
        }
        else if(indexPath.row == 2 || indexPath.row == 3){
            return 60;
        }/*else if(indexPath.row == 4){
            if(url_type == URL_TYPE.NO_GIVEAWAY_URL || url_type == URL_TYPE.GIVEAWAY_CLICKED){
                return 0;
            }
            else{
                return 70;
            }
        }else if(indexPath.row == 5){
            
            if(url_type == URL_TYPE.GIVEAWAY_CLICKED){
               return UITableViewAutomaticDimension;
            }else{
                return 40.0;
            }
            
        }*/
        else{
            return 50.0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            
            let linkUrl = String(format: "https://app.crypto.kred/coin/%@/%d?request=%@", selectedCoin?.symbol ?? "", selectedCoin?.sequence ?? 0, selectedCoin?.code ?? "")
            
             return  setLinkCell(tableView: tableView, indexPath: indexPath, title: "Share this link for anyone to request this coin:  ", url: linkUrl);
        
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKTitleTVCell") as! CKBaseTVCell
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = CKTheme.boldFont(17.0)
            cell.anotherTitleLabel.textColor = CKColor.titleGreyColor;
            cell.anotherTitleLabel.font = CKTheme.mediumFont(15.0)
            return  cell;
        }
        else if(indexPath.row == 2 || indexPath.row == 3){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKCodeTVCell") as! CKBaseTVCell
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = CKTheme.boldFont(17.0)
            cell.anotherTitleLabel.textColor = CKColor.titleGreyColor;
            cell.anotherTitleLabel.font = CKTheme.boldFont(22.0)
            cell.anotherTitleLabel.backgroundColor = CKColor.tableviewCellColor2;
            cell.anotherTitleLabel.clipsToBounds = true;
            cell.anotherTitleLabel.layer.cornerRadius = 3.0;
            if(indexPath.row == 2){
                cell.titleLabel.text = "Code:"
                cell.anotherTitleLabel.text = selectedCoin?.code ?? ""
            }else{
                cell.titleLabel.text = "Mobile:"
                cell.anotherTitleLabel.text = getCountryContactNo()
            }
            return  cell;
        
        }/*else if(indexPath.row == 4){
                let cell = tableView.dequeueReusableCell(withIdentifier: "CKURLTVCell") as! CKBaseTVCell
                CKTheme.getCKButtonStyle(cell.rightsideBtn, fontSizeL: 18.0)
                cell.baseDelegate = self;
                cell.rightsideBtn.tag = indexPath.row;
                return  cell;
        }*/
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKInfoTVCell") as! CKBaseTVCell
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = CKTheme.boldFont(15.0)
             cell.titleLabel.setFAText(prefixText: "", icon: .FAInfoCircle, postfixText: " Anyone can claim without approval", size: 15.0)
             return  cell;
            
            /*
            
            if(indexPath.row == 5 && url_type == URL_TYPE.GIVEAWAY_CLICKED){
                return  setLinkCell(tableView: tableView, indexPath: indexPath, title: "Share this link to giveaway this coin:  ", url: "https://app.crypto.kred/claim/coin/1025?giveaway=1&claim=16282936624");
            }else{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CKInfoTVCell") as! CKBaseTVCell
            cell.titleLabel.textColor = CKColor.titleGreyColor;
            cell.titleLabel.font = CKTheme.boldFont(15.0)
            
                if(indexPath.row == 5){
                    if(url_type == URL_TYPE.NO_GIVEAWAY_URL){
                        cell.titleLabel.setFAText(prefixText: "", icon: .FAInfoCircle, postfixText: " No giveaway URLs available ", size: 15.0)
                        cell.titleLabel.textColor = UIColor.red;
                    }else{
                         cell.titleLabel.setFAText(prefixText: "", icon: .FAInfoCircle, postfixText: " This coin has 1 Giveaway URL available", size: 15.0)
                    }
                }else{
                    cell.titleLabel.setFAText(prefixText: "", icon: .FAInfoCircle, postfixText: " Anyone can claim without approval", size: 15.0)
                }
                
                return  cell;
            }*/
            
        }
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func setLinkCell(tableView: UITableView, indexPath: IndexPath, title: String, url: String) -> CKBaseTVCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKCopyTVCell") as! CKBaseTVCell
        
        cell.rightsideBtn.titleLabel?.font = CKTheme.mediumFont(15.0)
        cell.rightsideBtn.setFAText(prefixText: title, icon: .FACopy, postfixText: "", size: 15.0, forState: UIControlState.normal)
        cell.rightsideBtn.setFATitleColor(color: CKColor.titleGreyColor)
        cell.baseDelegate = self;
        cell.rightsideBtn.tag = indexPath.row;
        
        cell.anotherDividerView.backgroundColor = CKColor.tableviewCellColor2;
        cell.anotherDividerView.clipsToBounds = true;
        cell.anotherDividerView.layer.cornerRadius = 3.0;
       
        cell.anotherTitleLabel.textColor = CKColor.titleGreyColor;
        cell.anotherTitleLabel.font = CKTheme.mediumFont(16.0)
        cell.anotherTitleLabel.text = url;
        cell.anotherTitleLabel.sizeToFit()
        
        
        
        return  cell;
    }
    
    
    func getCountryContactNo() -> String{
        let locale = Locale.current
        print(locale.regionCode ?? "")
        if (locale.regionCode == "AU"){
            return "+61448101909"
        }else if (locale.regionCode == "EU" || locale.regionCode == "UK"){
            return "+442033229216"
        }else{
            return "+14156445733"
        }
    }
    

    
}

extension GiveawayRequestLinkVC: CKBaseTVCellDelegate{
    
    func rightBtnClickedTVCellDelegateMethod(_ sender: UIButton) {
        if(sender.tag == 4){
            url_type = URL_TYPE.GIVEAWAY_CLICKED;
            self.tableView.reloadData()
            
        }else if(sender.tag == 0){
            //copy request link
            UIPasteboard.general.string = "https://app.crypto.kred/coin/hello2/1?request=01255"
        }else{
            //copy giveway link
            UIPasteboard.general.string = "https://app.crypto.kred/claim/coin/1025?giveaway=1&claim=16282936624"
            
        }
    }
}



