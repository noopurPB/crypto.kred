//
//  GiveCoinChooseVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol GiveCoinChooseVCDelegate{
    @objc optional func chooseNextBtnClickedDelegateMethod(coin: Coin?)
}
class GiveCoinChooseVC: GiveCoinCommonVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var previousCoinBtn: UIButton!
    @IBOutlet weak var nextCoinBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedCoin: Coin?
    
    var selectedIndex: Int = 0
    var coinsList = [Coin]()
    var delegate: GiveCoinChooseVCDelegate?
    var draftChooseCoin: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.isScrollEnabled = false;
        self.subHeadingLbl.font = CKTheme.mediumFont(20.0)
        
        if(draftChooseCoin == true){
            self.subHeadingLbl.text = "Choose a Draft coin"
            self.nextBtn.setTitle("SELECT COIN", for: UIControlState.normal)
        }else{
            self.subHeadingLbl.text = "Choose a Coin to give"
             self.nextBtn.setTitle("NEXT", for: UIControlState.normal)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(draftChooseCoin == true){
                self.coinDraftServiceMethod()
        }else{
            if(selectedCoin == nil){
                self.coinUserCollectionServiceMethod()
            }else{
                self.coinsList = [selectedCoin!]
                self.coinsCommonHandler(self.coinsList)
            }
        }
    }
    
    
    @IBAction func nextCoinBtnClicked(_ sender: Any) {
        if(selectedIndex < coinsList.count-1){
            self.collectionView.scrollToItem(at:IndexPath(item: selectedIndex+1, section: 0), at: .right, animated: true)
            selectedIndex = selectedIndex+1;
        }
    }
    
    
    @IBAction func previousCoinBtnClicked(_ sender: UIButton) {
        if(selectedIndex > 0){
            self.collectionView.scrollToItem(at:IndexPath(item: selectedIndex-1, section: 0), at: .left, animated: true)
            selectedIndex = selectedIndex-1;
        }
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(coinsList.count != 0){
            if(draftChooseCoin == true){
                CKGlobalVariables.sharedManager.coin = self.coinsList[selectedIndex]
                createCoinDoneVCCalled()
            }else{
                self.givecoinToVC(coin: self.coinsList[selectedIndex])
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- UICollectionView DataSource and Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return coinsList.count;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CKWebViewCVCell",for:indexPath) as! CKBaseCVCell
        
        let coin = self.coinsList[indexPath.row]
        cell.coinView.coin = coin;
        cell.coinView.backgroundColor = UIColor.clear;
        
        DispatchQueue.main.async {
            cell.coinView.loadCoin(webView: cell.coinView.frontWebView!)
            cell.coinView.loadCoin(webView: cell.coinView.backWebView!)
        }
        return cell;
    }
    
    // MARK:- UICollectionViewDelegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 290, height: 220)
    }
    
    
    
    func coinUserCollectionServiceMethod() {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        //if(userCollection == true){
        params["user"] = CKGlobalVariables.sharedManager.loggedInUserId;
        params["minted"] = true
        //}
        self.coinCollectionServiceMethod(params: params,  path: CKServiceConstants.COIN_URL)
    }
    
    
    func coinDraftServiceMethod() {
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        self.coinCollectionServiceMethod(params: params, path: CKServiceConstants.COIN_DRAFT_URL)
    }
    
    
    
    
    func coinCollectionServiceMethod(params: [String: Any], path: String) {
        let pageId = CKGlobalMethods.getPageId(self.coinsList.count)
        
        let cks = CKGetService()
        
        //params["count"] = CKServiceConstants.ITEM_PER_PAGE;
        //params["page"] = pageId
        
        if(pageId == 1){
            showSearchView(self.view, searchText: "GIVE_COIN")
        }
        
        cks.getCoinsList(params: params, path: path, responseHandler: { response  in
            // self.loadmore = self.loadMoreWebservice(self.coinsList)
            if(pageId == 1){
            self.coinsCommonHandler(response.coins)
            }else{
            self.coinsCommonHandler(self.coinsList + response.coins)
            }
            }, cacheHandler: { response  in
                if(pageId == 1){
                    self.coinsCommonHandler(response.coins)
                }
        } ,errorHandler: { error in
            self.hideSearchView()
            self.errorHandler(error)
        })
        
        
       
        
    }
    
    func coinsCommonHandler(_ coins: [Coin]){
        self.hideSearchView()
        self.coinsList = coins
        if(coinsList.count > 1){
            self.previousCoinBtn.isHidden = false
            self.nextCoinBtn.isHidden = false
        }else{
            self.previousCoinBtn.isHidden = true
            self.nextCoinBtn.isHidden = true
        }
        self.collectionView.reloadData()
    }
    
    
    
}

