//
//  GiveCoinToVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol GiveCoinToVCDelegate{
    @objc optional func nextBtnClickedDelegateMethod(sendTo: String?)
}

class GiveCoinToVC: GiveCoinCommonVC, UITextFieldDelegate {
    
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var toView: UIView!
    @IBOutlet weak var toTextField: UITextField!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var giveawayBtn: UIButton!
    var selectedCoin: Coin?
    var delegate: GiveCoinToVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI();
    }
    
    
    func initUI(){
        
        self.giveawayBtn.setFATitleColor(color: CKColor.titleGreyColor)
        self.giveawayBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        self.giveawayBtn.titleLabel?.font = CKTheme.boldFont(14.0)
        
        let btnText = NSLocalizedString("giveaway_and_request_links", comment: "") + " "
        self.giveawayBtn.setFAText(prefixText: btnText, icon: .FALink, postfixText: "", size: 14.0, forState: UIControlState.normal)
        
        self.setView(viewL: self.toView)
        self.setView(viewL: self.nameView)
        
        self.toTextField.delegate = self;
        self.nameTextField.delegate = self;
        
        self.setLabel(labelL: self.toLabel)
        self.setLabel(labelL: self.nameLabel)
        
        CKTheme.setTextFieldStyle(self.toTextField, placeholder: self.toTextField.placeholder!)
        CKTheme.setTextFieldStyle(self.nameTextField, placeholder: self.nameTextField.placeholder!)
    }
    
    
    func setView(viewL: UIView){
        viewL.clipsToBounds = true;
        viewL.layer.borderColor = CKColor.titleGreyColor.cgColor
        viewL.layer.borderWidth = 1.0;
        viewL.layer.cornerRadius = 4.0;
    }
    
    
    func setLabel(labelL: UILabel){
        labelL.textColor = CKColor.textFieldColor;
        labelL.font = CKTheme.mediumFont(15.0)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(toTextField.text?.count == 0){
            self.showErrorMessage("Please enter valid phone or email address")
        }else{
            
            self.givecoinMessageVC(coin: self.selectedCoin!, sendTo: self.toTextField.text ?? "")
            
            //delegate?.nextBtnClickedDelegateMethod!(sendTo: CKGlobalMethods.trimString(self.toTextField.text))
        }
    
    }
    
  
    @IBAction func giveAwayRequestLinks(_ sender: UIButton) {
        self.giveawayRequestLinkVC(coinL: self.selectedCoin)
    }
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
        
    }
    
    
}
