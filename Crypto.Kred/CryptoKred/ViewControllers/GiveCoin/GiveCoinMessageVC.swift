//
//  GiveCoinMessageVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

@objc protocol GiveCoinMessageVCDelegate{
    @objc optional func giveCoinBtnClickedDelegateMethod()
}

class GiveCoinMessageVC: GiveCoinCommonVC, UITextViewDelegate {
    
    @IBOutlet weak var privateMessageBtn: UIButton!
    @IBOutlet weak var messagePlaceholder: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var sendTo: String?
    var selectedCoin: Coin?
    
    var delegate: GiveCoinMessageVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messageTextView.clipsToBounds = true
        self.messageTextView.layer.borderColor = CKColor.titleGreyColor.cgColor
        self.messageTextView.layer.borderWidth = 1.0;
        self.messageTextView.layer.cornerRadius = 6.0;
        
        self.messageTextView.delegate = self;
        
        self.messagePlaceholder.textColor = CKColor.textFieldPlaceholderColor;
        self.privateMessageBtn.setTitleColor(CKColor.titleGreyColor, for: UIControlState.normal)
        self.privateMessageBtn.titleLabel?.font = CKTheme.regularFont(12.0)
        
    }
    
    
    @IBAction func privateMessageBtnClicked(_ sender: UIButton) {
        self.privateMessageBtn.isSelected = !self.privateMessageBtn.isSelected;
    }
    
    @IBAction func giveCoinBtnClicked(_ sender: Any) {
        self.sendCoinServiceCalled()
        // self.delegate?.giveCoinBtnClickedDelegateMethod!()
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        //self.givecoinDoneVC();
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    //MARK: TextView Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        hidePlaceholder(textView, placeholder: self.messagePlaceholder)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
       
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        hidePlaceholder(textView, placeholder:  self.messagePlaceholder)
    }
    
    
    func hidePlaceholder(_ textView: UITextView, placeholder: UILabel){
        if(textView.text.count == 0){
            placeholder.isHidden = false
        }else{
            placeholder.isHidden = true
        }
    }
    
    
    func sendCoinServiceCalled(){
        let cks = CKPostService()
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        params["coin"] = self.selectedCoin?.coin ?? 0
        
        if(CKGlobalMethods.isValidEmail(sendTo) == true){
            params["platform"] = "email"
        }else{
            params["platform"] = "sms"
        }
        params["address"] = sendTo ?? ""
        params["message"] = messageTextView.text
        params["private"] = self.privateMessageBtn.isSelected;
        
       // print(params)
        
        self.showLoadingDialog("")
        cks.sendACoin(params: params, responseHandler: { response in
            self.hideLoadingDialog()
            if(response.error != nil){
                self.showErrorMessage(response.message)
            }else{
                self.giveCoinDoneCalled()
            }
            
        }, errorHandler: { error in
            self.hideLoadingDialog()
            self.errorHandler(error)
        })
        
    }
    
    
    func giveCoinDoneCalled(){
        self.givecoinDoneVC(sendTo: sendTo ?? "")
    }
    
   
}
