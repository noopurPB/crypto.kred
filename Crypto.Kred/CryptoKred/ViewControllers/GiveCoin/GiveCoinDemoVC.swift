//
//  GiveCoinDemoVC.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 01/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//


import Foundation
import UIKit

class GiveCoinDemoVC: GiveCoinCommonVC {
    
    var selectedCoin: Coin?
   
    
    @IBOutlet weak var givecoinImageBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let n = Int(arc4random_uniform(UInt32(giveCoinArray.count)))
        let image = UIImage(named: giveCoinArray[n])
        self.givecoinImageBtn.setImage(image, for: UIControlState.normal)
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.givecoinChooseVC(coin: self.selectedCoin, draftCoin: false);
    }
    
    
}


