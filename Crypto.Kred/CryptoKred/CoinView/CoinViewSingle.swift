//
//  CoinViewSingle.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 04/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CoinViewSingle: UIView, UIWebViewDelegate {
    
    var webView: UIWebView?
    var coin: Coin?
    var showFrontCoin: Bool?
    var coinSize: Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        self.configureBar(coinSize: frame.width)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.configureBar(coinSize: frame.width)
    }
    
    
    func configureBar(coinSize: CGFloat) {
        self.coinSize = Int(coinSize);
        webView = UIWebView(frame: CGRect(x: 0, y: 0, width: coinSize+15 , height: coinSize+40));
        webView?.delegate = self;
        self.webViewTheme(webView: webView!)
        self.loadCoin(webView: webView!)
        self.addSubview(webView!);
    }
    
    
    func webViewTheme(webView: UIWebView){
        webView.isOpaque = false;
        webView.isUserInteractionEnabled = false;
        webView.scalesPageToFit = true;
        webView.backgroundColor = UIColor.clear;
        webView.tintColor = UIColor.clear
    }
    
    
    func loadCoin(webView: UIWebView) {
        let url = URL(fileURLWithPath: Bundle.main.path(forResource:"index", ofType: "html")!)
         webView.loadRequest(URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 60*100))
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        setCoinValues(coin: self.coin)
    }
    
    
    func setCoinValues(coin: Coin?){
        var coinColor = coin?.coinColor ?? CKColor.defaultCoinColor;
        if(!coinColor.contains("#")){
            coinColor = "#" + coinColor;
        }
        
        
        var imageUrl: String?
      //  var videoUrl: String?
        var coinValue: String?
        var upperText: String?
        
        
        var patternColor = coin?.pattern_color ?? CKColor.defaultCoinColor;
        if(!patternColor.contains("#")){
            patternColor = "#" + patternColor;
        }
        
        var textColor = coin?.text_color ?? "DD4525";
        if(textColor != "" && !textColor.contains("#")){
            textColor = "#" + textColor;
        }
        
      
        
        if(showFrontCoin == true){
            /*if(CKGlobalMethods.urlIsImage(string: coin?.front ?? "") == false){
                videoUrl = coin?.front ?? ""
            }else{
                imageUrl = coin?.front ?? ""
            }*/
            imageUrl = coin?.front ?? ""
           
            coinValue =  CKGlobalMethods.setCoinFrontValue(coin: coin)
            upperText = coin?.coinName?.uppercased() ?? ""
        }else{
            /*if(CKGlobalMethods.urlIsImage(string: coin?.back ?? "") == false){
                videoUrl = coin?.back ?? ""
            }else{
                imageUrl = coin?.back ?? ""
            }*/
            imageUrl = coin?.back ?? ""
            coinValue =  CKGlobalMethods.setCoinBackValue(coin: coin)
            upperText = coin?.issuerName?.uppercased() ?? ""
        }
        
        
        upperText = upperText?.replacingOccurrences(of: "'", with: "&#39;")
        
        
        let coinValues = String(format: "setCoinValues('%@', %d, '%@', '%@', '%@', '%@', '%@', '%@','%@')",
                                     imageUrl ?? "" ,
                                     self.coinSize!,
                                     upperText!,
                                     coinValue!,
                                     coinColor, "", //videoUrl ?? "",
                                     coin?.pattern ?? "", patternColor, textColor)
        
      //  let escapedUrl = coinValues.addingPercentEncoding(withAllowedCharacters: .)
        
        self.webView?.stringByEvaluatingJavaScript(from: coinValues)
        
     //imageL, widthL, upperTextL, lowerTextL,backgroundColorL
    }
    
    
    
    
    
}
