//
//  CoinView.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 04/04/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import UIKit

class CoinView: UIView, UIWebViewDelegate {
    
    var frontWebView: UIWebView?
    var backWebView: UIWebView?
    var coin: Coin?
   
    var coinSize: Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        self.configureBar(coinSize: frame.height)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.configureBar(coinSize: frame.height)
    }
    
    
    func configureBar(coinSize: CGFloat) {
        self.coinSize = Int(coinSize);
        frontWebView = UIWebView(frame: CGRect(x: 0, y: 0, width: coinSize , height: coinSize));
        frontWebView?.delegate = self;
        self.webViewTheme(webView: frontWebView!)
        self.loadCoin(webView: frontWebView!)
        
        
        backWebView = UIWebView(frame: CGRect(x: 120, y: 15, width: coinSize-20 , height: coinSize-25));
        backWebView?.delegate = self;
        self.webViewTheme(webView: backWebView!)
        self.loadCoin(webView: backWebView!)
        
        self.addSubview(backWebView!);
        self.addSubview(frontWebView!);
    }
    
 
    func webViewTheme(webView: UIWebView){
        webView.isOpaque = false;
        webView.isUserInteractionEnabled = false;
        webView.scalesPageToFit = true;
        webView.backgroundColor = UIColor.clear;
        webView.tintColor = UIColor.clear
    }
    
    
    
    func loadCoin(webView: UIWebView) {
        let url = URL(fileURLWithPath: Bundle.main.path(forResource:"index", ofType: "html")!)
        webView.loadRequest(URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 60*100))
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
       setCoinValues(coin: self.coin)
    }
    
    
    func setCoinValues(coin: Coin?){
        var coinColor = coin?.coinColor ?? CKColor.defaultCoinColor;
        if(!coinColor.contains("#")){
            coinColor = "#" + coinColor;
        }
        
        
        var patternColor = coin?.pattern_color ?? CKColor.defaultCoinColor;
        if(!patternColor.contains("#")){
            patternColor = "#" + patternColor;
        }
        
        var textColor = coin?.text_color ?? CKColor.defaultCoinColor;
        if(!textColor.contains("#")){
            textColor = "#" + textColor;
        }
        
        var frontImageUrl: String?
       // var frontVideoUrl: String?
        
        var backImageUrl: String?
      //  var backVideoUrl: String?
        
        /*if(CKGlobalMethods.urlIsImage(string: coin?.front ?? "") == false){
            frontVideoUrl = coin?.front ?? ""
        }else{
            frontImageUrl = coin?.front ?? ""
        }*/
        frontImageUrl = coin?.front ?? ""
        
        /*if(CKGlobalMethods.urlIsImage(string: coin?.back ?? "") == false){
            backVideoUrl = coin?.back ?? ""
        }else{
            backImageUrl = coin?.back ?? ""
        }*/
        backImageUrl = coin?.back ?? ""
          
        let coinFrontValue =  CKGlobalMethods.setCoinFrontValue(coin: coin)
        let coinBackValue =  CKGlobalMethods.setCoinBackValue(coin: coin)
        
       
        
        let frontCoinValues = String(format: "setCoinValues('%@', %d, '%@', '%@', '%@', '%@', '%@' ,'%@','%@')",
                                     frontImageUrl ?? "" ,
                                     self.coinSize! - 10,
                                     coin?.coinName?.uppercased() ?? "",
                                     coinFrontValue!,
                                     coinColor,
                                     "", //frontVideoUrl ?? "",
                                     coin?.pattern ?? "" ,
                                     patternColor, textColor)
        self.frontWebView?.stringByEvaluatingJavaScript(from: frontCoinValues)
        
        let backCoinValues = String(format: "setCoinValues('%@', %d, '%@', '%@', '%@', '%@', '%@' ,'%@','%@')",
                                    backImageUrl ?? "",
                                    self.coinSize! - 30,
                                    coin?.issuerName?.uppercased() ?? "",
                                    coinBackValue!,
                                    coinColor,
                                    "", //backVideoUrl ?? "",
                                    coin?.pattern ?? "" ,
                                    patternColor, textColor)
        self.backWebView?.stringByEvaluatingJavaScript(from: backCoinValues)
    }
    
}
