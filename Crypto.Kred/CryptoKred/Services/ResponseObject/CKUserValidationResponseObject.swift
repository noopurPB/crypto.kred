//
//  CKUserValidationResponseObject.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 20/03/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKUserValidationResponseObject: Mappable{
    
    var phone: Bool?
    var user_id: String?
    var email: Bool?
    var kreddomain: Bool?
    var error: String?
    var message: String?
    var home: String?
    var login: GrabUser?
    var user: GrabUser?
    var contacts = [GrabUser]()
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.phone <- map["phone"]
        self.user_id <- map["user_id"]
        self.email <- map["email"]
        self.kreddomain <- map["kreddomain"]
        self.error <- map["error"]
        self.message <- map["message"]
        self.home <- map["home"]
        self.login <- map["login"]
        self.contacts <- map["contacts"]
        self.user <- map["user"]
    }
    
}


