//
//  CKFileUploadResponseObject.swift
//  Grab
//
//  Created by Noopur Virmani on 04/05/16.
//  Copyright © 2016 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKFileUploadResponseObject: Mappable{
    
    var length: String?
    var name: String?
    var url: String?
    var error: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self.length <- map["length"]
        
        self.name <- map["name"]
        
        self.url <- map["url"]
        
        if(self.url == nil){
            self.url = ""
        }
        
        self.error <- map["error"]
       
        
    }
    
}
