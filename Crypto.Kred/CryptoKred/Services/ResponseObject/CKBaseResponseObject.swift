//
//  CKBaseResponseObject.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKBaseResponseObject: Mappable{
    
    var error: String?
    var status: String?
    var cls: String?
    var code: String?
    var missingL = [String]()
    var coins = [Coin]()
    var message: String?
    var batch: Coin?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        self.error <- map["error"]
        self.message <- map["message"]
        self.status <- map["status"]
        self.cls <- map["_cls"]
        self.code <- map["code"]
        self.missingL <- map["missing"]
        self.coins <- map["coins"]
        self.batch <- map["batch"]
        
    }
    
}


