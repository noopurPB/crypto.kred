//
//  CKCoinsResponseObject.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper

class CKCoinsResponseObject: Mappable{

    var error: String?
    var status: String?
    var wallets = [Wallet]()
    var coins = [Coin]()
    var coin: Coin?
    var sales = [Sale]()
    var messages = [CKMessage]()
    var balance: CKBalance?
    var auction: CKAuction?
    var auctions = [CKAuction]()
    var history =  [CKHistory]()
    var requests = [CKRequest]()
    var tags = [CKTag]()
    var transactions = [CKTransaction]()
    var invoice: CKInvoice?
    
    var limits: CKLimits?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.error <- map["error"]
        self.status <- map["status"]
        self.coins <- map["coins"]
        
        if(map["drafts"].isKeyPresent){
            self.coins <- map["drafts"]
        }
        
        self.coin <- map["coin"]
        self.wallets <- map["wallets"]
        self.sales <- map["sales"]
        self.sales <- map["market"]
        self.messages <- map["messages"]
        self.balance <- map["balance"]
        if(map["comments"].isKeyPresent){
            self.messages <- map["comments"]
        }
        self.auction <- map["auction"]
        self.auctions <- map["auctions"]
        self.history <- map["history"]
        self.requests <- map["requests"]
        self.tags <- map["tags"]
        self.transactions <- map["lines"]
        
        self.invoice <- map["invoice"]
        self.limits <- map["limits"]
    }
    
}

