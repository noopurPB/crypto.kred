//
//  CKServiceConstants.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation

struct CKServiceConstants {
    
    static let COMPANY_URL = "http://www.grab.best/"
    static let COMPANY_URL_NAME = "Grab.Best"
    
    static let SERVICE_TIME_OUT = 60.0; // seconds
    
    static let SET_COOKIE = "Set-Cookie";
    static let PHPSESSID = "PHPSESSID";
    static let EN_4_LANGUAGE = "en4_language";
    static let EN_4_LOCALE = "en4_locale";
    
    static let ITEM_PER_PAGE = 50;
    static let CURRENCY = "USD";
    static let PAYMENT_PROVIDER = "Stripe";
    
    static let DEV_KEY = "awetrv.jfsde";
    
    static let COIN_CURRENCY = 1;
    
    static let SIGNUP_URL = "https://claim.peoplebrowsr.com/ul_register/dotceo";
    static let SEND_COIN_URL = "https://claim.peoplebrowsr.com/send_coin/dotceo";
    
    static let USER_HOME_URL = "/user/home";
    static let USER_DOMAIN_CHECK_URL = "/account/claim/quick_kred"
    static let USER_CHECK_URL = "/account/check";
    static let PHONE_SMS_SEND_URL = "/account/claim/validate";
    static let PHONE_SMS_ACCEPT_URL = "/account/claim/confirm";
    
    
    static let BASE_URL = "https://api.grab.live";
    static let BASE_URL_DOMAIN = "https://claim.peoplebrowsr.com";
    
    
    static let LOGIN_URL = "/account/login";
    static let LOGOUT_URL = "/account/logout";
    static let ACCOUNT_UPDATE = "/account/update";
    static let FORGOT_PASSWORD = "/account/forgot";
    //static let USER_BIO_URL = "/user/bio";
    static let USER_PROFILE = "/user/profile"
    static let CONTACT_EDIT = "/contact/edit"
    
    //static let BASE_URL = "https://dev.grab.live";
   
    
    static let COIN_MARKET_URL = "/coin/market";
    static let COIN_AUCTION_LIST_URL = "/coin/auctions";
    static let COIN_MINT_URL = "/coin/mint";
    static let COIN_URL = "/coin/coins";
    static let COIN_GIVEN_URL = "/coin/given";
    static let COIN_INFO_URL = "/coin/coin";
    static let COIN_WALLET_URL = "/coin/wallets";
    static let COIN_MESSAGES_URL = "/widget/messages";
    static let COIN_COMMENTS_URL = "/widget/comments";
    static let FILE_UPLOAD = "/file/upload";
    
    static let COIN_SELL_URL = "/coin/sell";
    static let COIN_AUCTION_URL = "/coin/auction";
    static let COIN_BALANCE_URL = "/coin/balance";
    static let COIN_CLAIM_URL = "/coin/collect";
    static let COIN_HISTORY_URL = "/coin/history";
    static let COIN_REQUEST_URL = "/coin/request";
    static let COIN_REQUESTS_URL = "/coin/requests";
    
    static let COIN_POPULAR_TAGS_URL = "/coin/populartags";
   
    static let COIN_LIKE_URL = "/coin/like";
    static let COIN_UNLIKE_URL = "/coin/unlike";
    static let COIN_FLAG_URL = "/coin/flag";
    static let COIN_ETH_WALLET_URL = "/coin/open";
    static let COIN_CIRCULATION_URL = "/coin/owners";
    static let TRANSACTION_HISTORY_URL = "/coin/ledger2";
    

    static let COIN_LIMIT_URL = "/coin/limits";
    static let COIN_PURCHASE_URL = "/coin/purchase";
    
    
    static let COIN_DRAFT_URL = "/coin/drafts";
    static let COIN_SAVE_DRAFT_URL = "/coin/draft";
    
    
    static let GRAB_POST = "/grab/post"
    static let GRAB_COMMENT = "/grab/comment"
    static let MESSAGE_LIKE = "/grab/like";
    static let MESSAGE_UNLIKE = "/grab/unlike";
    static let CONTACTS_URL = "/contact/find";
    
    
    static let MESSAGE_DELETE = "/widget/remove";
        
    static func initializeParams()->[String: AnyObject]{
        let parameters = [
            "token": CKGlobalVariables.sharedManager.loggedInToken //"d806633b-b8b3-4c56-ba31-f361315c79d4"
        ]
        return parameters as [String : AnyObject]
    }

}
