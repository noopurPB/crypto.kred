//
//  CKPostService.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class CKPostService: CKBaseService {
    
    //MARK: Create a Coin
    func createAMintCoin(draft: Bool ,params: [String: Any] , responseHandler:@escaping (_ result: CKBaseResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path: String = "";
        if(draft == true){
            path = CKServiceConstants.COIN_SAVE_DRAFT_URL;
        }else{
            path = CKServiceConstants.COIN_MINT_URL;
        }
        
       
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKBaseResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Send a coin
    func sendACoin(params: [String: Any] , responseHandler:@escaping (_ result: CKBaseResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.SEND_COIN_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKBaseResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
        
       // self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Image Upload
    func uploadImageServiceMethod(_ base64String: String, attachmentExt: String ,responseHandler:@escaping (_ result: CKFileUploadResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        params["name"] =  getFilename(ext: attachmentExt)
        params["content"] = base64String
        
        let path = CKServiceConstants.FILE_UPLOAD
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKFileUploadResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
    
    
    //MARK: Request a coin
    func requestACoin(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_REQUEST_URL
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Post to grab/ Edit Post
    
    //MARK: Send a coin
    func writeAPost(addComment: Bool,params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = "";
        if(addComment == true){
            path = CKServiceConstants.GRAB_COMMENT;
        }else{
            path = CKServiceConstants.GRAB_POST;
        }
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Coin Like/Unlike
    
    func coinLikeUnlike(_ like: Bool, params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = ""
        if like {
            path = CKServiceConstants.COIN_LIKE_URL;
        }else{
            path = CKServiceConstants.COIN_UNLIKE_URL;
        }
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
    
    //MARK: Message Like/Unlike
    
    func likeUnlikeMessages(_ like: Bool, params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = ""
        if like {
            path = CKServiceConstants.MESSAGE_LIKE
        }else{
            path = CKServiceConstants.MESSAGE_UNLIKE
        }
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
    
        self.postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
    
    
    //MARK: Message Delete
    
    func deleteMessages(params: [String: Any] ,pathExt: String,responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = CKServiceConstants.MESSAGE_DELETE + "/" + pathExt;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        
        self.postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
    
    
    //MARK: Auction Coin
    
    func auctionCoin(params: [String: Any] ,responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_AUCTION_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
    
    //MARK: Sell Coin
    
    func sellCoin(params: [String: Any] ,responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_SELL_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
    }
    
   
    //MARK: Claim a coin
    func claimCoin(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = CKServiceConstants.COIN_CLAIM_URL;
       
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: FLag a coin
    func flagCoin(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = CKServiceConstants.COIN_FLAG_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Connect to ETH
    func connectETHWallet(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = CKServiceConstants.COIN_ETH_WALLET_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Purchase API
    
    func purchase(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = CKServiceConstants.COIN_PURCHASE_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
 
    
   /* class func postGrabServiceMethod(_ editTags: Bool,id: String? ,ids: [String]?, postMessage: String?, postImageUrl: String?, postTags: [String], rating: Int? , company: String? , responseHandler:@escaping (_ result: BaseResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: Constants.GrabErrorType) -> Void){
        
        var params = [String: Any]()
        params = ServiceConstants.initializeParams()
        
        
        if(ids != nil && ids!.count > 0){
            params["ids"] = ids
        }
        
        if(postMessage != nil){
            params["text"] = postMessage
        }
        
        if(postImageUrl != nil){
            params["media"] = postImageUrl
        }
        
        
        params["tags"] = postTags
        
        
        if(company != nil && company != ""){
            var rulesParams: [String: Any] = ["rating": rating != nil ? (rating ?? 0)! : "0"]
            rulesParams["company"] = company
            params["data"] = rulesParams
        }
        
        var path = ""
        if(editTags == false){
            if(id != nil){
                params["id"] = id
            }
            path = ServiceConstants.GRAB_POST
        }else{
            if(id != nil){
                params["message"] = id
            }
            path = ServiceConstants.GRAB_EDIT
        }
        
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<BaseResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        postObjectForUrl(path, params: params ,responseHandler:objectResponseHandler,errorHandler: errorHandler)
 
    }*/
    
    
    
    
}
