//
//  BaseService.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import Alamofire
import Haneke

class CKBaseService {
    
    //MARK:  Post Request
    
   func postObjectForUrl(_ strURL: String, params : [String: Any], responseHandler:@escaping (_ result: AnyObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
    
       let urlString: String = self.returnURL(strURL: strURL)!
    
        print(params)
       let escapedAddress = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        print(escapedAddress ?? "")
        
        let URLString = URL(string: escapedAddress!)
        
        var headers = [String: String]()
        headers["Content-Type"] = "application/json"
 
        Alamofire.request(URLString! ,method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            switch(response.result){
            case .success(let value):
                print(value)
                responseHandler(value as AnyObject)
            case .failure(let error):
                let message = error.localizedDescription
                if(message.lowercased().contains("internet connection appears to be offline") == true || message.lowercased().contains("request timed out") == true){
                    errorHandler(CKConstants.AppErrorType.POST_NO_NETWORK)
                }else{
                    errorHandler(CKConstants.AppErrorType.ERRORTYPE(error))
                }
            }
        }
    }
    
    //MARK: Cache Post Call
    func postCacheObjectForUrl(_ strURL: String, params : [String: Any], responseHandler:@escaping (_ result: AnyObject) -> Void, cacheHandler:@escaping (_ result: AnyObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        let cache = Shared.dataCache
        cache.fetch(key: strURL as String).onSuccess { data in
            let dataArchive = NSKeyedUnarchiver.unarchiveObject(with: (data))
            cacheHandler(dataArchive! as AnyObject)
        }
        postObjectForUrl(strURL, params: params, responseHandler: responseHandler, errorHandler: errorHandler)
    }
    
    
    
    
    //MARK:  Get Request
    
    func getObjectForUrl(_ strURL: String, params : [String: Any], showCache: Bool? ,responseHandler:@escaping (_ result: AnyObject) -> Void, cacheHandler:@escaping (_ result: AnyObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let cache = Shared.dataCache
        
        //BASE URL
        var urlString: String = self.returnURL(strURL: strURL)! + "?" + convertParamsToString(params)
       
       
        let escapedAddress = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        print(escapedAddress ?? "")
        
        //2
        cache.fetch(key: urlString).onSuccess { data in
            let dataArchive = NSKeyedUnarchiver.unarchiveObject(with: (data))
            if(showCache != nil && showCache == true){
                cacheHandler(dataArchive as AnyObject)
            }
        }
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let URL = Foundation.URL(string: escapedAddress!)!
        
        
        var mutableUrlRequest = URLRequest(url: URL)
        mutableUrlRequest.httpMethod = "GET"
        mutableUrlRequest.timeoutInterval = 3000
        mutableUrlRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        
        
        Alamofire.request(mutableUrlRequest)
            .responseJSON {response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch(response.result){
                    
                case .success(let value):
                    print("response ", value)
                    
                    let dataArchive = NSKeyedArchiver.archivedData(withRootObject: value) //as Data
                    
                    //3
                    cache.set(value: dataArchive, key: urlString)
                    
                    guard let valueL = value as? [String : AnyObject] else {
                        let valueStr = value as? String
                        let params : [String : AnyObject] = ["status": valueStr as AnyObject]
                        responseHandler(params as AnyObject)
                        return
                    }
                   // let valueL = value as! NSDictionary
                    if(valueL["status"] != nil ){
                        responseHandler(value as AnyObject)
                    }else{
                        responseHandler(value as AnyObject)
                    }
                   
                    
                case .failure(let error):
                    errorHandler(CKConstants.AppErrorType.ERRORTYPE(error))
                    
                }
        }
        
    }
    
    func returnURL(strURL: String?) -> String?{
        //BASE URL
        var urlString: String = "";
        if(strURL?.contains(CKServiceConstants.BASE_URL_DOMAIN) == true){
            urlString = strURL!;
        }else{
            urlString = CKServiceConstants.BASE_URL + strURL!;
        }
        return urlString;
    }
    
    
    func getFilename(ext: String) -> String {
        let dateFormatter = CKGlobalMethods.getFormatter()
        dateFormatter.dateFormat = "yyyy-MM-ddhh:mm:ss"
        let dateStr = dateFormatter.string(from: Date())
        return String(format: "iosapp_user_image%@.%@",dateStr, ext)
    }
    
    
   func convertParamsToString(_ params: [String: Any])->String{
        var paramString = ""
        for(key, value) in params{
            paramString = paramString + key + "=" + String(describing: value) + "&"
        }
        return paramString
    }
    
}
