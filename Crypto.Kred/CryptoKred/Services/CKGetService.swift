//
//  CKGetService.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 15/02/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import Foundation
import ObjectMapper


class CKGetService: CKBaseService {
    
    //MARK: Get wallet
    func getWallet(params: [String: Any], responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void)
    {
        let path = CKServiceConstants.COIN_WALLET_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
    func getUnmintedCoin(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_URL;
       
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        self.getObjectForUrl(path, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Get coin info
    
    func getCoinInfo(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_INFO_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Get coin info
    
    func getCoinHistory(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_HISTORY_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Get coin messages
    
    func getCoinMessages(showCache: Bool ,messageType: CKConstants.MESSAGES_TYPE ,grab: String, params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var path = "";
        if(messageType == CKConstants.MESSAGES_TYPE.COMMENTS){
            path = CKServiceConstants.COIN_COMMENTS_URL
            
        }else{
            path = CKServiceConstants.COIN_MESSAGES_URL + "/" + grab;
            
        }
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            cacheHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: showCache, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Get Collections coins
    
    func getCoinsList(params: [String: Any] , path: String? , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path!, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Get Coin Request
    func getCoinsRequest(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        let path = CKServiceConstants.COIN_REQUESTS_URL;
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Get POPULAR TAGS
    func getPopluarTags(responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        var params = [String: Any]()
        params = CKServiceConstants.initializeParams()
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        let path = CKServiceConstants.COIN_POPULAR_TAGS_URL;
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: GET TRANASCTION HISTORY
    func getTransactionHistory(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        let path = CKServiceConstants.TRANSACTION_HISTORY_URL;
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    //
    
    //MARK: Get CirculationList
    
    func getCirculationList(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_CIRCULATION_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: Get Contact List
    func getContactsList(params: [String: Any] , responseHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, cacheHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.CONTACTS_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Get Coin balance
    func getCoinBalance(params: [String: Any] , responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, cacheHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.COIN_BALANCE_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            cacheHandler(response!)
        }
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: User Validations
    
    func userCheck(params: [String: Any] , responseHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, cacheHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.USER_CHECK_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            cacheHandler(response!)
        }
        self.getObjectForUrl(path, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    
    //MARK: Get user domain Profile
    //MARK: User Info : /user/home
    
    func getUserDomainProfile(params: [String: Any] , responseHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, cacheHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        let path = CKServiceConstants.USER_HOME_URL;
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        func objectCacheHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            cacheHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: true, responseHandler: objectResponseHandler(_:), cacheHandler: objectCacheHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: User Validations
    func userValidationAPI(path: String ,params: [String: Any], responseHandler:@escaping (_ result: CKUserValidationResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKUserValidationResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(path, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
        
        //   self.postObjectForUrl(path, params: params, responseHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    //MARK: User Validations
    func getUserLimits(params: [String: Any], responseHandler:@escaping (_ result: CKCoinsResponseObject) -> Void, errorHandler:@escaping (_ errorResponse: CKConstants.AppErrorType) -> Void){
        
        func objectResponseHandler(_ response: AnyObject){
            let response = Mapper<CKCoinsResponseObject> ().map(JSONObject:response)
            responseHandler(response!)
        }
        
        self.getObjectForUrl(CKServiceConstants.COIN_LIMIT_URL, params: params, showCache: false, responseHandler: objectResponseHandler(_:), cacheHandler: objectResponseHandler(_:), errorHandler: errorHandler)
    }
    
    
    
}
