//
//  ViewController.swift
//  Crypto.Kred
//
//  Created by Noopur Virmani on 22/01/18.
//  Copyright © 2018 Noopur Virmani. All rights reserved.
//

import UIKit

class ViewController: CKBaseVC, SideMenuControllerDelegate{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let sideViewVC = self.revealViewController().rightViewController as? SideMenuController
        sideViewVC?.delegate = self
        navBarStyle?.showNavigationRightSideMenuBarItem(self, navTitle: "")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let token = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_TOKEN_KEY)
        if(token != nil && token != ""){
            CKGlobalVariables.sharedManager.loggedInToken = token!
            CKGlobalVariables.sharedManager.loggedInUsername = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_USERNAME)!
            CKGlobalVariables.sharedManager.sessionId = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_SESSION_ID)!
            CKGlobalVariables.sharedManager.loggedInEmail = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_EMAIL_KEY)!
            CKGlobalVariables.sharedManager.loggedInPassword = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_PASSWORD_KEY)!
            CKGlobalVariables.sharedManager.loggedInAvatar = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_AVATAR)!
            CKGlobalVariables.sharedManager.loggedInUserId = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_USER_ID)!
            
            let wallet = CKGlobalMethods.getStringValueFromPrefrences(CKConstants.PREF_WALLET_KEY);
            CKGlobalVariables.sharedManager.walletId = Int(wallet ?? "0");
            
            //marketPlaceCalledVC(animation: false, selectedSegment: 0)
        }else{
            //loginViewControllerCalled()
        }
        
        self.exploreCalledVC(animation: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tapOnRowAtIndexpath(_ indexpath: NSInteger, searchText: String?){
        self.revealViewController().revealToggle(animated: true)
        if(indexpath == 0){
            self.loginViewControllerCalled(login: true)
        }else if(indexpath == 1){
            self.loginViewControllerCalled(login: false)
        }
        else if(indexpath == 2){
            self.buymoreMainVCCalled(_animated: false)
        }
        else if(indexpath == 4){
            createCoinVCCalled()
        }else if(indexpath == 5){
            //my designs
            self.myDesignCalledVC()
        }else if(indexpath == 6){
           self.claimCoinCalledVC()
        }
        else if(indexpath == 8){
            exploreCalledVC(animation: false)
            
        }else if(indexpath == 9){
            self.collectionViewVCCalled(userCollection: false, selectedCoin: nil)
        }
        else if(indexpath == 10){
            marketPlaceCalledVC(animation: false, selectedCoin: nil)
        }else if(indexpath == 11){
            //Newsfeed
            self.newsfeedVCCalled(false)
        }else if(indexpath == 13){
            //contacts
            self.contactsVCCalled();
            
        }else if(indexpath == 14){
            //transaction history
            self.transactionVCCalled()
            
        }else if(indexpath == 16){
           //Connect to ETH
            self.connectEthWalletCalledVC()
            
        }else if(indexpath == 17){
            //Outreach
            self.outreachVCCalled()
        }else if(indexpath == 18){
            //Export Ckr
            self.exportCKrVCCalled();
        }else{
            logoutConfirmAlert()
        }
    }
    
    func logoutConfirmAlert(){
        let alert = UIAlertController(title: "",
                                      message: "",
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = CKColor.themeAnotherColor
        
        let attributedMessage = NSAttributedString(string: NSLocalizedString("confirm_logout_message", comment: ""), attributes: [
            NSAttributedStringKey.font : Theme.regularFont(18.0),
            NSAttributedStringKey.foregroundColor : CKColor.darkGrayColor
            ])
        
        
        alert.setValue(attributedMessage, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("alert_no", comment: ""), style: UIAlertActionStyle.default) { UIAlertAction in
            
        })
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("alert_yes", comment: ""), style: UIAlertActionStyle.default) { UIAlertAction in
            self.logoutActionCommonMethod()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func logoutActionCommonMethod(){
      
        CKGlobalMethods.saveStringValueInPrefrences("", key: CKConstants.PREF_TOKEN_KEY)
        CKGlobalMethods.saveStringValueInPrefrences("", key: CKConstants.PREF_SESSION_ID)
        CKGlobalVariables.sharedManager.loggedInAvatar = ""
        self.navigationController?.popToRootViewController(animated: false)
    }
    

    
}

